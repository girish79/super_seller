package com.Goingbo.Superseller.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.Goingbo.Superseller.Adapter.Seller_Order_New_Adpater;
import com.Goingbo.Superseller.Adapter.Seller_order_List_Adpater;
import com.Goingbo.Superseller.Interface.ApiService;
import com.Goingbo.Superseller.Interface.SelectedAdapterInterface;
import com.Goingbo.Superseller.Model.Bookingpre;
import com.Goingbo.Superseller.Model.NewOrderLISTSeller.NewSellerOrderList;
import com.Goingbo.Superseller.Model.NewOrderLISTSeller.Sellerorderlist;

import com.Goingbo.Superseller.Model.NewOrder_List.NewUSEROrderList;
import com.Goingbo.Superseller.Model.NewOrder_List.Userorderlist;
import com.Goingbo.Superseller.Model.SellerUorder.SellerOrder;
import com.Goingbo.Superseller.Model.SellerUorder.Uorderlist;
import com.Goingbo.Superseller.R;
import com.Goingbo.Superseller.Util.CheckConnectivity;
import com.Goingbo.Superseller.Util.LibConstant;
import com.Goingbo.Superseller.Util.RetrofitClient;
import com.Goingbo.Superseller.Util.SessionManager;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Seller_order_list extends Fragment {

    LinearLayout orderlistdummy;
    View view;
    RecyclerView seller_rcv_order;
    SessionManager sessionManager;
    HashMap<String,String> hashMap;
    String seller_id;
    Toolbar toolbar;
    ProgressDialog progressDialog ;
    ArrayList<
            Sellerorderlist> uarrayList;
    TextView tv_order_list;
    private CheckConnectivity checkConnectivity;
    boolean isshowing=true;

    public Seller_order_list() {
        // Required empty public constructor
    }


    public static Seller_order_list newInstance(String param1, String param2) {
        Seller_order_list fragment = new Seller_order_list();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_seller_order_list, container, false);
        sessionManager=new SessionManager(getActivity().getApplicationContext());
        checkConnectivity = new CheckConnectivity(getContext());
        hashMap=sessionManager.getsoicalDetails();
        seller_id=hashMap.get(LibConstant.socialid);
        init();


        return  view;
    }


    public void init(){
        seller_rcv_order=view.findViewById(R.id.seller_rcv_order);
        orderlistdummy=view.findViewById(R.id.orderlistdummy);
        tv_order_list=view.findViewById(R.id.tv_order_list);
        seller_rcv_order.setHasFixedSize(false);
        seller_rcv_order.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
        uarrayList=new ArrayList<>();
        toolbar=getActivity().findViewById(R.id.title_seller_dashboard);
        toolbar.setTitle("New Order's");
        orderlist();

    }

    private void orderlist() {
        if(!process()) {
            ProgressDialog progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Loading......");
            progressDialog.show();
            Call<NewSellerOrderList> call = RetrofitClient.getClient("https://www.goingbo.com/").create(ApiService.class).
                    getsellerorder(seller_id);
            call.enqueue(new Callback<NewSellerOrderList>() {
                @Override
                public void onResponse(Call<NewSellerOrderList> call, Response<NewSellerOrderList> response) {
                    progressDialog.dismiss();
                    if (!response.body().isError()) {
                        uarrayList.clear();
                        NewSellerOrderList newUSEROrderList=response.body();
                        if (newUSEROrderList.getSellerorderlist().size() > 0) {
                            orderlistdummy.setVisibility(View.GONE);
                            for (int k = 0; k < newUSEROrderList.getSellerorderlist().size(); k++) {
                                if (newUSEROrderList.getSellerorderlist().get(k).getOrderStatus().equalsIgnoreCase("Delivered")) {

                                } else if (newUSEROrderList.getSellerorderlist().get(k).getOrderStatus().equalsIgnoreCase("Order Cancel")) {

                                } else {
                                    uarrayList.add(newUSEROrderList.getSellerorderlist().get(k));
                                }
                            }
                            seller_rcv_order.setVisibility(View.VISIBLE);
                            tv_order_list.setVisibility(View.VISIBLE);
                            int count =0;
                            double orderid =0;
                            ArrayList<ArrayList<Sellerorderlist>> nestedList = new ArrayList<ArrayList<Sellerorderlist>>();
                            nestedList.clear();
                            ArrayList<
                                    Sellerorderlist>asgashgash = new ArrayList<>();
                            asgashgash.clear();
                            for (int v=0;v<uarrayList.size();v++)  {
                                if(nestedList.size() == 0){
                                    count++;
                                    asgashgash.add(uarrayList.get(v));
                                    nestedList.add(asgashgash);
                                    Log.e("nestedList0", "" + nestedList.toString());
                                    orderid=Double.parseDouble(newUSEROrderList.getSellerorderlist().get(v).getOrderId());
                                }else{
                                    double orderidsecount=Double.parseDouble( newUSEROrderList.getSellerorderlist().get(v).getOrderId());
                                    if(orderid != orderidsecount){
                                        count++;
                                        asgashgash = new ArrayList<>();
                                        asgashgash.clear();
                                        asgashgash.add(newUSEROrderList.getSellerorderlist().get(v));
                                        nestedList.add(asgashgash);
                                        orderid=Double.parseDouble(newUSEROrderList.getSellerorderlist().get(v).getOrderId());
                                        Log.e("nestedList2", "" + nestedList.toString());
                                    }else{
                                        if(!asgashgash.contains(newUSEROrderList.getSellerorderlist().get(v))) {
                                            asgashgash.add(newUSEROrderList.getSellerorderlist().get(v));
                                            nestedList.set(count-1,asgashgash);
                                            Log.e("nestedList3", "" + nestedList.toString());
                                        }
                                    }
                                }
                            }

                            Log.e("nestedList", "" + nestedList.size());
                            Seller_Order_New_Adpater seller_order_new_adpater=new Seller_Order_New_Adpater(nestedList, getContext(), new SelectedAdapterInterface() {
                                @Override
                                public void getAdapter(Integer position, String resultindex) {
                                    if(resultindex.equals("Accept")){
                                        customdialogbox(position,nestedList.get(position).get(0).getOrderStatus());
                                    }
                                }
                            });
                            seller_rcv_order.setAdapter(seller_order_new_adpater);
                            seller_rcv_order.setHasFixedSize(true);
                            seller_rcv_order.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
                            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(seller_rcv_order.getContext(),
                                            DividerItemDecoration.VERTICAL);
                            seller_rcv_order.addItemDecoration(dividerItemDecoration);
                        } else {
                            orderlistdummy.setVisibility(View.VISIBLE);
                            seller_rcv_order.setVisibility(View.GONE);
                            tv_order_list.setVisibility(View.GONE);
                        }
                    }

                }

                @Override
                public void onFailure(Call<NewSellerOrderList> call, Throwable t) {
                    progressDialog.dismiss();
                }
            });



        }
    }

    private void customdialogbox(Integer position,String status) {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_develery_order);

        Spinner dialog_order_spinner=dialog.findViewById(R.id.dialog_order_spinner);
        TextInputEditText seller_otp_delivery_number=dialog.findViewById(R.id.seller_otp_delivery_number);
        TextInputLayout textinput_seller_otp_delivery_number=dialog.findViewById(R.id.textinput_seller_otp_delivery_number);

        // Custom choices
        List<CharSequence> choices = new ArrayList<>();
        if(status.equalsIgnoreCase("Order Has Been Accepted")){
            choices.add("Order Shipped");
            choices.add("Order In Route");
            choices.add("Delivered");

        }else
        if(status.equalsIgnoreCase("Order Shipped")) {
            choices.add("Order In Route");
            choices.add("Delivered");

        }else if(status.equalsIgnoreCase("Order In Route")){
            choices.add("Delivered");

        }
        else {
            choices.add("Order Has Been Accepted");
            choices.add("Order Shipped");
            choices.add("Order In Route");
            choices.add("Delivered");
            choices.add("Order Cancel");
        }

        // Create an ArrayAdapter with custom choices
        ArrayAdapter<CharSequence> adapter = new ArrayAdapter<CharSequence>(getContext(), android.R.layout.simple_spinner_item, choices);

        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Set the adapter to th spinner
        dialog_order_spinner.setAdapter(adapter);
        dialog_order_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(dialog_order_spinner.getSelectedItem().toString().equals("Delivered")){
                    seller_otp_delivery_number.setVisibility(View.VISIBLE);
                    textinput_seller_otp_delivery_number.setVisibility(View.VISIBLE);
                    seller_otp_delivery_number.setText("");

                }else{
                    textinput_seller_otp_delivery_number.setVisibility(View.GONE);
                    seller_otp_delivery_number.setVisibility(View.GONE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Button btn_dialog_cancel = (Button) dialog.findViewById(R.id.btn_order_dialog_cancel);
        ImageView image_dialogbox_close =  dialog.findViewById(R.id.image_dialogbox_close);
        image_dialogbox_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dialog.isShowing()){
                    dialog.hide();
                }
            }
        }); btn_dialog_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dialog.isShowing()){
                    dialog.hide();
                }
            }
        });
        Button btn_order_dialog_yes=dialog.findViewById(R.id.btn_order_dialog_yes);
        btn_order_dialog_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!process()) {
                    if (dialog.isShowing()) {
                        progressDialog = new ProgressDialog(getContext());
                        progressDialog.setMessage("Loading......");
                        progressDialog.show();
                        if(dialog_order_spinner.getSelectedItem().toString().equals("Delivered")){
                            if(seller_otp_delivery_number.getText().toString().length()==0){
                                seller_otp_delivery_number.setError("Please enter OTP number");
                            }else if(seller_otp_delivery_number.getText().toString().length()<6){
                                seller_otp_delivery_number.setError("Please enter OTP number");
                            }else {
                                Call<Bookingpre> callupdate = RetrofitClient.getClient("https://www.goingbo.com/").create(ApiService.class)
                                        .update_delivery_status(seller_id, uarrayList.get(position).getOrderId(), dialog_order_spinner.getSelectedItem().toString(), Integer.parseInt(seller_otp_delivery_number.getText().toString()));
                                callupdate.enqueue(new Callback<Bookingpre>() {
                                    @Override
                                    public void onResponse(Call<Bookingpre> call, Response<Bookingpre> response) {
                                        progressDialog.dismiss();
                                        if (!response.body().getError()) {
                                            dialog.hide();
                                            orderlist();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<Bookingpre> call, Throwable t) {
                                        progressDialog.dismiss();
                                    }
                                });
                            }
                        }else {
                            Call<Bookingpre> callupdate = RetrofitClient.getClient("https://www.goingbo.com/").create(ApiService.class)
                                    .updateseller_order(uarrayList.get(position).getOrderId(), seller_id, dialog_order_spinner.getSelectedItem().toString());
                            callupdate.enqueue(new Callback<Bookingpre>() {
                                @Override
                                public void onResponse(Call<Bookingpre> call, Response<Bookingpre> response) {
                                    progressDialog.dismiss();
                                    if (!response.body().getError()) {
                                        dialog.hide();
                                        orderlist();
                                    }
                                }

                                @Override
                                public void onFailure(Call<Bookingpre> call, Throwable t) {
                                    progressDialog.dismiss();
                                }
                            });
                        }


                    }
                }
            }
        });

        dialog.show();




    }
    private boolean process() {
        boolean show = true;
        if (checkConnectivity.isConnected()) {
            show = false;

        } else {

            showDialog();
            show = true;
        }
        return show;
    }

    private void showDialog() {

        Dialog dialog = new Dialog(getContext(), R.style.FullScreenDialogWithStatusBarColorAccent);
        if (!dialog.isShowing()){
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.no_internet);
            Window window = dialog.getWindow();
            assert window != null;
            WindowManager.LayoutParams wlp = Objects.requireNonNull(window).getAttributes();
            wlp.gravity = Gravity.CENTER;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
            window.setAttributes(wlp);
            Objects.requireNonNull(dialog.getWindow()).setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            Button wifi_btn = dialog.findViewById(R.id.wifi_btn);
            Button mobile_data_btn = dialog.findViewById(R.id.mobile_data_btn);
            dialog.show();
            dialog.setCancelable(false);
            wifi_btn.setOnClickListener(v -> {
                dialog.dismiss();
                isshowing = false;
                WifiManager wifiMgr = (WifiManager)getActivity(). getApplicationContext().getSystemService(Context.WIFI_SERVICE);

                assert wifiMgr != null;
                if (wifiMgr.isWifiEnabled()) { // Wi-Fi adapter is ON

                    WifiInfo wifiInfo = wifiMgr.getConnectionInfo();

                    if (wifiInfo.getNetworkId() == -1) {

                        showDialog();
                        // Not connected to an access point
                    } else {
                        dialog.dismiss();
                        process();
                    }
                    // Connected to an access point
                } else {
                    startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS)); // Wi-Fi adapter is OFF
                }
            });
            mobile_data_btn.setOnClickListener(v -> {
                dialog.dismiss();
                isshowing = false;
                startActivityForResult(new Intent(android.provider.Settings.ACTION_DATA_ROAMING_SETTINGS), 0);
                if (checkConnectivity.isConnected()) {
                    process();

                } else {
                    showDialog();
                }
            });
        }

    }
}
