package com.Goingbo.Superseller.Adapter;

import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.Goingbo.Superseller.Interface.SelectedAdapterInterface;
import com.Goingbo.Superseller.Model.Productadmin.Productseller;
import com.Goingbo.Superseller.Model.seller_unit.UnitSeller;
import com.Goingbo.Superseller.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class adminselectedAdapted extends RecyclerView.Adapter<adminselectedAdapted.ProductView> {
    Context context;
    ArrayList<Productseller> arrayList;
    SelectedAdapterInterface selectedAdapterInterface;
    public adminselectedAdapted(Context context, ArrayList<Productseller> arrayList, SelectedAdapterInterface selectedAdapterInterface) {
        this.context = context;
        this.arrayList = arrayList;
        this.selectedAdapterInterface=selectedAdapterInterface;
    }

    @NonNull
    @Override
    public ProductView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.seller_product_admin, parent, false);
        return new ProductView(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductView holder, int position) {

        String dataname=arrayList.get(position).getProName().substring(0,1).toUpperCase()+arrayList.get(position).getProName().substring(1);

        holder.selected_product_name.setText(Html.fromHtml(dataname));
        holder.selected_product_category.setText(Html.fromHtml(arrayList.get(position).getCategoryName()));
        Log.e("catname",""+arrayList.get(position).getCategoryName());
        holder.selected_product_description.setText(Html.fromHtml(arrayList.get(position).getProDes()));

        holder.btn_admin_edit_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedAdapterInterface.getAdapter(position,"edit");
            }
        });
        holder.btn_admin_remove_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedAdapterInterface.getAdapter(position,"remove");
            }
        });
        String imagelink="https://www.goingbo.com/public/product_images/"+arrayList.get(position).getProImage();
        Picasso.get().load(imagelink).into(holder.select_product_image_list);
        holder.unit_recyclerview.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL,false));
        holder.unit_recyclerview.setHasFixedSize(false);
        Gson gson = new Gson();
        TypeToken<ArrayList<UnitSeller>> token = new TypeToken<ArrayList<UnitSeller>>() {
        };
        String reader=arrayList.get(position).getProductOfferPrice();
        Log.e("printdata",reader);
        try {
            List<UnitSeller> unitlist = Arrays.asList(gson.fromJson(reader,
                    UnitSeller[].class));


            Unit_recyclerview_seller_myproduct unit_recyclerview_seller_myproduct = new Unit_recyclerview_seller_myproduct(context, unitlist.get(0).getProductdetail());
            holder.unit_recyclerview.setAdapter(unit_recyclerview_seller_myproduct);
        } catch (Exception e){e.printStackTrace();}

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ProductView extends RecyclerView.ViewHolder {
        public ImageView select_product_image_list;
        public TextView selected_product_name;
        public TextView selected_product_category;
        public TextView selected_product_description;
        public Button btn_admin_edit_product,btn_admin_remove_product;

        public RecyclerView unit_recyclerview;

        public ProductView(@NonNull View itemView) {
            super(itemView);
            select_product_image_list = itemView.findViewById(R.id.select_admin_image_list);
            selected_product_name = itemView.findViewById(R.id.selected_admin_name);
            selected_product_category = itemView.findViewById(R.id.selected_admin_category);
            selected_product_description = itemView.findViewById(R.id.selected_admin_description);

            btn_admin_edit_product = itemView.findViewById(R.id.btn_admin_edit_product);
            btn_admin_remove_product = itemView.findViewById(R.id.btn_admin_remove_product);
            unit_recyclerview=itemView.findViewById(R.id.unit_recyclerview);


        }


    }
}
