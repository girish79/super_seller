package com.Goingbo.Superseller.Model.Productadmin;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class Productseller implements Serializable, Parcelable
{

    @SerializedName("category_name")
    @Expose
    private String categoryName;
    @SerializedName("admin_id")
    @Expose
    private String adminId;
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("product_price")
    @Expose
    private Object productPrice;
    @SerializedName("product_offer_price")
    @Expose
    private String productOfferPrice;
    @SerializedName("Pro_type")
    @Expose
    private String proType;
    @SerializedName("Pro_category")
    @Expose
    private String proCategory;
    @SerializedName("Pro_name")
    @Expose
    private String proName;
    @SerializedName("Pro_image")
    @Expose
    private String proImage;
    @SerializedName("Pro_des")
    @Expose
    private String proDes;
    public final static Creator<Productseller> CREATOR = new Creator<Productseller>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Productseller createFromParcel(Parcel in) {
            return new Productseller(in);
        }

        public Productseller[] newArray(int size) {
            return (new Productseller[size]);
        }

    }
            ;
    private final static long serialVersionUID = 6082351473908288612L;

    protected Productseller(Parcel in) {
        this.categoryName = ((String) in.readValue((String.class.getClassLoader())));
        this.adminId = ((String) in.readValue((String.class.getClassLoader())));
        this.productId = ((String) in.readValue((String.class.getClassLoader())));
        this.productPrice = ((Object) in.readValue((Object.class.getClassLoader())));
        this.productOfferPrice = ((String) in.readValue((String.class.getClassLoader())));
        this.proType = ((String) in.readValue((String.class.getClassLoader())));
        this.proCategory = ((String) in.readValue((String.class.getClassLoader())));
        this.proName = ((String) in.readValue((String.class.getClassLoader())));
        this.proImage = ((String) in.readValue((String.class.getClassLoader())));
        this.proDes = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public Productseller() {
    }

    /**
     *
     * @param proCategory
     * @param proType
     * @param productId
     * @param proImage
     * @param adminId
     * @param proName
     * @param categoryName
     * @param proDes
     * @param productPrice
     * @param productOfferPrice
     */
    public Productseller(String categoryName, String adminId, String productId, Object productPrice, String productOfferPrice, String proType, String proCategory, String proName, String proImage, String proDes) {
        super();
        this.categoryName = categoryName;
        this.adminId = adminId;
        this.productId = productId;
        this.productPrice = productPrice;
        this.productOfferPrice = productOfferPrice;
        this.proType = proType;
        this.proCategory = proCategory;
        this.proName = proName;
        this.proImage = proImage;
        this.proDes = proDes;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Productseller withCategoryName(String categoryName) {
        this.categoryName = categoryName;
        return this;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public Productseller withAdminId(String adminId) {
        this.adminId = adminId;
        return this;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Productseller withProductId(String productId) {
        this.productId = productId;
        return this;
    }

    public Object getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(Object productPrice) {
        this.productPrice = productPrice;
    }

    public Productseller withProductPrice(Object productPrice) {
        this.productPrice = productPrice;
        return this;
    }

    public String getProductOfferPrice() {
        return productOfferPrice;
    }

    public void setProductOfferPrice(String productOfferPrice) {
        this.productOfferPrice = productOfferPrice;
    }

    public Productseller withProductOfferPrice(String productOfferPrice) {
        this.productOfferPrice = productOfferPrice;
        return this;
    }

    public String getProType() {
        return proType;
    }

    public void setProType(String proType) {
        this.proType = proType;
    }

    public Productseller withProType(String proType) {
        this.proType = proType;
        return this;
    }

    public String getProCategory() {
        return proCategory;
    }

    public void setProCategory(String proCategory) {
        this.proCategory = proCategory;
    }

    public Productseller withProCategory(String proCategory) {
        this.proCategory = proCategory;
        return this;
    }

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }

    public Productseller withProName(String proName) {
        this.proName = proName;
        return this;
    }

    public String getProImage() {
        return proImage;
    }

    public void setProImage(String proImage) {
        this.proImage = proImage;
    }

    public Productseller withProImage(String proImage) {
        this.proImage = proImage;
        return this;
    }

    public String getProDes() {
        return proDes;
    }

    public void setProDes(String proDes) {
        this.proDes = proDes;
    }

    public Productseller withProDes(String proDes) {
        this.proDes = proDes;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("categoryName", categoryName).append("adminId", adminId).append("productId", productId).append("productPrice", productPrice).append("productOfferPrice", productOfferPrice).append("proType", proType).append("proCategory", proCategory).append("proName", proName).append("proImage", proImage).append("proDes", proDes).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(proCategory).append(proType).append(productId).append(proImage).append(adminId).append(proName).append(categoryName).append(proDes).append(productPrice).append(productOfferPrice).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Productseller) == false) {
            return false;
        }
        Productseller rhs = ((Productseller) other);
        return new EqualsBuilder().append(proCategory, rhs.proCategory).append(proType, rhs.proType).append(productId, rhs.productId).append(proImage, rhs.proImage).append(adminId, rhs.adminId).append(proName, rhs.proName).append(categoryName, rhs.categoryName).append(proDes, rhs.proDes).append(productPrice, rhs.productPrice).append(productOfferPrice, rhs.productOfferPrice).isEquals();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(categoryName);
        dest.writeValue(adminId);
        dest.writeValue(productId);
        dest.writeValue(productPrice);
        dest.writeValue(productOfferPrice);
        dest.writeValue(proType);
        dest.writeValue(proCategory);
        dest.writeValue(proName);
        dest.writeValue(proImage);
        dest.writeValue(proDes);
    }

    public int describeContents() {
        return 0;
    }

}