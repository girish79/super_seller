package com.Goingbo.Superseller.Model.PackLoginAdmin;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class Success implements Serializable, Parcelable
{

    @SerializedName("users")
    @Expose
    private Users users;
    public final static Creator<Success> CREATOR = new Creator<Success>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Success createFromParcel(Parcel in) {
            return new Success(in);
        }

        public Success[] newArray(int size) {
            return (new Success[size]);
        }

    }
            ;
    private final static long serialVersionUID = -8022995678156664310L;

    protected Success(Parcel in) {
        this.users = ((Users) in.readValue((Users.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public Success() {
    }

    /**
     *
     * @param users
     */
    public Success(Users users) {
        super();
        this.users = users;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public Success withUsers(Users users) {
        this.users = users;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("users", users).toString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(users);
    }

    public int describeContents() {
        return 0;
    }

}