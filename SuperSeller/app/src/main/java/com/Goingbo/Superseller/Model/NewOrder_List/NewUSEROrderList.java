package com.Goingbo.Superseller.Model.NewOrder_List;

import android.os.Parcel;
import android.os.Parcelable;

import com.Goingbo.Superseller.Model.NewOrderLISTSeller.Sellerorderlist;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class NewUSEROrderList implements Serializable, Parcelable
{

    @SerializedName("error")
    @Expose
    private boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("userorderlist")
    @Expose
    private List<Sellerorderlist> userorderlist = new ArrayList<Sellerorderlist>();
    public final static Creator<Sellerorderlist> CREATOR = new Creator<Sellerorderlist>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Sellerorderlist createFromParcel(Parcel in) {
            return new Sellerorderlist(in);
        }

        public Sellerorderlist[] newArray(int size) {
            return (new Sellerorderlist[size]);
        }

    }
            ;
    private final static long serialVersionUID = -666379532269092352L;

    protected NewUSEROrderList(Parcel in) {
        this.error = ((boolean) in.readValue((boolean.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.userorderlist, (Sellerorderlist.class.getClassLoader()));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public NewUSEROrderList() {
    }

    /**
     *
     * @param userorderlist
     * @param error
     * @param message
     */
    public NewUSEROrderList(boolean error, String message, List<Sellerorderlist> userorderlist) {
        super();
        this.error = error;
        this.message = message;
        this.userorderlist = userorderlist;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public NewUSEROrderList withError(boolean error) {
        this.error = error;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public NewUSEROrderList withMessage(String message) {
        this.message = message;
        return this;
    }

    public List<Sellerorderlist> getUserorderlist() {
        return userorderlist;
    }

    public void setUserorderlist(List<Sellerorderlist> userorderlist) {
        this.userorderlist = userorderlist;
    }

    public NewUSEROrderList withUserorderlist(List<Sellerorderlist> userorderlist) {
        this.userorderlist = userorderlist;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("error", error).append("message", message).append("userorderlist", userorderlist).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(userorderlist).append(error).append(message).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof NewUSEROrderList) == false) {
            return false;
        }
        NewUSEROrderList rhs = ((NewUSEROrderList) other);
        return new EqualsBuilder().append(userorderlist, rhs.userorderlist).append(error, rhs.error).append(message, rhs.message).isEquals();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(error);
        dest.writeValue(message);
        dest.writeList(userorderlist);
    }

    public int describeContents() {
        return 0;
    }

}