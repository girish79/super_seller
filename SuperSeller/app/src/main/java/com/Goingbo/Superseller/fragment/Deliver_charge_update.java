package com.Goingbo.Superseller.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.Goingbo.Superseller.Activity.Seller_OTP_Login;
import com.Goingbo.Superseller.Adapter.Seller_delivery_cost_adpater;
import com.Goingbo.Superseller.Interface.ApiService;
import com.Goingbo.Superseller.Interface.SelectedAdapterInterface;
import com.Goingbo.Superseller.Model.Bookingpre;
import com.Goingbo.Superseller.Model.Deliver_seller_cost.DeliveryCharger;
import com.Goingbo.Superseller.Model.SellerDeliveryUnit;
import com.Goingbo.Superseller.Model.seller_unit.Productdetail;
import com.Goingbo.Superseller.Model.seller_unit.UnitSeller;
import com.Goingbo.Superseller.R;
import com.Goingbo.Superseller.Util.CheckConnectivity;
import com.Goingbo.Superseller.Util.LibConstant;
import com.Goingbo.Superseller.Util.RetrofitClient;
import com.Goingbo.Superseller.Util.SessionManager;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Deliver_charge_update extends Fragment {

    View view;
    TextInputEditText ed_min_delivery;
    SessionManager sessionManager;
    Button delivery_btn;
    Toolbar toolbar;
    AlertDialog dialog;
    Button btn_add_delivery_unit;
    private CheckConnectivity checkConnectivity;
    boolean isshowing = true;
    TextView ed_free_amount_delivery;
    ArrayList<SellerDeliveryUnit> arrayList = new ArrayList<>();
    RecyclerView rcv_delivery_seller_unit;
    LinearLayoutManager linearLayoutManager;

    public Deliver_charge_update() {
        // Required empty public constructor
    }


    public static Deliver_charge_update newInstance(String param1, String param2) {
        Deliver_charge_update fragment = new Deliver_charge_update();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_deliver_charge_update, container, false);
        checkConnectivity = new CheckConnectivity(getContext());
        sessionManager = new SessionManager(getContext().getApplicationContext());
        HashMap<String, String> hashMap = sessionManager.getsoicalDetails();
        int seller_id = Integer.parseInt(hashMap.get(LibConstant.socialid));

        init();
        toolbar = getActivity().findViewById(R.id.title_seller_dashboard);
        toolbar.setTitle("Delivery Charge");
        Bundle bundle = this.getArguments();
        String cost = bundle.getString("cost");
        String amount = bundle.getString("amount");
        arrayList=new ArrayList<>();

        delivercharge(seller_id);
        ed_min_delivery.setText(cost);
        ed_free_amount_delivery.setText("Delivery charger for free Deliver " +cost + "(" + getResources().getString(R.string.Rs) + ")");
        ed_min_delivery.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ed_free_amount_delivery.setText("Delivery charger for free Deliver " + s + "(" + getResources().getString(R.string.Rs) + ")");
                arrayList.clear();
                linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
                rcv_delivery_seller_unit.setLayoutManager(linearLayoutManager);
                Seller_delivery_cost_adpater seller_delivery_cost_adpater = new Seller_delivery_cost_adpater(arrayList, getContext(), new SelectedAdapterInterface() {
                    @Override
                    public void getAdapter(Integer position, String resultindex) {

                    }
                });
                rcv_delivery_seller_unit.setAdapter(seller_delivery_cost_adpater);
                rcv_delivery_seller_unit.setHasFixedSize(true);

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        btn_add_delivery_unit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (arrayList.size() < 3) {
                    unitaddshowDialog();
                }
            }
        });
        delivery_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!process()) {
                    if (ed_min_delivery.getText().toString().length() == 0) {
                        ed_min_delivery.setError("Please Insert Your Delivery amount  !");
                    }
                    if (arrayList.size() == 0) {
                        AlertDialog alertdialog = new AlertDialog.Builder(getContext())
                                .setTitle("Alert")
                                .setMessage("Please Add Distance and Delivery Charger  !")
                                .setCancelable(false)
                                .setPositiveButton("OK", (dialog, which) -> {
                                    dialog.dismiss();

                                }).create();
                        if (!alertdialog.isShowing()) {
                            alertdialog.show();
                        }
                    } else {
                        ProgressDialog progressDialog = new ProgressDialog(getContext());
                        progressDialog.setMessage("Loading......!");
                        progressDialog.show();
                        JSONArray myCustomArray = new JSONArray();
                        for (int p = 0; p < arrayList.size(); p++) {
                            JSONObject jsonParams = new JSONObject();
                            try {
                                jsonParams.put("order", arrayList.get(p).getOrder());
                                jsonParams.put("delcharge", arrayList.get(p).getDelcharge());
                                jsonParams.put("distance", arrayList.get(p).getDistance());
                                myCustomArray.put(jsonParams);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }

                        JSONObject jsonParams = new JSONObject();
                        try {
                            jsonParams.put("seller_id", seller_id);
                            jsonParams.put("min_order_amount", ed_min_delivery.getText().toString());
                            jsonParams.put("delivery_charge", myCustomArray);
                            Log.e("printmap", "" + jsonParams);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        RequestBody body =
                                RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),
                                        jsonParams.toString());


                        Call<Bookingpre>
                                updatecall = RetrofitClient.getClient("https://www.goingbo.com/")
                                .create(ApiService.class).updateseller_delivery_charger(body);
                        updatecall.enqueue(new Callback<Bookingpre>() {
                            @Override
                            public void onResponse(Call<Bookingpre> call, Response<Bookingpre> response) {
                                progressDialog.dismiss();
                                if (!response.body().getError()) {
                                    AlertDialog alertdialog = new AlertDialog.Builder(getContext())
                                            .setTitle("Alert")
                                            .setMessage(response.body().getMessage())
                                            .setCancelable(false)
                                            .setPositiveButton("OK", (dialog, which) -> {
                                                dialog.dismiss();
                                                getActivity().onBackPressed();
                                            }).create();
                                    if (!alertdialog.isShowing()) {
                                        alertdialog.show();
                                    }
                                } else {
                                    AlertDialog alertdialog = new AlertDialog.Builder(getContext())
                                            .setTitle("Alert")
                                            .setMessage(response.body().getMessage())
                                            .setCancelable(false)
                                            .setPositiveButton("OK", (dialog, which) -> {
                                                dialog.dismiss();

                                            }).create();
                                    if (!alertdialog.isShowing()) {
                                        alertdialog.show();
                                    }
                                }

                            }

                            @Override
                            public void onFailure(Call<Bookingpre> call, Throwable t) {
                                progressDialog.dismiss();
                                AlertDialog alertdialog = new AlertDialog.Builder(getContext())
                                        .setTitle("Alert")
                                        .setMessage("API failed due to some technical problem, Please try again ! ")
                                        .setCancelable(false)
                                        .setPositiveButton("OK", (dialog, which) -> {
                                            dialog.dismiss();

                                        }).create();
                                if (!alertdialog.isShowing()) {
                                    alertdialog.show();
                                }
                            }
                        });


                    }
                }
            }
        });


        return view;
    }
    private void delivercharge(int seller_id) {

        if (!process()) {
            ProgressDialog  progressDialog = new ProgressDialog(getContext());

            progressDialog.setMessage("Loading......");
            progressDialog.show();
            Call<DeliveryCharger> calldelivery = RetrofitClient.getClient("https://www.goingbo.com/").
                    create(ApiService.class).checkdelivery(seller_id);
            calldelivery.enqueue(new Callback<DeliveryCharger>() {
                @Override
                public void onResponse(Call<DeliveryCharger> call, Response<DeliveryCharger> response) {
                    //  Log.e("deliverychrage",""+response.body().toString());
                    progressDialog.dismiss();
                    if (!response.body().isError()) {
                        DeliveryCharger deliveryCharger = response.body();
                        if(deliveryCharger.getDeliverycost()!=null){
                            if (deliveryCharger.getDeliverycost().size() > 0) {


                                    // Toast.makeText(getContext(), "" + deliveryCharger.getDeliverycost().get(k).getDeliveryCharge(), Toast.LENGTH_SHORT).show();
                                    //Toast.makeText(getContext(), "" + deliveryCharger.getDeliverycost().get(k).getMinOrder(), Toast.LENGTH_SHORT).show();
                                    Gson gson = new Gson();
                                    String reader = deliveryCharger.getDeliverycost().get(0).getDeliveryCharge() ;
                                    Log.e("deliverycost",""+reader);
                                    try {
                                        List<SellerDeliveryUnit> sublist = gson.fromJson(reader, new TypeToken<List<SellerDeliveryUnit>>() {
                                        }.getType());
                                        // delivery_amount.setText("" + );
                                        if (sublist.size() > 0) {



                                           // ed_min_delivery.setText("" + arrayList.get(0).getOrder());
                                            //ed_free_amount_delivery.setText("Delivery charger for free Deliver " + arrayList.get(0).getOrder() + "(" + getResources().getString(R.string.Rs) + ")");
                                            arrayList.addAll(sublist);
                                            Log.e("deliverycosts", "" + arrayList.size());
                                            LinearLayoutManager  linearLayoutManagers = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                                            rcv_delivery_seller_unit.setLayoutManager(linearLayoutManagers);
                                            Seller_delivery_cost_adpater seller_delivery_cost_adpater = new Seller_delivery_cost_adpater(arrayList, getContext(), new SelectedAdapterInterface() {
                                                @Override
                                                public void getAdapter(Integer position, String resultindex) {
                                                    if (resultindex.equals("edit")) {
                                                        edituint_delivery(position);
                                                    } else if (resultindex.equals("delete")) {
                                                        arrayList.remove(arrayList.get(position));
                                                        rcv_delivery_seller_unit.setLayoutManager(linearLayoutManager);
                                                     //   rcv_delivery_seller_unit.removeViewAt(position);
                                                        setadapterunit();
                                                    }
                                                }
                                            });
                                            rcv_delivery_seller_unit.setAdapter(seller_delivery_cost_adpater);
                                            rcv_delivery_seller_unit.setHasFixedSize(true);
                                        }
                                    }catch (Exception e){

                                    }

                                }
                                calldelivery.isCanceled();

                            }}
                        //commissioncheck(seller_id);

                }

                @Override
                public void onFailure(Call<DeliveryCharger> call, Throwable t) {
                    progressDialog.dismiss();
                }
            });
        }

    }

    private void init() {
        ed_min_delivery = view.findViewById(R.id.ed_min_delivery);
        delivery_btn = view.findViewById(R.id.delivery_btn);
        btn_add_delivery_unit = view.findViewById(R.id.btn_add_delivery_unit);
        ed_free_amount_delivery = view.findViewById(R.id.ed_free_amount_delivery);
        rcv_delivery_seller_unit = view.findViewById(R.id.rcv_delivery_seller_unit);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private boolean process() {
        boolean show = true;
        if (checkConnectivity.isConnected()) {
            show = false;
        } else {
            showDialog();
            show = true;
        }
        return show;
    }

    private void showDialog() {
        Dialog dialog = new Dialog(getContext(), R.style.FullScreenDialogWithStatusBarColorAccent);
        if (!dialog.isShowing()) {
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.no_internet);
            Window window = dialog.getWindow();
            assert window != null;
            WindowManager.LayoutParams wlp = Objects.requireNonNull(window).getAttributes();
            wlp.gravity = Gravity.CENTER;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
            window.setAttributes(wlp);
            Objects.requireNonNull(dialog.getWindow()).setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            Button wifi_btn = dialog.findViewById(R.id.wifi_btn);
            Button mobile_data_btn = dialog.findViewById(R.id.mobile_data_btn);
            dialog.show();
            dialog.setCancelable(false);
            wifi_btn.setOnClickListener(v -> {
                dialog.dismiss();
                isshowing = false;
                WifiManager wifiMgr = (WifiManager) getActivity().getApplicationContext().getSystemService(Context.WIFI_SERVICE);

                assert wifiMgr != null;
                if (wifiMgr.isWifiEnabled()) { // Wi-Fi adapter is ON

                    WifiInfo wifiInfo = wifiMgr.getConnectionInfo();

                    if (wifiInfo.getNetworkId() == -1) {

                        showDialog();
                        // Not connected to an access point
                    } else {
                        dialog.dismiss();
                        process();
                    }
                    // Connected to an access point
                } else {
                    startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS)); // Wi-Fi adapter is OFF
                }
            });
            mobile_data_btn.setOnClickListener(v -> {
                dialog.dismiss();
                isshowing = false;
                startActivityForResult(new Intent(android.provider.Settings.ACTION_DATA_ROAMING_SETTINGS), 0);
                if (checkConnectivity.isConnected()) {
                    process();

                } else {
                    showDialog();
                }
            });
        }

    }

    private void unitaddshowDialog() {
        Log.e("freeamount", ed_min_delivery.getText().toString());
        if (ed_min_delivery.getText().toString().isEmpty()) {
            ed_min_delivery.setError("Please insert your delivery amount");
        } else {
            int getorderamountfree = Integer.parseInt(ed_min_delivery.getText().toString());
            if (getorderamountfree > 0) {
                Dialog dialog = new Dialog(getContext(), R.style.FullScreenDialog);
                if (!dialog.isShowing()) {
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.dialog_delivery_unit_set);
                    Window window = dialog.getWindow();
                    assert window != null;
                    WindowManager.LayoutParams wlp = Objects.requireNonNull(window).getAttributes();
                    wlp.gravity = Gravity.CENTER;
                    wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
                    window.setAttributes(wlp);
                    Objects.requireNonNull(dialog.getWindow()).setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                    ImageView delivery_charger_unit_close = dialog.findViewById(R.id.delivery_charger_unit_close);
                    TextInputEditText ed_km_delivery = dialog.findViewById(R.id.ed_km_delivery);
                    TextInputEditText ed_charge_delivery = dialog.findViewById(R.id.ed_charge_delivery);
                    androidx.appcompat.widget.AppCompatButton btn_unit_dialogbox_submit = dialog.findViewById(R.id.btn_unit_dialogbox_submit);
//                    if (arrayList.size() > 0) {
//                        ed_km_delivery.setText(arrayList.get(arrayList.size() - 1).getDistance());
//                        ed_charge_delivery.setText(arrayList.get(arrayList.size() - 1).getDelchrage());
//                    }
                    dialog.show();
                    delivery_charger_unit_close.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (dialog.isShowing()) {
                                dialog.dismiss();
                            }
                        }
                    });
                    btn_unit_dialogbox_submit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (ed_km_delivery.getText().toString().length() == 0) {
                                ed_km_delivery.setError("Please enter delivery distance KM !");
                            } else if (ed_charge_delivery.getText().toString().length() == 0) {
                                ed_charge_delivery.setError("Please enter delivery charger !");
                            } else {
                                double amount=Double.parseDouble( ed_charge_delivery.getText().toString());
                                if(amount==0){
                                    ed_charge_delivery.setError("Please enter delivery charger greater than zero !");
                                }else {
                                    SellerDeliveryUnit productdetails = new SellerDeliveryUnit();
                                    productdetails.setDelcharge(ed_charge_delivery.getText().toString());
                                    productdetails.setDistance(ed_km_delivery.getText().toString());
                                    productdetails.setOrder(ed_min_delivery.getText().toString());
                                    if (arrayList.size() == 0) {
                                        arrayList.add(productdetails);
                                        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
                                        rcv_delivery_seller_unit.setLayoutManager(linearLayoutManager);
                                        Seller_delivery_cost_adpater seller_delivery_cost_adpater = new Seller_delivery_cost_adpater(arrayList, getContext(), new SelectedAdapterInterface() {
                                            @Override
                                            public void getAdapter(Integer position, String resultindex) {
                                                if (resultindex.equals("edit")) {
                                                    edituint_delivery(position);
                                                } else if (resultindex.equals("delete")) {
                                                    arrayList.remove(arrayList.get(position));
                                                    rcv_delivery_seller_unit.setLayoutManager(linearLayoutManager);
                                                    rcv_delivery_seller_unit.removeViewAt(position);
                                                    setadapterunit();
                                                }
                                            }
                                        });
                                        rcv_delivery_seller_unit.setAdapter(seller_delivery_cost_adpater);
                                        rcv_delivery_seller_unit.setHasFixedSize(true);
                                        dialog.dismiss();
                                    } else {
                                        Integer distance = Integer.parseInt(arrayList.get(arrayList.size() - 1).getDistance());
                                        Integer dynamicdistance = Integer.parseInt(ed_km_delivery.getText().toString());
                                        Integer delchrager = Integer.parseInt(ed_charge_delivery.getText().toString());
                                        Integer dynamicdelcharger = Integer.parseInt(arrayList.get(arrayList.size() - 1).getDelcharge());
                                        if (distance >= dynamicdistance) {
                                            ed_km_delivery.setError("Please Enter greater " + distance + " Delivery Distance KM !");
                                        } else {
                                            arrayList.add(productdetails);
                                            linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
                                            rcv_delivery_seller_unit.setLayoutManager(linearLayoutManager);
                                            Seller_delivery_cost_adpater seller_delivery_cost_adpater = new Seller_delivery_cost_adpater(arrayList, getContext(), new SelectedAdapterInterface() {
                                                @Override
                                                public void getAdapter(Integer position, String resultindex) {
                                                    if (resultindex.equals("edit")) {
                                                        edituint_delivery(position);
                                                    } else if (resultindex.equals("delete")) {
                                                        arrayList.remove(arrayList.get(position));
                                                        rcv_delivery_seller_unit.setLayoutManager(linearLayoutManager);
                                                        rcv_delivery_seller_unit.removeViewAt(position);
                                                        setadapterunit();
                                                    }
                                                }
                                            });
                                            rcv_delivery_seller_unit.setAdapter(seller_delivery_cost_adpater);
                                            rcv_delivery_seller_unit.setHasFixedSize(true);
                                            dialog.dismiss();
                                        }
                                    }
                                }

                            }
                        }
                    });
                    dialog.setCancelable(false);

                }
            } else {
                ed_min_delivery.setError("Please insert your delivery amount");
            }
        }

    }

    public void setadapterunit() {
        if (arrayList.size() > 0) {
            linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
            rcv_delivery_seller_unit.setLayoutManager(linearLayoutManager);
            Seller_delivery_cost_adpater seller_delivery_cost_adpater = new Seller_delivery_cost_adpater(arrayList, getContext(), new SelectedAdapterInterface() {
                @Override
                public void getAdapter(Integer position, String resultindex) {
                    if (resultindex.equals("edit")) {
                        edituint_delivery(position);
                    } else if (resultindex.equals("delete")) {
                        arrayList.remove(arrayList.get(position));
                        rcv_delivery_seller_unit.setLayoutManager(linearLayoutManager);
                        rcv_delivery_seller_unit.removeViewAt(position);
                        setadapterunit();
                    }
                }
            });
            rcv_delivery_seller_unit.setAdapter(seller_delivery_cost_adpater);
            rcv_delivery_seller_unit.setHasFixedSize(true);
        }
    }

    private void edituint_delivery(Integer position) {
        Dialog dialog = new Dialog(getContext(), R.style.FullScreenDialog);
        if (!dialog.isShowing()) {
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_delivery_unit_set);
            Window window = dialog.getWindow();
            assert window != null;
            WindowManager.LayoutParams wlp = Objects.requireNonNull(window).getAttributes();
            wlp.gravity = Gravity.CENTER;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
            window.setAttributes(wlp);
            Objects.requireNonNull(dialog.getWindow()).setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            ImageView delivery_charger_unit_close = dialog.findViewById(R.id.delivery_charger_unit_close);
            TextInputEditText ed_km_delivery = dialog.findViewById(R.id.ed_km_delivery);
            TextInputEditText ed_charge_delivery = dialog.findViewById(R.id.ed_charge_delivery);
            androidx.appcompat.widget.AppCompatButton btn_unit_dialogbox_submit = dialog.findViewById(R.id.btn_unit_dialogbox_submit);
            if (arrayList.size() > 0) {
                ed_km_delivery.setText(arrayList.get(position).getDistance());
                ed_charge_delivery.setText(arrayList.get(position).getDelcharge());
            }
            dialog.show();
            delivery_charger_unit_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
            });
            btn_unit_dialogbox_submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ed_km_delivery.getText().toString().length() == 0) {
                        ed_km_delivery.setError("Please enter delivery distance KM !");
                    } else if (ed_charge_delivery.getText().toString().length() == 0) {
                        ed_charge_delivery.setError("Please enter delivery charger !");
                    } else {
                       double amount=Double.parseDouble( ed_charge_delivery.getText().toString());
                       if(amount==0){
                           ed_charge_delivery.setError("Please enter delivery charger greater than zero !");
                       }else {
                           SellerDeliveryUnit productdetails = new SellerDeliveryUnit();
                           productdetails.setDelcharge(ed_charge_delivery.getText().toString());
                           productdetails.setDistance(ed_km_delivery.getText().toString());
                           productdetails.setOrder(ed_min_delivery.getText().toString());
                           if (position > 0) {
                               Integer distance = Integer.parseInt(arrayList.get(position - 1).getDistance());
                               Integer dynamicdistance = Integer.parseInt(ed_km_delivery.getText().toString());
                               Integer delchrager = Integer.parseInt(ed_charge_delivery.getText().toString());
                               if (distance >= dynamicdistance) {
                                   ed_km_delivery.setError("Please enter greater " + distance + " delivery distance KM !");
                               } else {
                                   arrayList.set(position, productdetails);
                                   linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
                                   rcv_delivery_seller_unit.setLayoutManager(linearLayoutManager);
                                   Seller_delivery_cost_adpater seller_delivery_cost_adpater = new Seller_delivery_cost_adpater(arrayList, getContext(), new SelectedAdapterInterface() {
                                       @Override
                                       public void getAdapter(Integer position, String resultindex) {
                                           if (resultindex.equals("edit")) {
                                               edituint_delivery(position);

                                           } else if (resultindex.equals("delete")) {
                                               arrayList.remove(arrayList.get(position));
                                               rcv_delivery_seller_unit.setLayoutManager(linearLayoutManager);
                                               rcv_delivery_seller_unit.removeViewAt(position);
                                               setadapterunit();
                                           }
                                       }
                                   });
                                   rcv_delivery_seller_unit.setAdapter(seller_delivery_cost_adpater);
                                   rcv_delivery_seller_unit.setHasFixedSize(true);
                                   dialog.dismiss();
                               }
                           } else {
                               Integer distance = Integer.parseInt(arrayList.get(position).getDistance());
                               Integer dynamicdistance = Integer.parseInt(ed_km_delivery.getText().toString());
                               Integer delchrager = Integer.parseInt(ed_charge_delivery.getText().toString());
                               if (distance >= dynamicdistance) {
                                   ed_km_delivery.setError("Please Enter greater " + distance + " Delivery Distance KM !");
                               } else {
                                   arrayList.set(position, productdetails);
                                   linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
                                   rcv_delivery_seller_unit.setLayoutManager(linearLayoutManager);
                                   Seller_delivery_cost_adpater seller_delivery_cost_adpater = new Seller_delivery_cost_adpater(arrayList, getContext(), new SelectedAdapterInterface() {
                                       @Override
                                       public void getAdapter(Integer position, String resultindex) {
                                           if (resultindex.equals("edit")) {
                                               edituint_delivery(position);

                                           } else if (resultindex.equals("delete")) {
                                               arrayList.remove(arrayList.get(position));
                                               rcv_delivery_seller_unit.setLayoutManager(linearLayoutManager);
                                               rcv_delivery_seller_unit.removeViewAt(position);
                                               setadapterunit();
                                           }
                                       }
                                   });
                                   rcv_delivery_seller_unit.setAdapter(seller_delivery_cost_adpater);
                                   rcv_delivery_seller_unit.setHasFixedSize(true);
                                   dialog.dismiss();
                               }
                           }
                       }

                    }
                }
            });
            dialog.setCancelable(false);

        }
    }
}