package com.Goingbo.Superseller.Model.CommissionSeller;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class WalletCost implements Serializable, Parcelable
{

    @SerializedName("pending_amount")
    @Expose
    private String pendingAmount;
    @SerializedName("paid_amount")
    @Expose
    private String paidAmount;
    public final static Creator<WalletCost> CREATOR = new Creator<WalletCost>() {


        @SuppressWarnings({
                "unchecked"
        })
        public WalletCost createFromParcel(Parcel in) {
            return new WalletCost(in);
        }

        public WalletCost[] newArray(int size) {
            return (new WalletCost[size]);
        }

    }
            ;
    private final static long serialVersionUID = 7524266598046046377L;

    protected WalletCost(Parcel in) {
        this.pendingAmount = ((String) in.readValue((String.class.getClassLoader())));
        this.paidAmount = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public WalletCost() {
    }

    /**
     *
     * @param paidAmount
     * @param pendingAmount
     */
    public WalletCost(String pendingAmount, String paidAmount) {
        super();
        this.pendingAmount = pendingAmount;
        this.paidAmount = paidAmount;
    }

    public String getPendingAmount() {
        return pendingAmount;
    }

    public void setPendingAmount(String pendingAmount) {
        this.pendingAmount = pendingAmount;
    }

    public WalletCost withPendingAmount(String pendingAmount) {
        this.pendingAmount = pendingAmount;
        return this;
    }

    public String getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(String paidAmount) {
        this.paidAmount = paidAmount;
    }

    public WalletCost withPaidAmount(String paidAmount) {
        this.paidAmount = paidAmount;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("pendingAmount", pendingAmount).append("paidAmount", paidAmount).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(paidAmount).append(pendingAmount).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof WalletCost) == false) {
            return false;
        }
        WalletCost rhs = ((WalletCost) other);
        return new EqualsBuilder().append(paidAmount, rhs.paidAmount).append(pendingAmount, rhs.pendingAmount).isEquals();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(pendingAmount);
        dest.writeValue(paidAmount);
    }

    public int describeContents() {
        return 0;
    }

}