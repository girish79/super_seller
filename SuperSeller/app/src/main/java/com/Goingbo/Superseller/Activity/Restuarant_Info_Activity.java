package com.Goingbo.Superseller.Activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.Goingbo.Superseller.Interface.ApiService;
import com.Goingbo.Superseller.Model.Bookingpre;
import com.Goingbo.Superseller.Model.RestuarantInfo;
import com.Goingbo.Superseller.R;
import com.Goingbo.Superseller.Util.CheckConnectivity;
import com.Goingbo.Superseller.Util.LibConstant;
import com.Goingbo.Superseller.Util.RetrofitClient;
import com.Goingbo.Superseller.Util.SessionManager;
import com.google.android.material.textfield.TextInputEditText;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Restuarant_Info_Activity extends AppCompatActivity {
    ImageView restuarant_Image,edit_image_Restuarant;
    TextInputEditText add_restuarant_name,add_restuarant_address,add_restuarant_contact;
    Button selected_restuarant_info_submit;
    SessionManager sessionManager;
    CheckConnectivity checkConnectivity;
    static final int REQUEST_PERMISSION_WRITE_EXTERNAL_STORAGE =
            1002;
    HashMap<String ,String> hashmapseller;
    int permissionCount = 0;
    protected static final int CAMERA_REQUEST = 0;
    protected static final int GALLERY_PICTURE =1;
    String stringone="";
    boolean isshowing=true;
    ImageView image_backpress;
    HashMap<String,String>restaurantInfo;
    ProgressDialog progressDialog;
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restuarant__info_);
        init();
    }

    private void init() {
        try {
            restuarant_Image = findViewById(R.id.restuarant_Image);
            image_backpress = findViewById(R.id.image_backpress);
            image_backpress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            TextView title_text = findViewById(R.id.title_text);
            title_text.setText("Restaurant ");
            edit_image_Restuarant = findViewById(R.id.edit_image_Restuarant);
            add_restuarant_name = findViewById(R.id.add_restuarant_name);
            add_restuarant_address = findViewById(R.id.add_restuarant_address);
            add_restuarant_contact = findViewById(R.id.add_restuarant_contact);
            selected_restuarant_info_submit = findViewById(R.id.selected_restuarant_info_submit);
            sessionManager = new SessionManager(getApplicationContext());
            hashmapseller = sessionManager.getsoicalDetails();
            restaurantInfo = sessionManager.getrestaurantaddress();
            Log.e("restaurantdetails", "" + restaurantInfo.get(LibConstant.Rcontact));
            if (restaurantInfo.get(LibConstant.Rcontact) != null) {
                add_restuarant_contact.setText("" + restaurantInfo.get(LibConstant.Rcontact));
            }
            if (restaurantInfo.get(LibConstant.RADDRESS) != null) {
                add_restuarant_address.setText("" + restaurantInfo.get(LibConstant.RADDRESS));
                selected_restuarant_info_submit.setText("UPDATE");
            }
            if (restaurantInfo.get(LibConstant.RNAME) != null) {
                add_restuarant_name.setText("" + restaurantInfo.get(LibConstant.RNAME));
            }

            checkConnectivity = new CheckConnectivity(Restuarant_Info_Activity.this);
            edit_image_Restuarant.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectImage();
                }
            });
            selected_restuarant_info_submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!process()) {
                        if (stringone.length() == 0) {
                            new androidx.appcompat.app.AlertDialog.Builder(Objects.requireNonNull(Restuarant_Info_Activity.this))
                                    .setTitle("Alert")
                                    .setMessage("Please Insert Restaurant Image  !")
                                    .setCancelable(false)
                                    .setPositiveButton("OK", (dialog, which) -> {
                                        dialog.dismiss();
                                        selectImage();
                                    }).show();
                        } else if (add_restuarant_name.getText().toString().length() == 0) {
                            add_restuarant_name.setError("Please Enter Restaurant Name !");
                        } else if (add_restuarant_name.getText().toString().equals(" ")) {
                            add_restuarant_name.setError("Please Enter Restaurant Name !");
                        } else if (add_restuarant_address.getText().toString().length() == 0) {
                            add_restuarant_address.setError("Please Enter Restaurant Address !");
                        } else if (add_restuarant_address.getText().toString().equals(" ")) {
                            add_restuarant_address.setError("Please Enter Restaurant Address !");
                        } else if (add_restuarant_contact.getText().toString().length() == 0) {
                            add_restuarant_contact.setError("Please Enter Restaurant Contact Number !");
                        } else if (add_restuarant_contact.getText().toString().equals(" ")) {
                            add_restuarant_contact.setError("Please Enter Restaurant Contact Number !");
                        } else if (add_restuarant_contact.getText().toString().length() < 10) {
                            add_restuarant_contact.setError("Please Enter Correct Restaurant Contact Number !");
                        } else {
                            try {

                                MultipartBody.Part uploadfile;
                                String sellerid = hashmapseller.get(LibConstant.socialid);
                                Log.e("sendimage", "" + stringone);
                                File propertyImageFile1 = new File(stringone);
                                RequestBody videoBody1 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile1);
                                uploadfile = MultipartBody.Part.createFormData("uplaodfile", propertyImageFile1.getName(), videoBody1);
                                RequestBody r_name = RequestBody.create(MediaType.parse("text/plain"), add_restuarant_name.getText().toString());
                                RequestBody r_contact = (RequestBody) RequestBody.create(MediaType.parse("text/plain"), add_restuarant_contact.getText().toString());
                                RequestBody r_address = RequestBody.create(MediaType.parse("text/plain"), add_restuarant_address.getText().toString());
                                RequestBody seller_id = (RequestBody) RequestBody.create(MediaType.parse("text/plain"), sellerid);
                                progressDialog = new ProgressDialog(Restuarant_Info_Activity.this);
                                progressDialog.setMessage("Loading......");
                                progressDialog.show();
                                Call<RestuarantInfo> callrestaurant = RetrofitClient.getClient("https://www.goingbo.com/")
                                        .create(ApiService.class).
                                                updateRestaurant(r_name, r_contact, r_address, seller_id, uploadfile);
                                callrestaurant.enqueue(new Callback<RestuarantInfo>() {
                                    @Override
                                    public void onResponse(Call<RestuarantInfo> call, Response<RestuarantInfo> response) {
                                        RestuarantInfo bookingpre = response.body();
                                        if (!bookingpre.isError()) {
                                            new androidx.appcompat.app.AlertDialog.Builder(Objects.requireNonNull(Restuarant_Info_Activity.this))
                                                    .setTitle("Alert")
                                                    .setMessage(bookingpre.getMessage())
                                                    .setCancelable(false)
                                                    .setPositiveButton("OK", (dialog, which) -> {
                                                        dialog.dismiss();
                                                        progressDialog.dismiss();
                                                        sessionManager.setrestaurantddress(
                                                                sellerid, add_restuarant_name.getText().toString(),
                                                                add_restuarant_address.getText().toString(),
                                                                add_restuarant_contact.getText().toString(),
                                                                bookingpre.getImageinfo()
                                                        );
                                                        Log.v("sellerid", "" + sellerid);
                                                        Log.v("sellername", "" + add_restuarant_name.getText().toString());
                                                        Log.v("selleraddress", "" + add_restuarant_address.getText().toString());
                                                        Log.v("sellercontact", "" + add_restuarant_address.getText().toString());
                                                        Log.v("sellerimage", "" + bookingpre.getImageinfo());
                                                        onBackPressed();
                                                    }).show();
                                        } else {
                                            progressDialog.dismiss();
                                            new androidx.appcompat.app.AlertDialog.Builder(Objects.requireNonNull(Restuarant_Info_Activity.this))
                                                    .setTitle("Alert")
                                                    .setMessage(bookingpre.getMessage())
                                                    .setCancelable(false)
                                                    .setPositiveButton("OK", (dialog, which) -> {
                                                        dialog.dismiss();

                                                    }).show();
                                        }

                                    }

                                    @Override
                                    public void onFailure(Call<RestuarantInfo> call, Throwable t) {
                                        progressDialog.dismiss();
                                        new androidx.appcompat.app.AlertDialog.Builder(Objects.requireNonNull(Restuarant_Info_Activity.this))
                                                .setTitle("Alert")
                                                .setMessage("Api failed due to some technical problem , please try again !")
                                                .setCancelable(false)
                                                .setPositiveButton("OK", (dialog, which) -> {
                                                    dialog.dismiss();
                                                }).show();
                                    }
                                });
                            } catch (Exception ex) {
                                ex.printStackTrace();
                                new androidx.appcompat.app.AlertDialog.Builder(Objects.requireNonNull(Restuarant_Info_Activity.this))
                                        .setTitle("Alert")
                                        .setMessage("Api failed due to some technical problem , please try again !")
                                        .setCancelable(false)
                                        .setPositiveButton("OK", (dialog, which) -> {
                                            dialog.dismiss();

                                        }).show();
                            }

                        }
                    }
                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }


    }
    private void selectImage() {

        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};

        AlertDialog.Builder builders = new AlertDialog.Builder(Restuarant_Info_Activity.this);
        builders.setTitle("Restaurant");
        builders.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Take Photo")) {

                    cameraIntent("image");

                } else if (items[item].equals("Choose from Library")) {

                    galleryIntent("image");

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builders.show();
    }
    private void cameraIntent(String datatype) {
        if (permissionCount == 0) {
            if (ContextCompat.checkSelfPermission(Restuarant_Info_Activity.this, Manifest.permission.CAMERA) !=
                    PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(Restuarant_Info_Activity.this,new String[]
                                {Manifest.permission.CAMERA},
                        CAMERA_REQUEST);
            } else {
                permissionCount++;
                if (ContextCompat.checkSelfPermission
                        (Restuarant_Info_Activity.this,Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                        PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(Restuarant_Info_Activity.this,new String[]
                                    {Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            REQUEST_PERMISSION_WRITE_EXTERNAL_STORAGE);
                } else
                    permissionCount++;

                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, CAMERA_REQUEST);

            }


        } else {
            if (datatype == "image") {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, CAMERA_REQUEST);
            }

        }
    }
    private void galleryIntent(String datatype) {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto, GALLERY_PICTURE);
        // startActivityForResult(Intent.createChooser(intent, "Select File"),GALLERY_PICTURE);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {


                onCaptureImageResult(data);

            }

        }


        if (requestCode == GALLERY_PICTURE) {
            onSelectFromGalleryResult(data);
        }
    }
    private void onCaptureImageResult(Intent data) {
        //    Toast.makeText(getContext(), "Camera", Toast.LENGTH_SHORT).show();
        String partFilename = currentDateFormat();
        File file = storeCameraPhotoInSDCard((Bitmap) data.getExtras().get("data"), partFilename);
        stringone = file.getAbsolutePath();
        String valuepath = getImageUri(Restuarant_Info_Activity.this, (Bitmap) data.getExtras().get("data"));
        Log.e("imagepath", stringone);
        Bitmap bitmap = (Bitmap) data.getExtras().get("data");
        restuarant_Image.setImageBitmap((Bitmap) bitmap);



    }
    private void onSelectFromGalleryResult(Intent data)   {
        try {

            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            assert selectedImage != null;
            Cursor cursor = Objects.requireNonNull(Restuarant_Info_Activity.this).getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            assert cursor != null;
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String filePath = cursor.getString(columnIndex);
            cursor.close();


            int nh = (int) (BitmapFactory.decodeFile(filePath).getHeight() * (512.0 / BitmapFactory.decodeFile(filePath).getWidth()));
            Bitmap scaled = Bitmap.createScaledBitmap(BitmapFactory.decodeFile(filePath), 512, nh, true);
            String path = getRealPathFromURIPath(selectedImage, Restuarant_Info_Activity.this);
            //  add_select_seller_product.setImageBitmap(BitmapFactory.decodeFile(picturePath));
            if(path.length()>2) {
                restuarant_Image.setImageBitmap(scaled);
                stringone = path;
            }

        }catch ( Exception e){e.printStackTrace();}


    }
    public String getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return path;
    }
    private String currentDateFormat(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HH_mm_ss");
        String  currentTimeStamp = dateFormat.format(new Date());
        return currentTimeStamp;
    }
    private File storeCameraPhotoInSDCard(Bitmap bitmap, String currentDate){
        // Create an image file name
        File outputFile ;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            outputFile = new File(Environment.getExternalStorageDirectory(), "photo_" + currentDate + ".jpg");
        }else{
            outputFile = new File(Restuarant_Info_Activity.this.getExternalCacheDir(), "photo_" + currentDate + ".jpg");
        }
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(outputFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();
            return outputFile;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }
    private boolean process() {
        boolean show = true;
        if (checkConnectivity.isConnected()) {
            show = false;

        } else {

            showDialog();
            show = true;
        }
        return show;
    }

    private void showDialog() {

        Dialog dialog = new Dialog(Restuarant_Info_Activity.this, R.style.FullScreenDialogWithStatusBarColorAccent);
        if (!dialog.isShowing()){
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.no_internet);
            Window window = dialog.getWindow();
            assert window != null;
            WindowManager.LayoutParams wlp = Objects.requireNonNull(window).getAttributes();
            wlp.gravity = Gravity.CENTER;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
            window.setAttributes(wlp);
            Objects.requireNonNull(dialog.getWindow()).setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            Button wifi_btn = dialog.findViewById(R.id.wifi_btn);
            Button mobile_data_btn = dialog.findViewById(R.id.mobile_data_btn);
            dialog.show();
            dialog.setCancelable(false);
            wifi_btn.setOnClickListener(v -> {
                dialog.dismiss();
                isshowing = false;
                WifiManager wifiMgr = (WifiManager)Restuarant_Info_Activity.this. getApplicationContext().getSystemService(Context.WIFI_SERVICE);

                assert wifiMgr != null;
                if (wifiMgr.isWifiEnabled()) { // Wi-Fi adapter is ON

                    WifiInfo wifiInfo = wifiMgr.getConnectionInfo();

                    if (wifiInfo.getNetworkId() == -1) {

                        showDialog();
                        // Not connected to an access point
                    } else {
                        dialog.dismiss();
                        process();
                    }
                    // Connected to an access point
                } else {
                    startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS)); // Wi-Fi adapter is OFF
                }
            });
            mobile_data_btn.setOnClickListener(v -> {
                dialog.dismiss();
                isshowing = false;
                startActivityForResult(new Intent(android.provider.Settings.ACTION_DATA_ROAMING_SETTINGS), 0);
                if (checkConnectivity.isConnected()) {
                    process();

                } else {
                    showDialog();
                }
            });
        }

    }
    private String getRealPathFromURIPath(Uri contentURI, Activity activity) {
        try {
            Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
            if (cursor == null) {
                return contentURI.getPath();
            } else {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                return cursor.getString(idx);
            }
        }catch (Exception ex){ex.printStackTrace();
            return "";}
    }
}