package com.Goingbo.Superseller.Model.seller_unit;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class UnitSeller implements Serializable, Parcelable
{

    @SerializedName("productid")
    @Expose
    private String productid;
    @SerializedName("productdetail")
    @Expose
    private List<Productdetail> productdetail = new ArrayList<Productdetail>();
    public final static Creator<UnitSeller> CREATOR = new Creator<UnitSeller>() {


        @SuppressWarnings({
                "unchecked"
        })
        public UnitSeller createFromParcel(Parcel in) {
            return new UnitSeller(in);
        }

        public UnitSeller[] newArray(int size) {
            return (new UnitSeller[size]);
        }

    }
            ;
    private final static long serialVersionUID = -1375402849051442112L;

    protected UnitSeller(Parcel in) {
        this.productid = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.productdetail, (Productdetail.class.getClassLoader()));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public UnitSeller() {
    }

    public UnitSeller(String productid) {
        this.productid = productid;
    }

    /**
     *
     * @param productdetail
     * @param productid
     */
    public UnitSeller(String productid, List<Productdetail> productdetail) {
        super();
        this.productid = productid;
        this.productdetail = productdetail;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public UnitSeller withProductid(String productid) {
        this.productid = productid;
        return this;
    }

    public List<Productdetail> getProductdetail() {
        return productdetail;
    }

    public void setProductdetail(List<Productdetail> productdetail) {
        this.productdetail = productdetail;
    }

    public UnitSeller withProductdetail(List<Productdetail> productdetail) {
        this.productdetail = productdetail;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("productid", productid).append("productdetail", productdetail).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(productid).append(productdetail).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof UnitSeller) == false) {
            return false;
        }
        UnitSeller rhs = ((UnitSeller) other);
        return new EqualsBuilder().append(productid, rhs.productid).append(productdetail, rhs.productdetail).isEquals();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(productid);
        dest.writeList(productdetail);
    }

    public int describeContents() {
        return 0;
    }

}