package com.Goingbo.Superseller.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.provider.Settings;
import android.text.Html;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.Goingbo.Superseller.Activity.Menu_Item_Edit;
import com.Goingbo.Superseller.Activity.Restaurant_DashBoard;
import com.Goingbo.Superseller.Activity.Restuarant_Detail_forum;
import com.Goingbo.Superseller.Activity.Restuarant_Info_Activity;
import com.Goingbo.Superseller.Adapter.Restaurant_Menu_Adpater;
import com.Goingbo.Superseller.Interface.ApiService;
import com.Goingbo.Superseller.Interface.SelectedAdapterInterface;
import com.Goingbo.Superseller.Model.Bookingpre;
import com.Goingbo.Superseller.Model.Restaurant_Item_Menu.MENUSRESTURANT;
import com.Goingbo.Superseller.Model.Restaurant_Item_Menu.Restuarantmenus;
import com.Goingbo.Superseller.R;
import com.Goingbo.Superseller.Util.CheckConnectivity;
import com.Goingbo.Superseller.Util.LibConstant;
import com.Goingbo.Superseller.Util.RetrofitClient;
import com.Goingbo.Superseller.Util.SessionManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Restaurant_dashboard_Fragment extends Fragment {
    View view;
    SessionManager sessionManager;
    CheckConnectivity checkConnectivity;
    boolean isshowing=true;
    Button btn_restaurant_menu;
    CardView restaurant_menu_cart;
    RecyclerView restaurant_menu_category;
    RecyclerView restaurant_menu_list;
    LinearLayout dummy_layout_restaurant;
    private List<String> categoryList = new ArrayList<>();
    private ArrayList<MENUSRESTURANT> menuItemList=new ArrayList<>();
    LinearLayoutManager linearLayoutManager;
    LinearLayoutManager linearLayoutManagermenu;
    TextView tv_combos;
    ScrollView sc_restaurant;
    HashMap<String,String> hashMap;
    public int selectedPosition=-1;
    public CatAdapter mAdapter;
    Restaurant_Menu_Adpater restaurant_menu_adpater;
    AlertDialog alertdialog;
    LinearLayout linearlayout_restaurant_info;
    ProgressDialog progressDialog;
    LinearLayout linearlayout_scrollview_restaurant;
    public Restaurant_dashboard_Fragment() {
        // Required empty public constructor
    }
    public static Restaurant_dashboard_Fragment newInstance(String param1, String param2) {
        Restaurant_dashboard_Fragment fragment = new Restaurant_dashboard_Fragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_restaurant_dashboard, container, false);
        init();
        return view;
    }
    public void init(){
        sessionManager=new SessionManager(getContext().getApplicationContext());
        checkConnectivity=new CheckConnectivity(getContext());
        btn_restaurant_menu=view.findViewById(R.id.btn_restaurant_menu);
        sc_restaurant=view.findViewById(R.id.sc_restaurant);
        tv_combos=view.findViewById(R.id.tv_combos);

        restaurant_menu_cart=view.findViewById(R.id.restaurant_menu_cart);
        restaurant_menu_category=view.findViewById(R.id.restaurant_menu_category);
        linearlayout_restaurant_info=view.findViewById(R.id.linearlayout_restaurant_info);
        restaurant_menu_list=view.findViewById(R.id.restaurant_menu_list);
        linearlayout_scrollview_restaurant=view.findViewById(R.id.linearlayout_scrollview_restaurant);
        dummy_layout_restaurant=view.findViewById(R.id.dummy_layout_restaurant);
        btn_restaurant_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentaddmenu = new Intent(getContext(), Restuarant_Detail_forum.class);
                startActivity(intentaddmenu);
            }
        });

        hashMap=sessionManager.getrestaurantaddress();
        linearlayout_restaurant_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), Restuarant_Info_Activity.class));
            }
        });
        getmenuItem();

    }
    public void getmenuItem(){
        categoryList.clear();
        menuItemList.clear();
        restaurant_menu_cart.setVisibility(View.GONE);
        dummy_layout_restaurant.setVisibility(View.VISIBLE);
        String seller_id=hashMap.get(LibConstant.socialid);
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading......");
        progressDialog.show();
        try {
            if (!process()) {
                Call<Restuarantmenus> callmenu = RetrofitClient.getClient("https://www.goingbo.com/").create(ApiService.class).updatemenu(seller_id);
                callmenu.enqueue(new Callback<Restuarantmenus>() {
                    @Override
                    public void onResponse(Call<Restuarantmenus> call, Response<Restuarantmenus> response) {
                        Restuarantmenus menuitemresturant = response.body();
                        progressDialog.dismiss();
                        if (!menuitemresturant.isError()) {
                            if (menuitemresturant.getMENUSRESTURANT().size() != 0) {
                                restaurant_menu_cart.setVisibility(View.VISIBLE);
                                dummy_layout_restaurant.setVisibility(View.GONE);
                                menuItemList.clear();
                                menuItemList.addAll(menuitemresturant.getMENUSRESTURANT());
                                for (int k = 0; k < menuItemList.size(); k++) {
                                    Log.e("catsize", "" + categoryList.size());
                                    if (categoryList.size() == 0) {


                                        Log.e("categoriesname", "" + menuItemList.get(k).getMenuItemCategory());
                                        categoryList.add(menuItemList.get(k).getMenuItemCategory());

                                        mAdapter = new CatAdapter(categoryList, getContext(), new SelectedAdapterInterface() {
                                            @Override
                                            public void getAdapter(Integer position, String resultindex) {
                                                if (resultindex.length() > 0) {
                                                    ArrayList<MENUSRESTURANT> customlist = new ArrayList<>();
                                                    customlist.clear();
                                                    for (int s = 0; s < menuItemList.size(); s++) {
                                                        if (menuItemList.get(s).getMenuItemCategory().contains(categoryList.get(position))) {
                                                            customlist.add(menuItemList.get(s));
                                                        }
                                                    }

                                                    if (customlist.size() > 0) {
                                                        restaurant_menu_adpater = new Restaurant_Menu_Adpater(getContext(), customlist, new SelectedAdapterInterface() {
                                                            @Override
                                                            public void getAdapter(Integer position, String resultindex) {
                                                                if (resultindex.equals("edit")) {
                                                                    Intent intent = new Intent(getContext(), Menu_Item_Edit.class);
                                                                    intent.putExtra("menu_id", menuItemList.get(position).getMenuItemId());
                                                                    intent.putExtra("menu_tittle", menuItemList.get(position).getMenuItemName());
                                                                    intent.putExtra("menu_category", menuItemList.get(position).getMenuItemCategory());
                                                                    intent.putExtra("menu_detail", menuItemList.get(position).getMenuItemDetails());
                                                                    intent.putExtra("menu_image", menuItemList.get(position).getMenuItemImage());
                                                                    intent.putExtra("restaurantmenuitemsId", menuItemList.get(position).getRestaurantmenuitemsId());
                                                                    intent.putExtra("menu_details", menuItemList.get(position).getRestaurantmenuitemsMenuItems());
                                                                    intent.putExtra("senddata", "edit");
                                                                    startActivity(intent);
                                                                } else if (resultindex.equals("remove")) {
                                                                    alertdialog = new AlertDialog.Builder(getContext())
                                                                            .setTitle("Your Alert")
                                                                            .setMessage("Due you want to remove menu for list")
                                                                            .setCancelable(true)
                                                                            .setNegativeButton("Cancel", (dialog, which) -> {
                                                                                dialog.dismiss();
                                                                            })
                                                                            .setPositiveButton("OK", (dialog, which) -> {
                                                                                dialog.dismiss();
                                                                                if (!process()) {
                                                                                    Call<Bookingpre> calldelete = RetrofitClient.getClient("https://www.goingbo.com/").create(ApiService.class)
                                                                                            .deletemenu(menuItemList.get(position).getRestaurantmenuitemsId(), seller_id, menuItemList.get(position).getMenuItemId());
                                                                                    calldelete.enqueue(new Callback<Bookingpre>() {
                                                                                        @Override
                                                                                        public void onResponse(Call<Bookingpre> call, Response<Bookingpre> response) {
                                                                                            Bookingpre bookingpre = response.body();
                                                                                            if (!bookingpre.getError()) {
                                                                                                getmenuItem();
                                                                                            }
                                                                                        }

                                                                                        @Override
                                                                                        public void onFailure(Call<Bookingpre> call, Throwable t) {

                                                                                        }
                                                                                    });
                                                                                }
                                                                            }).create();
                                                                    if (!alertdialog.isShowing()) {
                                                                        alertdialog.show();
                                                                    }

                                                                }

                                                            }
                                                        });
                                                        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
                                                        restaurant_menu_list.setLayoutManager(linearLayoutManager);
                                                        restaurant_menu_list.setAdapter(restaurant_menu_adpater);
                                                        restaurant_menu_list.setHasFixedSize(true);

                                                    }

                                                }

                                            }
                                        });
                                        restaurant_menu_category.setAdapter(mAdapter);
                                        restaurant_menu_category.setItemAnimator(new DefaultItemAnimator());
                                        restaurant_menu_category.setAdapter(mAdapter);
                                        restaurant_menu_category.setHasFixedSize(true);
                                        linearLayoutManagermenu = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
                                        restaurant_menu_category.setLayoutManager(linearLayoutManagermenu);
                                    } else {
                                        boolean resulttype = categoryList.contains(menuItemList.get(k).getMenuItemCategory());
                                        if (!resulttype) {
                                            Log.e("categoriesname", "" + menuItemList.get(k).getMenuItemCategory());
                                            categoryList.add(menuItemList.get(k).getMenuItemCategory());
                                            mAdapter = new CatAdapter(categoryList, getContext(), new SelectedAdapterInterface() {
                                                @Override
                                                public void getAdapter(Integer position, String resultindex) {
                                                    if (resultindex.length() > 0) {
                                                        ArrayList<MENUSRESTURANT> customlist = new ArrayList<>();
                                                        customlist.clear();
                                                        for (int s = 0; s < menuItemList.size(); s++) {
                                                            if (menuItemList.get(s).getMenuItemCategory().contains(categoryList.get(position))) {
                                                                customlist.add(menuItemList.get(s));
                                                            }
                                                        }

                                                        if (customlist.size() > 0) {
                                                            restaurant_menu_adpater = new Restaurant_Menu_Adpater(getContext(), customlist, new SelectedAdapterInterface() {
                                                                @Override
                                                                public void getAdapter(Integer position, String resultindex) {
                                                                    if (resultindex.equals("edit")) {
                                                                        Intent intent = new Intent(getContext(), Menu_Item_Edit.class);
                                                                        intent.putExtra("menu_id", menuItemList.get(position).getMenuItemId());
                                                                        intent.putExtra("menu_tittle", menuItemList.get(position).getMenuItemName());
                                                                        intent.putExtra("menu_category", menuItemList.get(position).getMenuItemCategory());
                                                                        intent.putExtra("menu_detail", menuItemList.get(position).getMenuItemDetails());
                                                                        intent.putExtra("menu_image", menuItemList.get(position).getMenuItemImage());
                                                                        intent.putExtra("restaurantmenuitemsId", menuItemList.get(position).getRestaurantmenuitemsId());
                                                                        intent.putExtra("menu_details", menuItemList.get(position).getRestaurantmenuitemsMenuItems());
                                                                        intent.putExtra("senddata", "edit");
                                                                        startActivity(intent);
                                                                    } else if (resultindex.equals("remove")) {
                                                                        alertdialog = new AlertDialog.Builder(getContext())
                                                                                .setTitle("Your Alert")
                                                                                .setMessage("Due you want to remove menu for list")
                                                                                .setCancelable(true)
                                                                                .setNegativeButton("Cancel", (dialog, which) -> {
                                                                                    dialog.dismiss();
                                                                                })
                                                                                .setPositiveButton("OK", (dialog, which) -> {
                                                                                    dialog.dismiss();
                                                                                    if (!process()) {
                                                                                        Call<Bookingpre> calldelete = RetrofitClient.getClient("https://www.goingbo.com/").create(ApiService.class)
                                                                                                .deletemenu(menuItemList.get(position).getRestaurantmenuitemsId(), seller_id, menuItemList.get(position).getMenuItemId());
                                                                                        calldelete.enqueue(new Callback<Bookingpre>() {
                                                                                            @Override
                                                                                            public void onResponse(Call<Bookingpre> call, Response<Bookingpre> response) {
                                                                                                Bookingpre bookingpre = response.body();
                                                                                                if (!bookingpre.getError()) {
                                                                                                    getmenuItem();
                                                                                                }
                                                                                            }

                                                                                            @Override
                                                                                            public void onFailure(Call<Bookingpre> call, Throwable t) {

                                                                                            }
                                                                                        });
                                                                                    }
                                                                                }).create();
                                                                        if (!alertdialog.isShowing()) {
                                                                            alertdialog.show();
                                                                        }


                                                                    }

                                                                }
                                                            });
                                                            linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
                                                            restaurant_menu_list.setLayoutManager(linearLayoutManager);
                                                            restaurant_menu_list.setAdapter(restaurant_menu_adpater);
                                                            restaurant_menu_list.setHasFixedSize(true);
                                                        }

                                                    }

                                                }
                                            });
                                            restaurant_menu_category.setAdapter(mAdapter);
                                            restaurant_menu_category.setItemAnimator(new DefaultItemAnimator());
                                            restaurant_menu_category.setAdapter(mAdapter);
                                            restaurant_menu_category.setHasFixedSize(true);
                                            linearLayoutManagermenu = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
                                            restaurant_menu_category.setLayoutManager(linearLayoutManagermenu);

                                        }
                                    }

                                    restaurant_menu_adpater = new Restaurant_Menu_Adpater(getContext(), menuItemList, new SelectedAdapterInterface() {
                                        @Override
                                        public void getAdapter(Integer position, String resultindex) {
                                            if (resultindex.equals("edit")) {
                                                Intent intent = new Intent(getContext(), Menu_Item_Edit.class);
                                                intent.putExtra("menu_id", menuItemList.get(position).getMenuItemId());
                                                intent.putExtra("menu_tittle", menuItemList.get(position).getMenuItemName());
                                                intent.putExtra("menu_category", menuItemList.get(position).getMenuItemCategory());
                                                intent.putExtra("menu_detail", menuItemList.get(position).getMenuItemDetails());
                                                intent.putExtra("menu_image", menuItemList.get(position).getMenuItemImage());
                                                intent.putExtra("restaurantmenuitemsId", menuItemList.get(position).getRestaurantmenuitemsId());
                                                intent.putExtra("menu_details", menuItemList.get(position).getRestaurantmenuitemsMenuItems());
                                                intent.putExtra("senddata", "edit");
                                                startActivity(intent);
                                            } else if (resultindex.equals("remove")) {
                                                alertdialog = new AlertDialog.Builder(getContext())
                                                        .setTitle("Your Alert")
                                                        .setMessage("Due you want to remove menu for list")
                                                        .setCancelable(true)
                                                        .setNegativeButton("Cancel", (dialog, which) -> {
                                                            dialog.dismiss();
                                                        })
                                                        .setPositiveButton("OK", (dialog, which) -> {
                                                            dialog.dismiss();
                                                            if (!process()) {
                                                                Call<Bookingpre> calldelete = RetrofitClient.getClient("https://www.goingbo.com/").create(ApiService.class)
                                                                        .deletemenu(menuItemList.get(position).getRestaurantmenuitemsId(), seller_id, menuItemList.get(position).getMenuItemId());
                                                                calldelete.enqueue(new Callback<Bookingpre>() {
                                                                    @Override
                                                                    public void onResponse(Call<Bookingpre> call, Response<Bookingpre> response) {
                                                                        Bookingpre bookingpre = response.body();
                                                                        if (!bookingpre.getError()) {
                                                                            getmenuItem();
                                                                        }
                                                                    }

                                                                    @Override
                                                                    public void onFailure(Call<Bookingpre> call, Throwable t) {

                                                                    }
                                                                });
                                                            }

                                                        }).create();
                                                if (!alertdialog.isShowing()) {
                                                    alertdialog.show();
                                                }

                                            }

                                        }
                                    });
                                    linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
                                    restaurant_menu_list.setLayoutManager(linearLayoutManager);
                                    restaurant_menu_list.setAdapter(restaurant_menu_adpater);
                                    restaurant_menu_list.setHasFixedSize(true);
                                    scrollToRow(sc_restaurant,linearlayout_scrollview_restaurant,tv_combos);
                                }
                            } else {
                                restaurant_menu_cart.setVisibility(View.GONE);
                                dummy_layout_restaurant.setVisibility(View.VISIBLE);
                            }

                        } else {
                            restaurant_menu_cart.setVisibility(View.GONE);
                            dummy_layout_restaurant.setVisibility(View.VISIBLE);
                        }

                    }

                    @Override
                    public void onFailure(Call<Restuarantmenus> call, Throwable t) {
                        progressDialog.dismiss();
                        restaurant_menu_cart.setVisibility(View.GONE);
                        dummy_layout_restaurant.setVisibility(View.VISIBLE);
                    }
                });
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void scrollToRow(final ScrollView scrollView, final LinearLayout linearLayout, final TextView textViewToShow) {
        long delay = 100; //delay to let finish with possible modifications to ScrollView
        scrollView.postDelayed(new Runnable() {
            public void run() {
                Rect textRect = new Rect(); //coordinates to scroll to
                textViewToShow.getHitRect(textRect); //fills textRect with coordinates of TextView relative to its parent (LinearLayout)
                scrollView.requestChildRectangleOnScreen(linearLayout, textRect, false); //ScrollView will make sure, the given textRect is visible
            }
        }, delay);
    }

    private boolean process() {
        boolean show = true;
        if (checkConnectivity.isConnected()) {
            show = false;

        } else {

            showDialog();
            show = true;
        }
        return show;
    }
    private void showDialog() {

        Dialog dialog = new Dialog(getContext(), R.style.FullScreenDialogWithStatusBarColorAccent);
        if (!dialog.isShowing()){
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.no_internet);
            Window window = dialog.getWindow();
            assert window != null;
            WindowManager.LayoutParams wlp = Objects.requireNonNull(window).getAttributes();
            wlp.gravity = Gravity.CENTER;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
            window.setAttributes(wlp);
            Objects.requireNonNull(dialog.getWindow()).setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            Button wifi_btn = dialog.findViewById(R.id.wifi_btn);
            Button mobile_data_btn = dialog.findViewById(R.id.mobile_data_btn);
            dialog.show();
            dialog.setCancelable(false);
            wifi_btn.setOnClickListener(v -> {
                dialog.dismiss();
                isshowing = false;
                WifiManager wifiMgr = (WifiManager)getActivity(). getApplicationContext().getSystemService(Context.WIFI_SERVICE);

                assert wifiMgr != null;
                if (wifiMgr.isWifiEnabled()) { // Wi-Fi adapter is ON

                    WifiInfo wifiInfo = wifiMgr.getConnectionInfo();

                    if (wifiInfo.getNetworkId() == -1) {

                        showDialog();
                        // Not connected to an access point
                    } else {
                        dialog.dismiss();
                        process();
                    }
                    // Connected to an access point
                } else {
                    startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS)); // Wi-Fi adapter is OFF
                }
            });
            mobile_data_btn.setOnClickListener(v -> {
                dialog.dismiss();
                isshowing = false;
                startActivityForResult(new Intent(android.provider.Settings.ACTION_DATA_ROAMING_SETTINGS), 0);
                if (checkConnectivity.isConnected()) {
                    process();

                } else {
                    showDialog();
                }
            });
        }

    }
    public class CatAdapter extends RecyclerView.Adapter<CatAdapter.MyViewHolder> {

        List<String> categoryList;
        Context context;
        String Tag;
        SelectedAdapterInterface selectedAdapterInterface;

        private SparseBooleanArray selectedItems = new SparseBooleanArray();
        public CatAdapter(List<String> categoryList, Context context,SelectedAdapterInterface selectedAdapterInterface) {
            this.categoryList = categoryList;
            this.context = context;
            this.selectedAdapterInterface=selectedAdapterInterface;

        }

        @NonNull
        @Override
        public CatAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
            View itemView;

            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_category, parent, false);
            return new CatAdapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull final CatAdapter.MyViewHolder holder,final int position) {


            holder.title.setText(Html.fromHtml(categoryList.get(position)));


            holder.card_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String ids=categoryList.get(position);
                    selectedPosition=position;
                    //holder.card_view.setBackgroundColor(Color.parseColor("#bbbbbb"));
                    selectedAdapterInterface.getAdapter(position,ids);
                    notifyDataSetChanged();
                }
            });
            if(selectedPosition==position) {
                holder.card_view.setCardBackgroundColor(Color.parseColor("#bbbbbb"));
                holder.card_view.setCardBackgroundColor(Color.parseColor("#ffffff"));
                holder.title.setTextColor(context.getResources().getColor(R.color.black));
                holder.view_slid.setVisibility(View.VISIBLE);
            } else {

                holder.title.setTextColor(context.getResources().getColor(R.color.light_sliver));
                holder.view_slid.setVisibility(View.GONE);
            }

        }

        @Override
        public int getItemCount() {
            return categoryList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            //        ImageView imageView;
            public TextView title;
            public CardView card_view;
            public LinearLayout linear_layout_categories;
            public View view_slid;
            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                title = itemView.findViewById(R.id.category_title);
                card_view = itemView.findViewById(R.id.card_view_Category);
                linear_layout_categories = itemView.findViewById(R.id.linear_layout_categories);
                view_slid = itemView.findViewById(R.id.view_slid);
                //card_view.setBackgroundColor(Color.parseColor("#ffffff"));

            }
        }
    }
}