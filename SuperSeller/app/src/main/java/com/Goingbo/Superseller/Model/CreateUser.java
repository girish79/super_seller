package com.Goingbo.Superseller.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CreateUser implements Serializable
{

    @SerializedName("error")
    @Expose
    private boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("user")
    @Expose
    private List<User> user = new ArrayList<User>();
    private final static long serialVersionUID = 7485041893159797984L;

    /**
     * No args constructor for use in serialization
     *
     */
    public CreateUser() {
    }

    /**
     *
     * @param error
     * @param message
     * @param user
     */
    public CreateUser(boolean error, String message, List<User> user) {
        super();
        this.error = error;
        this.message = message;
        this.user = user;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public CreateUser withError(boolean error) {
        this.error = error;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CreateUser withMessage(String message) {
        this.message = message;
        return this;
    }

    public List<User> getUser() {
        return user;
    }

    public void setUser(List<User> user) {
        this.user = user;
    }

    public CreateUser withUser(List<User> user) {
        this.user = user;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("error", error).append("message", message).append("user", user).toString();
    }

}