package com.Goingbo.Superseller.Model;
import java.io.Serializable;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class SellerDeliveryUnit implements Serializable, Parcelable
{

    @SerializedName("order")
    @Expose
    private String order;
    @SerializedName("delcharge")
    @Expose
    private String delcharge;
    @SerializedName("distance")
    @Expose
    private String distance;
    public final static Parcelable.Creator<SellerDeliveryUnit> CREATOR = new Creator<SellerDeliveryUnit>() {


        @SuppressWarnings({
                "unchecked"
        })
        public SellerDeliveryUnit createFromParcel(Parcel in) {
            return new SellerDeliveryUnit(in);
        }

        public SellerDeliveryUnit[] newArray(int size) {
            return (new SellerDeliveryUnit[size]);
        }

    }
            ;
    private final static long serialVersionUID = -8629000471438825574L;

    protected SellerDeliveryUnit(Parcel in) {
        this.order = ((String) in.readValue((String.class.getClassLoader())));
        this.delcharge = ((String) in.readValue((String.class.getClassLoader())));
        this.distance = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public SellerDeliveryUnit() {
    }

    /**
     *
     * @param distance
     * @param delcharge
     * @param order
     */
    public SellerDeliveryUnit(String order, String delcharge, String distance) {
        super();
        this.order = order;
        this.delcharge = delcharge;
        this.distance = distance;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public SellerDeliveryUnit withOrder(String order) {
        this.order = order;
        return this;
    }

    public String getDelcharge() {
        return delcharge;
    }

    public void setDelcharge(String delcharge) {
        this.delcharge = delcharge;
    }

    public SellerDeliveryUnit withDelcharge(String delcharge) {
        this.delcharge = delcharge;
        return this;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public SellerDeliveryUnit withDistance(String distance) {
        this.distance = distance;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("order", order).append("delcharge", delcharge).append("distance", distance).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(distance).append(delcharge).append(order).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof SellerDeliveryUnit) == false) {
            return false;
        }
        SellerDeliveryUnit rhs = ((SellerDeliveryUnit) other);
        return new EqualsBuilder().append(distance, rhs.distance).append(delcharge, rhs.delcharge).append(order, rhs.order).isEquals();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(order);
        dest.writeValue(delcharge);
        dest.writeValue(distance);
    }

    public int describeContents() {
        return 0;
    }

}