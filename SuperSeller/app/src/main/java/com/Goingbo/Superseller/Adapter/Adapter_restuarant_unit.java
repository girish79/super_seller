package com.Goingbo.Superseller.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.Goingbo.Superseller.Interface.SelectedAdapterInterface;
import com.Goingbo.Superseller.Model.UntItem_Restuarant.MenuDetail;
import com.Goingbo.Superseller.R;

import java.util.List;

public class Adapter_restuarant_unit  extends  RecyclerView.Adapter<Adapter_restuarant_unit.ProductUnitEdit> {
        Context context;
        List<MenuDetail> karraylist;
        SelectedAdapterInterface selectedAdapterInterface;

public Adapter_restuarant_unit(Context context, List<MenuDetail> karraylist, SelectedAdapterInterface selectedAdapterInterface) {
        this.context = context;
        this.karraylist = karraylist;
        this.selectedAdapterInterface = selectedAdapterInterface;
        }

@NonNull
@Override
public ProductUnitEdit onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.row_edit_product_unit,parent,false);
        return new ProductUnitEdit(view);
        }

@SuppressLint("SetTextI18n")
@Override
public void onBindViewHolder(@NonNull ProductUnitEdit holder, int position) {
        holder.edit_inner_textView_price.setText(karraylist.get(position).getUnitPrice());
        holder.edit_inner_textView_offer_price.setText(karraylist.get(position).getUnitOffer());
        holder.edit_inner_textView_unit.setText(karraylist.get(position).getUnitWeight()+""+karraylist.get(position).getUnitType());
        holder.btn_edit_product_unit.setOnClickListener(new View.OnClickListener() {
@Override
public void onClick(View v) {
        selectedAdapterInterface.getAdapter(position,"edit");
        }
        });
        holder.btn_delete_product_unit.setOnClickListener(new View.OnClickListener() {
@Override
public void onClick(View v) {
        selectedAdapterInterface.getAdapter(position,"remove");

        }
        });
        }

@Override
public int getItemCount() {
        return karraylist.size();
        }

public class ProductUnitEdit extends RecyclerView.ViewHolder {
    TextView edit_inner_textView_unit;
    TextView edit_inner_textView_price;
    TextView edit_inner_textView_offer_price;
    Button btn_edit_product_unit;
    Button btn_delete_product_unit;
    public ProductUnitEdit(@NonNull View itemView) {
        super(itemView);
        edit_inner_textView_unit=itemView.findViewById(R.id.edit_inner_textView_unit);
        edit_inner_textView_price=itemView.findViewById(R.id.edit_inner_textView_price);
        edit_inner_textView_offer_price=itemView.findViewById(R.id.edit_inner_textView_offer_price);
        btn_edit_product_unit=itemView.findViewById(R.id.btn_edit_product_unit);
        btn_delete_product_unit=itemView.findViewById(R.id.btn_delete_product_unit);
    }
}
}
