package com.Goingbo.Superseller.Util;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

public class SessionToken {
    SharedPreferences pref;

    // Editor reference for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context _context;

    // Shared preferences mode
    int PRIVATE_MODE = 0;
    // Shared preferences file name
    public static final String PREFER_NAME = "Tokenfist";
    // All Shared Preferences Keys
    public static final String IS_TOKEN = "Tokenid";

    public SessionToken(Context _context) {
        this._context = _context;
        pref = _context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        editor = pref.edit();

    }

    public void SessionToken(String toker) {

        editor.putString(IS_TOKEN, toker);
        editor.commit();

    }

    public HashMap<String, String> getUserDetails() {

        //Use hashmap to store user credentials
        HashMap<String, String> Token = new HashMap<String, String>();
        Token.put(IS_TOKEN, pref.getString(IS_TOKEN, null));


        // return user
        return Token;

    }
}

