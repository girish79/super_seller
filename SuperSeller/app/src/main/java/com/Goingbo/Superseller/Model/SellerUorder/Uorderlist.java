package com.Goingbo.Superseller.Model.SellerUorder;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class Uorderlist implements Serializable, Parcelable
{

    @SerializedName("order_id")
    @Expose
    private String orderId;
    @SerializedName("orderdetails")
    @Expose
    private String orderdetails;
    @SerializedName("OrderStatus")
    @Expose
    private String orderStatus;
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("product_type")
    @Expose
    private String productType;
    @SerializedName("product_category")
    @Expose
    private String productCategory;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("product_image")
    @Expose
    private String productImage;
    @SerializedName("product_des")
    @Expose
    private String productDes;
    @SerializedName("users_id")
    @Expose
    private String usersId;
    @SerializedName("users_name")
    @Expose
    private String usersName;
    @SerializedName("users_email")
    @Expose
    private String usersEmail;
    @SerializedName("users_phone")
    @Expose
    private String usersPhone;
    @SerializedName("admins_id")
    @Expose
    private String adminsId;
    @SerializedName("admins_name")
    @Expose
    private String adminsName;
    @SerializedName("addresses_id")
    @Expose
    private String addressesId;
    @SerializedName("addresses_city")
    @Expose
    private String addressesCity;
    @SerializedName("addresses_state")
    @Expose
    private String addressesState;
    @SerializedName("addresses_zip")
    @Expose
    private String addressesZip;
    @SerializedName("addresses_street")
    @Expose
    private String addressesStreet;
    @SerializedName("addresses_block")
    @Expose
    private String addressesBlock;
    @SerializedName("addresses_landmark")
    @Expose
    private String addressesLandmark;
    @SerializedName("addresses_plot_number")
    @Expose
    private String addressesPlotNumber;
    @SerializedName("addresses_user_id")
    @Expose
    private String addressesUserId;
    public final static Creator<Uorderlist> CREATOR = new Creator<Uorderlist>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Uorderlist createFromParcel(Parcel in) {
            return new Uorderlist(in);
        }

        public Uorderlist[] newArray(int size) {
            return (new Uorderlist[size]);
        }

    }
            ;
    private final static long serialVersionUID = 6359506458628916367L;

    protected Uorderlist(Parcel in) {
        this.orderId = ((String) in.readValue((String.class.getClassLoader())));
        this.orderdetails = ((String) in.readValue((String.class.getClassLoader())));
        this.orderStatus = ((String) in.readValue((String.class.getClassLoader())));
        this.productId = ((String) in.readValue((String.class.getClassLoader())));
        this.productType = ((String) in.readValue((String.class.getClassLoader())));
        this.productCategory = ((String) in.readValue((String.class.getClassLoader())));
        this.productName = ((String) in.readValue((String.class.getClassLoader())));
        this.productImage = ((String) in.readValue((String.class.getClassLoader())));
        this.productDes = ((String) in.readValue((String.class.getClassLoader())));
        this.usersId = ((String) in.readValue((String.class.getClassLoader())));
        this.usersName = ((String) in.readValue((String.class.getClassLoader())));
        this.usersEmail = ((String) in.readValue((String.class.getClassLoader())));
        this.usersPhone = ((String) in.readValue((String.class.getClassLoader())));
        this.adminsId = ((String) in.readValue((String.class.getClassLoader())));
        this.adminsName = ((String) in.readValue((String.class.getClassLoader())));
        this.addressesId = ((String) in.readValue((String.class.getClassLoader())));
        this.addressesCity = ((String) in.readValue((String.class.getClassLoader())));
        this.addressesState = ((String) in.readValue((String.class.getClassLoader())));
        this.addressesZip = ((String) in.readValue((String.class.getClassLoader())));
        this.addressesStreet = ((String) in.readValue((String.class.getClassLoader())));
        this.addressesBlock = ((String) in.readValue((String.class.getClassLoader())));
        this.addressesLandmark = ((String) in.readValue((String.class.getClassLoader())));
        this.addressesPlotNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.addressesUserId = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public Uorderlist() {
    }

    /**
     *
     * @param addressesState
     * @param adminsId
     * @param adminsName
     * @param productId
     * @param orderId
     * @param addressesStreet
     * @param usersPhone
     * @param orderStatus
     * @param addressesZip
     * @param addressesId
     * @param addressesPlotNumber
     * @param productName
     * @param productCategory
     * @param addressesBlock
     * @param addressesUserId
     * @param productImage
     * @param usersName
     * @param usersId
     * @param addressesCity
     * @param addressesLandmark
     * @param orderdetails
     * @param usersEmail
     * @param productType
     * @param productDes
     */
    public Uorderlist(String orderId, String orderdetails, String orderStatus, String productId, String productType, String productCategory, String productName, String productImage, String productDes, String usersId, String usersName, String usersEmail, String usersPhone, String adminsId, String adminsName, String addressesId, String addressesCity, String addressesState, String addressesZip, String addressesStreet, String addressesBlock, String addressesLandmark, String addressesPlotNumber, String addressesUserId) {
        super();
        this.orderId = orderId;
        this.orderdetails = orderdetails;
        this.orderStatus = orderStatus;
        this.productId = productId;
        this.productType = productType;
        this.productCategory = productCategory;
        this.productName = productName;
        this.productImage = productImage;
        this.productDes = productDes;
        this.usersId = usersId;
        this.usersName = usersName;
        this.usersEmail = usersEmail;
        this.usersPhone = usersPhone;
        this.adminsId = adminsId;
        this.adminsName = adminsName;
        this.addressesId = addressesId;
        this.addressesCity = addressesCity;
        this.addressesState = addressesState;
        this.addressesZip = addressesZip;
        this.addressesStreet = addressesStreet;
        this.addressesBlock = addressesBlock;
        this.addressesLandmark = addressesLandmark;
        this.addressesPlotNumber = addressesPlotNumber;
        this.addressesUserId = addressesUserId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Uorderlist withOrderId(String orderId) {
        this.orderId = orderId;
        return this;
    }

    public String getOrderdetails() {
        return orderdetails;
    }

    public void setOrderdetails(String orderdetails) {
        this.orderdetails = orderdetails;
    }

    public Uorderlist withOrderdetails(String orderdetails) {
        this.orderdetails = orderdetails;
        return this;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Uorderlist withOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
        return this;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Uorderlist withProductId(String productId) {
        this.productId = productId;
        return this;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public Uorderlist withProductType(String productType) {
        this.productType = productType;
        return this;
    }

    public String getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }

    public Uorderlist withProductCategory(String productCategory) {
        this.productCategory = productCategory;
        return this;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Uorderlist withProductName(String productName) {
        this.productName = productName;
        return this;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public Uorderlist withProductImage(String productImage) {
        this.productImage = productImage;
        return this;
    }

    public String getProductDes() {
        return productDes;
    }

    public void setProductDes(String productDes) {
        this.productDes = productDes;
    }

    public Uorderlist withProductDes(String productDes) {
        this.productDes = productDes;
        return this;
    }

    public String getUsersId() {
        return usersId;
    }

    public void setUsersId(String usersId) {
        this.usersId = usersId;
    }

    public Uorderlist withUsersId(String usersId) {
        this.usersId = usersId;
        return this;
    }

    public String getUsersName() {
        return usersName;
    }

    public void setUsersName(String usersName) {
        this.usersName = usersName;
    }

    public Uorderlist withUsersName(String usersName) {
        this.usersName = usersName;
        return this;
    }

    public String getUsersEmail() {
        return usersEmail;
    }

    public void setUsersEmail(String usersEmail) {
        this.usersEmail = usersEmail;
    }

    public Uorderlist withUsersEmail(String usersEmail) {
        this.usersEmail = usersEmail;
        return this;
    }

    public String getUsersPhone() {
        return usersPhone;
    }

    public void setUsersPhone(String usersPhone) {
        this.usersPhone = usersPhone;
    }

    public Uorderlist withUsersPhone(String usersPhone) {
        this.usersPhone = usersPhone;
        return this;
    }

    public String getAdminsId() {
        return adminsId;
    }

    public void setAdminsId(String adminsId) {
        this.adminsId = adminsId;
    }

    public Uorderlist withAdminsId(String adminsId) {
        this.adminsId = adminsId;
        return this;
    }

    public String getAdminsName() {
        return adminsName;
    }

    public void setAdminsName(String adminsName) {
        this.adminsName = adminsName;
    }

    public Uorderlist withAdminsName(String adminsName) {
        this.adminsName = adminsName;
        return this;
    }

    public String getAddressesId() {
        return addressesId;
    }

    public void setAddressesId(String addressesId) {
        this.addressesId = addressesId;
    }

    public Uorderlist withAddressesId(String addressesId) {
        this.addressesId = addressesId;
        return this;
    }

    public String getAddressesCity() {
        return addressesCity;
    }

    public void setAddressesCity(String addressesCity) {
        this.addressesCity = addressesCity;
    }

    public Uorderlist withAddressesCity(String addressesCity) {
        this.addressesCity = addressesCity;
        return this;
    }

    public String getAddressesState() {
        return addressesState;
    }

    public void setAddressesState(String addressesState) {
        this.addressesState = addressesState;
    }

    public Uorderlist withAddressesState(String addressesState) {
        this.addressesState = addressesState;
        return this;
    }

    public String getAddressesZip() {
        return addressesZip;
    }

    public void setAddressesZip(String addressesZip) {
        this.addressesZip = addressesZip;
    }

    public Uorderlist withAddressesZip(String addressesZip) {
        this.addressesZip = addressesZip;
        return this;
    }

    public String getAddressesStreet() {
        return addressesStreet;
    }

    public void setAddressesStreet(String addressesStreet) {
        this.addressesStreet = addressesStreet;
    }

    public Uorderlist withAddressesStreet(String addressesStreet) {
        this.addressesStreet = addressesStreet;
        return this;
    }

    public String getAddressesBlock() {
        return addressesBlock;
    }

    public void setAddressesBlock(String addressesBlock) {
        this.addressesBlock = addressesBlock;
    }

    public Uorderlist withAddressesBlock(String addressesBlock) {
        this.addressesBlock = addressesBlock;
        return this;
    }

    public String getAddressesLandmark() {
        return addressesLandmark;
    }

    public void setAddressesLandmark(String addressesLandmark) {
        this.addressesLandmark = addressesLandmark;
    }

    public Uorderlist withAddressesLandmark(String addressesLandmark) {
        this.addressesLandmark = addressesLandmark;
        return this;
    }

    public String getAddressesPlotNumber() {
        return addressesPlotNumber;
    }

    public void setAddressesPlotNumber(String addressesPlotNumber) {
        this.addressesPlotNumber = addressesPlotNumber;
    }

    public Uorderlist withAddressesPlotNumber(String addressesPlotNumber) {
        this.addressesPlotNumber = addressesPlotNumber;
        return this;
    }

    public String getAddressesUserId() {
        return addressesUserId;
    }

    public void setAddressesUserId(String addressesUserId) {
        this.addressesUserId = addressesUserId;
    }

    public Uorderlist withAddressesUserId(String addressesUserId) {
        this.addressesUserId = addressesUserId;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("orderId", orderId).append("orderdetails", orderdetails).append("orderStatus", orderStatus).append("productId", productId).append("productType", productType).append("productCategory", productCategory).append("productName", productName).append("productImage", productImage).append("productDes", productDes).append("usersId", usersId).append("usersName", usersName).append("usersEmail", usersEmail).append("usersPhone", usersPhone).append("adminsId", adminsId).append("adminsName", adminsName).append("addressesId", addressesId).append("addressesCity", addressesCity).append("addressesState", addressesState).append("addressesZip", addressesZip).append("addressesStreet", addressesStreet).append("addressesBlock", addressesBlock).append("addressesLandmark", addressesLandmark).append("addressesPlotNumber", addressesPlotNumber).append("addressesUserId", addressesUserId).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(adminsId).append(orderId).append(addressesStreet).append(usersPhone).append(orderStatus).append(addressesZip).append(addressesPlotNumber).append(productName).append(productCategory).append(addressesUserId).append(productImage).append(addressesCity).append(orderdetails).append(usersEmail).append(productType).append(productDes).append(addressesState).append(adminsName).append(productId).append(addressesId).append(addressesBlock).append(usersName).append(usersId).append(addressesLandmark).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Uorderlist) == false) {
            return false;
        }
        Uorderlist rhs = ((Uorderlist) other);
        return new EqualsBuilder().append(adminsId, rhs.adminsId).append(orderId, rhs.orderId).append(addressesStreet, rhs.addressesStreet).append(usersPhone, rhs.usersPhone).append(orderStatus, rhs.orderStatus).append(addressesZip, rhs.addressesZip).append(addressesPlotNumber, rhs.addressesPlotNumber).append(productName, rhs.productName).append(productCategory, rhs.productCategory).append(addressesUserId, rhs.addressesUserId).append(productImage, rhs.productImage).append(addressesCity, rhs.addressesCity).append(orderdetails, rhs.orderdetails).append(usersEmail, rhs.usersEmail).append(productType, rhs.productType).append(productDes, rhs.productDes).append(addressesState, rhs.addressesState).append(adminsName, rhs.adminsName).append(productId, rhs.productId).append(addressesId, rhs.addressesId).append(addressesBlock, rhs.addressesBlock).append(usersName, rhs.usersName).append(usersId, rhs.usersId).append(addressesLandmark, rhs.addressesLandmark).isEquals();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(orderId);
        dest.writeValue(orderdetails);
        dest.writeValue(orderStatus);
        dest.writeValue(productId);
        dest.writeValue(productType);
        dest.writeValue(productCategory);
        dest.writeValue(productName);
        dest.writeValue(productImage);
        dest.writeValue(productDes);
        dest.writeValue(usersId);
        dest.writeValue(usersName);
        dest.writeValue(usersEmail);
        dest.writeValue(usersPhone);
        dest.writeValue(adminsId);
        dest.writeValue(adminsName);
        dest.writeValue(addressesId);
        dest.writeValue(addressesCity);
        dest.writeValue(addressesState);
        dest.writeValue(addressesZip);
        dest.writeValue(addressesStreet);
        dest.writeValue(addressesBlock);
        dest.writeValue(addressesLandmark);
        dest.writeValue(addressesPlotNumber);
        dest.writeValue(addressesUserId);
    }

    public int describeContents() {
        return 0;
    }

}