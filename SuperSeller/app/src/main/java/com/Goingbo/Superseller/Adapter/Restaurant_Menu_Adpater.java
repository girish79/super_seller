package com.Goingbo.Superseller.Adapter;

import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.Goingbo.Superseller.Interface.SelectedAdapterInterface;
import com.Goingbo.Superseller.Model.Productadmin.Productseller;
import com.Goingbo.Superseller.Model.Restaurant_Item_Menu.MENUSRESTURANT;
import com.Goingbo.Superseller.Model.Restuarant_Menu.MENUITEMRESTURANT;
import com.Goingbo.Superseller.Model.UNITRestaurant_menu.Restuarantmenusunit;
import com.Goingbo.Superseller.Model.UntItem_Restuarant.RestuarantUnitems;
import com.Goingbo.Superseller.Model.seller_unit.UnitSeller;
import com.Goingbo.Superseller.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Restaurant_Menu_Adpater extends RecyclerView.Adapter<Restaurant_Menu_Adpater.ProductView> {
    Context context;
    ArrayList<MENUSRESTURANT> arrayList;
    SelectedAdapterInterface selectedAdapterInterface;
    public Restaurant_Menu_Adpater(Context context, ArrayList<MENUSRESTURANT> arrayList, SelectedAdapterInterface selectedAdapterInterface) {
        this.context = context;
        this.arrayList = arrayList;
        this.selectedAdapterInterface=selectedAdapterInterface;
    }

    @NonNull
    @Override
    public ProductView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_restaurant_menu_item, parent, false);
        return new ProductView(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductView holder, int position) {
        String dataname=arrayList.get(position).getMenuItemName().substring(0,1).toUpperCase()+arrayList.get(position).getMenuItemName().substring(1);

        holder.selected_rest_name.setText(Html.fromHtml(dataname));
        holder.selected_rest_category.setText(Html.fromHtml(arrayList.get(position).getMenuItemCategory()));
        Log.e("catname",""+arrayList.get(position).getMenuItemCategory());
     //   holder.selected_product_description.setText(Html.fromHtml(arrayList.get(position).get()));

        holder.btn_rest_edit_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedAdapterInterface.getAdapter(position,"edit");
            }
        });
        holder.btn_rest_remove_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedAdapterInterface.getAdapter(position,"remove");
            }
        });
        String imagelink="https://www.goingbo.com/public/restorentmenuimage/"+arrayList.get(position).getMenuItemImage();
        Picasso.get().load(imagelink).into(holder.select_rest_image_list);
        holder.unit_rest_recyclerview.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL,false));
        holder.unit_rest_recyclerview.setHasFixedSize(false);
        Gson gson = new Gson();
        TypeToken<ArrayList<Restuarantmenusunit>> token = new TypeToken<ArrayList<Restuarantmenusunit>>() {
        };
        String reader=arrayList.get(position).getRestaurantmenuitemsMenuItems();
        Log.e("printdata",reader);
        try {
            List<RestuarantUnitems> unitlist = Arrays.asList(gson.fromJson(reader,
                    RestuarantUnitems[].class));


            Restaurant_menu_Unit_adpater unit_recyclerview_seller_myproduct = new Restaurant_menu_Unit_adpater(context, unitlist.get(0).getMenuDetail());
            holder.unit_rest_recyclerview.setAdapter(unit_recyclerview_seller_myproduct);
        } catch (Exception e){e.printStackTrace();}

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ProductView extends RecyclerView.ViewHolder {
        public ImageView select_rest_image_list;
        public TextView selected_rest_name;
        public TextView selected_rest_category;
        public Button btn_rest_edit_product,btn_rest_remove_product;

        public RecyclerView unit_rest_recyclerview;

        public ProductView(@NonNull View itemView) {
            super(itemView);
            select_rest_image_list = itemView.findViewById(R.id.select_rest_image_list);
            selected_rest_name = itemView.findViewById(R.id.selected_rest_name);
            selected_rest_category = itemView.findViewById(R.id.selected_rest_category);
            btn_rest_edit_product = itemView.findViewById(R.id.btn_rest_edit_product);
            btn_rest_remove_product = itemView.findViewById(R.id.btn_rest_remove_product);
            unit_rest_recyclerview=itemView.findViewById(R.id.unit_rest_recyclerview);


        }


    }
}
