package com.Goingbo.Superseller.Model.seller_categories;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class Cat implements Serializable, Parcelable
{

    @SerializedName("name")
    @Expose
    private String name;
    public final static Creator<Cat> CREATOR = new Creator<Cat>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Cat createFromParcel(Parcel in) {
            return new Cat(in);
        }

        public Cat[] newArray(int size) {
            return (new Cat[size]);
        }

    }
            ;
    private final static long serialVersionUID = -4070255796017326220L;

    protected Cat(Parcel in) {
        this.name = ((String) in.readValue((String.class.getClassLoader())));
    }

    public Cat() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Cat withName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("name", name).toString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(name);
    }

    public int describeContents() {
        return 0;
    }

}