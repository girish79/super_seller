package com.Goingbo.Superseller.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class DeliveryAdrress implements Serializable, Parcelable
{

    @SerializedName("error")
    @Expose
    private boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("addressid")
    @Expose
    private String addressid;
    public final static Parcelable.Creator<DeliveryAdrress> CREATOR = new Creator<DeliveryAdrress>() {


        @SuppressWarnings({
                "unchecked"
        })
        public DeliveryAdrress createFromParcel(Parcel in) {
            return new DeliveryAdrress(in);
        }

        public DeliveryAdrress[] newArray(int size) {
            return (new DeliveryAdrress[size]);
        }

    }
            ;
    private final static long serialVersionUID = 4233239529327586720L;

    protected DeliveryAdrress(Parcel in) {
        this.error = ((boolean) in.readValue((boolean.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        this.addressid = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public DeliveryAdrress() {
    }

    /**
     *
     * @param error
     * @param message
     * @param addressid
     */
    public DeliveryAdrress(boolean error, String message, String addressid) {
        super();
        this.error = error;
        this.message = message;
        this.addressid = addressid;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public DeliveryAdrress withError(boolean error) {
        this.error = error;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DeliveryAdrress withMessage(String message) {
        this.message = message;
        return this;
    }

    public String getAddressid() {
        return addressid;
    }

    public void setAddressid(String addressid) {
        this.addressid = addressid;
    }

    public DeliveryAdrress withAddressid(String addressid) {
        this.addressid = addressid;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("error", error).append("message", message).append("addressid", addressid).toString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(error);
        dest.writeValue(message);
        dest.writeValue(addressid);
    }

    public int describeContents() {
        return 0;
    }

}