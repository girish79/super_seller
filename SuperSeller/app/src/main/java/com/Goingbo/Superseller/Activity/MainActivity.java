package com.Goingbo.Superseller.Activity;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Goingbo.Superseller.Interface.ApiService;
import com.Goingbo.Superseller.Model.PackLoginAdmin.LoginsellerADmin;
import com.Goingbo.Superseller.Util.CheckConnectivity;
import com.Goingbo.Superseller.Util.RetrofitClient;
import com.Goingbo.Superseller.Util.SessionManager;
import com.Goingbo.Superseller.R;
import com.facebook.CallbackManager;
import com.facebook.login.widget.LoginButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity {
    TextInputLayout tl_seller_signin_email,tl_seller_signin_password;
    TextInputEditText seller_sginup_email,seller_signin_password;
    Button btn_Seller_signin,btn_Seller_signin_signup;
    TextView seller_signin_forgot,seller_terms_signin;
    private SessionManager session;
    private CheckConnectivity checkConnectivity;
    ProgressDialog progressDialog;
    TextView textview_forgot ,otplogin;
    Button btn_signin;
    boolean isshowing=true;
    public String email;
    public String name;
    RelativeLayout btn_google_login, facebook_login;
    public LoginButton login_button;
    public CallbackManager callbackManagerfacebook;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        checkConnectivity=new CheckConnectivity(getApplicationContext());
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        session = new SessionManager(getApplicationContext());
        session.setlocation( 0, 0);
        init();
        progressDialog=new ProgressDialog(MainActivity.this);
        progressDialog.setMessage("Login In Progress....!");
    }
    private void init() {
        tl_seller_signin_email=findViewById(R.id.tl_seller_signin_email);

        tl_seller_signin_password=findViewById(R.id.tl_seller_signin_password);
        seller_sginup_email=findViewById(R.id.seller_sginup_email);
        seller_signin_password=findViewById(R.id.seller_signin_password);
        btn_Seller_signin=findViewById(R.id.btn_Seller_signin);
        btn_Seller_signin_signup=findViewById(R.id.btn_Seller_signin_signup);
        seller_signin_forgot=findViewById(R.id.seller_signin_forgot);
        seller_terms_signin=findViewById(R.id.seller_terms_signin);
        seller_terms_signin.setOnClickListener(v -> {
            String data = "https://www.goingbo.com/privacy-policy";
            Intent pageintent = new Intent(Intent.ACTION_VIEW);
            pageintent.setData(Uri.parse(data));
            startActivity(pageintent);
        });

        btn_signin = findViewById(R.id.btn_signin);
        otplogin = findViewById(R.id.seller_otplogin);

        otplogin.setOnClickListener( v ->{
            Intent otplogin=new Intent(MainActivity.this,Seller_OTP_Login.class);
            otplogin.putExtra("checkingtype","otpcheck");
            startActivity(otplogin);
        } );
        //seller_signin_password.setText("$2y$10$iwIRVjI9I5IjgIgVINEBDuvfI5Wdw5DYFV0wWSuta1UUfo7neZ97e");
        btn_Seller_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                if (!process()) {
                    boolean flag = true;
                    if (Objects.requireNonNull(seller_sginup_email.getText()).toString().length() == 0) {
                        seller_sginup_email.setError("Please Enter Email");
                    } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(seller_sginup_email.getText().toString()).matches()) {
                        seller_sginup_email.setError("Please Enter valid Email");
                    } else if (Objects.requireNonNull(seller_signin_password.getText()).toString().equals("")) {
                        seller_sginup_email.setError(null);
                        tl_seller_signin_email.setError(null);
                        tl_seller_signin_password.setError("Please Enter Password");
                    } else if (seller_signin_password.getText().toString().length() < 6) {
                        seller_sginup_email.setError(null);
                        tl_seller_signin_email.setError(null);
                        tl_seller_signin_password.setError("Password minimum 6 character");
                    } else {

                        seller_sginup_email.setError(null);
                        tl_seller_signin_email.setError(null);
                        tl_seller_signin_password.setError(null);
                        seller_signin_password.setError(null);
                        progressDialog = new ProgressDialog(MainActivity.this);
                        progressDialog.setMessage("Loading......");
                        progressDialog.show();

                        Call<LoginsellerADmin> call = RetrofitClient.getClient("https://www.goingbo.com/").create(ApiService.class).getsellerdata(
                                seller_sginup_email.getText().toString().trim(), seller_signin_password.getText().toString());
                        call.enqueue(new Callback<LoginsellerADmin>() {
                            @Override
                            public void onResponse(@NonNull Call<LoginsellerADmin> call, @NonNull Response<LoginsellerADmin> response) {
                                assert response.body() != null;
                                Log.e("responselogin", "" + response.body().toString());
                                progressDialog.dismiss();
                                if (!response.body().isError()) {
                                    String id = "" + response.body().getSuccess().getUsers().getId();
                                    String name = "" + response.body().getSuccess().getUsers().getName();
                                    String emails = "";
                                    if (response.body().getSuccess().getUsers().getEmail() != null && response.body().getSuccess().getUsers().getEmail().length() == 0) {
                                        emails = "";
                                    } else {
                                        emails = response.body().getSuccess().getUsers().getEmail();
                                    }
                                    String phone = response.body().getSuccess().getUsers().getPhone();
                                    String Type = "Seller";
                                    session.sellermedialogin(id, name, emails, phone, Type);
                                    Intent intent = new Intent(MainActivity.this, Seller_dashboard.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    finish();

                                } else {
                                    new AlertDialog.Builder(Objects.requireNonNull(MainActivity.this))
                                            .setTitle(" Alert")
                                            .setMessage("Failed To Create Login !")
                                            .setCancelable(false)
                                            .setPositiveButton("OK", (dialog, which) -> dialog.dismiss()).show();
                                }
                            }

                            @Override
                            public void onFailure(@NonNull Call<LoginsellerADmin> call, @NonNull Throwable t) {
                                progressDialog.dismiss();
                                AlertDialog dialogs = new AlertDialog.Builder(Objects.requireNonNull(MainActivity.this))
                                        .setTitle("Alert")
                                        .setMessage("Api failed due to some technical problem , please try again !")
                                        .setCancelable(false)
                                        .setPositiveButton("OK", (dialog, which) -> dialog.dismiss()).create();
                                if (dialogs.isShowing()) {
                                    dialogs.show();
                                }
                            }

                        });
                    }
            }
        }});
        seller_signin_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


              Intent forgotpass=new Intent(MainActivity.this,Seller_OTP_Login.class);
              forgotpass.putExtra("checkingtype","forgot");
              startActivity(forgotpass);
            }
        });
        btn_Seller_signin_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,Seller_signup_Activity.class));
            }
        });
    }
    private boolean validateEmailAddress(String emailAddress) {
        String expression = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        CharSequence inputStr = emailAddress;
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        return matcher.matches();
    }
    private boolean isValidMobile(String phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }
    private boolean process() {
        boolean show=true;
        if (checkConnectivity.isConnected()) {
            show=false;

        } else {
            showDialog();
            show=true;
        }
        return show;
    }
    private void showDialog() {

        Dialog dialog = new Dialog(MainActivity.this, R.style.FullScreenDialogWithStatusBarColorAccent);
        if (!dialog.isShowing()) {
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.no_internet);
            Window window = dialog.getWindow();
            assert window != null;
            WindowManager.LayoutParams wlp = Objects.requireNonNull(window).getAttributes();
            wlp.gravity = Gravity.CENTER;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
            window.setAttributes(wlp);
            Objects.requireNonNull(dialog.getWindow()).setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            Button wifi_btn = dialog.findViewById(R.id.wifi_btn);
            Button mobile_data_btn = dialog.findViewById(R.id.mobile_data_btn);
            dialog.show();
            dialog.setCancelable(false);
            wifi_btn.setOnClickListener(v -> {
                dialog.dismiss();
                isshowing = false;
                WifiManager wifiMgr = (WifiManager) this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);

                assert wifiMgr != null;
                if (wifiMgr.isWifiEnabled()) { // Wi-Fi adapter is ON

                    WifiInfo wifiInfo = wifiMgr.getConnectionInfo();

                    if (wifiInfo.getNetworkId() == -1) {

                        showDialog();
                        // Not connected to an access point
                    } else {
                        dialog.dismiss();
                        process();
                    }
                    // Connected to an access point
                } else {
                    startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS)); // Wi-Fi adapter is OFF
                }
            });


            mobile_data_btn.setOnClickListener(v -> {
                dialog.dismiss();
                isshowing = false;
                startActivityForResult(new Intent(android.provider.Settings.ACTION_DATA_ROAMING_SETTINGS), 0);
                if (checkConnectivity.isConnected()) {
                    process();

                } else {
                    showDialog();
                }
            });
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        session = new SessionManager(getApplicationContext());
        session.setlocation( 0, 0);
    }
}