package com.Goingbo.Superseller.Util;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;


import com.Goingbo.Superseller.Activity.MainActivity;

import java.util.HashMap;

public class SessionManager {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
    int PRIVATE_MODE = 0;
    public static final String PREFER_NAME = "SessionSeller";
    public static final String IS_USER_LOGIN = "UserLogged";
    public static final String IS_AUSER_LOGIN = "AgentLogged";
    public static final String IS_update_profile = "ProfileUpdate";
    public static final String IS_social_profile = "SocialProfile";
    public static String imagedata = "";

    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setpasswordsharepreference(String pass) {

        editor.putString(LibConstant.PASSWORD, pass);
        editor.commit();
    }

    public void setImagesharepreference(String imagea) {
        imagedata = imagea;
        editor.putString(LibConstant.IMAGE, imagea);
    }

    public void phoneLogin(String phone) {

        editor.putString(LibConstant.phonelog, phone);
    }
    public void setlocation(double lat,double log){
        editor.putString(LibConstant.lat,""+lat);
        editor.putString(LibConstant.lon, ""+log);
        editor.commit();
    }
    public   HashMap<String, String> getlocation() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(LibConstant.lat, pref.getString(LibConstant.lat, null));
        user.put(LibConstant.lon, pref.getString(LibConstant.lon, null));
        return user;
    }
    public void phonevefication(String verify) {
        editor.putBoolean(IS_social_profile, true);
        editor.putString(LibConstant.phonelog, verify);
    }
    public void setuseraddress(String  lat,String lon, String uaddressid){
        editor.putString(LibConstant.lat,lat);

        editor.putString(LibConstant.lon, lon);
        editor.putString(LibConstant.UAddressid,uaddressid);
        editor.commit();

    }
    public void setrestaurantddress(String  id,String name,String address, String contact,String rimage){
        editor.putString(LibConstant.socialid,id);
        editor.putString(LibConstant.RNAME,name);
        editor.putString(LibConstant.RADDRESS,address);
        editor.putString(LibConstant.Rcontact,contact);
        editor.putString(LibConstant.Rimage,rimage);
        editor.commit();

    }
    public HashMap<String, String> getaddressDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(LibConstant.socialid, pref.getString(LibConstant.socialid, null));
        user.put(LibConstant.lat, pref.getString(LibConstant.lat, null));
        user.put(LibConstant.lon, pref.getString(LibConstant.lon, null));
        user.put(LibConstant.UAddressid, pref.getString(LibConstant.UAddressid, null));
        return user;
    }
    public HashMap<String, String> getrestaurantaddress() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(LibConstant.socialid, pref.getString(LibConstant.socialid, null));
        user.put(LibConstant.RNAME, pref.getString(LibConstant.RNAME, null));
        user.put(LibConstant.RADDRESS, pref.getString(LibConstant.RADDRESS, null));
        user.put(LibConstant.Rcontact, pref.getString(LibConstant.Rcontact, null));
        user.put(LibConstant.Rimage, pref.getString(LibConstant.Rimage, null));
        return user;
    }
    public void storeotp(int n) {
        editor.putString(LibConstant.otpnumber, "" + n);
    }

    public void socialmedialogin(String id, String name, String emailid, String Type) {
        editor.putBoolean(IS_social_profile, true);
        editor.putString(LibConstant.Email, emailid);
        editor.putString(LibConstant.Name, name);
        editor.putString(LibConstant.Type, Type);
        editor.putString(LibConstant.socialid, id);
        editor.putBoolean(IS_AUSER_LOGIN, false);
        editor.putBoolean(IS_USER_LOGIN, false);
        editor.commit();

    }
    public void sellermedialogin(String id, String name, String email,String phone ,String Type) {
        editor.putBoolean(IS_social_profile, false);
        // Storing name in pref
        editor.putString(LibConstant.socialid, id);
        editor.putString(LibConstant.USER_ID, id);
        editor.putString(LibConstant.Name, name);

        editor.putString(LibConstant.Email, email);
        editor.putString(LibConstant.Phone, phone);
        editor.putString(LibConstant.Type, Type);
        editor.putBoolean(IS_AUSER_LOGIN, true);
        editor.putBoolean(IS_USER_LOGIN, false);
        editor.commit();

    }

    public void createLoginSession(String id, String name, String email, String Phone, String Type) {
        // Storing login value as TRUE
        Log.d("halfSeller", "User Seller");
        editor.clear();
        editor.putBoolean(IS_USER_LOGIN, true);
        editor.putBoolean(IS_AUSER_LOGIN, false);
        editor.putBoolean(IS_social_profile, false);

        // Storing name in pref
        editor.putString(LibConstant.socialid, id);
        editor.putString(LibConstant.USER_ID, id);
        editor.putString(LibConstant.Name, name);

        editor.putString(LibConstant.Email, email);
        editor.putString(LibConstant.Phone, Phone);
        editor.putString(LibConstant.Type, Type);

        // commit changes
        editor.commit();
    }

    public HashMap<String, String> getsoicalDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(LibConstant.socialid, pref.getString(LibConstant.socialid, null));
        user.put(LibConstant.Name, pref.getString(LibConstant.Name, null));
        user.put(LibConstant.Email, pref.getString(LibConstant.Email, null));
        user.put(LibConstant.Phone, pref.getString(LibConstant.Phone, null));
        user.put(LibConstant.Type, pref.getString(LibConstant.Type, null));
        return user;
    }

    public HashMap<String, String> getphoneDetails() {

        //Use hashmap to store user credentials
        HashMap<String, String> user = new HashMap<String, String>();

        // user name
        user.put(LibConstant.socialid, pref.getString(LibConstant.socialid, null));
        user.put(LibConstant.phonelog, pref.getString(LibConstant.phonelog, null));

        return user;
    }

    public HashMap<String, String> getotpnumber() {

        //Use hashmap to store user credentials
        HashMap<String, String> user = new HashMap<String, String>();

        user.put(LibConstant.phonelog, pref.getString(LibConstant.otpnumber, null));

        return user;
    }

    public void logoutUser() {

        // Clearing all user data from Shared Preferences
        editor.clear();
        editor.clear();

        editor.commit();

        // After logout redirect user to Login Activity
        Intent i = new Intent(_context, MainActivity.class);

        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Staring Login Activity
        _context.startActivity(i);

    }

    public boolean isUserLoggedIn() {
        return pref.getBoolean(IS_USER_LOGIN, false);
    }

    public boolean isprofileIn() {
        return pref.getBoolean(IS_update_profile, false);
    }

    public boolean isAUserLoggedIn() {
        return pref.getBoolean(IS_AUSER_LOGIN, false);
    }

    public boolean issocialLoggedIn() {
        return pref.getBoolean(IS_social_profile, false);
    }
}

