package com.Goingbo.Superseller.Model.UNITRestaurant_menu;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class MenuDetail implements Serializable
{

    @SerializedName("unit_type")
    @Expose
    private String unitType;
    @SerializedName("unit_weight")
    @Expose
    private String unitWeight;
    @SerializedName("unit_price")
    @Expose
    private String unitPrice;
    @SerializedName("unit_offer")
    @Expose
    private String unitOffer;
    @SerializedName("menu_description")
    @Expose
    private String menuDescription;
    private final static long serialVersionUID = -6852662676724211126L;

    /**
     * No args constructor for use in serialization
     *
     */
    public MenuDetail() {
    }

    /**
     *
     * @param unitType
     * @param unitWeight
     * @param unitPrice
     * @param menuDescription
     * @param unitOffer
     */
    public MenuDetail(String unitType, String unitWeight, String unitPrice, String unitOffer, String menuDescription) {
        super();
        this.unitType = unitType;
        this.unitWeight = unitWeight;
        this.unitPrice = unitPrice;
        this.unitOffer = unitOffer;
        this.menuDescription = menuDescription;
    }

    public String getUnitType() {
        return unitType;
    }

    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    public MenuDetail withUnitType(String unitType) {
        this.unitType = unitType;
        return this;
    }

    public String getUnitWeight() {
        return unitWeight;
    }

    public void setUnitWeight(String unitWeight) {
        this.unitWeight = unitWeight;
    }

    public MenuDetail withUnitWeight(String unitWeight) {
        this.unitWeight = unitWeight;
        return this;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }

    public MenuDetail withUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
        return this;
    }

    public String getUnitOffer() {
        return unitOffer;
    }

    public void setUnitOffer(String unitOffer) {
        this.unitOffer = unitOffer;
    }

    public MenuDetail withUnitOffer(String unitOffer) {
        this.unitOffer = unitOffer;
        return this;
    }

    public String getMenuDescription() {
        return menuDescription;
    }

    public void setMenuDescription(String menuDescription) {
        this.menuDescription = menuDescription;
    }

    public MenuDetail withMenuDescription(String menuDescription) {
        this.menuDescription = menuDescription;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("unitType", unitType).append("unitWeight", unitWeight).append("unitPrice", unitPrice).append("unitOffer", unitOffer).append("menuDescription", menuDescription).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(unitType).append(unitWeight).append(unitPrice).append(unitOffer).append(menuDescription).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof MenuDetail) == false) {
            return false;
        }
        MenuDetail rhs = ((MenuDetail) other);
        return new EqualsBuilder().append(unitType, rhs.unitType).append(unitWeight, rhs.unitWeight).append(unitPrice, rhs.unitPrice).append(unitOffer, rhs.unitOffer).append(menuDescription, rhs.menuDescription).isEquals();
    }

}
