package com.Goingbo.Superseller.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.Goingbo.Superseller.Adapter.ProductaddAdpater;
import com.Goingbo.Superseller.Interface.ApiService;
import com.Goingbo.Superseller.Model.productdetails.Product;
import com.Goingbo.Superseller.Model.productdetails.ProductData;
import com.Goingbo.Superseller.R;
import com.Goingbo.Superseller.Util.CheckConnectivity;
import com.Goingbo.Superseller.Util.RetrofitClient;

import java.util.ArrayList;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Search_productFragment extends Fragment {
    private EditText search_product;
    private Button btn_product_add_seller;
    private RecyclerView search_product_list;
    private View view;
    private ArrayList<Product> arrayList;
    Toolbar toolbar;
    LinearLayout search_dummy_delivery;
    LinearLayoutManager layoutManager ;
    private CheckConnectivity checkConnectivity;
    boolean isshowing=true;

    public Search_productFragment() {
        // Required empty public constructor
    }


    public static Search_productFragment newInstance(String param1, String param2) {
        Search_productFragment fragment = new Search_productFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_search_product, container, false);
        checkConnectivity = new CheckConnectivity(getContext());
        init();
        arrayList=new ArrayList<>();
        toolbar=getActivity().findViewById(R.id.title_seller_dashboard);
        toolbar.setTitle("Search Product");

        search_product.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence newText, int start, int before, int count) {
                if(newText.length()>0){
                    if(!process()) {
                        Call<ProductData> calldata = RetrofitClient.getClient("https://www.goingbo.com/").create(ApiService.class).getProduct(String.valueOf(newText));
                        calldata.enqueue(new Callback<ProductData>() {
                            @Override
                            public void onResponse(Call<ProductData> call, Response<ProductData> response) {
                                assert response.body() != null;
                                if (!response.body().isError()) {

                                    arrayList.clear();
                                    ProductData productData = response.body();
                                    if (productData.getProduct().size() > 0) {
                                        // arrayList.addAll(productData.getProduct());
                                        search_dummy_delivery.setVisibility(View.GONE);
                                        search_product_list.setVisibility(View.VISIBLE);
                                        for (int v = 0; v < productData.getProduct().size(); v++) {
                                            Product products = new Product();
                                            String id= productData.getProduct().get(v).getId();
                                            String name=productData.getProduct().get(v).getProName();
                                            String catid=productData.getProduct().get(v).getProCategory();
                                            String description = productData.getProduct().get(v).getProDes();
                                            String image=productData.getProduct().get(v).getProImage();
                                            String type=productData.getProduct().get(v).getProType();
                                            String cname=productData.getProduct().get(v).getCategroyName();
                                            products.setId(id);
                                            products.setProName(name);
                                            products.setProCategory(catid);
                                            String delStr = "&lt;p&gt;";

                                            description.replace(delStr, "");
                                            description.replace(";br&gt", "");
                                            description.replace(";&lt", "");
                                            description.replace("\\/p&gt;", "");
                                            description.replace("p&gt;", "");
                                            products.setProDes(description);

                                            // products.setProPrice(productData.getProduct().get(v).getProPrice());
                                            products.setProImage(image);
                                            products.setProType(type);
                                            products.setCategroyName(cname);
                                            Log.e("categroyname",cname);
//                                            products.setProQty(productData.getProduct().get(v).getProQty());
                                            arrayList.add(products);

                                        }
                                        ProductaddAdpater productaddAdpater = new ProductaddAdpater(getContext(), arrayList);
                                        search_product_list.setAdapter(productaddAdpater);
                                        // search_product_list.addItemDecoration(new EqualSpacingItemDecoration(16, EqualSpacingItemDecoration.HORIZONTAL));


                                        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(search_product_list.getContext(),
                                                layoutManager.getOrientation());
                                        search_product_list.addItemDecoration(dividerItemDecoration);

                                    } else {
                                        search_dummy_delivery.setVisibility(View.VISIBLE);
                                        search_product_list.setVisibility(View.GONE);
                                    }
                                }

                            }

                            @Override
                            public void onFailure(Call<ProductData> call, Throwable t) {

                            }
                        });
                    }
                }




            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        search_product.requestFocus();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        return view;
    }

    private void init() {
        search_product=view.findViewById(R.id.edit_search);
        search_dummy_delivery=view.findViewById(R.id.search_dummy_delivery);
        btn_product_add_seller=view.findViewById(R.id.btn_product_add_seller);
        search_product_list =view.findViewById(R.id.search_product_list);
        search_product_list.setHasFixedSize(false);
        layoutManager=new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        search_product_list.setLayoutManager(layoutManager);
        btn_product_add_seller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment=new Select_Product();
                Bundle bundle=new Bundle();
                bundle.putString("senddata","add");
                fragment.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.seller_fragment_dashboard,fragment,"search_list").addToBackStack("search_list").commit();

            }
        });

    }
    private boolean process() {
        boolean show = true;
        if (checkConnectivity.isConnected()) {
            show = false;

        } else {

            showDialog();
            show = true;
        }
        return show;
    }

    private void showDialog() {

        Dialog dialog = new Dialog(getContext(), R.style.FullScreenDialogWithStatusBarColorAccent);
        if (!dialog.isShowing()){
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.no_internet);
            Window window = dialog.getWindow();
            assert window != null;
            WindowManager.LayoutParams wlp = Objects.requireNonNull(window).getAttributes();
            wlp.gravity = Gravity.CENTER;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
            window.setAttributes(wlp);
            Objects.requireNonNull(dialog.getWindow()).setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            Button wifi_btn = dialog.findViewById(R.id.wifi_btn);
            Button mobile_data_btn = dialog.findViewById(R.id.mobile_data_btn);
            dialog.show();
            dialog.setCancelable(false);
            wifi_btn.setOnClickListener(v -> {
                dialog.dismiss();
                isshowing = false;
                WifiManager wifiMgr = (WifiManager)getActivity(). getApplicationContext().getSystemService(Context.WIFI_SERVICE);

                assert wifiMgr != null;
                if (wifiMgr.isWifiEnabled()) { // Wi-Fi adapter is ON

                    WifiInfo wifiInfo = wifiMgr.getConnectionInfo();

                    if (wifiInfo.getNetworkId() == -1) {

                        showDialog();
                        // Not connected to an access point
                    } else {
                        dialog.dismiss();
                        process();
                    }
                    // Connected to an access point
                } else {
                    startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS)); // Wi-Fi adapter is OFF
                }
            });
            mobile_data_btn.setOnClickListener(v -> {
                dialog.dismiss();
                isshowing = false;
                startActivityForResult(new Intent(android.provider.Settings.ACTION_DATA_ROAMING_SETTINGS), 0);
                if (checkConnectivity.isConnected()) {
                    process();

                } else {
                    showDialog();
                }
            });
        }

    }
}
