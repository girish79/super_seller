package com.Goingbo.Superseller.Model.Product_type;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class Cat implements Serializable, Parcelable
{

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("category_name")
    @Expose
    private String categoryName;
    @SerializedName("category_image")
    @Expose
    private String categoryImage;
    @SerializedName("slug")
    @Expose
    private String slug;
    public final static Creator<Cat> CREATOR = new Creator<Cat>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Cat createFromParcel(Parcel in) {
            return new Cat(in);
        }

        public Cat[] newArray(int size) {
            return (new Cat[size]);
        }

    }
            ;
    private final static long serialVersionUID = -2153466382271401541L;

    protected Cat(Parcel in) {
        this.id = ((int) in.readValue((int.class.getClassLoader())));
        this.categoryName = ((String) in.readValue((String.class.getClassLoader())));
        this.categoryImage = ((String) in.readValue((String.class.getClassLoader())));
        this.slug = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public Cat() {
    }

    /**
     *
     * @param categoryImage
     * @param id
     * @param categoryName
     * @param slug
     */
    public Cat(int id, String categoryName, String categoryImage, String slug) {
        super();
        this.id = id;
        this.categoryName = categoryName;
        this.categoryImage = categoryImage;
        this.slug = slug;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Cat withId(int id) {
        this.id = id;
        return this;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Cat withCategoryName(String categoryName) {
        this.categoryName = categoryName;
        return this;
    }

    public String getCategoryImage() {
        return categoryImage;
    }

    public void setCategoryImage(String categoryImage) {
        this.categoryImage = categoryImage;
    }

    public Cat withCategoryImage(String categoryImage) {
        this.categoryImage = categoryImage;
        return this;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public Cat withSlug(String slug) {
        this.slug = slug;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("categoryName", categoryName).append("categoryImage", categoryImage).append("slug", slug).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(id).append(categoryImage).append(categoryName).append(slug).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Cat) == false) {
            return false;
        }
        Cat rhs = ((Cat) other);
        return new EqualsBuilder().append(id, rhs.id).append(categoryImage, rhs.categoryImage).append(categoryName, rhs.categoryName).append(slug, rhs.slug).isEquals();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(categoryName);
        dest.writeValue(categoryImage);
        dest.writeValue(slug);
    }

    public int describeContents() {
        return 0;
    }

}