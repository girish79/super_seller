package com.Goingbo.Superseller.Util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class OTPAutoRead extends BroadcastReceiver {

    private static final String LOG_TAG = OTPAutoRead.class.getCanonicalName();
    private static final String mReadPermission = "android.permission.READ_SMS";
    private static final String mReceivePermission = "android.permission.RECEIVE_SMS";
    private List<SMSRule> smsRules;
    private Context context;
    private String OTP;
    public OTPAutoRead() {throw new InstantiationError("Empty Constructor");}
    public OTPAutoRead(@NonNull Context context, final List<SMSRule> smsRules) {
        if (smsRules == null || smsRules.size() == 0) {
            try {
                throw new Exception("No SMS Rules Found");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        this.smsRules = smsRules;
        this.context = context;
        CheckSMSReadPermission();
        IntentFilter itf = new IntentFilter();
        itf.addAction("android.provider.Telephony.SMS_RECEIVED");
        itf.setPriority(999);
        context.registerReceiver(this,itf);
    }
    private void CheckSMSReadPermission() {
        PackageManager packageManager = context.getPackageManager();
        String packageName = context.getPackageName();
        int readPermission = packageManager.checkPermission(mReadPermission,packageName);
        int receivePermission = packageManager.checkPermission(mReceivePermission, packageName);
        boolean canRead = (readPermission == PackageManager.PERMISSION_GRANTED);
        if (!canRead)
            Toast.makeText(context,"Please enable SMS read permission for auto OTP read", Toast.LENGTH_SHORT).show();
    }
    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            if (Objects.equals( intent.getAction() , "android.provider.Telephony.SMS_RECEIVED" ))
                tryReceiveMessage(intent);
        }   catch (Exception e) {
            Log.i(LOG_TAG, "Failed to read SMS", e);
        }
    }
    private void tryReceiveMessage(Intent intent) {
        Bundle bundle = intent.getExtras();
        SmsMessage[] messages = null;
        if (bundle != null) {
            Object[] pdus = (Object[]) bundle.get("pdus");
            if (pdus != null) {
                messages = new SmsMessage[pdus.length];
                for (int i = 0; i < messages.length; i++) {
                    messages[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                    String messageFrom = messages[i].getOriginatingAddress().toUpperCase();
                    String messageBody = messages[i].getMessageBody().toUpperCase();
                    Log.d(LOG_TAG, "Message is from: " + messageFrom + " and its content is: " + messageBody);
                    SMS sms = new SMS();
                    sms.setMessage(messageBody).setAddress(messageFrom);
                    processOTP(sms);
                }
            }
        }
    }


    private void processOTP(SMS sms) {
        int i;
        for (i = 0; i < smsRules.size(); i ++) {
            if (sms.getAddress().toUpperCase().contains(smsRules.get(i).getSender().toUpperCase())) {
                Pattern pattern = smsRules.get(i).getOTPPattern();
                System.out.println(pattern.pattern());
                System.out.println(sms.getMessage());
                Matcher matcher = pattern.matcher(sms.getMessage().toUpperCase());
                if (matcher.find()) {
                    OTP = matcher.group(1);
                    Log.i(LOG_TAG,"Extracted OTP is: " + OTP);
                    Toast.makeText(context,"OTP RECEIVED IS: " + OTP,Toast.LENGTH_SHORT).show();
                    return;
                } else
                    Log.d(LOG_TAG,"Failed to extract OTP");
            }
        }
    }
    public String getOTP() {
        return OTP;
    }
}