package com.Goingbo.Superseller.Model.Restaurant_Item_Menu;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Restuarantmenus implements Serializable
{

    @SerializedName("error")
    @Expose
    private boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("MENUS_RESTURANT")
    @Expose
    private List<MENUSRESTURANT> mENUSRESTURANT = new ArrayList<MENUSRESTURANT>();
    private final static long serialVersionUID = 1829757605909537561L;

    /**
     * No args constructor for use in serialization
     *
     */
    public Restuarantmenus() {
    }

    /**
     *
     * @param error
     * @param message
     * @param mENUSRESTURANT
     */
    public Restuarantmenus(boolean error, String message, List<MENUSRESTURANT> mENUSRESTURANT) {
        super();
        this.error = error;
        this.message = message;
        this.mENUSRESTURANT = mENUSRESTURANT;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public Restuarantmenus withError(boolean error) {
        this.error = error;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Restuarantmenus withMessage(String message) {
        this.message = message;
        return this;
    }

    public List<MENUSRESTURANT> getMENUSRESTURANT() {
        return mENUSRESTURANT;
    }

    public void setMENUSRESTURANT(List<MENUSRESTURANT> mENUSRESTURANT) {
        this.mENUSRESTURANT = mENUSRESTURANT;
    }

    public Restuarantmenus withMENUSRESTURANT(List<MENUSRESTURANT> mENUSRESTURANT) {
        this.mENUSRESTURANT = mENUSRESTURANT;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("error", error).append("message", message).append("mENUSRESTURANT", mENUSRESTURANT).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(error).append(message).append(mENUSRESTURANT).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Restuarantmenus) == false) {
            return false;
        }
        Restuarantmenus rhs = ((Restuarantmenus) other);
        return new EqualsBuilder().append(error, rhs.error).append(message, rhs.message).append(mENUSRESTURANT, rhs.mENUSRESTURANT).isEquals();
    }

}