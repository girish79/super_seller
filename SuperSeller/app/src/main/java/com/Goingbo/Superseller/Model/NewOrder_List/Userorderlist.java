package com.Goingbo.Superseller.Model.NewOrder_List;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class Userorderlist implements Serializable, Parcelable
{

    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("Pro_type")
    @Expose
    private String proType;
    @SerializedName("Pro_category")
    @Expose
    private String proCategory;
    @SerializedName("Pro_name")
    @Expose
    private String proName;
    @SerializedName("Pro_qty")
    @Expose
    private long proQty;
    @SerializedName("Pro_price")
    @Expose
    private long proPrice;
    @SerializedName("Pro_image")
    @Expose
    private String proImage;
    @SerializedName("Pro_des")
    @Expose
    private String proDes;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("adminPhone")
    @Expose
    private String adminPhone;
    @SerializedName("adminEmail")
    @Expose
    private String adminEmail;
    @SerializedName("adminName")
    @Expose
    private String adminName;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("otp")
    @Expose
    private String otp;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("lat")
    @Expose
    private double lat;
    @SerializedName("lng")
    @Expose
    private double lng;
    @SerializedName("status")
    @Expose
    private long status;
    @SerializedName("verifyToken")
    @Expose
    private Object verifyToken;
    @SerializedName("remember_token")
    @Expose
    private String rememberToken;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("zip")
    @Expose
    private long zip;
    @SerializedName("street")
    @Expose
    private String street;
    @SerializedName("block")
    @Expose
    private String block;
    @SerializedName("landmark")
    @Expose
    private String landmark;
    @SerializedName("plot_number")
    @Expose
    private String plotNumber;
    @SerializedName("user_id")
    @Expose
    private long userId;
    @SerializedName("pro_quentity")
    @Expose
    private long proQuentity;
    @SerializedName("product_id")
    @Expose
    private long productId;
    @SerializedName("seller_id")
    @Expose
    private long sellerId;
    @SerializedName("address_id")
    @Expose
    private long addressId;
    @SerializedName("orderid")
    @Expose
    private String orderid;
    @SerializedName("orderdetails")
    @Expose
    private String orderdetails;
    @SerializedName("OrderStatus")
    @Expose
    private String orderStatus;
    @SerializedName("cuser_delivery")
    @Expose
    private String cuserDelivery;
    @SerializedName("cuser_id")
    @Expose
    private long cuserId;
    @SerializedName("cseller_id")
    @Expose
    private long csellerId;
    @SerializedName("coupon_code")
    @Expose
    private String couponCode;
    @SerializedName("order_id")
    @Expose
    private String orderId;
    public final static Creator<Userorderlist> CREATOR = new Creator<Userorderlist>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Userorderlist createFromParcel(Parcel in) {
            return new Userorderlist(in);
        }

        public Userorderlist[] newArray(int size) {
            return (new Userorderlist[size]);
        }

    }
            ;
    private final static long serialVersionUID = 3568435948773356332L;

    protected Userorderlist(Parcel in) {
        this.id = ((long) in.readValue((long.class.getClassLoader())));
        this.proType = ((String) in.readValue((String.class.getClassLoader())));
        this.proCategory = ((String) in.readValue((String.class.getClassLoader())));
        this.proName = ((String) in.readValue((String.class.getClassLoader())));
        this.proQty = ((long) in.readValue((long.class.getClassLoader())));
        this.proPrice = ((long) in.readValue((long.class.getClassLoader())));
        this.proImage = ((String) in.readValue((String.class.getClassLoader())));
        this.proDes = ((String) in.readValue((String.class.getClassLoader())));
        this.createdAt = ((String) in.readValue((String.class.getClassLoader())));
        this.updatedAt = ((String) in.readValue((String.class.getClassLoader())));
        this.adminPhone = ((String) in.readValue((String.class.getClassLoader())));
        this.adminEmail = ((String) in.readValue((String.class.getClassLoader())));
        this.adminName = ((String) in.readValue((String.class.getClassLoader())));
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.email = ((String) in.readValue((String.class.getClassLoader())));
        this.phone = ((String) in.readValue((String.class.getClassLoader())));
        this.otp = ((String) in.readValue((String.class.getClassLoader())));
        this.password = ((String) in.readValue((String.class.getClassLoader())));
        this.lat = ((double) in.readValue((double.class.getClassLoader())));
        this.lng = ((double) in.readValue((double.class.getClassLoader())));
        this.status = ((long) in.readValue((long.class.getClassLoader())));
        this.verifyToken = ((Object) in.readValue((Object.class.getClassLoader())));
        this.rememberToken = ((String) in.readValue((String.class.getClassLoader())));
        this.city = ((String) in.readValue((String.class.getClassLoader())));
        this.state = ((String) in.readValue((String.class.getClassLoader())));
        this.zip = ((long) in.readValue((long.class.getClassLoader())));
        this.street = ((String) in.readValue((String.class.getClassLoader())));
        this.block = ((String) in.readValue((String.class.getClassLoader())));
        this.landmark = ((String) in.readValue((String.class.getClassLoader())));
        this.plotNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.userId = ((long) in.readValue((long.class.getClassLoader())));
        this.proQuentity = ((long) in.readValue((long.class.getClassLoader())));
        this.productId = ((long) in.readValue((long.class.getClassLoader())));
        this.sellerId = ((long) in.readValue((long.class.getClassLoader())));
        this.addressId = ((long) in.readValue((long.class.getClassLoader())));
        this.orderid = ((String) in.readValue((String.class.getClassLoader())));
        this.orderdetails = ((String) in.readValue((String.class.getClassLoader())));
        this.orderStatus = ((String) in.readValue((String.class.getClassLoader())));
        this.cuserDelivery = ((String) in.readValue((String.class.getClassLoader())));
        this.cuserId = ((long) in.readValue((long.class.getClassLoader())));
        this.csellerId = ((long) in.readValue((long.class.getClassLoader())));
        this.couponCode = ((String) in.readValue((String.class.getClassLoader())));
        this.orderId = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public Userorderlist() {
    }

    /**
     *
     * @param proCategory
     * @param proType
     * @param verifyToken
     * @param proImage
     * @param city
     * @param orderId
     * @param orderStatus
     * @param proPrice
     * @param addressId
     * @param proQuentity
     * @param proQty
     * @param createdAt
     * @param password
     * @param sellerId
     * @param street
     * @param orderdetails
     * @param block
     * @param id
     * @param proName
     * @param state
     * @param landmark
     * @param cuserId
     * @param email
     * @param lat
     * @param updatedAt
     * @param adminEmail
     * @param zip
     * @param lng
     * @param productId
     * @param orderid
     * @param otp
     * @param userId
     * @param adminName
     * @param adminPhone
     * @param phone
     * @param name
     * @param cuserDelivery
     * @param plotNumber
     * @param rememberToken
     * @param proDes
     * @param csellerId
     * @param couponCode
     * @param status
     */
    public Userorderlist(long id, String proType, String proCategory, String proName, long proQty, long proPrice, String proImage, String proDes, String createdAt, String updatedAt, String adminPhone, String adminEmail, String adminName, String name, String email, String phone, String otp, String password, double lat, double lng, long status, Object verifyToken, String rememberToken, String city, String state, long zip, String street, String block, String landmark, String plotNumber, long userId, long proQuentity, long productId, long sellerId, long addressId, String orderid, String orderdetails, String orderStatus, String cuserDelivery, long cuserId, long csellerId, String couponCode, String orderId) {
        super();
        this.id = id;
        this.proType = proType;
        this.proCategory = proCategory;
        this.proName = proName;
        this.proQty = proQty;
        this.proPrice = proPrice;
        this.proImage = proImage;
        this.proDes = proDes;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.adminPhone = adminPhone;
        this.adminEmail = adminEmail;
        this.adminName = adminName;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.otp = otp;
        this.password = password;
        this.lat = lat;
        this.lng = lng;
        this.status = status;
        this.verifyToken = verifyToken;
        this.rememberToken = rememberToken;
        this.city = city;
        this.state = state;
        this.zip = zip;
        this.street = street;
        this.block = block;
        this.landmark = landmark;
        this.plotNumber = plotNumber;
        this.userId = userId;
        this.proQuentity = proQuentity;
        this.productId = productId;
        this.sellerId = sellerId;
        this.addressId = addressId;
        this.orderid = orderid;
        this.orderdetails = orderdetails;
        this.orderStatus = orderStatus;
        this.cuserDelivery = cuserDelivery;
        this.cuserId = cuserId;
        this.csellerId = csellerId;
        this.couponCode = couponCode;
        this.orderId = orderId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Userorderlist withId(long id) {
        this.id = id;
        return this;
    }

    public String getProType() {
        return proType;
    }

    public void setProType(String proType) {
        this.proType = proType;
    }

    public Userorderlist withProType(String proType) {
        this.proType = proType;
        return this;
    }

    public String getProCategory() {
        return proCategory;
    }

    public void setProCategory(String proCategory) {
        this.proCategory = proCategory;
    }

    public Userorderlist withProCategory(String proCategory) {
        this.proCategory = proCategory;
        return this;
    }

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }

    public Userorderlist withProName(String proName) {
        this.proName = proName;
        return this;
    }

    public long getProQty() {
        return proQty;
    }

    public void setProQty(long proQty) {
        this.proQty = proQty;
    }

    public Userorderlist withProQty(long proQty) {
        this.proQty = proQty;
        return this;
    }

    public long getProPrice() {
        return proPrice;
    }

    public void setProPrice(long proPrice) {
        this.proPrice = proPrice;
    }

    public Userorderlist withProPrice(long proPrice) {
        this.proPrice = proPrice;
        return this;
    }

    public String getProImage() {
        return proImage;
    }

    public void setProImage(String proImage) {
        this.proImage = proImage;
    }

    public Userorderlist withProImage(String proImage) {
        this.proImage = proImage;
        return this;
    }

    public String getProDes() {
        return proDes;
    }

    public void setProDes(String proDes) {
        this.proDes = proDes;
    }

    public Userorderlist withProDes(String proDes) {
        this.proDes = proDes;
        return this;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Userorderlist withCreatedAt(String createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Userorderlist withUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public String getAdminPhone() {
        return adminPhone;
    }

    public void setAdminPhone(String adminPhone) {
        this.adminPhone = adminPhone;
    }

    public Userorderlist withAdminPhone(String adminPhone) {
        this.adminPhone = adminPhone;
        return this;
    }

    public String getAdminEmail() {
        return adminEmail;
    }

    public void setAdminEmail(String adminEmail) {
        this.adminEmail = adminEmail;
    }

    public Userorderlist withAdminEmail(String adminEmail) {
        this.adminEmail = adminEmail;
        return this;
    }

    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName;
    }

    public Userorderlist withAdminName(String adminName) {
        this.adminName = adminName;
        return this;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Userorderlist withName(String name) {
        this.name = name;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Userorderlist withEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Userorderlist withPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public Userorderlist withOtp(String otp) {
        this.otp = otp;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Userorderlist withPassword(String password) {
        this.password = password;
        return this;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public Userorderlist withLat(double lat) {
        this.lat = lat;
        return this;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public Userorderlist withLng(double lng) {
        this.lng = lng;
        return this;
    }

    public long getStatus() {
        return status;
    }

    public void setStatus(long status) {
        this.status = status;
    }

    public Userorderlist withStatus(long status) {
        this.status = status;
        return this;
    }

    public Object getVerifyToken() {
        return verifyToken;
    }

    public void setVerifyToken(Object verifyToken) {
        this.verifyToken = verifyToken;
    }

    public Userorderlist withVerifyToken(Object verifyToken) {
        this.verifyToken = verifyToken;
        return this;
    }

    public String getRememberToken() {
        return rememberToken;
    }

    public void setRememberToken(String rememberToken) {
        this.rememberToken = rememberToken;
    }

    public Userorderlist withRememberToken(String rememberToken) {
        this.rememberToken = rememberToken;
        return this;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Userorderlist withCity(String city) {
        this.city = city;
        return this;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Userorderlist withState(String state) {
        this.state = state;
        return this;
    }

    public long getZip() {
        return zip;
    }

    public void setZip(long zip) {
        this.zip = zip;
    }

    public Userorderlist withZip(long zip) {
        this.zip = zip;
        return this;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Userorderlist withStreet(String street) {
        this.street = street;
        return this;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public Userorderlist withBlock(String block) {
        this.block = block;
        return this;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public Userorderlist withLandmark(String landmark) {
        this.landmark = landmark;
        return this;
    }

    public String getPlotNumber() {
        return plotNumber;
    }

    public void setPlotNumber(String plotNumber) {
        this.plotNumber = plotNumber;
    }

    public Userorderlist withPlotNumber(String plotNumber) {
        this.plotNumber = plotNumber;
        return this;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public Userorderlist withUserId(long userId) {
        this.userId = userId;
        return this;
    }

    public long getProQuentity() {
        return proQuentity;
    }

    public void setProQuentity(long proQuentity) {
        this.proQuentity = proQuentity;
    }

    public Userorderlist withProQuentity(long proQuentity) {
        this.proQuentity = proQuentity;
        return this;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public Userorderlist withProductId(long productId) {
        this.productId = productId;
        return this;
    }

    public long getSellerId() {
        return sellerId;
    }

    public void setSellerId(long sellerId) {
        this.sellerId = sellerId;
    }

    public Userorderlist withSellerId(long sellerId) {
        this.sellerId = sellerId;
        return this;
    }

    public long getAddressId() {
        return addressId;
    }

    public void setAddressId(long addressId) {
        this.addressId = addressId;
    }

    public Userorderlist withAddressId(long addressId) {
        this.addressId = addressId;
        return this;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public Userorderlist withOrderid(String orderid) {
        this.orderid = orderid;
        return this;
    }

    public String getOrderdetails() {
        return orderdetails;
    }

    public void setOrderdetails(String orderdetails) {
        this.orderdetails = orderdetails;
    }

    public Userorderlist withOrderdetails(String orderdetails) {
        this.orderdetails = orderdetails;
        return this;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Userorderlist withOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
        return this;
    }

    public String getCuserDelivery() {
        return cuserDelivery;
    }

    public void setCuserDelivery(String cuserDelivery) {
        this.cuserDelivery = cuserDelivery;
    }

    public Userorderlist withCuserDelivery(String cuserDelivery) {
        this.cuserDelivery = cuserDelivery;
        return this;
    }

    public long getCuserId() {
        return cuserId;
    }

    public void setCuserId(long cuserId) {
        this.cuserId = cuserId;
    }

    public Userorderlist withCuserId(long cuserId) {
        this.cuserId = cuserId;
        return this;
    }

    public long getCsellerId() {
        return csellerId;
    }

    public void setCsellerId(long csellerId) {
        this.csellerId = csellerId;
    }

    public Userorderlist withCsellerId(long csellerId) {
        this.csellerId = csellerId;
        return this;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public Userorderlist withCouponCode(String couponCode) {
        this.couponCode = couponCode;
        return this;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Userorderlist withOrderId(String orderId) {
        this.orderId = orderId;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("proType", proType).append("proCategory", proCategory).append("proName", proName).append("proQty", proQty).append("proPrice", proPrice).append("proImage", proImage).append("proDes", proDes).append("createdAt", createdAt).append("updatedAt", updatedAt).append("adminPhone", adminPhone).append("adminEmail", adminEmail).append("adminName", adminName).append("name", name).append("email", email).append("phone", phone).append("otp", otp).append("password", password).append("lat", lat).append("lng", lng).append("status", status).append("verifyToken", verifyToken).append("rememberToken", rememberToken).append("city", city).append("state", state).append("zip", zip).append("street", street).append("block", block).append("landmark", landmark).append("plotNumber", plotNumber).append("userId", userId).append("proQuentity", proQuentity).append("productId", productId).append("sellerId", sellerId).append("addressId", addressId).append("orderid", orderid).append("orderdetails", orderdetails).append("orderStatus", orderStatus).append("cuserDelivery", cuserDelivery).append("cuserId", cuserId).append("csellerId", csellerId).append("couponCode", couponCode).append("orderId", orderId).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(proCategory).append(proType).append(verifyToken).append(proImage).append(city).append(orderId).append(orderStatus).append(proPrice).append(addressId).append(proQuentity).append(proQty).append(createdAt).append(password).append(sellerId).append(street).append(orderdetails).append(block).append(id).append(proName).append(state).append(landmark).append(cuserId).append(email).append(lat).append(updatedAt).append(adminEmail).append(zip).append(lng).append(productId).append(orderid).append(otp).append(userId).append(adminName).append(adminPhone).append(phone).append(name).append(cuserDelivery).append(plotNumber).append(rememberToken).append(proDes).append(csellerId).append(couponCode).append(status).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Userorderlist) == false) {
            return false;
        }
        Userorderlist rhs = ((Userorderlist) other);
        return new EqualsBuilder().append(proCategory, rhs.proCategory).append(proType, rhs.proType).append(verifyToken, rhs.verifyToken).append(proImage, rhs.proImage).append(city, rhs.city).append(orderId, rhs.orderId).append(orderStatus, rhs.orderStatus).append(proPrice, rhs.proPrice).append(addressId, rhs.addressId).append(proQuentity, rhs.proQuentity).append(proQty, rhs.proQty).append(createdAt, rhs.createdAt).append(password, rhs.password).append(sellerId, rhs.sellerId).append(street, rhs.street).append(orderdetails, rhs.orderdetails).append(block, rhs.block).append(id, rhs.id).append(proName, rhs.proName).append(state, rhs.state).append(landmark, rhs.landmark).append(cuserId, rhs.cuserId).append(email, rhs.email).append(lat, rhs.lat).append(updatedAt, rhs.updatedAt).append(adminEmail, rhs.adminEmail).append(zip, rhs.zip).append(lng, rhs.lng).append(productId, rhs.productId).append(orderid, rhs.orderid).append(otp, rhs.otp).append(userId, rhs.userId).append(adminName, rhs.adminName).append(adminPhone, rhs.adminPhone).append(phone, rhs.phone).append(name, rhs.name).append(cuserDelivery, rhs.cuserDelivery).append(plotNumber, rhs.plotNumber).append(rememberToken, rhs.rememberToken).append(proDes, rhs.proDes).append(csellerId, rhs.csellerId).append(couponCode, rhs.couponCode).append(status, rhs.status).isEquals();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(proType);
        dest.writeValue(proCategory);
        dest.writeValue(proName);
        dest.writeValue(proQty);
        dest.writeValue(proPrice);
        dest.writeValue(proImage);
        dest.writeValue(proDes);
        dest.writeValue(createdAt);
        dest.writeValue(updatedAt);
        dest.writeValue(adminPhone);
        dest.writeValue(adminEmail);
        dest.writeValue(adminName);
        dest.writeValue(name);
        dest.writeValue(email);
        dest.writeValue(phone);
        dest.writeValue(otp);
        dest.writeValue(password);
        dest.writeValue(lat);
        dest.writeValue(lng);
        dest.writeValue(status);
        dest.writeValue(verifyToken);
        dest.writeValue(rememberToken);
        dest.writeValue(city);
        dest.writeValue(state);
        dest.writeValue(zip);
        dest.writeValue(street);
        dest.writeValue(block);
        dest.writeValue(landmark);
        dest.writeValue(plotNumber);
        dest.writeValue(userId);
        dest.writeValue(proQuentity);
        dest.writeValue(productId);
        dest.writeValue(sellerId);
        dest.writeValue(addressId);
        dest.writeValue(orderid);
        dest.writeValue(orderdetails);
        dest.writeValue(orderStatus);
        dest.writeValue(cuserDelivery);
        dest.writeValue(cuserId);
        dest.writeValue(csellerId);
        dest.writeValue(couponCode);
        dest.writeValue(orderId);
    }

    public int describeContents() {
        return 0;
    }

}