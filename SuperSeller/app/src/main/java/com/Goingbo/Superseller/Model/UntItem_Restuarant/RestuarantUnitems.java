package com.Goingbo.Superseller.Model.UntItem_Restuarant;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class RestuarantUnitems implements Serializable, Parcelable
{

    @SerializedName("menu_item_id")
    @Expose
    private String menuItemId;
    @SerializedName("menu_detail")
    @Expose
    private List<MenuDetail> menuDetail = new ArrayList<MenuDetail>();
    public final static Parcelable.Creator<RestuarantUnitems> CREATOR = new Creator<RestuarantUnitems>() {


        @SuppressWarnings({
                "unchecked"
        })
        public RestuarantUnitems createFromParcel(Parcel in) {
            return new RestuarantUnitems(in);
        }

        public RestuarantUnitems[] newArray(int size) {
            return (new RestuarantUnitems[size]);
        }

    }
            ;
    private final static long serialVersionUID = 4614039529986116498L;

    protected RestuarantUnitems(Parcel in) {
        this.menuItemId = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.menuDetail, (MenuDetail.class.getClassLoader()));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public RestuarantUnitems() {
    }

    /**
     *
     * @param menuItemId
     * @param menuDetail
     */
    public RestuarantUnitems(String menuItemId, List<MenuDetail> menuDetail) {
        super();
        this.menuItemId = menuItemId;
        this.menuDetail = menuDetail;
    }

    public String getMenuItemId() {
        return menuItemId;
    }

    public void setMenuItemId(String menuItemId) {
        this.menuItemId = menuItemId;
    }

    public RestuarantUnitems withMenuItemId(String menuItemId) {
        this.menuItemId = menuItemId;
        return this;
    }

    public List<MenuDetail> getMenuDetail() {
        return menuDetail;
    }

    public void setMenuDetail(List<MenuDetail> menuDetail) {
        this.menuDetail = menuDetail;
    }

    public RestuarantUnitems withMenuDetail(List<MenuDetail> menuDetail) {
        this.menuDetail = menuDetail;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("menuItemId", menuItemId).append("menuDetail", menuDetail).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(menuItemId).append(menuDetail).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof RestuarantUnitems) == false) {
            return false;
        }
        RestuarantUnitems rhs = ((RestuarantUnitems) other);
        return new EqualsBuilder().append(menuItemId, rhs.menuItemId).append(menuDetail, rhs.menuDetail).isEquals();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(menuItemId);
        dest.writeList(menuDetail);
    }

    public int describeContents() {
        return 0;
    }

}