package com.Goingbo.Superseller.Model.NotificationData;
import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class NotificationResponse implements Serializable
{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("notifiable_id")
    @Expose
    private int notifiableId;
    @SerializedName("notifiable_type")
    @Expose
    private String notifiableType;
    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("read_at")
    @Expose
    private Object readAt;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    private final static long serialVersionUID = 5375997381019223628L;

    /**
     * No args constructor for use in serialization
     *
     */
    public NotificationResponse() {
    }

    /**
     *
     * @param createdAt
     * @param data
     * @param notifiableId
     * @param notifiableType
     * @param id
     * @param type
     * @param readAt
     * @param updatedAt
     */
    public NotificationResponse(String id, String type, int notifiableId, String notifiableType, Data data, Object readAt, String createdAt, String updatedAt) {
        super();
        this.id = id;
        this.type = type;
        this.notifiableId = notifiableId;
        this.notifiableType = notifiableType;
        this.data = data;
        this.readAt = readAt;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public NotificationResponse withId(String id) {
        this.id = id;
        return this;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public NotificationResponse withType(String type) {
        this.type = type;
        return this;
    }

    public int getNotifiableId() {
        return notifiableId;
    }

    public void setNotifiableId(int notifiableId) {
        this.notifiableId = notifiableId;
    }

    public NotificationResponse withNotifiableId(int notifiableId) {
        this.notifiableId = notifiableId;
        return this;
    }

    public String getNotifiableType() {
        return notifiableType;
    }

    public void setNotifiableType(String notifiableType) {
        this.notifiableType = notifiableType;
    }

    public NotificationResponse withNotifiableType(String notifiableType) {
        this.notifiableType = notifiableType;
        return this;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public NotificationResponse withData(Data data) {
        this.data = data;
        return this;
    }

    public Object getReadAt() {
        return readAt;
    }

    public void setReadAt(Object readAt) {
        this.readAt = readAt;
    }

    public NotificationResponse withReadAt(Object readAt) {
        this.readAt = readAt;
        return this;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public NotificationResponse withCreatedAt(String createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public NotificationResponse withUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("type", type).append("notifiableId", notifiableId).append("notifiableType", notifiableType).append("data", data).append("readAt", readAt).append("createdAt", createdAt).append("updatedAt", updatedAt).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(createdAt).append(data).append(notifiableId).append(notifiableType).append(id).append(type).append(readAt).append(updatedAt).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof NotificationResponse) == false) {
            return false;
        }
        NotificationResponse rhs = ((NotificationResponse) other);
        return new EqualsBuilder().append(createdAt, rhs.createdAt).append(data, rhs.data).append(notifiableId, rhs.notifiableId).append(notifiableType, rhs.notifiableType).append(id, rhs.id).append(type, rhs.type).append(readAt, rhs.readAt).append(updatedAt, rhs.updatedAt).isEquals();
    }

}