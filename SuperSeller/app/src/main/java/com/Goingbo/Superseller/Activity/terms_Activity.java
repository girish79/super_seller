package com.Goingbo.Superseller.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.Goingbo.Superseller.R;

public class terms_Activity extends AppCompatActivity {
    TextView title_text;
    ImageView image_backpress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_);
        title_text=findViewById(R.id.title_text);
        title_text.setText("Terms & Condition");
        image_backpress=findViewById(R.id.image_backpress);
        image_backpress.setOnClickListener(v -> onBackPressed());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}