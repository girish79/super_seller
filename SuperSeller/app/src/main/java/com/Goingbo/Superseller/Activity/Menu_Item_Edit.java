package com.Goingbo.Superseller.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.Goingbo.Superseller.Adapter.Adapter_restuarant_unit;
import com.Goingbo.Superseller.Adapter.Restaurant_menu_Unit_adpater;
import com.Goingbo.Superseller.Interface.ApiService;
import com.Goingbo.Superseller.Interface.SelectedAdapterInterface;
import com.Goingbo.Superseller.Model.Bookingpre;
import com.Goingbo.Superseller.Model.UNITRestaurant_menu.Restuarantmenusunit;
import com.Goingbo.Superseller.Model.UntItem_Restuarant.MenuDetail;
import com.Goingbo.Superseller.Model.UntItem_Restuarant.RestuarantUnitems;
import com.Goingbo.Superseller.R;
import com.Goingbo.Superseller.Util.CheckConnectivity;
import com.Goingbo.Superseller.Util.LibConstant;
import com.Goingbo.Superseller.Util.RetrofitClient;
import com.Goingbo.Superseller.Util.SessionManager;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Menu_Item_Edit extends AppCompatActivity {
ImageView product_image_menu_uploading;
TextInputEditText add_selected_menu_title;
TextInputEditText add_selected_menu_categories;

Button btn_edit_add_menu_unit;
RecyclerView edit_unit_menu_recyclerView;
Button selected_menu_submit;
CheckConnectivity checkConnectivity;
SessionManager sessionManager;
String menu_id,menu_tittle,menu_category,menu_detail,menu_image,senddata;
HashMap<String,String> hashmapsellerr;
List<RestuarantUnitems> unitlist;
List<MenuDetail> sublist = new ArrayList<>();
ImageView image_backpress;
boolean isshowing=true;
TextView title_text;
String restaurant_id;
ProgressDialog progressDialog;
TextInputEditText add_package_costing;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu__item__edit);
        init();
        senddata=getIntent().getStringExtra("senddata");
        unitlist=new ArrayList<>();
        if(senddata.equals("edit")){
            restaurant_id=getIntent().getStringExtra("restaurantmenuitemsId");
            Log.e("menu_ids",""+restaurant_id);
            //menu_details
            Gson gson = new Gson();
            TypeToken<ArrayList<Restuarantmenusunit>> token = new TypeToken<ArrayList<Restuarantmenusunit>>() {
            };
            String reader=getIntent().getStringExtra("menu_details");

            try {
                unitlist = Arrays.asList(gson.fromJson(reader,
                        RestuarantUnitems[].class));

                if(unitlist.get(0).getMenuDetail().size()>0) {
                    sublist.addAll(unitlist.get(0).getMenuDetail());
                    Adapter_restuarant_unit edit_product_unit_recyclerview = new Adapter_restuarant_unit(Menu_Item_Edit.this, unitlist.get(0).getMenuDetail(), new SelectedAdapterInterface() {
                        @Override
                        public void getAdapter(Integer position, String resultindex) {
                            if (resultindex.equalsIgnoreCase("edit")) {
                                editDetailunit(position, unitlist.get(0).getMenuDetail().get(position).getUnitType(), menu_id);


                            } else if (resultindex.equalsIgnoreCase("remove")) {
                                if (unitlist.get(0).getMenuDetail().size() > 0) {
                                    unitlist.get(0).getMenuDetail().remove(unitlist.get(0).getMenuDetail().get(position));
                                    //  Toast.makeText(getContext(), "" + unitlist.get(0).getProductdetail().size(), Toast.LENGTH_SHORT).show();
                                    edit_unit_menu_recyclerView.removeViewAt(position);

                                }

                            }


                        }
                    });
                    LinearLayoutManager layoutManager = new LinearLayoutManager(Menu_Item_Edit.this, LinearLayoutManager.VERTICAL, false);
                    edit_unit_menu_recyclerView.setLayoutManager(layoutManager);
                    edit_unit_menu_recyclerView.setHasFixedSize(false);
                    edit_unit_menu_recyclerView.setAdapter(edit_product_unit_recyclerview);
                    edit_product_unit_recyclerview.notifyDataSetChanged();
                }


            } catch (Exception e){e.printStackTrace();}

        }
    }
    public  void init(){
try {
    sublist.clear();
    product_image_menu_uploading = findViewById(R.id.product_image_menu_uploading);
    add_selected_menu_title = findViewById(R.id.add_selected_menu_title);
    title_text = findViewById(R.id.title_text);
    title_text.setText("Menu Edit");
    image_backpress = findViewById(R.id.image_backpress);
    image_backpress.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            onBackPressed();
        }
    });
    add_selected_menu_title.setEnabled(false);
    add_selected_menu_categories = findViewById(R.id.add_selected_menu_categories);
    add_selected_menu_categories.setEnabled(false);
    //  add_selected_menu_descr=findViewById(R.id.add_selected_menu_descr);
    btn_edit_add_menu_unit = findViewById(R.id.btn_edit_add_menu_unit);
    edit_unit_menu_recyclerView = findViewById(R.id.edit_unit_menu_recyclerView);
    selected_menu_submit = findViewById(R.id.selected_menu_submit);
    add_package_costing=findViewById(R.id.add_package_costing);
    menu_id = getIntent().getStringExtra("menu_id");
    menu_tittle = getIntent().getStringExtra("menu_tittle");
    add_selected_menu_title.setText(menu_tittle);
    menu_category = getIntent().getStringExtra("menu_category");
    add_selected_menu_categories.setText(menu_category);
    menu_detail = getIntent().getStringExtra("menu_detail");
    //  add_selected_menu_descr.setText(menu_detail);
    String imagelink = "https://www.goingbo.com/public/restorentmenuimage/" + getIntent().getStringExtra("menu_image");
    Picasso.get().load(imagelink).into(product_image_menu_uploading);
    senddata = getIntent().getStringExtra("senddata");
    checkConnectivity = new CheckConnectivity(Menu_Item_Edit.this);
    sessionManager = new SessionManager(getApplicationContext());
    hashmapsellerr = sessionManager.getsoicalDetails();

    btn_edit_add_menu_unit.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (unitlist.size() > 0) {
                if (unitlist.get(0).getMenuDetail() != null) {
                    if (unitlist.get(0).getMenuDetail().size() > 0) {
                        selecteditem(unitlist.get(0).getMenuDetail().get(0).getUnitType());
                    } else {
                        updatelist();
                    }
                } else {
                    updatelist();
                }
            } else {
                updatelist();
            }
        }
    });

    selected_menu_submit.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(add_package_costing.getText().length()==0) {
                add_package_costing.setError("Please insert packaging cost !");
            } else
            if (unitlist != null) {
                if (unitlist.size() == 0) {
                    new androidx.appcompat.app.AlertDialog.Builder(Objects.requireNonNull(Menu_Item_Edit.this))
                            .setTitle("Alert")
                            .setMessage("Please add menu price/unit !")
                            .setCancelable(false)
                            .setPositiveButton("OK", (dialog, which) -> {
                                dialog.dismiss();

                            }).show();
                } else {
                    if (unitlist.get(0).getMenuDetail().size() == 0) {
                        new androidx.appcompat.app.AlertDialog.Builder(Objects.requireNonNull(Menu_Item_Edit.this))
                                .setTitle("Alert")
                                .setMessage("Please add menu price/unit !")
                                .setCancelable(false)
                                .setPositiveButton("OK", (dialog, which) -> {
                                    dialog.dismiss();

                                }).show();
                    } else {
                        JSONObject contactsObj = new JSONObject();
                        Log.v("sendlist", "" + unitlist.toString());

                        try {
                            JSONArray jArray = new JSONArray();

                            for (int l = 0; l < unitlist.get(0).getMenuDetail().size(); l++) {
                                try {
                                    JSONObject arraysObj = new JSONObject();
                                    arraysObj.put("unit_type", unitlist.get(0).getMenuDetail().get(l).getUnitType());
                                    arraysObj.put("unit_weight", unitlist.get(0).getMenuDetail().get(l).getUnitWeight());
                                    arraysObj.put("unit_price", unitlist.get(0).getMenuDetail().get(l).getUnitPrice());
                                    arraysObj.put("unit_offer", unitlist.get(0).getMenuDetail().get(l).getUnitOffer());
                                    arraysObj.put("menu_description", unitlist.get(0).getMenuDetail().get(l).getMenuDescription());
                                    jArray.put(arraysObj);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            Log.v("sendlist", "" + jArray.toString());
                            contactsObj.put("menu_item_id", menu_id);
                            contactsObj.put("menu_detail", jArray);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        JSONObject jsonParams = new JSONObject();
                        String seller_id = hashmapsellerr.get(LibConstant.socialid);
//put something inside the map, could be null
                        try {


                            if (senddata.equals("search")) {
                                if (!process()) {
                                    jsonParams.put("seller_id", seller_id);
                                    jsonParams.put("menu", contactsObj);
                                    jsonParams.put("menu_item_id", menu_id);
                                    RequestBody body =
                                            RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),
                                                    jsonParams.toString());
                                    progressDialog = new ProgressDialog(Menu_Item_Edit.this);
                                    progressDialog.setMessage("Loading......");
                                    progressDialog.show();
                                    Call<Bookingpre> callmenuinsert = RetrofitClient.getClient("https://www.goingbo.com/").create(ApiService.class)
                                            .setmenuItems(body);
                                    callmenuinsert.enqueue(new Callback<Bookingpre>() {
                                        @Override
                                        public void onResponse(Call<Bookingpre> call, Response<Bookingpre> response) {
                                            progressDialog.dismiss();
                                            Bookingpre bookingpre = response.body();
                                            if (!bookingpre.getError()) {
                                                new androidx.appcompat.app.AlertDialog.Builder(Objects.requireNonNull(Menu_Item_Edit.this))
                                                        .setTitle("Alert")
                                                        .setMessage(bookingpre.getMessage())
                                                        .setCancelable(false)
                                                        .setPositiveButton("OK", (dialog, which) -> {
                                                            dialog.dismiss();
                                                            onBackPressed();
                                                        }).show();
                                            } else {
                                                new androidx.appcompat.app.AlertDialog.Builder(Objects.requireNonNull(Menu_Item_Edit.this))
                                                        .setTitle("Alert")
                                                        .setMessage(bookingpre.getMessage())
                                                        .setCancelable(false)
                                                        .setPositiveButton("OK", (dialog, which) -> {
                                                            dialog.dismiss();

                                                        }).show();
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<Bookingpre> call, Throwable t) {
                                            progressDialog.dismiss();
                                            new androidx.appcompat.app.AlertDialog.Builder(Objects.requireNonNull(Menu_Item_Edit.this))
                                                    .setTitle("Alert")
                                                    .setMessage("Api failed due to some technical problem , please try again !")
                                                    .setCancelable(false)
                                                    .setPositiveButton("OK", (dialog, which) -> {
                                                        dialog.dismiss();

                                                    }).show();
                                        }
                                    });
                                }
                            } else if (senddata.equals("edit")) {
                                if (!process()) {
                                    jsonParams.put("seller_id", seller_id);
                                    jsonParams.put("menu_item_id", restaurant_id);
                                    jsonParams.put("menu", contactsObj);
                                    Log.e("printmap", "" + restaurant_id);
                                    RequestBody body =RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),
                                                    jsonParams.toString());
                                    progressDialog = new ProgressDialog(Menu_Item_Edit.this);
                                    progressDialog.setMessage("Loading......");
                                    progressDialog.show();
                                    Call<Bookingpre> callmenuinsert = RetrofitClient.getClient("https://www.goingbo.com/").create(ApiService.class)
                                            .updatemenuItems(body);
                                    callmenuinsert.enqueue(new Callback<Bookingpre>() {
                                        @Override
                                        public void onResponse(Call<Bookingpre> call, Response<Bookingpre> response) {
                                            progressDialog.dismiss();
                                            Bookingpre bookingpre = response.body();
                                            if (!bookingpre.getError()) {
                                                new androidx.appcompat.app.AlertDialog.Builder(Objects.requireNonNull(Menu_Item_Edit.this))
                                                        .setTitle("Alert")
                                                        .setMessage(bookingpre.getMessage())
                                                        .setCancelable(false)
                                                        .setPositiveButton("OK", (dialog, which) -> {
                                                            dialog.dismiss();
                                                            onBackPressed();
                                                        }).show();
                                            } else {
                                                new androidx.appcompat.app.AlertDialog.Builder(Objects.requireNonNull(Menu_Item_Edit.this))
                                                        .setTitle("Alert")
                                                        .setMessage(bookingpre.getMessage())
                                                        .setCancelable(false)
                                                        .setPositiveButton("OK", (dialog, which) -> {
                                                            dialog.dismiss();

                                                        }).show();
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<Bookingpre> call, Throwable t) {
                                            progressDialog.dismiss();
                                            new androidx.appcompat.app.AlertDialog.Builder(Objects.requireNonNull(Menu_Item_Edit.this))
                                                    .setTitle("Alert")
                                                    .setMessage("Api failed due to some technical problem , please try again !")
                                                    .setCancelable(false)
                                                    .setPositiveButton("OK", (dialog, which) -> {
                                                        dialog.dismiss();

                                                    }).show();
                                        }
                                    });
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else {
                new androidx.appcompat.app.AlertDialog.Builder(Objects.requireNonNull(Menu_Item_Edit.this))
                        .setTitle("Alert")
                        .setMessage("Please add menu price/unit !")
                        .setCancelable(false)
                        .setPositiveButton("OK", (dialog, which) -> {
                            dialog.dismiss();

                        }).show();
            }
        }
    });
}catch (Exception e){
    e.printStackTrace();
}
    }
    private boolean process() {
        boolean show = true;
        if (checkConnectivity.isConnected()) {
            show = false;

        } else {

            showDialog();
            show = true;
        }
        return show;
    }
    private void showDialog() {

        Dialog dialog = new Dialog(Menu_Item_Edit.this, R.style.FullScreenDialogWithStatusBarColorAccent);
        if (!dialog.isShowing()){
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.no_internet);
            Window window = dialog.getWindow();
            assert window != null;
            WindowManager.LayoutParams wlp = Objects.requireNonNull(window).getAttributes();
            wlp.gravity = Gravity.CENTER;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
            window.setAttributes(wlp);
            Objects.requireNonNull(dialog.getWindow()).setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            Button wifi_btn = dialog.findViewById(R.id.wifi_btn);
            Button mobile_data_btn = dialog.findViewById(R.id.mobile_data_btn);
            dialog.show();
            dialog.setCancelable(false);
            wifi_btn.setOnClickListener(v -> {
                dialog.dismiss();
                isshowing = false;
                WifiManager wifiMgr = (WifiManager)getApplicationContext().getSystemService(Context.WIFI_SERVICE);

                assert wifiMgr != null;
                if (wifiMgr.isWifiEnabled()) { // Wi-Fi adapter is ON

                    WifiInfo wifiInfo = wifiMgr.getConnectionInfo();

                    if (wifiInfo.getNetworkId() == -1) {

                        showDialog();
                        // Not connected to an access point
                    } else {
                        dialog.dismiss();
                        process();
                    }
                    // Connected to an access point
                } else {
                    startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS)); // Wi-Fi adapter is OFF
                }
            });
            mobile_data_btn.setOnClickListener(v -> {
                dialog.dismiss();
                isshowing = false;
                startActivityForResult(new Intent(android.provider.Settings.ACTION_DATA_ROAMING_SETTINGS), 0);
                if (checkConnectivity.isConnected()) {
                    process();

                } else {
                    showDialog();
                }
            });
        }

    }
    private void updatelist() {
        Dialog dialog = new Dialog(Menu_Item_Edit.this, R.style.FullScreenDialogWithStatusBarColorAccent);
        if (!dialog.isShowing()){
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_edit_product_unit);
            Window window = dialog.getWindow();
            assert window != null;
            WindowManager.LayoutParams wlp = Objects.requireNonNull(window).getAttributes();
            wlp.gravity = Gravity.CENTER;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
            window.setAttributes(wlp);
            Objects.requireNonNull(dialog.getWindow()).setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            Spinner seller_spinner_unit_array=dialog.findViewById(R.id.seller_spinner_unit_array);
            ImageView product_unit_close=dialog.findViewById(R.id.product_unit_close);
            product_unit_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(Menu_Item_Edit.this,
                    R.array.unit_array_menu, android.R.layout.simple_spinner_item);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            seller_spinner_unit_array.setAdapter(adapter);


//            if (compareValue != null) {
//                int spinnerPosition = adapter.getPosition(compareValue);
//                Log.e("dataspinner",""+spinnerPosition);
//                seller_spinner_unit_array.setSelection(spinnerPosition);
           // }
            TextInputEditText seller_weight_unit=dialog.findViewById(R.id.seller_weight_unit);
           // seller_weight_unit.setVisibility(View.GONE);
            TextInputEditText seller_product_price=dialog.findViewById(R.id.seller_product_price);
          //  seller_product_price.setText(unitlist.get(0).getUnit().get(position).getUnitPrice());
            TextInputLayout textInputLayout_weight=dialog.findViewById(R.id.textInputLayout_weight);
            TextView label_weight=dialog.findViewById(R.id.label_weight);
            TextInputEditText seller_product_offer_price=dialog.findViewById(R.id.seller_product_offer_price);

            TextInputEditText add_selected_menu_descr=dialog.findViewById(R.id.add_selected_menu_descr);
            add_selected_menu_descr.setVisibility(View.VISIBLE);
                    TextView label_description=dialog.findViewById(R.id.label_description);
            label_description.setVisibility(View.VISIBLE);
           // seller_product_offer_price.setText(unitlist.get(0).getUnit().get(position).getUnitOffer());
            Button seller_save_button=dialog.findViewById(R.id.seller_save_button);
            seller_save_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(seller_spinner_unit_array.getSelectedItemPosition()==0){
                        AlertDialog alertdialog = new AlertDialog.Builder(Menu_Item_Edit.this)
                                .setTitle("Alert")
                                .setMessage("Please Select Unit Type  !")
                                .setCancelable(false)
                                .setPositiveButton("OK", (dialog, which) -> {
                                    dialog.dismiss();

                                }).create();
                        if (!alertdialog.isShowing()) {
                            alertdialog.show();
                        }
                    }else if(seller_product_price.getText().toString().length()==0){
                        seller_product_price.setError("Please Enter Price !");
                    }else if (seller_product_offer_price.getText().toString().length()==0){
                        seller_product_offer_price.setError("Please Enter Offer Price !");
                    }else if (Double.parseDouble(seller_product_price.getText().toString())<Double.parseDouble(seller_product_offer_price.getText().toString())) {
                        seller_product_offer_price.setError(" Offer Price could not be greater than product price  !");
                    }
                    else {
                        if(seller_weight_unit.getVisibility()==View.GONE) {
                            MenuDetail productdetails = new MenuDetail();
                            productdetails.setUnitType(seller_spinner_unit_array.getSelectedItem().toString());
                            productdetails.setUnitWeight(seller_weight_unit.getText().toString());
                            productdetails.setUnitPrice(seller_product_price.getText().toString());
                            productdetails.setUnitOffer(seller_product_offer_price.getText().toString());
                            productdetails.setMenuDescription(add_selected_menu_descr.getText().toString());



                            // unitlist.get(0).getProductdetail().remove(unitlist.get(0).getProductdetail().get(position));


                                sublist.add(productdetails);
                            if(unitlist.size()>0){

                                unitlist.set(0,new RestuarantUnitems(menu_id, sublist));
                            }else {

                                unitlist.add(new RestuarantUnitems(menu_id, sublist));
                            }

                            Log.e("arraylistdatap", "" + unitlist.toString());
                            if (unitlist.size() > 0) {

                                Adapter_restuarant_unit edit_product_unit_recyclerview = new Adapter_restuarant_unit(Menu_Item_Edit.this, unitlist.get(0).getMenuDetail(), new SelectedAdapterInterface() {
                                    @Override
                                    public void getAdapter(Integer position, String resultindex) {
                                        if (resultindex.equalsIgnoreCase("edit")) {
                                            editDetailunit(position, unitlist.get(0).getMenuDetail().get(position).getUnitType(), menu_id);

                                            //

                                        } else if (resultindex.equalsIgnoreCase("remove")) {
                                            if (unitlist.get(0).getMenuDetail().size() > 0) {
                                                sublist.remove(position);
                                                unitlist.get(0).getMenuDetail().remove(unitlist.get(0).getMenuDetail().get(position));

                                                //  Toast.makeText(getContext(), "" + unitlist.get(0).getProductdetail().size(), Toast.LENGTH_SHORT).show();
                                                edit_unit_menu_recyclerView.removeViewAt(position);

                                            }

                                        }


                                    }
                                });
                                LinearLayoutManager layoutManager = new LinearLayoutManager(Menu_Item_Edit.this, LinearLayoutManager.VERTICAL, false);
                                edit_unit_menu_recyclerView.setLayoutManager(layoutManager);
                                edit_unit_menu_recyclerView.setHasFixedSize(false);
                                edit_unit_menu_recyclerView.setAdapter(edit_product_unit_recyclerview);
                                edit_product_unit_recyclerview.notifyDataSetChanged();

                            }
                        }else{
                            if(seller_weight_unit.getText().toString().length()==0){
                                seller_weight_unit.setError("Please Enter Weight !");
                            }else{
                                MenuDetail productdetails = new MenuDetail();
                                productdetails.setUnitType(seller_spinner_unit_array.getSelectedItem().toString());
                                productdetails.setUnitWeight(seller_weight_unit.getText().toString());
                                productdetails.setUnitPrice(seller_product_price.getText().toString());
                                productdetails.setUnitOffer(seller_product_offer_price.getText().toString());
                                productdetails.setMenuDescription(add_selected_menu_descr.getText().toString());

                                sublist.add(productdetails);
                                if(unitlist.size()>0){

                                    unitlist.set(0,new RestuarantUnitems(menu_id, sublist));
                                }else {

                                    unitlist.add(new RestuarantUnitems(menu_id, sublist));
                                }

                                Log.e("arraylistdatap", "" + unitlist.toString());
                                if (unitlist.size() > 0) {

                                    Adapter_restuarant_unit edit_product_unit_recyclerview = new Adapter_restuarant_unit(Menu_Item_Edit.this, unitlist.get(0).getMenuDetail(), new SelectedAdapterInterface() {
                                        @Override
                                        public void getAdapter(Integer position, String resultindex) {
                                            if (resultindex.equalsIgnoreCase("edit")) {
                                                editDetailunit(position, unitlist.get(0).getMenuDetail().get(position).getUnitType(), menu_id);

                                                //

                                            } else if (resultindex.equalsIgnoreCase("remove")) {
                                                if (unitlist.get(0).getMenuDetail().size() > 0) {
                                                    sublist.remove(position);
                                                    unitlist.get(0).getMenuDetail().remove(unitlist.get(0).getMenuDetail().get(position));

                                                    //  Toast.makeText(getContext(), "" + unitlist.get(0).getProductdetail().size(), Toast.LENGTH_SHORT).show();
                                                    edit_unit_menu_recyclerView.removeViewAt(position);

                                                }

                                            }


                                        }
                                    });
                                    LinearLayoutManager layoutManager = new LinearLayoutManager(Menu_Item_Edit.this, LinearLayoutManager.VERTICAL, false);
                                    edit_unit_menu_recyclerView.setLayoutManager(layoutManager);
                                    edit_unit_menu_recyclerView.setHasFixedSize(false);
                                    edit_unit_menu_recyclerView.setAdapter(edit_product_unit_recyclerview);
                                    edit_product_unit_recyclerview.notifyDataSetChanged();

                                }
                            }
                        }
                        // setUnit(unitlist.get(0).getProductid());
                        dialog.dismiss();

                    }
                }
            });
            if(seller_spinner_unit_array!=null){
                seller_spinner_unit_array.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        if(position==0){
                            textInputLayout_weight.setVisibility(View.GONE);
                            label_weight.setVisibility(View.GONE);
                            seller_weight_unit.setVisibility(View.GONE);
                            seller_weight_unit.setText("");
                        }
                        else if(position==1){
                            textInputLayout_weight.setVisibility(View.GONE);
                            label_weight.setVisibility(View.GONE);
                            seller_weight_unit.setVisibility(View.GONE);
                            seller_weight_unit.setText("");
                        }
                        else if(position==2){
                            textInputLayout_weight.setVisibility(View.GONE);
                            label_weight.setVisibility(View.GONE);
                            seller_weight_unit.setVisibility(View.GONE);
                            seller_weight_unit.setText("");
                        }
                        else if(position==3){
                            textInputLayout_weight.setVisibility(View.GONE);
                            label_weight.setVisibility(View.GONE);
                            seller_weight_unit.setVisibility(View.GONE);
                            seller_weight_unit.setText("");
                        }
                        else{
                            textInputLayout_weight.setVisibility(View.VISIBLE);
                            label_weight.setVisibility(View.VISIBLE);
                            seller_weight_unit.setVisibility(View.VISIBLE);
                            seller_weight_unit.setText("");
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }


                });
            }



            dialog.show();
            dialog.setCancelable(false);


        }
    }
    private void selecteditem(String unitytype) {
        Dialog dialog = new Dialog(Menu_Item_Edit.this, R.style.FullScreenDialogWithStatusBarColorAccent);
        if (!dialog.isShowing()){
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_edit_product_unit);
            Window window = dialog.getWindow();
            assert window != null;
            WindowManager.LayoutParams wlp = Objects.requireNonNull(window).getAttributes();
            wlp.gravity = Gravity.CENTER;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
            window.setAttributes(wlp);
            Objects.requireNonNull(dialog.getWindow()).setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            Spinner seller_spinner_unit_array=dialog.findViewById(R.id.seller_spinner_unit_array);
            ImageView product_unit_close=dialog.findViewById(R.id.product_unit_close);
            product_unit_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(Menu_Item_Edit.this,
                    R.array.unit_array_menu, android.R.layout.simple_spinner_item);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            seller_spinner_unit_array.setAdapter(adapter);


            if (unitytype != null) {
                int spinnerPosition = adapter.getPosition(unitytype);
                Log.e("dataspinner",""+spinnerPosition);
                seller_spinner_unit_array.setSelection(spinnerPosition);
                seller_spinner_unit_array.setSelection(spinnerPosition,false);
            }
            TextInputEditText seller_weight_unit=dialog.findViewById(R.id.seller_weight_unit);
            // seller_weight_unit.setVisibility(View.GONE);
            TextInputEditText seller_product_price=dialog.findViewById(R.id.seller_product_price);
            //  seller_product_price.setText(unitlist.get(0).getUnit().get(position).getUnitPrice());
            TextInputLayout textInputLayout_weight=dialog.findViewById(R.id.textInputLayout_weight);
            TextView label_weight=dialog.findViewById(R.id.label_weight);
            TextInputEditText seller_product_offer_price=dialog.findViewById(R.id.seller_product_offer_price);
            // seller_product_offer_price.setText(unitlist.get(0).getUnit().get(position).getUnitOffer());
            Button seller_save_button=dialog.findViewById(R.id.seller_save_button);
            TextInputEditText add_selected_menu_descr=dialog.findViewById(R.id.add_selected_menu_descr);
            add_selected_menu_descr.setVisibility(View.VISIBLE);
            TextView label_description=dialog.findViewById(R.id.label_description);
            label_description.setVisibility(View.VISIBLE);
            seller_save_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(seller_spinner_unit_array.getSelectedItemPosition()==0){
                        AlertDialog alertdialog = new AlertDialog.Builder(Menu_Item_Edit.this)
                                .setTitle("Alert")
                                .setMessage("Please Select Unit Type  !")
                                .setCancelable(false)
                                .setPositiveButton("OK", (dialog, which) -> {
                                    dialog.dismiss();

                                }).create();
                        if (!alertdialog.isShowing()) {
                            alertdialog.show();
                        }
                    }else if(seller_product_price.getText().toString().length()==0){
                        seller_product_price.setError("Please Enter Price !");
                    }else if (seller_product_offer_price.getText().toString().length()==0){
                        seller_product_offer_price.setError("Please Enter Offer Price !");
                    }else if (Double.parseDouble(seller_product_price.getText().toString())<Double.parseDouble(seller_product_offer_price.getText().toString())) {
                        seller_product_offer_price.setError(" Offer Price could not be greater than product price  !");
                    }
                    else {
                        if(seller_weight_unit.getVisibility()==View.GONE) {
                            MenuDetail productdetails = new MenuDetail();
                            productdetails.setUnitType(seller_spinner_unit_array.getSelectedItem().toString());
                            productdetails.setUnitWeight(seller_weight_unit.getText().toString());
                            productdetails.setUnitPrice(seller_product_price.getText().toString());
                            productdetails.setUnitOffer(seller_product_offer_price.getText().toString());
                            productdetails.setMenuDescription(add_selected_menu_descr.getText().toString());

                            int size = unitlist.get(0).getMenuDetail().size();
                            Toast.makeText(Menu_Item_Edit.this, "sublist......"+size, Toast.LENGTH_SHORT).show();
                            // unitlist.get(0).getProductdetail().remove(unitlist.get(0).getProductdetail().get(position));
                            sublist.add(productdetails);
                            Log.e("arraylistdatap", "" + unitlist.toString());
                            Log.e("arraylist", "" + sublist.toString());
                            if(unitlist.size()>0){

                                unitlist.set(0,new RestuarantUnitems(menu_id, sublist));
                            }else {

                                unitlist.add(new RestuarantUnitems(menu_id, sublist));
                            }



                            if (unitlist.size() > 0) {

                                Adapter_restuarant_unit edit_product_unit_recyclerview = new Adapter_restuarant_unit(Menu_Item_Edit.this, unitlist.get(0).getMenuDetail(), new SelectedAdapterInterface() {
                                    @Override
                                    public void getAdapter(Integer position, String resultindex) {
                                        if (resultindex.equalsIgnoreCase("edit")) {
                                            editDetailunit(position, unitlist.get(0).getMenuDetail().get(position).getUnitType(), menu_id);


                                        } else if (resultindex.equalsIgnoreCase("remove")) {
                                            if (unitlist.get(0).getMenuDetail().size() > 0) {
                                                sublist.remove(position);
                                                unitlist.get(0).getMenuDetail().remove(unitlist.get(0).getMenuDetail().get(position));

                                                //  Toast.makeText(getContext(), "" + unitlist.get(0).getProductdetail().size(), Toast.LENGTH_SHORT).show();
                                                edit_unit_menu_recyclerView.removeViewAt(position);

                                            }

                                        }


                                    }
                                });
                                LinearLayoutManager layoutManager = new LinearLayoutManager(Menu_Item_Edit.this, LinearLayoutManager.VERTICAL, false);
                                edit_unit_menu_recyclerView.setLayoutManager(layoutManager);
                                edit_unit_menu_recyclerView.setHasFixedSize(false);
                                edit_unit_menu_recyclerView.setAdapter(edit_product_unit_recyclerview);
                                edit_product_unit_recyclerview.notifyDataSetChanged();

                            }
                        }else{
                            if(seller_weight_unit.getText().toString().length()==0){
                                seller_weight_unit.setError("Please Enter Weight !");
                            }else{
                                MenuDetail productdetails = new MenuDetail();
                                productdetails.setUnitType(seller_spinner_unit_array.getSelectedItem().toString());
                                productdetails.setUnitWeight(seller_weight_unit.getText().toString());
                                productdetails.setUnitPrice(seller_product_price.getText().toString());
                                productdetails.setUnitOffer(seller_product_offer_price.getText().toString());
                                productdetails.setMenuDescription(add_selected_menu_descr.getText().toString());

                                int size = unitlist.get(0).getMenuDetail().size();

                                // unitlist.get(0).getProductdetail().remove(unitlist.get(0).getProductdetail().get(position));
                                sublist.add(productdetails);
                                if(unitlist.size()>0){

                                    unitlist.set(0,new RestuarantUnitems(menu_id, sublist));
                                }else {

                                    unitlist.add(new RestuarantUnitems(menu_id, sublist));
                                }

                                Log.e("arraylistdatap", "" + unitlist.toString());
                                if (unitlist.size() > 0) {

                                    Adapter_restuarant_unit edit_product_unit_recyclerview = new Adapter_restuarant_unit(Menu_Item_Edit.this, unitlist.get(0).getMenuDetail(), new SelectedAdapterInterface() {
                                        @Override
                                        public void getAdapter(Integer position, String resultindex) {
                                            if (resultindex.equalsIgnoreCase("edit")) {
                                                editDetailunit(position, unitlist.get(0).getMenuDetail().get(position).getUnitType(), menu_id);

                                                //

                                            } else if (resultindex.equalsIgnoreCase("remove")) {
                                                if (unitlist.get(0).getMenuDetail().size() > 0) {
                                                    sublist.remove(position);
                                                    unitlist.get(0).getMenuDetail().remove(unitlist.get(0).getMenuDetail().get(position));

                                                    //  Toast.makeText(getContext(), "" + unitlist.get(0).getProductdetail().size(), Toast.LENGTH_SHORT).show();
                                                    edit_unit_menu_recyclerView.removeViewAt(position);

                                                }

                                            }


                                        }
                                    });
                                    LinearLayoutManager layoutManager = new LinearLayoutManager(Menu_Item_Edit.this, LinearLayoutManager.VERTICAL, false);
                                    edit_unit_menu_recyclerView.setLayoutManager(layoutManager);
                                    edit_unit_menu_recyclerView.setHasFixedSize(false);
                                    edit_unit_menu_recyclerView.setAdapter(edit_product_unit_recyclerview);
                                    edit_product_unit_recyclerview.notifyDataSetChanged();

                                }
                            }
                        }
                        // setUnit(unitlist.get(0).getProductid());
                        dialog.dismiss();

                    }
                }
            });
            if(seller_spinner_unit_array!=null){
                seller_spinner_unit_array.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        if(position==0){
                            textInputLayout_weight.setVisibility(View.GONE);
                            label_weight.setVisibility(View.GONE);
                            seller_weight_unit.setVisibility(View.GONE);
                            seller_weight_unit.setText("");
                        }
                        else if(position==1){
                            textInputLayout_weight.setVisibility(View.GONE);
                            label_weight.setVisibility(View.GONE);
                            seller_weight_unit.setVisibility(View.GONE);
                            seller_weight_unit.setText("");
                        }
                        else if(position==2){
                            textInputLayout_weight.setVisibility(View.GONE);
                            label_weight.setVisibility(View.GONE);
                            seller_weight_unit.setVisibility(View.GONE);
                            seller_weight_unit.setText("");
                        }
                        else if(position==3){
                            textInputLayout_weight.setVisibility(View.GONE);
                            label_weight.setVisibility(View.GONE);
                            seller_weight_unit.setVisibility(View.GONE);
                            seller_weight_unit.setText("");
                        }
                        else{
                            textInputLayout_weight.setVisibility(View.VISIBLE);
                            label_weight.setVisibility(View.VISIBLE);
                            seller_weight_unit.setVisibility(View.VISIBLE);
                            seller_weight_unit.setText("");
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }


                });
            }



            dialog.show();
            dialog.setCancelable(false);


        }
    }
    private void editDetailunit(Integer position,String compareValue,String productid) {
        Dialog dialog = new Dialog(Menu_Item_Edit.this, R.style.FullScreenDialogWithStatusBarColorAccent);
        if (!dialog.isShowing()){
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_edit_product_unit);
            Window window = dialog.getWindow();
            assert window != null;
            WindowManager.LayoutParams wlp = Objects.requireNonNull(window).getAttributes();
            wlp.gravity = Gravity.CENTER;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
            window.setAttributes(wlp);
            Objects.requireNonNull(dialog.getWindow()).setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            Spinner seller_spinner_unit_array=dialog.findViewById(R.id.seller_spinner_unit_array);
            ImageView product_unit_close=dialog.findViewById(R.id.product_unit_close);
            product_unit_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(Menu_Item_Edit.this,
                    R.array.unit_array_menu, android.R.layout.simple_spinner_item);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            seller_spinner_unit_array.setAdapter(adapter);

            if (compareValue != null) {
                int spinnerPosition = adapter.getPosition(compareValue);
                Log.e("dataspinner",""+spinnerPosition);
                seller_spinner_unit_array.setSelection(spinnerPosition);
            }
            TextInputEditText seller_weight_unit=dialog.findViewById(R.id.seller_weight_unit);
            seller_weight_unit.setText(unitlist.get(0).getMenuDetail().get(position).getUnitWeight());
            TextInputEditText seller_product_price=dialog.findViewById(R.id.seller_product_price);
            seller_product_price.setText(unitlist.get(0).getMenuDetail().get(position).getUnitPrice());

            TextInputEditText seller_product_offer_price=dialog.findViewById(R.id.seller_product_offer_price);
            seller_product_offer_price.setText(unitlist.get(0).getMenuDetail().get(position).getUnitOffer());
            Button seller_save_button=dialog.findViewById(R.id.seller_save_button);
            TextInputEditText add_selected_menu_descr=dialog.findViewById(R.id.add_selected_menu_descr);
            add_selected_menu_descr.setText(unitlist.get(0).getMenuDetail().get(position).getMenuDescription());
            add_selected_menu_descr.setVisibility(View.VISIBLE);
            TextView label_description=dialog.findViewById(R.id.label_description);
            label_description.setVisibility(View.VISIBLE);
            seller_save_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(seller_spinner_unit_array.getSelectedItemPosition()==0){
                        AlertDialog alertdialog = new AlertDialog.Builder(Menu_Item_Edit.this)
                                .setTitle("Alert")
                                .setMessage("Please Select Unit Type  !")
                                .setCancelable(false)
                                .setPositiveButton("OK", (dialog, which) -> {
                                    dialog.dismiss();

                                }).create();
                        if (!alertdialog.isShowing()) {
                            alertdialog.show();
                        }
                    }else if(seller_product_price.getText().toString().length()==0){
                        seller_product_price.setError("Please Enter Price !");
                    }else if (seller_product_offer_price.getText().toString().length()==0){
                        seller_product_offer_price.setError("Please Enter Offer Price !");
                    }else if (Double.parseDouble(seller_product_price.getText().toString())<Double.parseDouble(seller_product_offer_price.getText().toString())) {
                        seller_product_offer_price.setError(" Offer Price could not be greater than product price  !");
                    }
                    else {
                        if(seller_weight_unit.getVisibility()==View.GONE) {
                            MenuDetail productdetails = new MenuDetail();
                            productdetails.setUnitType(seller_spinner_unit_array.getSelectedItem().toString());
                            productdetails.setUnitWeight(seller_weight_unit.getText().toString());
                            productdetails.setUnitPrice(seller_product_price.getText().toString());
                            productdetails.setUnitOffer(seller_product_offer_price.getText().toString());
                            productdetails.setMenuDescription(add_selected_menu_descr.getText().toString());
                            int size = unitlist.get(0).getMenuDetail().size();

                            // unitlist.get(0).getProductdetail().remove(unitlist.get(0).getProductdetail().get(position));

                            if(unitlist.size()>0){
                               // unitlist.get(0).getMenuDetail().remove(unitlist.get(0).getMenuDetail().get(position));
                                sublist.set(position,productdetails);
                                unitlist.get(0).getMenuDetail().set(position,productdetails);
                            }else {

                                unitlist.add(new RestuarantUnitems(menu_id, sublist));
                            }
                            Log.e("arraylistdatap", "" + unitlist.toString());
                            if (unitlist.size() > 0) {

                                Adapter_restuarant_unit edit_product_unit_recyclerview = new Adapter_restuarant_unit(Menu_Item_Edit.this, unitlist.get(0).getMenuDetail(), new SelectedAdapterInterface() {
                                    @Override
                                    public void getAdapter(Integer position, String resultindex) {
                                        if (resultindex.equalsIgnoreCase("edit")) {
                                            editDetailunit(position, unitlist.get(0).getMenuDetail().get(position).getUnitType(), menu_id);

                                            //

                                        } else if (resultindex.equalsIgnoreCase("remove")) {
                                            if (unitlist.get(0).getMenuDetail().size() > 0) {
                                                sublist.remove(position);
                                                unitlist.get(0).getMenuDetail().remove(unitlist.get(0).getMenuDetail().get(position));

                                                //  Toast.makeText(getContext(), "" + unitlist.get(0).getProductdetail().size(), Toast.LENGTH_SHORT).show();
                                                edit_unit_menu_recyclerView.removeViewAt(position);

                                            }

                                        }


                                    }
                                });
                                LinearLayoutManager layoutManager = new LinearLayoutManager(Menu_Item_Edit.this, LinearLayoutManager.VERTICAL, false);
                                edit_unit_menu_recyclerView.setLayoutManager(layoutManager);
                                edit_unit_menu_recyclerView.setHasFixedSize(false);
                                edit_unit_menu_recyclerView.setAdapter(edit_product_unit_recyclerview);
                                edit_product_unit_recyclerview.notifyDataSetChanged();

                            }
                        }else{
                            if(seller_weight_unit.getText().toString().length()==0){
                                seller_weight_unit.setError("Please Enter Weight !");
                            }else{
                                MenuDetail productdetails = new MenuDetail();
                                productdetails.setUnitType(seller_spinner_unit_array.getSelectedItem().toString());
                                productdetails.setUnitWeight(seller_weight_unit.getText().toString());
                                productdetails.setUnitPrice(seller_product_price.getText().toString());
                                productdetails.setUnitOffer(seller_product_offer_price.getText().toString());
                                productdetails.setMenuDescription(add_selected_menu_descr.getText().toString());

                                int size = unitlist.get(0).getMenuDetail().size();

                                // unitlist.get(0).getProductdetail().remove(unitlist.get(0).getProductdetail().get(position));

                                if(unitlist.size()>0){

                                    unitlist.set(0,new RestuarantUnitems(menu_id, sublist));
                                }else {

                                    unitlist.add(new RestuarantUnitems(menu_id, sublist));
                                }
                                Log.e("arraylistdatap", "" + unitlist.toString());
                                if (unitlist.size() > 0) {

                                    Adapter_restuarant_unit edit_product_unit_recyclerview = new Adapter_restuarant_unit(Menu_Item_Edit.this, unitlist.get(0).getMenuDetail(), new SelectedAdapterInterface() {
                                        @Override
                                        public void getAdapter(Integer position, String resultindex) {
                                            if (resultindex.equalsIgnoreCase("edit")) {
                                                editDetailunit(position, unitlist.get(0).getMenuDetail().get(position).getUnitType(), menu_id);

                                                //

                                            } else if (resultindex.equalsIgnoreCase("remove")) {
                                                if (unitlist.get(0).getMenuDetail().size() > 0) {
                                                    sublist.remove(position);
                                                    unitlist.get(0).getMenuDetail().remove(unitlist.get(0).getMenuDetail().get(position));

                                                    //  Toast.makeText(getContext(), "" + unitlist.get(0).getProductdetail().size(), Toast.LENGTH_SHORT).show();
                                                    edit_unit_menu_recyclerView.removeViewAt(position);
                                                }

                                            }


                                        }
                                    });
                                    LinearLayoutManager layoutManager = new LinearLayoutManager(Menu_Item_Edit.this, LinearLayoutManager.VERTICAL, false);
                                    edit_unit_menu_recyclerView.setLayoutManager(layoutManager);
                                    edit_unit_menu_recyclerView.setHasFixedSize(false);
                                    edit_unit_menu_recyclerView.setAdapter(edit_product_unit_recyclerview);
                                    edit_product_unit_recyclerview.notifyDataSetChanged();

                                }
                            }
                        }
                        // setUnit(unitlist.get(0).getProductid());
                        dialog.dismiss();

                    }
                }
            });
            TextInputLayout textInputLayout_weight=dialog.findViewById(R.id.textInputLayout_weight);
            TextView label_weight=dialog.findViewById(R.id.label_weight);
            if(seller_spinner_unit_array!=null){
                seller_spinner_unit_array.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        if(position==0){
                            textInputLayout_weight.setVisibility(View.GONE);
                            label_weight.setVisibility(View.GONE);
                            seller_weight_unit.setVisibility(View.GONE);
                            seller_weight_unit.setText("");
                        }
                        else if(position==1){
                            textInputLayout_weight.setVisibility(View.GONE);
                            label_weight.setVisibility(View.GONE);
                            seller_weight_unit.setVisibility(View.GONE);
                            seller_weight_unit.setText("");
                        }
                        else if(position==2){
                            textInputLayout_weight.setVisibility(View.GONE);
                            label_weight.setVisibility(View.GONE);
                            seller_weight_unit.setVisibility(View.GONE);
                            seller_weight_unit.setText("");
                        }
                        else if(position==3){
                            textInputLayout_weight.setVisibility(View.GONE);
                            label_weight.setVisibility(View.GONE);
                            seller_weight_unit.setVisibility(View.GONE);
                            seller_weight_unit.setText("");
                        }
                        else{
                            textInputLayout_weight.setVisibility(View.VISIBLE);
                            label_weight.setVisibility(View.VISIBLE);
                            seller_weight_unit.setVisibility(View.VISIBLE);

                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }


                });
            }
            dialog.show();
            dialog.setCancelable(false);


        }
    }
}