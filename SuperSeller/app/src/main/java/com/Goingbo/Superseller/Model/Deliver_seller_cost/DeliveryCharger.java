package com.Goingbo.Superseller.Model.Deliver_seller_cost;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DeliveryCharger implements Serializable, Parcelable
{

    @SerializedName("error")
    @Expose
    private boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("deliverycost")
    @Expose
    private List<Deliverycost> deliverycost = new ArrayList<Deliverycost>();
    public final static Creator<DeliveryCharger> CREATOR = new Creator<DeliveryCharger>() {


        @SuppressWarnings({
                "unchecked"
        })
        public DeliveryCharger createFromParcel(Parcel in) {
            return new DeliveryCharger(in);
        }

        public DeliveryCharger[] newArray(int size) {
            return (new DeliveryCharger[size]);
        }

    }
            ;
    private final static long serialVersionUID = 8754223990466620005L;

    protected DeliveryCharger(Parcel in) {
        this.error = ((boolean) in.readValue((boolean.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.deliverycost, (Deliverycost.class.getClassLoader()));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public DeliveryCharger() {
    }

    /**
     *
     * @param error
     * @param message
     * @param deliverycost
     */
    public DeliveryCharger(boolean error, String message, List<Deliverycost> deliverycost) {
        super();
        this.error = error;
        this.message = message;
        this.deliverycost = deliverycost;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public DeliveryCharger withError(boolean error) {
        this.error = error;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DeliveryCharger withMessage(String message) {
        this.message = message;
        return this;
    }

    public List<Deliverycost> getDeliverycost() {
        return deliverycost;
    }

    public void setDeliverycost(List<Deliverycost> deliverycost) {
        this.deliverycost = deliverycost;
    }

    public DeliveryCharger withDeliverycost(List<Deliverycost> deliverycost) {
        this.deliverycost = deliverycost;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("error", error).append("message", message).append("deliverycost", deliverycost).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(error).append(message).append(deliverycost).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof DeliveryCharger) == false) {
            return false;
        }
        DeliveryCharger rhs = ((DeliveryCharger) other);
        return new EqualsBuilder().append(error, rhs.error).append(message, rhs.message).append(deliverycost, rhs.deliverycost).isEquals();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(error);
        dest.writeValue(message);
        dest.writeList(deliverycost);
    }

    public int describeContents() {
        return 0;
    }

}