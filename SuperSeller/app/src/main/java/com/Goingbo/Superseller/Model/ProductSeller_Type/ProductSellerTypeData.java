package com.Goingbo.Superseller.Model.ProductSeller_Type;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ProductSellerTypeData implements Serializable, Parcelable
{

    @SerializedName("error")
    @Expose
    private boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("Typecat")
    @Expose
    private List<Typecat> typecat = new ArrayList<Typecat>();
    public final static Creator<ProductSellerTypeData> CREATOR = new Creator<ProductSellerTypeData>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ProductSellerTypeData createFromParcel(Parcel in) {
            return new ProductSellerTypeData(in);
        }

        public ProductSellerTypeData[] newArray(int size) {
            return (new ProductSellerTypeData[size]);
        }

    }
            ;
    private final static long serialVersionUID = 4302773072559725552L;

    protected ProductSellerTypeData(Parcel in) {
        this.error = ((boolean) in.readValue((boolean.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.typecat, (Typecat.class.getClassLoader()));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public ProductSellerTypeData() {
    }

    /**
     *
     * @param typecat
     * @param error
     * @param message
     */
    public ProductSellerTypeData(boolean error, String message, List<Typecat> typecat) {
        super();
        this.error = error;
        this.message = message;
        this.typecat = typecat;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public ProductSellerTypeData withError(boolean error) {
        this.error = error;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ProductSellerTypeData withMessage(String message) {
        this.message = message;
        return this;
    }

    public List<Typecat> getTypecat() {
        return typecat;
    }

    public void setTypecat(List<Typecat> typecat) {
        this.typecat = typecat;
    }

    public ProductSellerTypeData withTypecat(List<Typecat> typecat) {
        this.typecat = typecat;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("error", error).append("message", message).append("typecat", typecat).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(typecat).append(error).append(message).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ProductSellerTypeData) == false) {
            return false;
        }
        ProductSellerTypeData rhs = ((ProductSellerTypeData) other);
        return new EqualsBuilder().append(typecat, rhs.typecat).append(error, rhs.error).append(message, rhs.message).isEquals();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(error);
        dest.writeValue(message);
        dest.writeList(typecat);
    }

    public int describeContents() {
        return 0;
    }

}