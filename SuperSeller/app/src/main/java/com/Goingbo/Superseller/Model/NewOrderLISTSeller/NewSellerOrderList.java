package com.Goingbo.Superseller.Model.NewOrderLISTSeller;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class NewSellerOrderList implements Serializable, Parcelable
{

    @SerializedName("error")
    @Expose
    private boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("Sellerorderlist")
    @Expose
    private List<Sellerorderlist> sellerorderlist = new ArrayList<Sellerorderlist>();
    public final static Parcelable.Creator<NewSellerOrderList> CREATOR = new Creator<NewSellerOrderList>() {


        @SuppressWarnings({
                "unchecked"
        })
        public NewSellerOrderList createFromParcel(Parcel in) {
            return new NewSellerOrderList(in);
        }

        public NewSellerOrderList[] newArray(int size) {
            return (new NewSellerOrderList[size]);
        }

    }
            ;
    private final static long serialVersionUID = 7409134771510263488L;

    protected NewSellerOrderList(Parcel in) {
        this.error = ((boolean) in.readValue((boolean.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.sellerorderlist, (Sellerorderlist.class.getClassLoader()));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public NewSellerOrderList() {
    }

    /**
     *
     * @param sellerorderlist
     * @param error
     * @param message
     */
    public NewSellerOrderList(boolean error, String message, List<Sellerorderlist> sellerorderlist) {
        super();
        this.error = error;
        this.message = message;
        this.sellerorderlist = sellerorderlist;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public NewSellerOrderList withError(boolean error) {
        this.error = error;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public NewSellerOrderList withMessage(String message) {
        this.message = message;
        return this;
    }

    public List<Sellerorderlist> getSellerorderlist() {
        return sellerorderlist;
    }

    public void setSellerorderlist(List<Sellerorderlist> sellerorderlist) {
        this.sellerorderlist = sellerorderlist;
    }

    public NewSellerOrderList withSellerorderlist(List<Sellerorderlist> sellerorderlist) {
        this.sellerorderlist = sellerorderlist;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("error", error).append("message", message).append("sellerorderlist", sellerorderlist).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(sellerorderlist).append(error).append(message).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof NewSellerOrderList) == false) {
            return false;
        }
        NewSellerOrderList rhs = ((NewSellerOrderList) other);
        return new EqualsBuilder().append(sellerorderlist, rhs.sellerorderlist).append(error, rhs.error).append(message, rhs.message).isEquals();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(error);
        dest.writeValue(message);
        dest.writeList(sellerorderlist);
    }

    public int describeContents() {
        return 0;
    }

}