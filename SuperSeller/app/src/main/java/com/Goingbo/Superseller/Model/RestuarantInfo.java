package com.Goingbo.Superseller.Model;
import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class RestuarantInfo implements Serializable
{

    @SerializedName("error")
    @Expose
    private boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("imageinfo")
    @Expose
    private String imageinfo;
    private final static long serialVersionUID = -8840381906328451396L;

    /**
     * No args constructor for use in serialization
     *
     */
    public RestuarantInfo() {
    }

    /**
     *
     * @param imageinfo
     * @param error
     * @param message
     */
    public RestuarantInfo(boolean error, String message, String imageinfo) {
        super();
        this.error = error;
        this.message = message;
        this.imageinfo = imageinfo;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public RestuarantInfo withError(boolean error) {
        this.error = error;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public RestuarantInfo withMessage(String message) {
        this.message = message;
        return this;
    }

    public String getImageinfo() {
        return imageinfo;
    }

    public void setImageinfo(String imageinfo) {
        this.imageinfo = imageinfo;
    }

    public RestuarantInfo withImageinfo(String imageinfo) {
        this.imageinfo = imageinfo;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("error", error).append("message", message).append("imageinfo", imageinfo).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(imageinfo).append(error).append(message).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof RestuarantInfo) == false) {
            return false;
        }
        RestuarantInfo rhs = ((RestuarantInfo) other);
        return new EqualsBuilder().append(imageinfo, rhs.imageinfo).append(error, rhs.error).append(message, rhs.message).isEquals();
    }

}