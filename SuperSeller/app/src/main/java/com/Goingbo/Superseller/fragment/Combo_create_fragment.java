package com.Goingbo.Superseller.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.Goingbo.Superseller.R;

public class Combo_create_fragment extends Fragment {

    public Combo_create_fragment() {
        // Required empty public constructor
    }


    public static Combo_create_fragment newInstance(String param1, String param2) {
        Combo_create_fragment fragment = new Combo_create_fragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_combo_create_fragment, container, false);
    }
}