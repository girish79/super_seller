package com.Goingbo.Superseller.Model.RestaurantMenus;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class RestuarantMenus implements Serializable, Parcelable
{

    @SerializedName("error")
    @Expose
    private boolean error;
    @SerializedName("restaurantitesm")
    @Expose
    private List<Restaurantitesm> restaurantitesm = new ArrayList<Restaurantitesm>();
    @SerializedName("message")
    @Expose
    private String message;
    public final static Parcelable.Creator<RestuarantMenus> CREATOR = new Creator<RestuarantMenus>() {


        @SuppressWarnings({"unchecked"})
        public RestuarantMenus createFromParcel(Parcel in) {
            return new RestuarantMenus(in);
        }

        public RestuarantMenus[] newArray(int size) {
            return (new RestuarantMenus[size]);
        }

    };
    private final static long serialVersionUID = -1780662349240062319L;

    protected RestuarantMenus(Parcel in) {
        this.error = ((boolean) in.readValue((boolean.class.getClassLoader())));
        in.readList(this.restaurantitesm, (Restaurantitesm.class.getClassLoader()));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public RestuarantMenus() {
    }

    /**
     *
     * @param restaurantitesm
     * @param error
     * @param message
     */
    public RestuarantMenus(boolean error, List<Restaurantitesm> restaurantitesm, String message) {
        super();
        this.error = error;
        this.restaurantitesm = restaurantitesm;
        this.message = message;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public RestuarantMenus withError(boolean error) {
        this.error = error;
        return this;
    }

    public List<Restaurantitesm> getRestaurantitesm() {
        return restaurantitesm;
    }

    public void setRestaurantitesm(List<Restaurantitesm> restaurantitesm) {
        this.restaurantitesm = restaurantitesm;
    }

    public RestuarantMenus withRestaurantitesm(List<Restaurantitesm> restaurantitesm) {
        this.restaurantitesm = restaurantitesm;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public RestuarantMenus withMessage(String message) {
        this.message = message;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("error", error).append("restaurantitesm", restaurantitesm).append("message", message).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(error).append(message).append(restaurantitesm).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof RestuarantMenus) == false) {
            return false;
        }
        RestuarantMenus rhs = ((RestuarantMenus) other);
        return new EqualsBuilder().append(error, rhs.error).append(message, rhs.message).append(restaurantitesm, rhs.restaurantitesm).isEquals();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(error);
        dest.writeList(restaurantitesm);
        dest.writeValue(message);
    }

    public int describeContents() {
        return 0;
    }

}