package com.Goingbo.Superseller.Model.ProductSeller_Type;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class Typecat implements Serializable, Parcelable
{

    @SerializedName("Pro_type")
    @Expose
    private String proType;
    public final static Creator<Typecat> CREATOR = new Creator<Typecat>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Typecat createFromParcel(Parcel in) {
            return new Typecat(in);
        }

        public Typecat[] newArray(int size) {
            return (new Typecat[size]);
        }

    }
            ;
    private final static long serialVersionUID = -8670872670050366675L;

    protected Typecat(Parcel in) {
        this.proType = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public Typecat() {
    }

    /**
     *
     * @param proType
     */
    public Typecat(String proType) {
        super();
        this.proType = proType;
    }

    public String getProType() {
        return proType;
    }

    public void setProType(String proType) {
        this.proType = proType;
    }

    public Typecat withProType(String proType) {
        this.proType = proType;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("proType", proType).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(proType).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Typecat) == false) {
            return false;
        }
        Typecat rhs = ((Typecat) other);
        return new EqualsBuilder().append(proType, rhs.proType).isEquals();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(proType);
    }

    public int describeContents() {
        return 0;
    }

}