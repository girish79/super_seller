package com.Goingbo.Superseller.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;

import android.location.LocationManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.Goingbo.Superseller.Interface.ApiService;
import com.Goingbo.Superseller.Model.Bookingpre;
import com.Goingbo.Superseller.Model.CreateUser;
import com.Goingbo.Superseller.Model.PackLoginSeller.LoginsellerProduct;
import com.Goingbo.Superseller.R;
import com.Goingbo.Superseller.Util.CheckConnectivity;
import com.Goingbo.Superseller.Util.GPSTracker;
import com.Goingbo.Superseller.Util.LibConstant;
import com.Goingbo.Superseller.Util.LocationTrack;

import com.Goingbo.Superseller.Util.RetrofitClient;
import com.Goingbo.Superseller.Util.SessionManager;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.easywaylocation.EasyWayLocation;
import com.example.easywaylocation.Listener;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.JsonObject;
import com.hbb20.CountryCodePicker;
import com.nabinbhandari.android.permissions.PermissionHandler;
import com.nabinbhandari.android.permissions.Permissions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static com.example.easywaylocation.EasyWayLocation.LOCATION_SETTING_REQUEST_CODE;

public class Seller_signup_Activity extends AppCompatActivity implements Listener {
    TextView showpass;
    EditText seller_password, seller_login_email, seller_login_otp, seller_num_otp_page;
    CountryCodePicker seller_std_code;
    ImageView image_backpress;
    TextView title_text, seller_sendotp, tv_otp_seller, seller_terms_signup, tv_login_seller;
    EditText seller_login_name;
    LocationManager locationManager;
    LocationListener locationListener;
    Button seller_signup_btn;
    GPSTracker gps;
    private String otp = "";
    private SessionManager session;
    private CheckConnectivity checkConnectivity;
    ProgressDialog progressDialog;
    boolean isshowing = true;
    Runnable r;
    private static final int REQUEST_CODE_PERMISSION = 2;
    String mPermission = ACCESS_FINE_LOCATION;
    boolean internal = false;
    Handler handler;
    RequestQueue mRequestQueue;
    int count = 0;
    boolean actount = false;
    boolean  checked=false;
    LinearLayout seller_location;
    EditText seller_address, edit_seller_state, seller_edit_zip, edit_seller_city;
    public static double  longtatude = 0, latatude = 0;
    private ArrayList permissionsToRequest;
    private ArrayList permissionsRejected = new ArrayList();
    private ArrayList permissions = new ArrayList();
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    private static final int REQUEST_CHECK_SETTINGS = 0x1;
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    private final static String KEY_REQUESTING_LOCATION_UPDATES = "requesting-location-updates";
    private final static String KEY_LOCATION = "location";
    private final static String KEY_LAST_UPDATED_TIME_STRING = "last-updated-time-string";

    private FusedLocationProviderClient mFusedLocationClient;
    private SettingsClient mSettingsClient;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private LocationCallback mLocationCallback;
    private Location mCurrentLocation;
    private final static int ALL_PERMISSIONS_RESULT = 101;
    LocationTrack locationTrack;
    private boolean mRequestingLocationUpdates;
    EasyWayLocation easyWayLocation;
    EditText seller_seller_number;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller_signup_);
        session = new SessionManager(getApplicationContext());
        init();
        easyWayLocation = new EasyWayLocation(Seller_signup_Activity.this, false,this);
        checkConnectivity = new CheckConnectivity(getApplicationContext());
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

    }
    @Override
    protected void onPause() {
        super.onPause();
        easyWayLocation.endUpdates();

    }
    private void init() {
        showpass = findViewById(R.id.showpass);
        seller_password = findViewById(R.id.seller_password);
        seller_login_email = findViewById(R.id.seller_login_email);
        title_text = findViewById(R.id.title_text);
        image_backpress = findViewById(R.id.image_backpress);
        seller_sendotp = findViewById(R.id.seller_sendotp);
        seller_num_otp_page = findViewById(R.id.seller_num_otp_page);
        seller_login_otp = findViewById(R.id.seller_login_otp);
        tv_otp_seller = findViewById(R.id.tv_otp_seller);
        seller_terms_signup = findViewById(R.id.seller_terms_signup);
        seller_signup_btn = findViewById(R.id.seller_signup_btn);
        edit_seller_state = findViewById(R.id.edit_seller_state);
        edit_seller_city = findViewById(R.id.edit_seller_city);
        tv_login_seller = findViewById(R.id.tv_login_seller);
        seller_address = findViewById(R.id.seller_address);
        seller_std_code = findViewById(R.id.seller_std_code);
        seller_edit_zip = findViewById(R.id.seller_edit_zip);
        seller_seller_number = findViewById(R.id.seller_seller_number);

        edit_seller_state.setKeyListener(null);
        edit_seller_city.setKeyListener(null);
        seller_edit_zip.setKeyListener(null);
        seller_location = findViewById(R.id.seller_location);
        seller_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //newlocation();
                // getlocationdata();
                if(permissiongrant()) {

                    startActivity(new Intent(Seller_signup_Activity.this,Seller_map_Activity.class));
                }
            }
        });
        seller_login_name = findViewById(R.id.seller_login_name);
        image_backpress.setOnClickListener(V -> {
            onBackPressed();
        });
        title_text.setText("SignUp");
        passwordshowing();
        seller_login_otp.setVisibility(View.GONE);
        tv_otp_seller.setVisibility(View.GONE);
        seller_sendotp.setTextColor(getResources().getColor(R.color.navy_blue));
        seller_sendotp.setOnClickListener(v -> {
            if (Objects.requireNonNull(seller_num_otp_page.getText()).toString().length() == 0) {

                seller_num_otp_page.setError("Please Enter phone");

            } else if (seller_num_otp_page.getText().toString().length() < 10) {
                seller_num_otp_page.setError("Please Enter correct phone");

            } else {
                actount = true;
                seller_sendotp.setClickable(false);
                progressDialog = new ProgressDialog(Seller_signup_Activity.this);

                progressDialog.setMessage("OTP Send......");
                progressDialog.show();

                Call<Bookingpre> callverify= RetrofitClient.getClient("https://www.goingbo.com/")
                        .create(ApiService.class).otpsellerverfyphone(seller_num_otp_page.getText().toString());
                callverify.enqueue(new Callback<Bookingpre>() {
                    @Override
                    public void onResponse(Call<Bookingpre> call, Response<Bookingpre> response) {
                        Bookingpre bookingpre=response.body();
                        if(!bookingpre.getError()){

                            hitotprate(seller_num_otp_page.getText().toString(), seller_sendotp);

                        }else{
                            new AlertDialog.Builder( Objects.requireNonNull(Seller_signup_Activity.this))
                                    .setTitle("Alert")
                                    .setMessage("Phone number already register !")
                                    .setCancelable(false)
                                    .setPositiveButton("OK", (dialog, which) -> {dialog.dismiss();
                                        progressDialog.dismiss();
                                        seller_sendotp.setClickable(true);
                                    }).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<Bookingpre> call, Throwable t) {
                        progressDialog.dismiss();
                        seller_sendotp.setClickable(true);
                        new AlertDialog.Builder( Objects.requireNonNull(Seller_signup_Activity.this))
                                .setTitle("Alert")
                                .setMessage(t.getMessage())
                                .setCancelable(false)
                                .setPositiveButton("OK", (dialog, which) -> {dialog.dismiss();
                                }).show();
                    }
                });



            }

        });
        seller_signup_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean flag =true;
                //   Toast.makeText(Seller_signup_Activity.this, ""+seller_num_otp_page.getText().toString(), Toast.LENGTH_SHORT).show();
                if (!process()) {
                    if (Objects.requireNonNull(seller_login_email.getText()).toString().trim().length() ==0) {
                        seller_login_email.setError("Please enter emal id");
                    }else if (!validateEmailAddress(seller_login_email.getText().toString())) {
                        seller_login_email.setError("Please enter valid emal id");
                    }else
                    if (Objects.requireNonNull(seller_num_otp_page.getText()).toString().length() == 0) {
                        seller_num_otp_page.setError("Please enter phone");
                    }
                    else if (seller_num_otp_page.getText().toString().length() < 10) {
                        seller_num_otp_page.setError("Please enter correct phone");
                    }
                    else if (seller_login_otp.getText().toString().length() == 0) {

                        new AlertDialog.Builder(Objects.requireNonNull(Seller_signup_Activity.this))
                                .setTitle("Alert")
                                .setMessage("Please verify phone number!")
                                .setCancelable(false)
                                .setPositiveButton("OK", (dialog, which) -> dialog.dismiss()).show();

                    }
                    else if (seller_login_otp.getText().toString().length() < 3) {
                        seller_login_otp.setError("Please enter valid OTP !");
                    }
                    else if(seller_seller_number.getText().toString().length()==0){
                        seller_seller_number.setError("Please enter your alternate number !");
                    }
                    else if(seller_seller_number.getText().toString().length()<10){
                        seller_seller_number.setError("Please enter your correct alternate number !");
                    }
                    else if (seller_login_name.getText().toString().length() == 0) {
                        seller_login_name.setError("Please enter shop Name !");
                    }
                    else if (Objects.requireNonNull(seller_password.getText()).toString().length() == 0) {
                        seller_password.setError("Please enter password");
                    }
                    else if (seller_password.getText().toString().length() < 6) {
                        seller_password.setError("Password more than 6 character");
                    }
                    else if (seller_address.getText().toString().length() == 0) {
                        seller_address.setError("Please enter your address ");
                    }
                    else if (edit_seller_state.getText().toString().length() == 0) {
                        edit_seller_state.setError("Please enter your state name");
                    }
                    else if (edit_seller_state.getText().toString().length() == 0) {
                        new AlertDialog.Builder(Objects.requireNonNull(Seller_signup_Activity.this))
                                .setTitle("Alert")
                                .setMessage("Please click on find your location for find state  !")
                                .setCancelable(false)
                                .setPositiveButton("OK", (dialog, which) -> dialog.dismiss()).show();
                    }
                    else if (seller_edit_zip.getText().toString().length() < 6) {
                        new AlertDialog.Builder(Objects.requireNonNull(Seller_signup_Activity.this))
                                .setTitle("Alert")
                                .setMessage("Please click on find your location For Find Zip!")
                                .setCancelable(false)
                                .setPositiveButton("OK", (dialog, which) -> dialog.dismiss()).show();
                    }
                    else if (edit_seller_city.getText().toString().length() == 0) {
                        new AlertDialog.Builder(Objects.requireNonNull(Seller_signup_Activity.this))
                                .setTitle("Alert")
                                .setMessage("Please click on find Your location for Find city !")
                                .setCancelable(false)
                                .setPositiveButton("OK", (dialog, which) -> dialog.dismiss()).show();
                    }

                    else {
                        Log.e("positionsystem",""+latatude+"   "+longtatude);
                        if (latatude == 0 && longtatude == 0) {
                            new AlertDialog.Builder(Objects.requireNonNull(Seller_signup_Activity.this))
                                    .setTitle(" Alert")
                                    .setMessage("Please use gps for find location !")
                                    .setCancelable(false)
                                    .setNegativeButton("Cancel", ((dialog, which) -> dialog.dismiss()))
                                    .setPositiveButton("OK", (dialog, which) -> {
                                        dialog.dismiss();
                                    }).show();

                        }
                        else {

                            progressDialog = new ProgressDialog(Seller_signup_Activity.this);

                            progressDialog.setMessage("Loading......");
                            progressDialog.show();
                            Call<Bookingpre> callcheck = RetrofitClient.getClient("https://www.goingbo.com/").create(ApiService.class)
                                    .getcheckdata(Objects.requireNonNull(seller_login_email.getText()).toString(), Objects.requireNonNull(seller_login_email.getText()).toString());
                            callcheck.enqueue(new Callback<Bookingpre>() {
                                @Override
                                public void onResponse(Call<Bookingpre> call, Response<Bookingpre> response) {
                                    Bookingpre bookcheck = response.body();
                                    progressDialog.dismiss();
                                    if (!bookcheck.getError()) {
                                        progressDialog.show();
                                        Call<LoginsellerProduct> callthrid = RetrofitClient.getClient("https://www.goingbo.com/").create(ApiService.class).getcreatenewseller(
                                                seller_login_name.getText().toString(), seller_login_email.getText().toString(),
                                                seller_num_otp_page.getText().toString(), seller_seller_number.getText().toString(),seller_password.getText().toString(),seller_password.getText().toString(), longtatude, latatude,
                                                seller_address.getText().toString(),
                                                edit_seller_city.getText().toString(),
                                                edit_seller_state.getText().toString(),
                                                seller_edit_zip.getText().toString());
                                        callthrid.enqueue(new Callback<LoginsellerProduct>() {
                                            @Override
                                            public void onResponse(@NonNull Call<LoginsellerProduct> callsecond, @NonNull Response<LoginsellerProduct> response) {
                                                assert response.body() != null;
                                                if (!response.body().isError()) {
                                                    String id = ""+response.body().getSuccess().getUser().getId();
                                                    String name =""+ response.body().getSuccess().getUser().getName();
                                                    String emails="";
                                                    if(response.body().getSuccess().getUser().getEmail()!=null && response.body().getSuccess().getUser().getEmail().length()==0){
                                                        emails = "";
                                                    }else {
                                                        emails = response.body().getSuccess().getUser().getEmail();
                                                    }
                                                    String phone = response.body().getSuccess().getUser().getPhone();
                                                    String Type = "Seller";
                                                    progressDialog.dismiss();
                                                    session.sellermedialogin(id, name, emails, phone, Type);
                                                    Intent intent = new Intent(Seller_signup_Activity.this, Seller_dashboard.class);
                                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK );
                                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                    startActivity(intent);
                                                    finish();

                                                } else {
                                                    new AlertDialog.Builder(Objects.requireNonNull(Seller_signup_Activity.this))
                                                            .setTitle(" Alert")
                                                            .setMessage("Failed To Create Login !")
                                                            .setCancelable(false)
                                                            .setPositiveButton("OK", (dialog, which) -> dialog.dismiss()).show();
                                                }
                                            }

                                            @Override
                                            public void onFailure(@NonNull Call<LoginsellerProduct> callsecond, @NonNull Throwable t) {
                                                new AlertDialog.Builder(Objects.requireNonNull(Seller_signup_Activity.this))
                                                        .setTitle("Alert")
                                                        .setMessage(t.getMessage())
                                                        .setCancelable(false)
                                                        .setPositiveButton("OK", (dialog, which) -> dialog.dismiss()).show();
                                            }
                                        });
//                                        Call<LoginsellerProduct> callsecond = RetrofitClient.getClient("https://www.goingbo.com/").create(ApiService.class)
//                                                .getcreateseller(
//                                                seller_login_name.getText().toString(), seller_login_email.getText().toString(),
//                                                seller_num_otp_page.getText().toString(), seller_password.getText().toString(),seller_password.getText().toString(), longtatude, latatude);
//                                        callsecond.enqueue(new Callback<LoginsellerProduct>() {
//                                            @Override
//                                            public void onResponse(@NonNull Call<LoginsellerProduct> callsecond, @NonNull Response<LoginsellerProduct> response) {
//                                                progressDialog.dismiss();
//                                                assert response.body() != null;
//                                                if (!response.body().isError()) {
//                                                    String id = ""+response.body().getSuccess().getUser().getId();
//                                                    String name =""+ response.body().getSuccess().getUser().getName();
//                                                    String emails="";
//                                                    if(response.body().getSuccess().getUser().getEmail()!=null && response.body().getSuccess().getUser().getEmail().length()==0){
//                                                        emails = "";
//                                                    }else {
//                                                        emails = response.body().getSuccess().getUser().getEmail();
//                                                    }
//                                                    String phone = response.body().getSuccess().getUser().getPhone();
//                                                    String Type = "Seller";
//                                                    progressDialog.dismiss();
//                                                    session.sellermedialogin(id, name, emails, phone, Type);
//                                                    Intent intent = new Intent(Seller_signup_Activity.this, Seller_dashboard.class);
//                                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK );
//                                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                                    startActivity(intent);
//                                                    finish();
//
//                                                } else {
//                                                    new AlertDialog.Builder(Objects.requireNonNull(Seller_signup_Activity.this))
//                                                            .setTitle(" Alert")
//                                                            .setMessage("Failed To Create Login !")
//                                                            .setCancelable(false)
//                                                            .setPositiveButton("OK", (dialog, which) -> dialog.dismiss()).show();
//                                                }
//                                            }
//
//                                            @Override
//                                            public void onFailure(@NonNull Call<LoginsellerProduct> callsecond, @NonNull Throwable t) {
//                                                progressDialog.dismiss();
//                                                new AlertDialog.Builder(Objects.requireNonNull(Seller_signup_Activity.this))
//                                                        .setTitle("Alert")
//                                                        .setMessage(t.getMessage())
//                                                        .setCancelable(false)
//                                                        .setPositiveButton("OK", (dialog, which) -> dialog.dismiss()).show();
//                                            }
//                                        });


                                    } else {
                                        new AlertDialog.Builder(Objects.requireNonNull(Seller_signup_Activity.this))
                                                .setTitle(" Alert")
                                                .setMessage(bookcheck.getMessage())
                                                .setCancelable(false)
                                                .setPositiveButton("OK", (dialog, which) -> dialog.dismiss()).show();
                                    }

                                }

                                @Override
                                public void onFailure(Call<Bookingpre> call, Throwable t) {
                                    progressDialog.dismiss();
                                }
                            });
                        }


                    }
                }
            }
        });


    }

//    private void newlocation() {
//        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
//        mSettingsClient = LocationServices.getSettingsClient(this);
//        mRequestingLocationUpdates = true;
//        startLocationUpdates();
//        createLocationCallback();
//        createLocationRequest();
//        buildLocationSettingsRequest();
//    }

//    private void createLocationCallback() {
//        mLocationCallback = new LocationCallback() {
//            @Override
//            public void onLocationResult(LocationResult locationResult) {
//                super.onLocationResult(locationResult);
//
//                mCurrentLocation = locationResult.getLastLocation();
//                updateLocation(mCurrentLocation.getLongitude(),mCurrentLocation.getLatitude());
//            }
//        };
//    }
//
//    private void buildLocationSettingsRequest() {
//        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
//        builder.addLocationRequest(mLocationRequest);
//        mLocationSettingsRequest = builder.build();
//    }
//
//    private void createLocationRequest() {
//        mLocationRequest = new LocationRequest();
//        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
//        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
//
//        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
//    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        switch (requestCode) {
//            // Check for the integer request code originally supplied to startResolutionForResult().
//            case REQUEST_CHECK_SETTINGS:
//                switch (resultCode) {
//                    case Activity.RESULT_OK:
//
//                        break;
//                    case Activity.RESULT_CANCELED:
//
//                        mRequestingLocationUpdates = false;
//
//                        break;
//                }
//                break;
//        }
//    }

    public void startUpdatesButtonHandler(View view) {
        if (!mRequestingLocationUpdates) {
            mRequestingLocationUpdates = true;

            startLocationUpdates();
        }
    }


    private void startLocationUpdates() {
        // Begin by checking if the device has the necessary location settings.
        mSettingsClient.checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {

                        //noinspection MissingPermission
                        if (ActivityCompat.checkSelfPermission(Seller_signup_Activity.this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(Seller_signup_Activity.this,
                                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                            return;
                        }
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                                mLocationCallback, Looper.myLooper());


                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:

                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the
                                    // result in onActivityResult().
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(Seller_signup_Activity.this, REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sie) {

                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage = "Location settings are inadequate, and cannot be " +
                                        "fixed here. Fix in Settings.";

                                mRequestingLocationUpdates = false;
                        }


                    }
                });
    }

    @SuppressLint("SetTextI18n")
    public void passwordshowing() {
        showpass.setOnClickListener(v -> {
            if (showpass.getText().toString().equalsIgnoreCase("Show")) {
                seller_password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                showpass.setText("Hide");
                seller_password.setSelection(seller_password.getText().length());
            } else {
                showpass.setText("Show");
                seller_password.setTransformationMethod(new PasswordTransformationMethod());
                seller_password.setSelection(seller_password.getText().length());
            }
        });
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    private boolean validateEmailAddress(String emailAddress) {
        String expression = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(emailAddress);
        return matcher.matches();
    }
    private boolean process() {
        boolean show = true;
        if (checkConnectivity.isConnected()) {
            show = false;
        } else {
            showDialog();
        }
        return show;
    }
    private void showDialog() {

        Dialog dialog = new Dialog(Objects.requireNonNull(Seller_signup_Activity.this), R.style.FullScreenDialogWithStatusBarColorAccent);
        if (!dialog.isShowing()) {
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.no_internet);
            Window window = dialog.getWindow();
            assert window != null;
            WindowManager.LayoutParams wlp = Objects.requireNonNull(window).getAttributes();
            wlp.gravity = Gravity.CENTER;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
            window.setAttributes(wlp);
            Objects.requireNonNull(dialog.getWindow()).setLayout(
                    WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            Button wifi_btn = dialog.findViewById(R.id.wifi_btn);
            Button mobile_data_btn = dialog.findViewById(R.id.mobile_data_btn);
            dialog.show();
            dialog.setCancelable(false);
            wifi_btn.setOnClickListener(v -> {
                dialog.dismiss();
                isshowing = false;
                WifiManager wifiMgr = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                assert wifiMgr != null;
                if (wifiMgr.isWifiEnabled()) {
                    WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
                    if (wifiInfo.getNetworkId() == -1) {
                        showDialog();
                    } else {
                        dialog.dismiss();
                        process();
                    }
                } else {
                    startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS)); // Wi-Fi adapter is OFF
                }
            });
            mobile_data_btn.setOnClickListener(v -> {
                dialog.dismiss();
                isshowing = false;
                startActivityForResult(new Intent(android.provider.Settings.ACTION_DATA_ROAMING_SETTINGS), 0);
                if (checkConnectivity.isConnected()) {
                    process();
                } else {
                    showDialog();
                }
            });
        }

    }
    @SuppressLint("DefaultLocale")
    public static String getRandomNumberString() {
        int number1 = Integer.parseInt(new DecimalFormat("0").format(new Random().nextInt(9)));
        int number2 = Integer.parseInt(new DecimalFormat("0").format(new Random().nextInt(9)));
        int number3 = Integer.parseInt(new DecimalFormat("0").format(new Random().nextInt(9)));
        int number4 = Integer.parseInt(new DecimalFormat("0").format(new Random().nextInt(9)));
        int number5 = Integer.parseInt(new DecimalFormat("0").format(new Random().nextInt(9)));
        int number6 = Integer.parseInt(new DecimalFormat("0").format(new Random().nextInt(9)));
        String otpnumber=""+number1;
        otpnumber+=""+number2;
        otpnumber+=""+number3;
        otpnumber+=""+number4;
        otpnumber+=""+number5;
        otpnumber+=""+number6;
        Log.e("number", "" + number1);
        Log.e("number", "" + number2);
        Log.e("number", "" + number3);
        Log.e("number", "" + number4);
        Log.e("number", "" + number5);
        Log.e("number", "" + number6);
        Log.e("number", "" + otpnumber);
        return otpnumber ;
    }
    private void hitotprate(String phone, TextView rtimer) {


        rtimer.setClickable(false);
        if (count < 6) {

            otp = getRandomNumberString();
            if (otp.length()< 3) {
                hitotprate(phone,rtimer);
            } else {


                mRequestQueue = Volley.newRequestQueue(Seller_signup_Activity.this);
                JSONObject params = new JSONObject();
                try {
                    params.put("authkey", "290188AwxNn68y5d5a9b35");
                    params.put("template_id", "5e426d42d6fc0507882c425a");
                    params.put("mobile", phone);
                    params.put("invisible", 1);
                    params.put("otp", otp);
                    params.put("message","Your Goingbo Seller Login OTP");
                    params.put("userip", "43.230.197.198");
                    params.put("email", "ganeshku73@gmail.com");
                    params.put("otp_length", 6);
                    params.put("otp_expiry", 10);
                    String url = "https://api.msg91.com/api/v5/otp";
                    JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                            url,params,
                            response -> {
                                progressDialog.dismiss();
                                Log.v("responsesmsotp", response.toString());
                                Log.v("responsesmsotp", "" + otp);
                                JSONObject jsonObject = null;
                                String typesucess= null;
                                try {
                                    typesucess = response.getString("type");
                                    if(typesucess.equalsIgnoreCase("success")) {
                                        new AlertDialog.Builder(Objects.requireNonNull(Seller_signup_Activity.this))
                                                .setTitle("Alert")
                                                .setMessage("Requested OTP has been successfully send to your device !")
                                                .setCancelable(false)
                                                .setPositiveButton("OK", (dialog, which) -> {
                                                    dialog.dismiss();
                                                    seller_login_otp.setVisibility(View.VISIBLE);
                                                    tv_otp_seller.setVisibility(View.VISIBLE);
                                                }).show();

                                        rtimer.setClickable(false);
                                        count++;
                                        new CountDownTimer(60000, 1000) {
                                            public void onTick(long millisUntilFinished) {
                                                rtimer.setTextColor(getResources().getColor(R.color.light_sliver));
                                                rtimer.setText("Resend  ( " + millisUntilFinished / 1000 + " sec )");
                                            }

                                            public void onFinish() {
                                                rtimer.setText("Resend ");
                                                rtimer.setTextColor(getResources().getColor(R.color.black));
                                                rtimer.setClickable(true);
                                            }
                                        }.start();
                                    }
                                    else{
                                        new AlertDialog.Builder(Objects.requireNonNull(Seller_signup_Activity.this))
                                                .setTitle("Alert")
                                                .setMessage("Requested OTP has not been send to your device !")
                                                .setCancelable(false)
                                                .setPositiveButton("OK", (dialog, which) -> {
                                                    dialog.dismiss();

                                                }).show();
                                        rtimer.setText("send ");
                                        rtimer.setTextColor(getResources().getColor(R.color.black));
                                        rtimer.setClickable(true);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    new AlertDialog.Builder(Objects.requireNonNull(Seller_signup_Activity.this))
                                            .setTitle("Alert")
                                            .setMessage("Requested OTP has not been send to your device , due to technical problem please try again  !")
                                            .setCancelable(false)
                                            .setPositiveButton("OK", (dialog, which) -> {
                                                dialog.dismiss();

                                            }).show();
                                    rtimer.setText("send ");
                                    rtimer.setTextColor(getResources().getColor(R.color.black));
                                    rtimer.setClickable(true);
                                }






                            }, error -> {
                        progressDialog.dismiss(); rtimer.setClickable(true);
                    }) ;

// Adding request to request queue
                    mRequestQueue.add(jsonObjReq);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        } else{
            new AlertDialog.Builder(Objects.requireNonNull(Seller_signup_Activity.this))
                    .setTitle("Alert")
                    .setMessage("you have succeeded maximum limit of OTP requesting !")
                    .setCancelable(false)
                    .setPositiveButton("OK", (dialog, which) -> {
                        dialog.dismiss();

                    }).show();
        }
    }
    public void startHandler() {
        handler.postDelayed(r, 10 * 60 * 1000);

    }

    // longtatude=  (double) (location.getLatitude() * 1E6);
    //                latatude=      (double) (location.getLongitude() * 1E6);
    //                updateLocation(location);
    public void getlocationdata() {
        //    Toast.makeText(Seller_signup_Activity.this, "Click", Toast.LENGTH_SHORT).show();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            permissions.add(ACCESS_FINE_LOCATION);
            permissions.add(ACCESS_COARSE_LOCATION);

            permissionsToRequest = findUnAskedPermissions(permissions);
            if (permissionsToRequest.size() > 0)
                requestPermissions((String[]) permissionsToRequest.toArray(new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
        }else{
            permissioncheck();
        }
        locationManager= (LocationManager) this.getSystemService(LOCATION_SERVICE);

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            locationTrack = new LocationTrack(Seller_signup_Activity.this);
            //   Toast.makeText(Seller_signup_Activity.this, "Fetch", Toast.LENGTH_SHORT).show();

            if (locationTrack.canGetLocation()) {
                if(locationTrack.getLatitude()!=0 && locationTrack.getLongitude()!=0) {
                    double longitude = locationTrack.getLongitude();
                    double latitude = locationTrack.getLatitude();
                    longtatude = longitude;
                    latatude = latitude;
                    updateLocation(longitude, latitude);
                }

            } else {

                locationTrack.showSettingsAlert();
            }
        }else{
            showGPSDisabledAlertToUser();
        }



    }

    private void permissioncheck() {
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
        String rationale = "Please provide location permission so that you can ...";
        Permissions.Options options = new Permissions.Options()
                .setRationaleDialogTitle("Info")
                .setSettingsDialogTitle("Warning");

        Permissions.check(this, permissions, rationale, options, new PermissionHandler() {
            @Override
            public void onGranted() {

            }

            @Override
            public void onDenied(Context context, ArrayList<String> deniedPermissions) {
                permissioncheck();
            }
        });
    }

    private boolean permissiongrant() {

        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
        String rationale = "Please provide location permission so that you can ...";
        Permissions.Options options = new Permissions.Options()
                .setRationaleDialogTitle("Info")
                .setSettingsDialogTitle("Warning");

        Permissions.check(this, permissions, rationale, options, new PermissionHandler() {
            @Override
            public void onGranted() {
                checked= true;
            }

            @Override
            public void onDenied(Context context, ArrayList<String> deniedPermissions) {
                permissiongrant();
                checked= false;
            }
        });
        return checked;
    }

    private void showGPSDisabledAlertToUser(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to enable it?")
                .setCancelable(false)
                .setPositiveButton("Goto Settings Page To Enable GPS",
                        new DialogInterface.OnClickListener(){
                            public void onClick(DialogInterface dialog, int id){
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(callGPSSettingIntent);
                            }
                        });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int id){
                        dialog.cancel();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }
    private ArrayList findUnAskedPermissions(ArrayList wanted) {
        ArrayList result = new ArrayList();

        for (Object perm : wanted) {
            if (!hasPermission((String) perm)) {
                result.add(perm);
            }
        }

        return result;
    }
    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }


        }else{
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION)) {

                    new AlertDialog.Builder(this)
                            .setTitle("Location Permission Needed")
                            .setMessage("This app needs the location permission, please accept to use location functionality")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                    ActivityCompat.requestPermissions(Seller_signup_Activity.this,
                                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                            99 );
                                }
                            })
                            .create()
                            .show();


                } else {
                    // No explanation needed, we can request the permission.
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            99 );
                }
            }
        }
        return true;
    }

    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(Seller_signup_Activity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                if (ContextCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, new android.location.LocationListener() {
                        @Override
                        public void onLocationChanged(Location location) {
                            Log.e("datapount", "" + location);
                        }

                        @Override
                        public void onStatusChanged(String provider, int status, Bundle extras) {

                        }

                        @Override
                        public void onProviderEnabled(String provider) {

                        }

                        @Override
                        public void onProviderDisabled(String provider) {

                        }
                    });
                }
            }
        }else{

        }
    }
    @SuppressLint("SetTextI18n")
    public void updateLocation (double longitude, double latitude){
        Log.d("latitube",""+latitude);
        Log.d("longitude",""+longitude);
        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);

            Log.e("selleraddress",""+addresses.toString());
            if (addresses .size()>0) {



                String street = addresses.get(0).getAddressLine(0);
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String zip = addresses.get(0).getPostalCode();
                String country = addresses.get(0).getCountryName();
                String block = addresses.get(0).getFeatureName();
                String subaddres=addresses.get(0).getAddressLine(1);


                seller_address.setText(street);
                edit_seller_city.setText(city);

                seller_edit_zip.setText(zip);

                edit_seller_state.setText(state);
                easyWayLocation.endUpdates();
            } else {
                //Toast.makeText(this, "No Address returned!", Toast.LENGTH_SHORT).show();
                new AlertDialog.Builder(Objects.requireNonNull(Seller_signup_Activity.this))
                        .setTitle(" Alert")
                        .setMessage("Your Location Not Found ,Please Try Again !")
                        .setCancelable(false)
                        .setPositiveButton("OK", (dialog, which) -> dialog.dismiss()).show();
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }
    public boolean getLocationFromAddress(String strAddress){
        Geocoder coder = new Geocoder(this);
        List<Address> address;
        boolean p1 = false;
        try {
            address = coder.getFromLocationName(strAddress,5);
            if (address==null) {
                return false;
            }
            Address location=address.get(0);
            location.getLatitude();
            location.getLongitude();

            p1 =true;
            longtatude=  (double) (location.getLatitude() * 1E6);
            latatude=      (double) (location.getLongitude() * 1E6);

            return p1;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return p1;
    }

    @Override
    public void locationOn() {
        easyWayLocation.startLocation();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case LOCATION_SETTING_REQUEST_CODE:
                easyWayLocation.onActivityResult(resultCode);
                break;
        }
    }
    @Override
    public void currentLocation(Location location) {
        StringBuilder data = new StringBuilder();
        data.append(location.getLatitude());
        data.append(" , ");
        data.append(location.getLongitude());
        longtatude = location.getLongitude();
        latatude = location.getLatitude();
        updateLocation(location.getLongitude(), location.getLatitude());
    }

    @Override
    public void locationCancelled() {

    }

    @Override
    protected void onResume() {
        super.onResume();
        session=new SessionManager(getApplicationContext());
        if(session.getlocation().get(LibConstant.lat)!=null && session.getlocation().get(LibConstant.lon)!=null){
            longtatude = Double.parseDouble(session.getlocation().get(LibConstant.lon));
            latatude = Double.parseDouble(session.getlocation().get(LibConstant.lat));
            if(longtatude>0 && latatude>0) {
                updateLocation(longtatude, latatude);
            }
        }

    }
}
