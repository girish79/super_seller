package com.Goingbo.Superseller.Model.Product_type;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.List;

public class ProductTypeData implements Serializable, Parcelable
{

    @SerializedName("error")
    @Expose
    private boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("cat")
    @Expose
    private List<Cat> cats = null;
    public final static Creator<ProductTypeData> CREATOR = new Creator<ProductTypeData>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ProductTypeData createFromParcel(Parcel in) {
            return new ProductTypeData(in);
        }

        public ProductTypeData[] newArray(int size) {
            return (new ProductTypeData[size]);
        }

    }
            ;
    private final static long serialVersionUID = -8461219115590821795L;

    protected ProductTypeData(Parcel in) {
        this.error = ((boolean) in.readValue((boolean.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.cats, (Cat.class.getClassLoader()));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public ProductTypeData() {
    }

    /**
     *
     * @param cats
     * @param error
     * @param message
     */
    public ProductTypeData(boolean error, String message, List<Cat> cats) {
        super();
        this.error = error;
        this.message = message;
        this.cats = cats;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public ProductTypeData withError(boolean error) {
        this.error = error;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ProductTypeData withMessage(String message) {
        this.message = message;
        return this;
    }

    public List<Cat> getCats() {
        return cats;
    }

    public void setCats(List<Cat> cats) {
        this.cats = cats;
    }

    public ProductTypeData withCat(List<Cat> cats) {
        this.cats = cats;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("error", error).append("message", message).append("cat", cats).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(error).append(message).append(cats).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ProductTypeData) == false) {
            return false;
        }
        ProductTypeData rhs = ((ProductTypeData) other);
        return new EqualsBuilder().append(error, rhs.error).append(message, rhs.message).append(cats, rhs.cats).isEquals();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(error);
        dest.writeValue(message);
        dest.writeList(cats);
    }

    public int describeContents() {
        return 0;
    }

}