package com.Goingbo.Superseller.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.provider.Settings;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.Goingbo.Superseller.Interface.ApiService;
import com.Goingbo.Superseller.Model.Bookingpre;
import com.Goingbo.Superseller.R;
import com.Goingbo.Superseller.Util.CheckConnectivity;
import com.Goingbo.Superseller.Util.LibConstant;
import com.Goingbo.Superseller.Util.RetrofitClient;
import com.Goingbo.Superseller.Util.SessionManager;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.HashMap;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserProfile extends AppCompatActivity {
    ImageView backprofile;
    TextInputEditText edit_profile_name, edit_profile_email, edit_profile_phoneno;
    Button profilesave;
    private SessionManager session;
    HashMap<String, String> datamap;
    CheckConnectivity checkConnectivity;
    String id ="";
    boolean isshowing =true;

    TextInputLayout textinputlayput_profile_alternate_phoneno;
    TextInputEditText edit_profile_alternate_phoneno,edit_address_name,edit_address_city,edit_address_state,edit_address_zip;
    LinearLayout find_location_seller_profile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        init();
        checkConnectivity=new CheckConnectivity(UserProfile.this);
        session=new SessionManager(getApplicationContext());
        backprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        datamap = session.getsoicalDetails();
        if(datamap.get(LibConstant.socialid)!=null){
            id=datamap.get(LibConstant.socialid);
        }
        if (datamap.get(LibConstant.Name) != null) {
            edit_profile_name.setText(datamap.get(LibConstant.Name));
        }
        if (datamap.get(LibConstant.Email) != null) {
            edit_profile_email.setText(datamap.get(LibConstant.Email));
        }
        if (datamap.get(LibConstant.Phone) != null) {
            edit_profile_phoneno.setText(datamap.get(LibConstant.Phone));
        }


        profilesave.setOnClickListener(v -> {
            if(!process()) {
                if (edit_profile_name.getText().toString().length() == 0) {
                    edit_profile_name.setError("Please enter name");
                } else if (edit_profile_email.getText().toString().length() == 0) {
                    edit_profile_email.setError("Please enter your email id");
                } else if (!validateEmailAddress(edit_profile_email.getText().toString())) {
                    edit_profile_email.setError("Please enter your correct email id");
                } else if (edit_profile_phoneno.getText().toString().length() == 0) {
                    edit_profile_phoneno.setError("Please enter your phone number");
                } else if (edit_profile_phoneno.getText().toString().length() > 10) {
                    edit_profile_phoneno.setError("Please enter your correct phone number");
                } else {
                 //   Toast.makeText(this, ""+edit_profile_phoneno.getText().toString(), Toast.LENGTH_SHORT).show();
                    ProgressDialog progressDialog = new ProgressDialog(UserProfile.this);

                    progressDialog.setMessage("Loading......");
                    progressDialog.show();
                    Call<Bookingpre> call =
                            RetrofitClient.getClient("https://www.goingbo.com/").create(ApiService.class)
                                    .checksellerprofile(edit_profile_name.getText().toString(),
                                            edit_profile_email.getText().toString(),
                                            edit_profile_phoneno.getText().toString(),
                                            "Seller");
                    call.enqueue(new Callback<Bookingpre>() {
                        @Override
                        public void onResponse(@NonNull Call<Bookingpre> call, @NonNull Response<Bookingpre> response) {
                            assert response.body() != null;
                            progressDialog.dismiss();
                            if (response.body().getMessage().equals("User profile updated successfully")) {

                                session.sellermedialogin(id, edit_profile_name.getText().toString(),
                                        edit_profile_email.getText().toString(),
                                        edit_profile_phoneno.getText().toString(),
                                        "Seller");
                       //         Toast.makeText(UserProfile.this, "Seller update successfully", Toast.LENGTH_SHORT).show();
                                onBackPressed();
                            }

                        }

                        @Override
                        public void onFailure(@NonNull Call<Bookingpre> call, @NonNull Throwable t) {
                            progressDialog.dismiss();
                        }
                    });
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void init() {
        backprofile = findViewById(R.id.backprofile);
        edit_profile_name = findViewById(R.id.edit_profile_name);
        edit_profile_email = findViewById(R.id.edit_profile_email);
        edit_profile_phoneno = findViewById(R.id.edit_profile_phoneno);

        profilesave = findViewById(R.id.profilesave);
    }
    private boolean validateEmailAddress(String emailAddress) {
        String expression ="[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        CharSequence inputStr = emailAddress;
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        return matcher.matches();
    }

    private boolean process() {
        boolean show=true;
        if (checkConnectivity.isConnected()) {
            show=false;

        } else {
            showDialog();
            show=true;
        }
        return show;
    }
    private void showDialog() {

        Dialog dialog = new Dialog(UserProfile.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.no_internet);
        Window window = dialog.getWindow();
        assert window != null;
        WindowManager.LayoutParams wlp = Objects.requireNonNull(window).getAttributes();
        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        Objects.requireNonNull(dialog.getWindow()).setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        Button wifi_btn = dialog.findViewById(R.id.wifi_btn);
        Button mobile_data_btn = dialog.findViewById(R.id.mobile_data_btn);
        if (!dialog.isShowing()) {
            dialog.show();
            dialog.setCancelable(false);
            wifi_btn.setOnClickListener(v -> {
                dialog.dismiss();
                isshowing = false;
                WifiManager wifiMgr = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);

                assert wifiMgr != null;
                if (wifiMgr.isWifiEnabled()) { // Wi-Fi adapter is ON

                    WifiInfo wifiInfo = wifiMgr.getConnectionInfo();

                    if (wifiInfo.getNetworkId() == -1) {

                        showDialog();
                        // Not connected to an access point
                    } else {
                        dialog.dismiss();
                        process();
                    }
                    // Connected to an access point
                } else {
                    startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS)); // Wi-Fi adapter is OFF
                }
            });


            mobile_data_btn.setOnClickListener(v -> {
                dialog.dismiss();
                isshowing = false;
                startActivityForResult(new Intent(android.provider.Settings.ACTION_DATA_ROAMING_SETTINGS), 0);
                if (checkConnectivity.isConnected()) {
                    process();

                } else {
                    showDialog();
                }
            });
        }

    }
}
