package com.Goingbo.Superseller.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.Goingbo.Superseller.Adapter.CustomArrayAdapter;
import com.Goingbo.Superseller.Adapter.Customcategoriesadd;
import com.Goingbo.Superseller.Adapter.Edit_Product_Unit_Recyclerview;
import com.Goingbo.Superseller.Interface.ApiService;
import com.Goingbo.Superseller.Interface.SelectedAdapterInterface;
import com.Goingbo.Superseller.Model.Bookingpre;
import com.Goingbo.Superseller.Model.ProductSeller_Type.ProductSellerTypeData;
import com.Goingbo.Superseller.Model.Product_type.ProductTypeData;
import com.Goingbo.Superseller.Model.productdetails.Product;
import com.Goingbo.Superseller.Model.seller_categories.Cat;
import com.Goingbo.Superseller.Model.seller_categories.Productcategories;
import com.Goingbo.Superseller.Model.seller_unit.Productdetail;
import com.Goingbo.Superseller.Model.seller_unit.UnitSeller;
import com.Goingbo.Superseller.R;
import com.Goingbo.Superseller.Util.CheckConnectivity;
import com.Goingbo.Superseller.Util.LibConstant;
import com.Goingbo.Superseller.Util.RetrofitClient;
import com.Goingbo.Superseller.Util.SessionManager;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Select_Product extends Fragment {

    protected static final int CAMERA_REQUEST = 0;
    protected static final int GALLERY_PICTURE = 1;
    static final int REQUEST_PERMISSION_WRITE_EXTERNAL_STORAGE =
            1002;
    public String userid = "";
    public ArrayList<Cat> arrayList;
    public ArrayList<com.Goingbo.Superseller.Model.Product_type.Cat> arrayListcat;
    public ArrayList<Product> arrylistproduct;
    public String stringone = "";
    public String producttypedata = "";
    String categoriesid = "";
    Button btn_edit_add_unit;
    ProgressDialog progressDialog;
    Toolbar toolbar;
    List<UnitSeller> unitlist;
    boolean isshowing = true;
    RecyclerView edit_unit_recyclerView;
    ArrayList<Productdetail> uproductdetials;
    RelativeLayout unitlayout;
    TextInputLayout textinputlayout_seller_price, textinputlayout_product_price, textinputlayout_product_quantity;
    LinearLayout linear_layout_unit;
    ProgressBar progressbar_seller_product_image;
    String mCurrentPhotoPath = "";
    int permissionCount = 0;
    private View view;
    private ImageView add_select_seller_product;
    private ImageView edit_image_product;
    private TextInputEditText add_selected_product_name, add_selected_product_descr, add_selected_product_quantity, selected_product_price;
    private TextInputEditText selected_selling_price;
    private Button selected_product_submit;
    private Spinner add_product_categories;
    private SessionManager sessionManager;
    private CheckConnectivity checkConnectivity;

    public Select_Product() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_select__product, container, false);
        checkConnectivity = new CheckConnectivity(getContext());
        init();
        progressDialog = new ProgressDialog(getContext());
        sessionManager = new SessionManager(getContext().getApplicationContext());
        arrayList = new ArrayList<>();
        arrayListcat = new ArrayList<>();
        arrylistproduct = new ArrayList<>();
        toolbar = getActivity().findViewById(R.id.title_seller_dashboard);
        toolbar.setTitle("Add Products");
        HashMap<String, String> hashMapdata = sessionManager.getsoicalDetails();
        userid = hashMapdata.get(LibConstant.socialid);
        unitlist = new ArrayList<>();
        Bundle bundle = this.getArguments();
        String typedata = bundle.getString("senddata");
        if (typedata.equalsIgnoreCase("add")) {
            unitlayout.setVisibility(View.GONE);
            linear_layout_unit.setVisibility(View.GONE);
            textinputlayout_product_price.setVisibility(View.VISIBLE);
            textinputlayout_product_quantity.setVisibility(View.VISIBLE);
            textinputlayout_seller_price.setVisibility(View.VISIBLE);
            progressbar_seller_product_image.setVisibility(View.GONE);
            if (!process()) {
                progressDialog.setMessage("Loading......");
                progressDialog.show();
                Call<ProductTypeData> call = RetrofitClient.getClient("https://www.goingbo.com/").create(ApiService.class).
                        getcategory(userid);
                call.enqueue(new Callback<ProductTypeData>() {
                    @Override
                    public void onResponse(Call<ProductTypeData> call, Response<ProductTypeData> response) {
                        if (response.body() != null) {
                            ProductTypeData categoriesdata = response.body();
                            if (!categoriesdata.isError()) {
                                arrayListcat.clear();
                                if (categoriesdata.getCats().size() > 0) {

                                    arrayListcat.addAll(categoriesdata.getCats());
                                    if (arrayList != null) {
                                        Customcategoriesadd adapter = new Customcategoriesadd(getContext(), R.layout.row_spinner_layout,
                                                arrayListcat
                                        );
                                        add_product_categories.setAdapter(adapter);
                                        progressDialog.dismiss();
                                        if (arrayListcat.size() > 0) {
                                            add_product_categories.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                                @Override
                                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                                    setproducttype(String.valueOf(arrayListcat.get(position).getId()));
                                                    producttypedata = "" + arrayListcat.get(position).getId();
                                                }

                                                @Override
                                                public void onNothingSelected(AdapterView<?> parent) {

                                                }
                                            });
                                        }
                                    }


                                }
                            }
                        }


                    }

                    @Override
                    public void onFailure(Call<ProductTypeData> call, Throwable t) {

                    }
                });
            }

            edit_image_product.setVisibility(View.VISIBLE);
            edit_image_product.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectImage();
                }
            });

            selected_product_submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!process()) {
                        // producttypedata = "" + arrayListcat.get(add_product_categories.getSelectedItemPosition()).getId();
//                        if (producttypedata.length() == 0) {
//                            setproducttype(String.valueOf(arrayListcat.get(add_product_categories.getSelectedItemPosition()).getId()));
//                        } else
                        if (stringone == null) {
                            AlertDialog alertdialog = new AlertDialog.Builder(getContext())
                                    .setTitle("Alert")
                                    .setMessage("Please Select Product image !")
                                    .setCancelable(false)
                                    .setPositiveButton("OK", (dialog, which) -> {
                                        dialog.dismiss();

                                    }).create();
                            if (!alertdialog.isShowing()) {
                                alertdialog.show();
                            }
                        } else if (stringone.length() == 0) {
                            AlertDialog alertdialog = new AlertDialog.Builder(getContext())
                                    .setTitle("Alert")
                                    .setMessage("Please Select Product image !")
                                    .setCancelable(false)
                                    .setPositiveButton("OK", (dialog, which) -> {
                                        dialog.dismiss();

                                    }).create();
                            if (!alertdialog.isShowing()) {
                                alertdialog.show();
                            }
                        } else if (add_selected_product_name.getText().toString().length() == 0) {
                            add_selected_product_name.setError("Please Enter Product Name");

                        } else if (add_selected_product_descr.getText().toString().length() == 0) {
                            add_selected_product_descr.setError("Please Enter Product Description");
                        } else if (add_selected_product_quantity.getText().toString().length() == 0) {
                            add_selected_product_quantity.setError("Please Enter Product Quantity");
                        } else if (selected_product_price.getText().toString().length() == 0) {
                            selected_product_price.setError("Please Enter Product Price ");
                        } else if (selected_selling_price.getText().toString().length() == 0) {
                            selected_selling_price.setError("Please Enter Product Selling");
                        } else if (Double.parseDouble(selected_product_price.getText().toString()) < Double.parseDouble(selected_selling_price.getText().toString())) {
                            selected_selling_price.setError("Product Selling price not be product Price !");
                        } else {
                            progressDialog.show();
                            File propertyImageFile1 = new File(stringone);
                            RequestBody videoBody1 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile1);
                            MultipartBody.Part body1 = MultipartBody.Part.createFormData("uploadfile", propertyImageFile1.getName(), videoBody1);
                            RequestBody ptype = RequestBody.create(MediaType.parse("text/plain"), producttypedata);
                            RequestBody pcategories = RequestBody.create(MediaType.parse("text/plain"),
                                    String.valueOf(arrayListcat.get(add_product_categories.getSelectedItemPosition()).getId()));
                            Log.e("productname", "" + add_selected_product_name.getText().toString());
                            Log.e("productname", "" + producttypedata);
                            Log.e("productname", "" + String.valueOf(arrayListcat.get(add_product_categories.getSelectedItemPosition()).getId()));
                            RequestBody pname = (RequestBody) RequestBody.create(MediaType.parse("text/plain"), add_selected_product_name.getText().toString());
                            RequestBody pqty = (RequestBody) RequestBody.create(MediaType.parse("text/plain"), add_selected_product_quantity.getText().toString());
                            RequestBody pprice = (RequestBody) RequestBody.create(MediaType.parse("text/plain"), selected_product_price.getText().toString());
                            RequestBody pdescr = (RequestBody) RequestBody.create(MediaType.parse("text/plain"), add_selected_product_descr.getText().toString());
                            RequestBody pselling = (RequestBody) RequestBody.create(MediaType.parse("text/plain"), selected_selling_price.getText().toString());

                            RequestBody puserid = (RequestBody) RequestBody.create(MediaType.parse("text/plain"), userid);
                            Call<Bookingpre> calladdproduct = RetrofitClient.getClient("https://www.goingbo.com/").create(ApiService.class).
                                    imageaddproductdetails(ptype, pcategories,
                                            pname, pqty, pprice, body1, pdescr
                                            , pselling, puserid);
                            calladdproduct.enqueue(new Callback<Bookingpre>() {
                                @Override
                                public void onResponse(Call<Bookingpre> call, Response<Bookingpre> response) {
                                    Log.e("response", "" + response.body().toString());
                                    progressDialog.dismiss();
                                    if (!response.body().getError()) {
                                        if (response.body().getMessage().equals("Seller product inserted successfully")) {
                                            AlertDialog alertdialog = new AlertDialog.Builder(getContext())
                                                    .setTitle("Alert")
                                                    .setMessage("Product inserted sucessfull !")
                                                    .setCancelable(false)
                                                    .setPositiveButton("OK", (dialog, which) -> {
                                                        dialog.dismiss();
                                                        getActivity().onBackPressed();
                                                    }).create();
                                            if (!alertdialog.isShowing()) {
                                                alertdialog.show();
                                            }

                                        }
                                    }
                                }

                                @Override
                                public void onFailure(Call<Bookingpre> call, Throwable t) {
                                    Log.e("response_error", "" + t.getMessage());
                                    progressDialog.dismiss();
                                }
                            });
                        }
                    }

                }
            });

        } else if (typedata.equalsIgnoreCase("search")) {
            unitlayout.setVisibility(View.VISIBLE);
            textinputlayout_product_price.setVisibility(View.GONE);
            textinputlayout_product_quantity.setVisibility(View.GONE);
            textinputlayout_seller_price.setVisibility(View.GONE);
            arrylistproduct.clear();
            String cname = bundle.getString("category_name");
            categoriesid = bundle.getString("productid");
            String name = bundle.getString("productname");
            String cat = bundle.getString("productcategories");
            String des = bundle.getString("productdescription");
            String image = bundle.getString("productimage");
            String type = bundle.getString("producttype");
            btn_edit_add_unit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Dialog dialog = new Dialog(getContext(), R.style.FullScreenDialog);
                    if (!dialog.isShowing()) {
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.dialog_edit_product_unit);
                        Window window = dialog.getWindow();
                        assert window != null;
                        WindowManager.LayoutParams wlp = Objects.requireNonNull(window).getAttributes();
                        wlp.gravity = Gravity.CENTER;
                        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
                        window.setAttributes(wlp);
                        Objects.requireNonNull(dialog.getWindow()).setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                        Spinner seller_spinner_unit_array = dialog.findViewById(R.id.seller_spinner_unit_array);
                        ImageView product_unit_close = dialog.findViewById(R.id.product_unit_close);
                        product_unit_close.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });
                        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                                R.array.unit_array, android.R.layout.simple_spinner_item);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        seller_spinner_unit_array.setAdapter(adapter);


                        TextInputEditText seller_weight_unit = dialog.findViewById(R.id.seller_weight_unit);

                        TextInputEditText seller_product_price = dialog.findViewById(R.id.seller_product_price);


                        TextInputEditText seller_product_offer_price = dialog.findViewById(R.id.seller_product_offer_price);

                        Button seller_save_button = dialog.findViewById(R.id.seller_save_button);
                        seller_save_button.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (seller_spinner_unit_array.getSelectedItemPosition() == 0) {
                                    AlertDialog alertdialog = new AlertDialog.Builder(getContext())
                                            .setTitle("Alert")
                                            .setMessage("Please Select Unit Type  !")
                                            .setCancelable(false)
                                            .setPositiveButton("OK", (dialog, which) -> {
                                                dialog.dismiss();

                                            }).create();
                                    if (!alertdialog.isShowing()) {
                                        alertdialog.show();
                                    }
                                } else if (seller_weight_unit.getText().toString().length() == 0) {
                                    seller_weight_unit.setError("Please Enter Weight Unit !");
                                } else if (seller_product_price.getText().toString().length() == 0) {
                                    seller_product_price.setError("Please Enter Price !");
                                } else if (seller_product_offer_price.getText().toString().length() == 0) {
                                    seller_product_offer_price.setError("Please Enter Offer Price !");
                                } else if (Double.parseDouble(seller_product_price.getText().toString()) < Double.parseDouble(seller_product_offer_price.getText().toString())) {
                                    seller_product_offer_price.setError(" Offer Price could not be greater than product price  !");
                                } else {
                                    if (unitlist.size() == 0) {
                                        Productdetail productdetails = new Productdetail();
                                        productdetails.setUnit(seller_weight_unit.getText().toString());
                                        productdetails.setUnittype(seller_spinner_unit_array.getSelectedItem().toString());
                                        productdetails.setPrice(seller_product_price.getText().toString());
                                        productdetails.setOfferPrice(seller_product_offer_price.getText().toString());
                                        List<Productdetail> listdetials = new ArrayList<>();
                                        listdetials.add(productdetails);
                                        unitlist.add(new UnitSeller(categoriesid, listdetials));
                                        edit_unit_recyclerView.setHasFixedSize(false);
                                        edit_unit_recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                                        setUnit(categoriesid);
                                        dialog.dismiss();
                                    } else {
                                        Productdetail productdetails = new Productdetail();
                                        productdetails.setUnit(seller_weight_unit.getText().toString());
                                        productdetails.setUnittype(seller_spinner_unit_array.getSelectedItem().toString());
                                        productdetails.setPrice(seller_product_price.getText().toString());
                                        productdetails.setOfferPrice(seller_product_offer_price.getText().toString());
                                        unitlist.get(0).getProductdetail().add(productdetails);
                                        edit_unit_recyclerView.setHasFixedSize(false);
                                        edit_unit_recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                                        setUnit(categoriesid);
                                        dialog.dismiss();
                                    }
                                    Log.e("girishprint", "" + unitlist.toString());
                                }
                            }
                        });


                        dialog.show();
                        dialog.setCancelable(false);


                    }
                }
            });
            arrylistproduct.add(new Product(cname, categoriesid, type, cat, name, image, des));
            // Toast.makeText(getContext(), " size"+arrylistproduct.size(), Toast.LENGTH_SHORT).show();
            arrayList.clear();
            for (int k = 0; k < arrylistproduct.size(); k++) {
                producttypedata = arrylistproduct.get(k).getProType();
                add_selected_product_name.setText(arrylistproduct.get(k).getProName());
                add_selected_product_descr.setText(arrylistproduct.get(k).getProDes());
                add_selected_product_name.setEnabled(false);
                add_selected_product_descr.setEnabled(false);
                selected_product_price.setEnabled(false);
                ;
                add_selected_product_quantity.setEnabled(false);

                String catid = arrylistproduct.get(k).getProCategory();
                stringone = "https://www.goingbo.com/public/product_images/" + arrylistproduct.get(k).getProImage();
                if (!process()) {


                    progressDialog.setMessage("Loading......");
                    progressDialog.show();
                    Call<Productcategories> call = RetrofitClient.getClient("https://www.goingbo.com/").create(ApiService.class).getcategoryselected(Integer.parseInt(cat));
                    call.enqueue(new Callback<Productcategories>() {
                        @Override
                        public void onResponse(Call<Productcategories> call, Response<Productcategories> response) {
                            if (response.body() != null) {
                                Productcategories categoriesdata = response.body();
                                if (!categoriesdata.isError()) {
                                    arrayList.clear();
                                    if (categoriesdata.getCat().size() > 0) {

                                        arrayList.addAll(categoriesdata.getCat());

                                        CustomArrayAdapter adapter = new CustomArrayAdapter(getContext(), R.layout.row_spinner_layout,
                                                arrayList
                                        );
                                        add_product_categories.setAdapter(adapter);
                                        progressDialog.dismiss();
                                        if (arrayList.size() > 0) {
                                            add_product_categories.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                                @Override
                                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                                    setproducttype(categoriesid);
                                                }

                                                @Override
                                                public void onNothingSelected(AdapterView<?> parent) {

                                                }
                                            });

                                        }


                                    }
                                } else {
                                    new AlertDialog.Builder(getContext())
                                            .setTitle("Your Alert")
                                            .setMessage("Categories is not Present in database !")
                                            .setCancelable(false)
                                            .setPositiveButton("OK", (dialog, which) -> {
                                                dialog.dismiss();
                                                getActivity().onBackPressed();
                                                // proDialog.dismiss();
                                            }).create().show();

                                }
                            }


                        }

                        @Override
                        public void onFailure(Call<Productcategories> call, Throwable t) {

                        }
                    });
                    Picasso.get().load(stringone).into(add_select_seller_product, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            if (progressbar_seller_product_image != null) {
                                progressbar_seller_product_image.setVisibility(View.GONE);
                            }
                        }

                        @Override
                        public void onError(Exception e) {

                        }


                    });
                }
            }
            edit_image_product.setVisibility(View.GONE);
            selected_product_submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!process()) {
                        if (producttypedata.length() == 0) {
                            setproducttype(categoriesid);
                        } else if (add_selected_product_name.getText().toString().length() == 0) {
                            add_selected_product_name.setError("Please Enter Product Name");

                        } else if (add_selected_product_descr.getText().toString().length() == 0) {
                            add_selected_product_descr.setError("Please Enter Product Description");
                        } else if (unitlist.size() == 0) {
                            AlertDialog alertdialog = new AlertDialog.Builder(getContext())
                                    .setTitle("Alert")
                                    .setMessage("Product inserted price / unit !")
                                    .setCancelable(false)
                                    .setPositiveButton("OK", (dialog, which) -> {

                                        dialog.dismiss();
                                    }).create();
                            if (!alertdialog.isShowing()) {
                                alertdialog.show();
                            }
                        } else {
                            if (unitlist.get(0).getProductdetail().size() == 0) {
                                AlertDialog alertdialog = new AlertDialog.Builder(getContext())
                                        .setTitle("Alert")
                                        .setMessage("Product inserted price / unit !")
                                        .setCancelable(false)
                                        .setPositiveButton("OK", (dialog, which) -> {

                                            dialog.dismiss();
                                        }).create();
                                if (!alertdialog.isShowing()) {
                                    alertdialog.show();
                                }
                            } else {
                                progressDialog.show();
                                JSONObject contactsObj = new JSONObject();
                                for (int i = 0; i < unitlist.size(); i++) {
                                    try {
                                        JSONArray jArray = new JSONArray();
                                        JSONObject arraysObj = new JSONObject();
                                        for (int l = 0; l < unitlist.get(i).getProductdetail().size(); l++) {
                                            try {
                                                arraysObj.put("unittype", unitlist.get(i).getProductdetail().get(l).getUnittype());
                                                arraysObj.put("unit", unitlist.get(i).getProductdetail().get(l).getUnit());
                                                arraysObj.put("price", unitlist.get(i).getProductdetail().get(l).getPrice());
                                                arraysObj.put("offer_price", unitlist.get(i).getProductdetail().get(l).getOfferPrice());
                                                jArray.put(arraysObj);
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                        contactsObj.put("productid", unitlist.get(i).getProductid());
                                        contactsObj.put("productdetail", jArray);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                                JSONObject jsonParams = new JSONObject();
//put something inside the map, could be null
                                try {
                                    jsonParams.put("Pro_type", type);
                                    jsonParams.put("productid", categoriesid);
                                    jsonParams.put("Pro_category", cat);
                                    jsonParams.put("Pro_name", name);
                                    jsonParams.put("Pro_price_details", contactsObj);
                                    jsonParams.put("Pro_image", image);
                                    jsonParams.put("Pro_des", des);
                                    jsonParams.put("seller_id", userid);

                                    Log.e("printmap", "" + jsonParams.toString());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                RequestBody body =
                                        RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),
                                                jsonParams.toString());
                                Call<Bookingpre> calladdproduct = RetrofitClient.getClient("https://www.goingbo.com/").create(ApiService.class)
                                        .seller_product_insert(body);
                                calladdproduct.enqueue(new Callback<Bookingpre>() {
                                    @Override
                                    public void onResponse(Call<Bookingpre> call, Response<Bookingpre> response) {
                                        progressDialog.dismiss();
                                        if (!response.body().getError()) {
                                            if (response.body().getMessage().equals("Seller product inserted successfully")) {
                                                AlertDialog alertdialog = new AlertDialog.Builder(getContext())
                                                        .setTitle("Alert")
                                                        .setMessage("Product inserted sucessfull !")
                                                        .setCancelable(false)
                                                        .setPositiveButton("OK", (dialog, which) -> {

                                                            dialog.dismiss();
                                                            getActivity().onBackPressed();
                                                        }).create();
                                                if (!alertdialog.isShowing()) {
                                                    alertdialog.show();
                                                }

                                            }
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<Bookingpre> call, Throwable t) {
                                        progressDialog.dismiss();
                                    }
                                });
                            }
                        }
                    }

                }
            });
        } else if (typedata.equalsIgnoreCase("edit")) {
            unitlayout.setVisibility(View.VISIBLE);
            textinputlayout_product_price.setVisibility(View.GONE);
            textinputlayout_product_quantity.setVisibility(View.GONE);
            textinputlayout_seller_price.setVisibility(View.GONE);
            toolbar.setTitle("Edit Product");
            arrylistproduct.clear();
            String cname = bundle.getString("category_name");
            String productid = bundle.getString("productid");
            String name = bundle.getString("productname");
            String cat = bundle.getString("productcategories");
            String des = bundle.getString("productdescription");
            String image = bundle.getString("productimage");
            String type = bundle.getString("producttype");
            String offerprice = bundle.getString("productOffer_price");
            Gson gson = new Gson();
            TypeToken<ArrayList<UnitSeller>> token = new TypeToken<ArrayList<UnitSeller>>() {
            };

            Log.e("printdata", offerprice);
            try {
                unitlist = Arrays.asList(gson.fromJson(offerprice,
                        UnitSeller[].class));
                setUnit(productid);

            } catch (Exception e) {
                e.printStackTrace();
            }
            btn_edit_add_unit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Dialog dialog = new Dialog(getContext(), R.style.FullScreenDialog);
                    if (!dialog.isShowing()) {
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.dialog_edit_product_unit);
                        Window window = dialog.getWindow();
                        assert window != null;
                        WindowManager.LayoutParams wlp = Objects.requireNonNull(window).getAttributes();
                        wlp.gravity = Gravity.CENTER;
                        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
                        window.setAttributes(wlp);
                        Objects.requireNonNull(dialog.getWindow()).setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                        Spinner seller_spinner_unit_array = dialog.findViewById(R.id.seller_spinner_unit_array);
                        ImageView product_unit_close = dialog.findViewById(R.id.product_unit_close);
                        product_unit_close.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });
                        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                                R.array.unit_array, android.R.layout.simple_spinner_item);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        seller_spinner_unit_array.setAdapter(adapter);


                        TextInputEditText seller_weight_unit = dialog.findViewById(R.id.seller_weight_unit);

                        TextInputEditText seller_product_price = dialog.findViewById(R.id.seller_product_price);


                        TextInputEditText seller_product_offer_price = dialog.findViewById(R.id.seller_product_offer_price);

                        Button seller_save_button = dialog.findViewById(R.id.seller_save_button);
                        seller_save_button.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (seller_spinner_unit_array.getSelectedItemPosition() == 0) {
                                    AlertDialog alertdialog = new AlertDialog.Builder(getContext())
                                            .setTitle("Alert")
                                            .setMessage("Please Select Unit Type  !")
                                            .setCancelable(false)
                                            .setPositiveButton("OK", (dialog, which) -> {
                                                dialog.dismiss();

                                            }).create();
                                    if (!alertdialog.isShowing()) {
                                        alertdialog.show();
                                    }
                                } else if (seller_weight_unit.getText().toString().length() == 0) {
                                    seller_weight_unit.setError("Please enter weight unit !");
                                } else if (seller_product_price.getText().toString().length() == 0) {
                                    seller_product_price.setError("Please enter price !");
                                } else if (seller_product_offer_price.getText().toString().length() == 0) {
                                    seller_product_offer_price.setError("Please enter offer price !");
                                } else if (Double.parseDouble(seller_product_price.getText().toString()) < Double.parseDouble(seller_product_offer_price.getText().toString())) {
                                    seller_product_offer_price.setError(" Offer Price could not be greater than product price  !");
                                } else {

                                    Productdetail productdetails = new Productdetail();
                                    productdetails.setUnit(seller_weight_unit.getText().toString());
                                    productdetails.setUnittype(seller_spinner_unit_array.getSelectedItem().toString());
                                    productdetails.setPrice(seller_product_price.getText().toString());
                                    productdetails.setOfferPrice(seller_product_offer_price.getText().toString());
                                    int size = unitlist.get(0).getProductdetail().size();
                                    unitlist.get(0).getProductdetail().add(productdetails);

                                    Log.e("arraylistdataa", "" + unitlist.toString());
                                    dialog.dismiss();

                                }
                            }
                        });


                        dialog.show();
                        dialog.setCancelable(false);


                    }
                }
            });
            arrylistproduct.add(new Product(cname, categoriesid, type, cat, name, image, des));
            // Toast.makeText(getContext(), " size"+arrylistproduct.size(), Toast.LENGTH_SHORT).show();
            arrayList.clear();

            for (int k = 0; k < arrylistproduct.size(); k++) {
                producttypedata = arrylistproduct.get(k).getProType();
                add_selected_product_name.setText(arrylistproduct.get(k).getProName());
                add_selected_product_descr.setText(arrylistproduct.get(k).getProDes());
                String catid = arrylistproduct.get(k).getProCategory();
                add_selected_product_name.setEnabled(false);
                add_selected_product_descr.setEnabled(false);
                selected_product_price.setEnabled(false);
                ;
                add_selected_product_quantity.setEnabled(false);
                stringone = "https://www.goingbo.com/public/product_images/" + arrylistproduct.get(k).getProImage();
                if (!process()) {
                    progressDialog.setMessage("Loading......");
                    progressDialog.show();
                    Call<Productcategories> call = RetrofitClient.getClient("https://www.goingbo.com/").create(ApiService.class).getcategoryselected(Integer.parseInt(cat));
                    call.enqueue(new Callback<Productcategories>() {
                        @Override
                        public void onResponse(Call<Productcategories> call, Response<Productcategories> response) {
                            if (response.body() != null) {
                                Productcategories categoriesdata = response.body();
                                if (!categoriesdata.isError()) {
                                    arrayList.clear();
                                    if (categoriesdata.getCat().size() > 0) {

                                        arrayList.addAll(categoriesdata.getCat());

                                        CustomArrayAdapter adapter = new CustomArrayAdapter(getContext(), R.layout.row_spinner_layout,
                                                arrayList
                                        );
                                        add_product_categories.setAdapter(adapter);
                                        progressDialog.dismiss();
                                        if (arrayList.size() > 0) {
                                            add_product_categories.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                                @Override
                                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                                    setproducttype(categoriesid);
                                                }

                                                @Override
                                                public void onNothingSelected(AdapterView<?> parent) {

                                                }
                                            });

                                        }


                                    }
                                } else {
                                    new AlertDialog.Builder(getContext())
                                            .setTitle("Your Alert")
                                            .setMessage("Categories is not present in database !")
                                            .setCancelable(false)
                                            .setPositiveButton("OK", (dialog, which) -> {
                                                dialog.dismiss();
                                                getActivity().onBackPressed();
                                                // proDialog.dismiss();
                                            }).create().show();

                                }
                            }


                        }

                        @Override
                        public void onFailure(Call<Productcategories> call, Throwable t) {

                        }
                    });
                    Picasso.get().load(stringone).into(add_select_seller_product, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            if (progressbar_seller_product_image != null) {
                                progressbar_seller_product_image.setVisibility(View.GONE);
                            }
                        }

                        @Override
                        public void onError(Exception e) {

                        }


                    });
                }
            }


            edit_image_product.setVisibility(View.GONE);

            selected_product_submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!process()) {
                        if (producttypedata.length() == 0) {
                            setproducttype(categoriesid);
                        } else if (add_selected_product_name.getText().toString().length() == 0) {
                            add_selected_product_name.setError("Please enter product name");
                        } else if (unitlist.get(0).getProductdetail().size() == 0) {
                            AlertDialog alertdialog = new AlertDialog.Builder(getContext())
                                    .setTitle("Alert")
                                    .setMessage("Product inserted price / unit !")
                                    .setCancelable(false)
                                    .setPositiveButton("OK", (dialog, which) -> {

                                        dialog.dismiss();
                                    }).create();
                            if (!alertdialog.isShowing()) {
                                alertdialog.show();
                            }
                        } else {
                            //  $Pro_type=$jsondata['Pro_type'];
                            //                        $Pro_category=$jsondata['Pro_category'];
                            //                        $Pro_name=$jsondata['Pro_name'];
                            //                        $offer_price=$jsondata['Pro_price_details'];
                            //                        $Pro_image=$jsondata['Pro_image'];
                            //                        $Pro_des=$jsondata['Pro_des'];
                            //                        $seller_id=$jsondata['seller_id'];


                            progressDialog.show();
                            JSONObject contactsObj = new JSONObject();
                            for (int i = 0; i < unitlist.size(); i++) {
                                try {
                                    JSONArray jArray = new JSONArray();

                                    for (int l = 0; l < unitlist.get(i).getProductdetail().size(); l++) {
                                        try {
                                            JSONObject arraysObj = new JSONObject();
                                            arraysObj.put("unittype", unitlist.get(i).getProductdetail().get(l).getUnittype());
                                            arraysObj.put("unit", unitlist.get(i).getProductdetail().get(l).getUnit());
                                            arraysObj.put("price", unitlist.get(i).getProductdetail().get(l).getPrice());
                                            arraysObj.put("offer_price", unitlist.get(i).getProductdetail().get(l).getOfferPrice());
                                            jArray.put(arraysObj);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    contactsObj.put("productid", unitlist.get(0).getProductid());
                                    contactsObj.put("productdetail", jArray);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            Log.e("jsonobject", "" + contactsObj.toString());
                            JSONObject jsonParams = new JSONObject();
//put something inside the map, could be null
                            try {
                                jsonParams.put("Pro_type", type);
                                jsonParams.put("productid", productid);
                                jsonParams.put("Pro_category", cat);
                                jsonParams.put("Pro_name", name);
                                jsonParams.put("Pro_price_details", contactsObj);
                                jsonParams.put("Pro_image", image);
                                jsonParams.put("Pro_des", des);
                                jsonParams.put("seller_id", userid);

                                Log.e("printmap", "" + jsonParams.toString());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            RequestBody body =
                                    RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),
                                            jsonParams.toString());
//serviceCaller is the interface initialized with retrofit.create...
                            Call<Bookingpre> calladdproduct = RetrofitClient.getClient("https://www.goingbo.com/").create(ApiService.class)
                                    .sendjsonobject(body);
                            calladdproduct.enqueue(new Callback<Bookingpre>() {
                                @Override
                                public void onResponse(Call<Bookingpre> call, Response<Bookingpre> response) {
                                    progressDialog.dismiss();
                                    if (!response.body().getError()) {
                                        if (response.body().getMessage().equals("Seller product inserted successfully")) {
                                            AlertDialog alertdialog = new AlertDialog.Builder(getContext())
                                                    .setTitle("Alert")
                                                    .setMessage("Product inserted sucessfull !")
                                                    .setCancelable(false)
                                                    .setPositiveButton("OK", (dialog, which) -> {

                                                        dialog.dismiss();
                                                        getActivity().onBackPressed();
                                                    }).create();
                                            if (!alertdialog.isShowing()) {
                                                alertdialog.show();
                                            }

                                        }
                                    }
                                }

                                @Override
                                public void onFailure(Call<Bookingpre> call, Throwable t) {
                                    progressDialog.dismiss();
                                }
                            });
                        }
                    }

                }
            });

        }

        return view;
    }

    private void setUnit(String productid) {
        if (unitlist.size() > 0) {

            Edit_Product_Unit_Recyclerview edit_product_unit_recyclerview = new Edit_Product_Unit_Recyclerview(getContext(), unitlist.get(0).getProductdetail(), new SelectedAdapterInterface() {
                @Override
                public void getAdapter(Integer position, String resultindex) {
                    if (resultindex.equalsIgnoreCase("edit")) {
                        updatelist(position, unitlist.get(0).getProductdetail().get(position).getUnittype(), productid);

                        //

                    } else if (resultindex.equalsIgnoreCase("remove")) {
                        if (unitlist.get(0).getProductdetail().size() > 0) {
                            unitlist.get(0).getProductdetail().remove(unitlist.get(0).getProductdetail().get(position));
                            //  Toast.makeText(getContext(), "" + unitlist.get(0).getProductdetail().size(), Toast.LENGTH_SHORT).show();
                            edit_unit_recyclerView.removeViewAt(position);
                            setUnit(productid);
                        }

                    }


                }
            });
            edit_unit_recyclerView.setAdapter(edit_product_unit_recyclerview);
            edit_product_unit_recyclerview.notifyDataSetChanged();
        }
    }

    private void updatelist(Integer position, String compareValue, String productid) {
        Dialog dialog = new Dialog(getContext(), R.style.FullScreenDialogWithStatusBarColorAccent);
        if (!dialog.isShowing()) {
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_edit_product_unit);
            Window window = dialog.getWindow();
            assert window != null;
            WindowManager.LayoutParams wlp = Objects.requireNonNull(window).getAttributes();
            wlp.gravity = Gravity.CENTER;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
            window.setAttributes(wlp);
            Objects.requireNonNull(dialog.getWindow()).setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            Spinner seller_spinner_unit_array = dialog.findViewById(R.id.seller_spinner_unit_array);
            ImageView product_unit_close = dialog.findViewById(R.id.product_unit_close);
            product_unit_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                    R.array.unit_array, android.R.layout.simple_spinner_item);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            seller_spinner_unit_array.setAdapter(adapter);

            if (compareValue != null) {
                int spinnerPosition = adapter.getPosition(compareValue);
                Log.e("dataspinner", "" + spinnerPosition);
                seller_spinner_unit_array.setSelection(spinnerPosition);
            }
            TextInputEditText seller_weight_unit = dialog.findViewById(R.id.seller_weight_unit);
            seller_weight_unit.setText(unitlist.get(0).getProductdetail().get(position).getUnit());
            TextInputEditText seller_product_price = dialog.findViewById(R.id.seller_product_price);
            seller_product_price.setText(unitlist.get(0).getProductdetail().get(position).getPrice());

            TextInputEditText seller_product_offer_price = dialog.findViewById(R.id.seller_product_offer_price);
            seller_product_offer_price.setText(unitlist.get(0).getProductdetail().get(position).getOfferPrice());
            Button seller_save_button = dialog.findViewById(R.id.seller_save_button);
            seller_save_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (seller_spinner_unit_array.getSelectedItemPosition() == 0) {
                        AlertDialog alertdialog = new AlertDialog.Builder(getContext())
                                .setTitle("Alert")
                                .setMessage("Please Select Unit Type  !")
                                .setCancelable(false)
                                .setPositiveButton("OK", (dialog, which) -> {
                                    dialog.dismiss();
                                }).create();
                        if (!alertdialog.isShowing()) {
                            alertdialog.show();
                        }
                    } else if (seller_weight_unit.getText().toString().length() == 0) {
                        seller_weight_unit.setError("Please enter weight unit !");
                    } else if (seller_product_price.getText().toString().length() == 0) {
                        seller_product_price.setError("Please enter price !");
                    } else if (seller_product_offer_price.getText().toString().length() == 0) {
                        seller_product_offer_price.setError("Please enter offer price !");
                    } else if (Double.parseDouble(seller_product_price.getText().toString()) < Double.parseDouble(seller_product_offer_price.getText().toString())) {
                        seller_product_offer_price.setError(" Offer price could not be greater than product price  !");
                    } else {
                        Productdetail productdetails = new Productdetail();
                        productdetails.setUnit(seller_weight_unit.getText().toString());
                        productdetails.setUnittype(seller_spinner_unit_array.getSelectedItem().toString());
                        productdetails.setPrice(seller_product_price.getText().toString());
                        productdetails.setOfferPrice(seller_product_offer_price.getText().toString());

                        int size = unitlist.get(0).getProductdetail().size();

                        // unitlist.get(0).getProductdetail().remove(unitlist.get(0).getProductdetail().get(position));
                        if (size > 0) {
                            unitlist.get(0).getProductdetail().set(size - 1, productdetails);
                        } else {
                            List<Productdetail> sublist = new ArrayList<>();
                            sublist.add(productdetails);
                            unitlist.add(new UnitSeller(productid, sublist));
                        }
                        Log.e("arraylistdatap", "" + unitlist.toString());
                        if (unitlist.size() > 0) {

                            Edit_Product_Unit_Recyclerview edit_product_unit_recyclerview = new Edit_Product_Unit_Recyclerview(getContext(), unitlist.get(0).getProductdetail(), new SelectedAdapterInterface() {
                                @Override
                                public void getAdapter(Integer position, String resultindex) {
                                    if (resultindex.equalsIgnoreCase("edit")) {
                                        updatelist(position, unitlist.get(0).getProductdetail().get(position).getUnittype(), productid);

                                        //

                                    } else if (resultindex.equalsIgnoreCase("remove")) {
                                        if (unitlist.get(0).getProductdetail().size() > 0) {
                                            unitlist.get(0).getProductdetail().remove(unitlist.get(0).getProductdetail().get(position));
                                            //  Toast.makeText(getContext(), "" + unitlist.get(0).getProductdetail().size(), Toast.LENGTH_SHORT).show();
                                            edit_unit_recyclerView.removeViewAt(position);
                                            setUnit(productid);
                                        }

                                    }


                                }
                            });
                            edit_unit_recyclerView.setAdapter(edit_product_unit_recyclerview);
                            edit_product_unit_recyclerview.notifyDataSetChanged();
                        }
                        // setUnit(unitlist.get(0).getProductid());
                        dialog.dismiss();
                    }
                }
            });


            dialog.show();
            dialog.setCancelable(false);


        }
    }

    private void setproducttype(String id) {
        // progressDialog.show();
        if (!process()) {
            Call<ProductSellerTypeData> callproduct = RetrofitClient.getClient("https://www.goingbo.com/").create(ApiService.class)
                    .getproducttype(id);
            callproduct.enqueue(new Callback<ProductSellerTypeData>() {
                @Override
                public void onResponse(Call<ProductSellerTypeData> call, Response<ProductSellerTypeData> response) {
                    if (response.body() != null) {
                        ProductSellerTypeData productTypeData = response.body();
                        if (!productTypeData.isError()) {


                            if (productTypeData.getTypecat().size() > 0) {
                                for (int v = 0; v < productTypeData.getTypecat().size(); v++) {
                                    producttypedata = productTypeData.getTypecat().get(v).getProType();
                                }
                                //  progressDialog.dismiss();
                            }
                        }
                    }


                }

                @Override
                public void onFailure(Call<ProductSellerTypeData> call, Throwable t) {

                }
            });
        }
    }

    private void selectImage() {

        final CharSequence[] items = {"Choose from library",
                "Cancel"};

        AlertDialog.Builder builders = new AlertDialog.Builder(getContext());
        builders.setTitle("Change postad Photo");
        builders.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

//                if (items[item].equals("Take Photo")) {
//
//                    cameraIntent("image");
//
//                } else
                if (items[item].equals("Choose from library")) {

                    galleryIntent("image");

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builders.show();
    }

    private void cameraIntent(String datatype) {
        if (permissionCount == 0) {
            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) !=
                    PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions((Activity) getContext(), new String[]
                                {Manifest.permission.CAMERA},
                        CAMERA_REQUEST);
            } else {
                permissionCount++;
                if (ContextCompat.checkSelfPermission
                        (getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                        PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions((Activity) getContext(), new String[]
                                    {Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            REQUEST_PERMISSION_WRITE_EXTERNAL_STORAGE);
                } else
                    permissionCount++;

                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, CAMERA_REQUEST);

            }


        } else {
            if (datatype == "image") {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, CAMERA_REQUEST);
            }

        }
    }

    private void galleryIntent(String datatype) {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto, GALLERY_PICTURE);
        // startActivityForResult(Intent.createChooser(intent, "Select File"),GALLERY_PICTURE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == CAMERA_REQUEST) {
//            if (resultCode == Activity.RESULT_OK) {
//
//
//                onCaptureImageResult(data);
//
//            }
//
//        }


        if (requestCode == GALLERY_PICTURE) {
            onSelectFromGalleryResult(data);
        }
    }

    private void onCaptureImageResult(Intent data) {
        //    Toast.makeText(getContext(), "Camera", Toast.LENGTH_SHORT).show();
        String partFilename = currentDateFormat();
        File file = storeCameraPhotoInSDCard((Bitmap) data.getExtras().get("data"), partFilename);
        stringone = file.getAbsolutePath();
        String valuepath = getImageUri(getContext(), (Bitmap) data.getExtras().get("data"));
        Log.e("imagepath", stringone);
        Bitmap bitmap = (Bitmap) data.getExtras().get("data");
        add_select_seller_product.setImageBitmap((Bitmap) bitmap);


    }

    public String getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return path;
    }

    private String currentDateFormat() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HH_mm_ss");
        String currentTimeStamp = dateFormat.format(new Date());
        return currentTimeStamp;
    }

    private File storeCameraPhotoInSDCard(Bitmap bitmap, String currentDate) {
        // Create an image file name
        File outputFile;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            outputFile = new File(Environment.getExternalStorageDirectory(), "photo_" + currentDate + ".jpg");
        } else {
            outputFile = new File(getContext().getExternalCacheDir(), "photo_" + currentDate + ".jpg");
        }
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(outputFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();
            return outputFile;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    private void onSelectFromGalleryResult(Intent data) {
        try {

            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            assert selectedImage != null;
            Cursor cursor = Objects.requireNonNull(getActivity()).getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            assert cursor != null;
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String filePath = cursor.getString(columnIndex);
            cursor.close();


            int nh = (int) (BitmapFactory.decodeFile(filePath).getHeight() * (512.0 / BitmapFactory.decodeFile(filePath).getWidth()));
            Bitmap scaled = Bitmap.createScaledBitmap(BitmapFactory.decodeFile(filePath), 512, nh, true);
            String path = getRealPathFromURIPath(selectedImage, getActivity());
            //  add_select_seller_product.setImageBitmap(BitmapFactory.decodeFile(picturePath));
            if (path.length() > 2) {

                progressbar_seller_product_image.setVisibility(View.GONE);


                add_select_seller_product.setImageBitmap(scaled);

                stringone = path;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void init() {
        add_select_seller_product = view.findViewById(R.id.product_image_uploading);
        edit_image_product = view.findViewById(R.id.edit_image_product);
        add_selected_product_name = view.findViewById(R.id.add_selected_product_name);
        add_selected_product_descr = view.findViewById(R.id.add_selected_product_descr);
        add_selected_product_quantity = view.findViewById(R.id.add_selected_product_quantity);
        selected_product_price = view.findViewById(R.id.selected_product_price);
        selected_selling_price = view.findViewById(R.id.selected_selling_price);
        selected_product_submit = view.findViewById(R.id.selected_product_submit);
        add_product_categories = view.findViewById(R.id.add_product_categories);
        btn_edit_add_unit = view.findViewById(R.id.btn_edit_add_unit);
        edit_unit_recyclerView = view.findViewById(R.id.edit_unit_recyclerView);
        progressbar_seller_product_image = view.findViewById(R.id.progressbar_seller_product_image);

        textinputlayout_product_price = view.findViewById(R.id.textinputlayout_product_price);
        textinputlayout_seller_price = view.findViewById(R.id.textinputlayout_seller_price);
        textinputlayout_product_quantity = view.findViewById(R.id.textinputlayout_product_quantity);
        linear_layout_unit = view.findViewById(R.id.linear_layout_unit);
        unitlayout = view.findViewById(R.id.unitlayout);
        edit_unit_recyclerView.setHasFixedSize(false);
        edit_unit_recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

    }

    private boolean process() {
        boolean show = true;
        if (checkConnectivity.isConnected()) {
            show = false;

        } else {

            showDialog();
            show = true;
        }
        return show;
    }

    private void showDialog() {

        Dialog dialog = new Dialog(getContext(), R.style.FullScreenDialogWithStatusBarColorAccent);
        if (!dialog.isShowing()) {
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.no_internet);
            Window window = dialog.getWindow();
            assert window != null;
            WindowManager.LayoutParams wlp = Objects.requireNonNull(window).getAttributes();
            wlp.gravity = Gravity.CENTER;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
            window.setAttributes(wlp);
            Objects.requireNonNull(dialog.getWindow()).setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            Button wifi_btn = dialog.findViewById(R.id.wifi_btn);
            Button mobile_data_btn = dialog.findViewById(R.id.mobile_data_btn);
            dialog.show();
            dialog.setCancelable(false);
            wifi_btn.setOnClickListener(v -> {
                dialog.dismiss();
                isshowing = false;
                WifiManager wifiMgr = (WifiManager) getActivity().getApplicationContext().getSystemService(Context.WIFI_SERVICE);

                assert wifiMgr != null;
                if (wifiMgr.isWifiEnabled()) { // Wi-Fi adapter is ON

                    WifiInfo wifiInfo = wifiMgr.getConnectionInfo();

                    if (wifiInfo.getNetworkId() == -1) {

                        showDialog();
                        // Not connected to an access point
                    } else {
                        dialog.dismiss();
                        process();
                    }
                    // Connected to an access point
                } else {
                    startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS)); // Wi-Fi adapter is OFF
                }
            });
            mobile_data_btn.setOnClickListener(v -> {
                dialog.dismiss();
                isshowing = false;
                startActivityForResult(new Intent(android.provider.Settings.ACTION_DATA_ROAMING_SETTINGS), 0);
                if (checkConnectivity.isConnected()) {
                    process();

                } else {
                    showDialog();
                }
            });
        }

    }

    private String getRealPathFromURIPath(Uri contentURI, Activity activity) {
        try {
            Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
            if (cursor == null) {
                return contentURI.getPath();
            } else {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                return cursor.getString(idx);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return "";
        }
    }
}
