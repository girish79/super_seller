package com.Goingbo.Superseller.Model;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class User implements Serializable
{

    @SerializedName("Clid")
    @Expose
    private String clid;
    @SerializedName("C_name")
    @Expose
    private String cName;
    @SerializedName("C_emailid")
    @Expose
    private String cEmailid;
    @SerializedName("C_mobile_no")
    @Expose
    private String cMobileNo;
    @SerializedName("C_type")
    @Expose
    private String cType;
    private final static long serialVersionUID = 2172239387077043383L;

    /**
     * No args constructor for use in serialization
     *
     */
    public User() {
    }

    /**
     *
     * @param cName
     * @param cType
     * @param clid
     * @param cMobileNo
     * @param cEmailid
     */
    public User(String clid, String cName, String cEmailid, String cMobileNo, String cType) {
        super();
        this.clid = clid;
        this.cName = cName;
        this.cEmailid = cEmailid;
        this.cMobileNo = cMobileNo;
        this.cType = cType;
    }

    public String getClid() {
        return clid;
    }

    public void setClid(String clid) {
        this.clid = clid;
    }

    public User withClid(String clid) {
        this.clid = clid;
        return this;
    }

    public String getCName() {
        return cName;
    }

    public void setCName(String cName) {
        this.cName = cName;
    }

    public User withCName(String cName) {
        this.cName = cName;
        return this;
    }

    public String getCEmailid() {
        return cEmailid;
    }

    public void setCEmailid(String cEmailid) {
        this.cEmailid = cEmailid;
    }

    public User withCEmailid(String cEmailid) {
        this.cEmailid = cEmailid;
        return this;
    }

    public String getCMobileNo() {
        return cMobileNo;
    }

    public void setCMobileNo(String cMobileNo) {
        this.cMobileNo = cMobileNo;
    }

    public User withCMobileNo(String cMobileNo) {
        this.cMobileNo = cMobileNo;
        return this;
    }

    public String getCType() {
        return cType;
    }

    public void setCType(String cType) {
        this.cType = cType;
    }

    public User withCType(String cType) {
        this.cType = cType;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("clid", clid).append("cName", cName).append("cEmailid", cEmailid).append("cMobileNo", cMobileNo).append("cType", cType).toString();
    }

}