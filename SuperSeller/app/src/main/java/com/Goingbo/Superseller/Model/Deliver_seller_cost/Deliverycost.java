package com.Goingbo.Superseller.Model.Deliver_seller_cost;
import java.io.Serializable;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Deliverycost implements Serializable, Parcelable
{

    @SerializedName("delivery_charge")
    @Expose
    private String deliveryCharge;
    public final static Parcelable.Creator<Deliverycost> CREATOR = new Creator<Deliverycost>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Deliverycost createFromParcel(Parcel in) {
            return new Deliverycost(in);
        }

        public Deliverycost[] newArray(int size) {
            return (new Deliverycost[size]);
        }

    }
            ;
    private final static long serialVersionUID = -1293825165134854616L;

    protected Deliverycost(Parcel in) {
        this.deliveryCharge = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public Deliverycost() {
    }

    /**
     *
     * @param deliveryCharge
     */
    public Deliverycost(String deliveryCharge) {
        super();
        this.deliveryCharge = deliveryCharge;
    }

    public String getDeliveryCharge() {
        return deliveryCharge;
    }

    public void setDeliveryCharge(String deliveryCharge) {
        this.deliveryCharge = deliveryCharge;
    }

    public Deliverycost withDeliveryCharge(String deliveryCharge) {
        this.deliveryCharge = deliveryCharge;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("deliveryCharge", deliveryCharge).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(deliveryCharge).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Deliverycost) == false) {
            return false;
        }
        Deliverycost rhs = ((Deliverycost) other);
        return new EqualsBuilder().append(deliveryCharge, rhs.deliveryCharge).isEquals();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(deliveryCharge);
    }

    public int describeContents() {
        return 0;
    }

}