package com.Goingbo.Superseller.Adapter;

import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.Goingbo.Superseller.Interface.SelectedAdapterInterface;
import com.Goingbo.Superseller.Model.NewOrderLISTSeller.Sellerorderlist;
import com.Goingbo.Superseller.Model.NewOrder_List.Userorderlist;
import com.Goingbo.Superseller.Model.SellerUorder.Uorderlist;
import com.Goingbo.Superseller.Model.seller_unit.Productdetail;
import com.Goingbo.Superseller.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Seller_order_List_Adpater extends RecyclerView.Adapter<Seller_order_List_Adpater.Seller_Order_View > {
    Context context;
    ArrayList<Sellerorderlist> arrayList;
   // SelectedAdapterInterface selectedAdapterInterface;

    public Seller_order_List_Adpater(Context context, ArrayList<Sellerorderlist> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
      //  this.selectedAdapterInterface = selectedAdapterInterface;
    }

    @NonNull
    @Override
    public Seller_Order_View onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.row_seller_order_list,parent,false);
        return new Seller_Order_View(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Seller_Order_View holder, int position) {
        String imagelink="https://www.goingbo.com/public/product_images/"+arrayList.get(position).getProImage();
        Picasso.get().load(imagelink).into(holder.seller_order_image);
        String dataname=arrayList.get(position).getProName().substring(0,1).toUpperCase()+arrayList.get(position).getProName().substring(1);

        holder.seller_order_item.setText(Html.fromHtml(dataname));
        String datanames=arrayList.get(position).getName().substring(0,1).toUpperCase()+arrayList.get(position).getName().substring(1);

        holder.seller_order_user_name.setText(datanames);
        String address=""+arrayList.get(position).getPlotNumber()+","+arrayList.get(position).getBlock()+","+
                arrayList.get(position).getStreet()+","+arrayList.get(position).getLandmark()+","+arrayList.get(position).getCity()+","+arrayList.get(position).getState()+","+
                arrayList.get(position).getZip();
        holder.seller_order_address.setText(address);
        holder.seller_order_user_phone.setText(arrayList.get(position).getPhone());

        Gson gson = new Gson();
        TypeToken<ArrayList<Productdetail>> token = new TypeToken<ArrayList<Productdetail>>() {
        };
        String reader="["+arrayList.get(position).getOrderdetails()+"]";
        Log.e("printdata",reader);
        try {
            List<Productdetail> unitlist = Arrays.asList(gson.fromJson(reader,
                    Productdetail[].class));
        holder.seller_order_quantity.setText(""+arrayList.get(position).getProQuentity());
        holder.seller_order_price.setText(unitlist.get(0).getOfferPrice());
        } catch (Exception e){e.printStackTrace();}
        Log.e("deilivery",arrayList.get(position).getOrderStatus());
        Log.e("deilivery",""+arrayList.get(position).getOrderStatus().length());
//        if(arrayList.get(position).getOrderStatus().trim().equalsIgnoreCase("Order Has Been Accepted")){
//            holder.seller_order_accept.setText("Update Status");
//        }else if(arrayList.get(position).getOrderStatus().trim().equalsIgnoreCase("Order Shipped")){
//            holder.seller_order_accept.setText("Update Status");
//        }else if (arrayList.get(position).getOrderStatus().trim().equalsIgnoreCase("Order In Route")){
//            holder.seller_order_accept.setText("Update Status");
//        } else {
//            holder.seller_order_accept.setText("Accept Order");
//        }
        holder.seller_order_accept.setVisibility(View.GONE);
//        holder.seller_order_accept.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                selectedAdapterInterface.getAdapter(position,"Accept");
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class Seller_Order_View extends RecyclerView.ViewHolder {
        ImageView seller_order_image;
        TextView seller_order_item,seller_order_user_name,seller_order_address,seller_order_quantity,seller_order_price;
        Button seller_order_accept;
        TextView seller_order_unit;
        TextView seller_order_user_phone;
        public Seller_Order_View(@NonNull View itemView) {
            super(itemView);
            seller_order_image=itemView.findViewById(R.id.seller_order_image);
            seller_order_item=itemView.findViewById(R.id.seller_order_item);
            seller_order_user_name=itemView.findViewById(R.id.seller_order_user_name);
            seller_order_address=itemView.findViewById(R.id.seller_order_address);
            seller_order_quantity=itemView.findViewById(R.id.seller_order_quantity);
            seller_order_price=itemView.findViewById(R.id.seller_order_price);
            seller_order_accept=itemView.findViewById(R.id.seller_order_accept);
            seller_order_unit=itemView.findViewById(R.id.seller_order_unit);
            seller_order_user_phone=itemView.findViewById(R.id.seller_order_user_phone);
        }
    }
}
