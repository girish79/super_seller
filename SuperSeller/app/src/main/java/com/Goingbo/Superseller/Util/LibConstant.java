package com.Goingbo.Superseller.Util;

public class LibConstant {
    public static final String Userdatabase = "UserInfo";
    public static final String status = "status";
    public static final String Name = "name";
    public static final String Email = "email";
    public static final String socialid="socialid";
    public static final String IMAGE = "user_image";
    public static final String Phone = "phone";
    public static final String Address = "address";
    public static final String City = "city";
    public static final String State = "state";
    public static final String Country = "country";
    public static final String postal_code = "postal_code";
    public static final String Otp = "otp";
    public static final String PASSWORD = "password";
    public static final String COMPANYTAXNUMBER = "company_tax_number";
    public static final String COMPANYID = "company_id";
    public static final String ACCOUNT = "account";
    public static final String IFSC = "ifsc";
    public static final String NOTIFICATION = "notification";
    public static final String USER_TYPE="USER_TYPE";
    public static final String USER_ID="USER_ID";
    public static final String Age="AGE";
    public static final String Type="TYPE";
    public static final String phonelog="PHONELOGIN";
    public static final String verfiynumber="VERFIYNUMBER";
    public static final String otpnumber="OTPNUMBER";
    public static final String lat="LAT";
    public static final String lon="LONGTTUBE";
    public  static final String  UAddressid="UserAddressid";
    public  static final String  RADDRESS="RestaurantAddress";
    public  static final String  Rcontact="Restaurantcontact";
    public  static final String  RNAME="RestaurantName";
    public static final String   Rimage="RestaurantImage";
}
