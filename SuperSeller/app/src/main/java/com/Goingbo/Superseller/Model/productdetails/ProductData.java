package com.Goingbo.Superseller.Model.productdetails;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.List;

public class ProductData implements Serializable, Parcelable
{

    @SerializedName("error")
    @Expose
    private boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("product")
    @Expose
    private List<Product> product = null;
    public final static Creator<ProductData> CREATOR = new Creator<ProductData>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ProductData createFromParcel(Parcel in) {
            return new ProductData(in);
        }

        public ProductData[] newArray(int size) {
            return (new ProductData[size]);
        }

    }
            ;
    private final static long serialVersionUID = 5789460438114582977L;

    protected ProductData(Parcel in) {
        this.error = ((boolean) in.readValue((boolean.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.product, (Product.class.getClassLoader()));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public ProductData() {
    }

    /**
     *
     * @param product
     * @param error
     * @param message
     */
    public ProductData(boolean error, String message, List<Product> product) {
        super();
        this.error = error;
        this.message = message;
        this.product = product;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public ProductData withError(boolean error) {
        this.error = error;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ProductData withMessage(String message) {
        this.message = message;
        return this;
    }

    public List<Product> getProduct() {
        return product;
    }

    public void setProduct(List<Product> product) {
        this.product = product;
    }

    public ProductData withProduct(List<Product> product) {
        this.product = product;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("error", error).append("message", message).append("product", product).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(product).append(error).append(message).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ProductData) == false) {
            return false;
        }
        ProductData rhs = ((ProductData) other);
        return new EqualsBuilder().append(product, rhs.product).append(error, rhs.error).append(message, rhs.message).isEquals();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(error);
        dest.writeValue(message);
        dest.writeList(product);
    }

    public int describeContents() {
        return 0;
    }

}