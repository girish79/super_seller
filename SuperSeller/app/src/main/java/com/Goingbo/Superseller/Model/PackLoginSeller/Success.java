package com.Goingbo.Superseller.Model.PackLoginSeller;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class Success implements Serializable, Parcelable
{

    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("message")
    @Expose
    private String message;
    public final static Creator<Success> CREATOR = new Creator<Success>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Success createFromParcel(Parcel in) {
            return new Success(in);
        }

        public Success[] newArray(int size) {
            return (new Success[size]);
        }

    }
            ;
    private final static long serialVersionUID = -1497449094632137634L;

    protected Success(Parcel in) {
        this.user = ((User) in.readValue((User.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public Success() {
    }

    /**
     *
     * @param message
     * @param user
     */
    public Success(User user, String message) {
        super();
        this.user = user;
        this.message = message;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Success withUser(User user) {
        this.user = user;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Success withMessage(String message) {
        this.message = message;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("user", user).append("message", message).toString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(user);
        dest.writeValue(message);
    }

    public int describeContents() {
        return 0;
    }

}