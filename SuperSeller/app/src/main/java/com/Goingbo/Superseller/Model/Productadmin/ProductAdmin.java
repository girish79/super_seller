package com.Goingbo.Superseller.Model.Productadmin;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.List;

public class ProductAdmin implements Serializable, Parcelable
{

    @SerializedName("error")
    @Expose
    private boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("productseller")
    @Expose
    private List<Productseller> productseller = null;
    public final static Creator<ProductAdmin> CREATOR = new Creator<ProductAdmin>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ProductAdmin createFromParcel(Parcel in) {
            return new ProductAdmin(in);
        }

        public ProductAdmin[] newArray(int size) {
            return (new ProductAdmin[size]);
        }

    }
            ;
    private final static long serialVersionUID = 6118879275751697057L;

    protected ProductAdmin(Parcel in) {
        this.error = ((boolean) in.readValue((boolean.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.productseller, (Productseller.class.getClassLoader()));
    }

    public ProductAdmin() {
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public ProductAdmin withError(boolean error) {
        this.error = error;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ProductAdmin withMessage(String message) {
        this.message = message;
        return this;
    }

    public List<Productseller> getProductseller() {
        return productseller;
    }

    public void setProductseller(List<Productseller> productseller) {
        this.productseller = productseller;
    }

    public ProductAdmin withProductseller(List<Productseller> productseller) {
        this.productseller = productseller;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("error", error).append("message", message).append("productseller", productseller).toString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(error);
        dest.writeValue(message);
        dest.writeList(productseller);
    }

    public int describeContents() {
        return 0;
    }

}