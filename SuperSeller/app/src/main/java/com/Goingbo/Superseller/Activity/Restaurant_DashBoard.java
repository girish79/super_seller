package com.Goingbo.Superseller.Activity;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NotificationCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.app.Dialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.Goingbo.Superseller.R;
import com.Goingbo.Superseller.Util.CheckConnectivity;
import com.Goingbo.Superseller.Util.LibConstant;
import com.Goingbo.Superseller.Util.SessionManager;
import com.Goingbo.Superseller.fragment.Restaurant_dashboard_Fragment;
import com.Goingbo.Superseller.fragment.Select_Product;
import com.Goingbo.Superseller.fragment.Seller_cancel_order;
import com.Goingbo.Superseller.fragment.Seller_daashboard_fragment;
import com.Goingbo.Superseller.fragment.Seller_delivery_order_fragment;
import com.Goingbo.Superseller.fragment.Seller_order_list;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.material.navigation.NavigationView;
import com.squareup.picasso.Picasso;

import org.json.JSONException;

import java.util.HashMap;
import java.util.Objects;

public class Restaurant_DashBoard extends AppCompatActivity {
    private NavigationView navigationView;
    private DrawerLayout drawer;
    private CheckConnectivity checkConnectivity;
    private GoogleApiClient mGoogleApiClient;
    private SessionManager session;
    private InputMethodManager imm;
    private View navHeader;
    private TextView txtName;
    private Toolbar toolbar;
    public static int navItemIndex = 0;
    public TextView address_restaurant;
    ImageView imageView;
    boolean isshowing=true;
    FragmentManager fragmentManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant__dash_board);
       init();
    }

    public void init(){
        session = new SessionManager(getApplicationContext());
        fragmentManager=getSupportFragmentManager();

        drawer=findViewById(R.id.restaurant_dashboard);
        imm =(InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
        checkConnectivity = new CheckConnectivity(Restaurant_DashBoard.this);
        navigationView = findViewById(R.id.nav_restaurant_dashboard);
        toolbar = findViewById(R.id.title_restaurant_dashboard);


        toolbar.setTitle("Dashboard");
        fragmentManager.beginTransaction().replace(R.id.restaurant_fragment_dashboard,new Restaurant_dashboard_Fragment()
                ,"Dashboard").commit();
        navigationView.setNavigationItemSelectedListener(menuItem -> {
            switch (menuItem.getItemId()) {
                case R.id.nav_restaurant_home:
                    fragmentManager.beginTransaction().replace(R.id.restaurant_fragment_dashboard,new Restaurant_dashboard_Fragment()
                            ,"Dashboard").commit();
                    break;
                case R.id.nav_restaurant_add_menu:
               Intent intentaddmenu = new Intent(Restaurant_DashBoard.this,Restuarant_Detail_forum.class);
               startActivity(intentaddmenu);
               break;
                case R.id.nav_restaurant_terms:
                    toolbar.setTitle("Terms & Condition");
                    Intent terms = new Intent(Restaurant_DashBoard.this,terms_Activity.class);
                    startActivity(terms);
                    break;

                case R.id.nav_restaurant_rate_us:

                    if (!process()) {
                        Uri uri = Uri.parse("market://details?id=" + getPackageName());
                        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                        try {
                            startActivity(goToMarket);
                        } catch (ActivityNotFoundException e) {
                            startActivity(new Intent(Intent.ACTION_VIEW,
                                    Uri.parse("http://play.google.com/store/apps/details?id=" + getPackageName())));
                        }
                    }
                    break;
                default:
                    drawer.closeDrawers();
                    break;
            }
            //Checking if the item is in checked state or not, if not make it in checked state
            if (menuItem.isChecked()) {
                menuItem.setChecked(false);
            } else {
                menuItem.setChecked(true);
            }
            menuItem.setChecked(true);
            drawer.closeDrawers();
            return true;
        });
        navHeader = navigationView.getHeaderView(0);
        txtName = navHeader.findViewById(R.id.clientusername);
        address_restaurant = navHeader.findViewById(R.id.address_restaurant);
        imageView = navHeader.findViewById(R.id.imageView);
        HashMap<String, String> hashmap = session.getrestaurantaddress();
        if (hashmap.get(LibConstant.RNAME) != null) {
            txtName.setText("" + hashmap.get(LibConstant.RNAME));
        } else {
            txtName.setText("");
        }
        if(hashmap.get(LibConstant.Rimage)!=null){
            Picasso.get().load(""+hashmap.get(LibConstant.Rimage)).into(imageView);
        }
        if(hashmap.get(LibConstant.RADDRESS)!=null){
            address_restaurant.setVisibility(View.VISIBLE);
            address_restaurant.setText(""+hashmap.get(LibConstant.RADDRESS));
        }

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(Restaurant_DashBoard.this, drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawer.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.white));
        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
        ImageView edit_profile = navHeader.findViewById(R.id.edit_profile);
        edit_profile.setOnClickListener(v -> {
            drawer.closeDrawers();
            startActivity(new Intent(Restaurant_DashBoard.this, Restuarant_Info_Activity.class));
        });
        navigationView.getMenu().getItem(0).setChecked(true);
    }
    @Override
    protected void onResume() {
        super.onResume();
        init();
//        shownotification("Girish","BEST DEVELOPER AT GOINBO INSYSTA DATASOFT PVT LTD","1");
    }
//    public void shownotification(final String title, final String message, final String user_id) {
//
//        Intent notificationIntent = new Intent(this, Restaurant_DashBoard.class);
//        notificationIntent.putExtra("notify", "notify");
////notificationIntent.putExtra("ffgg",Intent.getIntent());
//        Log.d("gfgfsahi", user_id);
//
//        PendingIntent notificationPendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
//        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "notification").setContentTitle(title).setSmallIcon(R.drawable.goinbo_mainlogo).setAutoCancel(true)
//                .setContentText(message)
//        .setWhen(System.currentTimeMillis())
//                .setContentIntent(notificationPendingIntent);
//
//        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//        notificationManager.notify(1, builder.build());
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            builder.setChannelId("com.example.snaparts");
//        }
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            NotificationChannel channel = new NotificationChannel(getPackageName(), "GoingBO",
//                    NotificationManager.IMPORTANCE_DEFAULT);
//            if (notificationManager != null) {
//                notificationManager.createNotificationChannel(channel);
//            }
//        }
//        notificationManager.notify(1,builder.build());
////        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://apanews.net/designoweb_api/index.php/services/singleRead"+"?id="+user_id, new Response.Listener<String>() {
////            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
////            @Override
////            public void onResponse(String response) {
////                Log.d("ndculddddturfdfdofnse", response);
////                try {
////                    JSONObject jsonObject = new JSONObject(response);
////                    JSONArray jsonArray = jsonObject.getJSONArray("data");
////                    JSONObject jsonObject1 = jsonArray.getJSONObject(0);
////                    String date=jsonObject1.getString("published");
////                    String tilte=jsonObject1.getString("title");
////                    s          = jsonObject1.getString("disk_image");
////                    subtitlee   = jsonObject1.getString("subtitle");
////                    content    = jsonObject1.getString("content");
////                    self      = jsonObject1.getString("self_explanatory");
////                    author    = jsonObject1.getString("post_scriptum");
////                    id        = jsonObject1.getString("id");
////                    if( subtitlee!=null) {
////                        Intent notificationIntent = new Intent(MyFirebaseMessagingService.this, CultureSub.class);
////
////
////                        Log.d("dfdfdfdfdfgtuiukn", user_id);
////                        notificationIntent.putExtra("content", content);
////                        notificationIntent.putExtra("update_date", date);
////                        notificationIntent.putExtra("image", s);
////                        notificationIntent.putExtra("self", self);
////                        notificationIntent.putExtra("author", author);
////                        notificationIntent.putExtra("idd", id);
////                        notificationIntent.putExtra("subtitle", subtitlee);
////                        //Log.d("fidnbfdfete", subtitlee);
////                        notificationIntent.putExtra("user_id", user_id);
////                        notificationIntent.putExtra("title", title);
////
////                        Log.d("fdfdfdf", self + s + author + s);
////                        Log.d("fdgftuumhb2132vf", notificationIntent.getStringExtra("content") + notificationIntent.getStringExtra("author"));
////
//////                    TaskStackBuilder stackBuilder = TaskStackBuilder.create(MyFirebaseMessagingService.this);
//////                    stackBuilder.addNextIntentWithParentStack(notificationIntent);
////                        PendingIntent notificationPendingIntent = PendingIntent.getActivity(Restaurant_DashBoard.this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
////                        NotificationCompat.Builder builder = new NotificationCompat.Builder(Restaurant_DashBoard.this, "notification").setContentTitle(title).setSmallIcon(R.drawable.goinbo_mainlogo).setAutoCancel(true)
////                                .setContentText(message).setContentInfo(user_id)
////                                .setContentIntent(notificationPendingIntent);
////                        ;
////                        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
////                        notificationManager.notify(1, builder.build());
////                    }
////                } catch (JSONException e) {
////                    e.printStackTrace();
////                }
////
////            }
////        }, new Response.ErrorListener() {
////            @Override
////            public void onErrorResponse(VolleyError error) {
////              //  Toast.makeText(MyFirebaseMessagingService.this, "Error in Server", Toast.LENGTH_SHORT).show();
////            }
////
////        }) {
//////            @Override
//////            protected Map<String, String> getParams() throws AuthFailureError {
//////                Map<String, String> map = new HashMap<>();
//////                map.put("id", user_id);
//////                Log.d("Fdfdfd",user_id);
//////                return map;
//////            }
////        };
////        RequestQueue requestQueue = Volley.newRequestQueue(this);
////        requestQueue.add(stringRequest);
//
//
//    }
private boolean process() {
    boolean show = true;
    if (checkConnectivity.isConnected()) {
        show = false;

    } else {

        showDialog();
        show = true;
    }
    return show;
}

    private void showDialog() {

        Dialog dialog = new Dialog(Restaurant_DashBoard.this, R.style.FullScreenDialogWithStatusBarColorAccent);
        if (!dialog.isShowing()){
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.no_internet);
            Window window = dialog.getWindow();
            assert window != null;
            WindowManager.LayoutParams wlp = Objects.requireNonNull(window).getAttributes();
            wlp.gravity = Gravity.CENTER;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
            window.setAttributes(wlp);
            Objects.requireNonNull(dialog.getWindow()).setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            Button wifi_btn = dialog.findViewById(R.id.wifi_btn);
            Button mobile_data_btn = dialog.findViewById(R.id.mobile_data_btn);
            dialog.show();
            dialog.setCancelable(false);
            wifi_btn.setOnClickListener(v -> {
                dialog.dismiss();
                isshowing = false;
                WifiManager wifiMgr = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);

                assert wifiMgr != null;
                if (wifiMgr.isWifiEnabled()) { // Wi-Fi adapter is ON

                    WifiInfo wifiInfo = wifiMgr.getConnectionInfo();

                    if (wifiInfo.getNetworkId() == -1) {

                        showDialog();
                        // Not connected to an access point
                    } else {
                        dialog.dismiss();
                        process();
                    }
                    // Connected to an access point
                } else {
                    startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS)); // Wi-Fi adapter is OFF
                }
            });
            mobile_data_btn.setOnClickListener(v -> {
                dialog.dismiss();
                isshowing = false;
                startActivityForResult(new Intent(android.provider.Settings.ACTION_DATA_ROAMING_SETTINGS), 0);
                if (checkConnectivity.isConnected()) {
                    process();

                } else {
                    showDialog();
                }
            });
        }

    }
}