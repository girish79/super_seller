package com.Goingbo.Superseller.Model.seller_unit;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class Productdetail implements Serializable, Parcelable
{

    @SerializedName("unittype")
    @Expose
    private String unittype;
    @SerializedName("unit")
    @Expose
    private String unit;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("offer_price")
    @Expose
    private String offerPrice;
    public final static Creator<Productdetail> CREATOR = new Creator<Productdetail>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Productdetail createFromParcel(Parcel in) {
            return new Productdetail(in);
        }

        public Productdetail[] newArray(int size) {
            return (new Productdetail[size]);
        }

    }
            ;
    private final static long serialVersionUID = 4637620403423140495L;

    protected Productdetail(Parcel in) {
        this.unittype = ((String) in.readValue((String.class.getClassLoader())));
        this.unit = ((String) in.readValue((String.class.getClassLoader())));
        this.price = ((String) in.readValue((String.class.getClassLoader())));
        this.offerPrice = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public Productdetail() {
    }

    /**
     *
     * @param unit
     * @param unittype
     * @param offerPrice
     * @param price
     */
    public Productdetail(String unittype, String unit, String price, String offerPrice) {
        super();
        this.unittype = unittype;
        this.unit = unit;
        this.price = price;
        this.offerPrice = offerPrice;
    }

    public String getUnittype() {
        return unittype;
    }

    public void setUnittype(String unittype) {
        this.unittype = unittype;
    }

    public Productdetail withUnittype(String unittype) {
        this.unittype = unittype;
        return this;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Productdetail withUnit(String unit) {
        this.unit = unit;
        return this;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Productdetail withPrice(String price) {
        this.price = price;
        return this;
    }

    public String getOfferPrice() {
        return offerPrice;
    }

    public void setOfferPrice(String offerPrice) {
        this.offerPrice = offerPrice;
    }

    public Productdetail withOfferPrice(String offerPrice) {
        this.offerPrice = offerPrice;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("unittype", unittype).append("unit", unit).append("price", price).append("offerPrice", offerPrice).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(unit).append(unittype).append(offerPrice).append(price).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Productdetail) == false) {
            return false;
        }
        Productdetail rhs = ((Productdetail) other);
        return new EqualsBuilder().append(unit, rhs.unit).append(unittype, rhs.unittype).append(offerPrice, rhs.offerPrice).append(price, rhs.price).isEquals();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(unittype);
        dest.writeValue(unit);
        dest.writeValue(price);
        dest.writeValue(offerPrice);
    }

    public int describeContents() {
        return 0;
    }

}