package com.Goingbo.Superseller.Activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.provider.Settings;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.Goingbo.Superseller.Interface.ApiService;
import com.Goingbo.Superseller.Model.Bookingpre;
import com.Goingbo.Superseller.R;
import com.Goingbo.Superseller.Util.CheckConnectivity;
import com.Goingbo.Superseller.Util.RetrofitClient;
import com.Goingbo.Superseller.Util.SessionManager;
import com.hbb20.CountryCodePicker;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Seller_OTP_Login extends AppCompatActivity {
    EditText edit_txt_phone_number;
    private SessionManager session;
    TextView verify_otp;
    private CheckConnectivity checkConnectivity;
    boolean isshowing=true;
    ImageView image_backpress;
    TextView title_text,title_textotp;
    CountryCodePicker ccp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller__o_t_p__login);
        edit_txt_phone_number = findViewById(R.id.seller_edit_text_phone_num_otp_page);
        verify_otp = findViewById(R.id.seller_verify_button_otp_page);
        image_backpress = findViewById(R.id.image_backpress);
        ccp = (CountryCodePicker) findViewById(R.id.seller_ccp);
        title_text = findViewById(R.id.title_text);
        session=new SessionManager(getApplicationContext());
        checkConnectivity=new CheckConnectivity(getApplicationContext());
        image_backpress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        title_text.setText("Phone Login");
        ccp.setOnCountryChangeListener(() -> {

        });
        verify_otp.setOnClickListener(v -> {
            if (!process()) //returns true if internet available
            {
                if ((edit_txt_phone_number.getText().toString().length() < 10) && (edit_txt_phone_number.getText().toString().length() != 0)) {
                    edit_txt_phone_number.setError("Invalid Number!");
                } else if (edit_txt_phone_number.getText().toString().length() == 0) {
                    edit_txt_phone_number.setError(" Please Enter Your Number!");
                } else {
                    try {
                        InputMethodManager imm = (InputMethodManager) this.getSystemService(INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
                    } catch (Exception e) {
                    }
                    ProgressDialog progressDialog = new ProgressDialog(Seller_OTP_Login.this);

                    progressDialog.setMessage("OTP Send......");
                    progressDialog.show();
                    Call<Bookingpre> callverify= RetrofitClient.getClient("https://www.goingbo.com/")
                            .create(ApiService.class).getsellerverfyphone(edit_txt_phone_number.getText().toString());
                    callverify.enqueue(new Callback<Bookingpre>() {
                        @Override
                        public void onResponse(Call<Bookingpre> call, Response<Bookingpre> response) {
                            Bookingpre bookingpre=response.body();
                            progressDialog.dismiss();
                            String intentdata=getIntent().getStringExtra("checkingtype");
                            if(!bookingpre.getError()){
                                Bundle bundle = new Bundle();
                                bundle.putString("phone", edit_txt_phone_number.getText().toString());
                                bundle.putString("contrycode", ""+ccp.getSelectedCountryCode());
                                bundle.putString("checktype", ""+intentdata);
                                Intent intent=new Intent(Seller_OTP_Login.this,MobileVarificationSeller.class);
                                intent.putExtras(bundle);
                                startActivity(intent);
                            }else{
                                new AlertDialog.Builder( Objects.requireNonNull(Seller_OTP_Login.this))
                                        .setTitle("Alert")
                                        .setMessage("Please signup first to login application !")
                                        .setCancelable(false)
                                        .setPositiveButton("OK", (dialog, which) -> {dialog.dismiss();
                                        }).show();
                            }

                        }

                        @Override
                        public void onFailure(Call<Bookingpre> call, Throwable t) {
                            progressDialog.dismiss();
                            new AlertDialog.Builder( Objects.requireNonNull(Seller_OTP_Login.this))
                                    .setTitle("Alert")
                                    .setMessage("Api failed due to some technical problem , please try again !")
                                    .setCancelable(false)
                                    .setPositiveButton("OK", (dialog, which) -> {dialog.dismiss();
                                    }).show();
                        }
                    });



                }
            }

        });
    }
    private boolean process() {
        boolean show=true;
        if (checkConnectivity.isConnected()) {
            show=false;

        } else {
            showDialog();
            show=true;
        }
        return show;
    }

    private void showDialog() {

        Dialog dialog = new Dialog(Seller_OTP_Login.this, android.R.style.Theme_Translucent_NoTitleBar);
        if (!dialog.isShowing()) {
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            dialog.setContentView(R.layout.no_internet);
            Window window = dialog.getWindow();
            assert window != null;
            WindowManager.LayoutParams wlp = Objects.requireNonNull(window).getAttributes();
            wlp.gravity = Gravity.CENTER;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
            window.setAttributes(wlp);
            Objects.requireNonNull(dialog.getWindow()).setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            Button wifi_btn = dialog.findViewById(R.id.wifi_btn);
            Button mobile_data_btn = dialog.findViewById(R.id.mobile_data_btn);
            dialog.show();
            dialog.setCancelable(false);
            wifi_btn.setOnClickListener(v -> {
                dialog.dismiss();
                isshowing = false;
                WifiManager wifiMgr = (WifiManager) this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                assert wifiMgr != null;
                if (wifiMgr.isWifiEnabled()) { // Wi-Fi adapter is ON
                    WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
                    if (wifiInfo.getNetworkId() == -1) {
                        showDialog();
                        // Not connected to an access point
                    } else {
                        dialog.dismiss();
                        process();
                    }
                    // Connected to an access point
                } else {
                    startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS)); // Wi-Fi adapter is OFF
                }
            });
            mobile_data_btn.setOnClickListener(v -> {
                dialog.dismiss();
                isshowing = false;
                startActivityForResult(new Intent(android.provider.Settings.ACTION_DATA_ROAMING_SETTINGS), 0);
                if (checkConnectivity.isConnected()) {
                    process();
                } else {
                    showDialog();
                }
            });
        }

    }

}