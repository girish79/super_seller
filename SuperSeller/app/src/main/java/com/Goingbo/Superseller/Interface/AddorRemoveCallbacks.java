package com.Goingbo.Superseller.Interface;

public interface AddorRemoveCallbacks {
    void onAddProduct();

    void onRemoveProduct();

    void updateTotalPrice();
}
