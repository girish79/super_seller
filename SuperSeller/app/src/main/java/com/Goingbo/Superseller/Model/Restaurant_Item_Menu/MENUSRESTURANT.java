package com.Goingbo.Superseller.Model.Restaurant_Item_Menu;
import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class MENUSRESTURANT implements Serializable
{

    @SerializedName("menuItem_id")
    @Expose
    private String menuItemId;
    @SerializedName("menuItem_category")
    @Expose
    private String menuItemCategory;
    @SerializedName("menuItem_name")
    @Expose
    private String menuItemName;
    @SerializedName("menuItem_image")
    @Expose
    private String menuItemImage;
    @SerializedName("menuItem_details")
    @Expose
    private String menuItemDetails;
    @SerializedName(" restaurantmenuitems_id")
    @Expose
    private String restaurantmenuitemsId;
    @SerializedName("seller_id")
    @Expose
    private String sellerId;
    @SerializedName(" restaurantmenuitems_menu_items")
    @Expose
    private String restaurantmenuitemsMenuItems;
    private final static long serialVersionUID = 9142774381246166150L;

    /**
     * No args constructor for use in serialization
     *
     */
    public MENUSRESTURANT() {
    }

    /**
     *
     * @param sellerId
     * @param restaurantmenuitemsId
     * @param menuItemName
     * @param menuItemImage
     * @param menuItemDetails
     * @param menuItemId
     * @param menuItemCategory
     * @param restaurantmenuitemsMenuItems
     */
    public MENUSRESTURANT(String menuItemId, String menuItemCategory, String menuItemName, String menuItemImage, String menuItemDetails, String restaurantmenuitemsId, String sellerId, String restaurantmenuitemsMenuItems) {
        super();
        this.menuItemId = menuItemId;
        this.menuItemCategory = menuItemCategory;
        this.menuItemName = menuItemName;
        this.menuItemImage = menuItemImage;
        this.menuItemDetails = menuItemDetails;
        this.restaurantmenuitemsId = restaurantmenuitemsId;
        this.sellerId = sellerId;
        this.restaurantmenuitemsMenuItems = restaurantmenuitemsMenuItems;
    }

    public String getMenuItemId() {
        return menuItemId;
    }

    public void setMenuItemId(String menuItemId) {
        this.menuItemId = menuItemId;
    }

    public MENUSRESTURANT withMenuItemId(String menuItemId) {
        this.menuItemId = menuItemId;
        return this;
    }

    public String getMenuItemCategory() {
        return menuItemCategory;
    }

    public void setMenuItemCategory(String menuItemCategory) {
        this.menuItemCategory = menuItemCategory;
    }

    public MENUSRESTURANT withMenuItemCategory(String menuItemCategory) {
        this.menuItemCategory = menuItemCategory;
        return this;
    }

    public String getMenuItemName() {
        return menuItemName;
    }

    public void setMenuItemName(String menuItemName) {
        this.menuItemName = menuItemName;
    }

    public MENUSRESTURANT withMenuItemName(String menuItemName) {
        this.menuItemName = menuItemName;
        return this;
    }

    public String getMenuItemImage() {
        return menuItemImage;
    }

    public void setMenuItemImage(String menuItemImage) {
        this.menuItemImage = menuItemImage;
    }

    public MENUSRESTURANT withMenuItemImage(String menuItemImage) {
        this.menuItemImage = menuItemImage;
        return this;
    }

    public String getMenuItemDetails() {
        return menuItemDetails;
    }

    public void setMenuItemDetails(String menuItemDetails) {
        this.menuItemDetails = menuItemDetails;
    }

    public MENUSRESTURANT withMenuItemDetails(String menuItemDetails) {
        this.menuItemDetails = menuItemDetails;
        return this;
    }

    public String getRestaurantmenuitemsId() {
        return restaurantmenuitemsId;
    }

    public void setRestaurantmenuitemsId(String restaurantmenuitemsId) {
        this.restaurantmenuitemsId = restaurantmenuitemsId;
    }

    public MENUSRESTURANT withRestaurantmenuitemsId(String restaurantmenuitemsId) {
        this.restaurantmenuitemsId = restaurantmenuitemsId;
        return this;
    }

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public MENUSRESTURANT withSellerId(String sellerId) {
        this.sellerId = sellerId;
        return this;
    }

    public String getRestaurantmenuitemsMenuItems() {
        return restaurantmenuitemsMenuItems;
    }

    public void setRestaurantmenuitemsMenuItems(String restaurantmenuitemsMenuItems) {
        this.restaurantmenuitemsMenuItems = restaurantmenuitemsMenuItems;
    }

    public MENUSRESTURANT withRestaurantmenuitemsMenuItems(String restaurantmenuitemsMenuItems) {
        this.restaurantmenuitemsMenuItems = restaurantmenuitemsMenuItems;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("menuItemId", menuItemId).append("menuItemCategory", menuItemCategory).append("menuItemName", menuItemName).append("menuItemImage", menuItemImage).append("menuItemDetails", menuItemDetails).append("restaurantmenuitemsId", restaurantmenuitemsId).append("sellerId", sellerId).append("restaurantmenuitemsMenuItems", restaurantmenuitemsMenuItems).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(sellerId).append(restaurantmenuitemsId).append(menuItemName).append(menuItemImage).append(menuItemDetails).append(menuItemId).append(menuItemCategory).append(restaurantmenuitemsMenuItems).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof MENUSRESTURANT) == false) {
            return false;
        }
        MENUSRESTURANT rhs = ((MENUSRESTURANT) other);
        return new EqualsBuilder().append(sellerId, rhs.sellerId).append(restaurantmenuitemsId, rhs.restaurantmenuitemsId).append(menuItemName, rhs.menuItemName).append(menuItemImage, rhs.menuItemImage).append(menuItemDetails, rhs.menuItemDetails).append(menuItemId, rhs.menuItemId).append(menuItemCategory, rhs.menuItemCategory).append(restaurantmenuitemsMenuItems, rhs.restaurantmenuitemsMenuItems).isEquals();
    }

}