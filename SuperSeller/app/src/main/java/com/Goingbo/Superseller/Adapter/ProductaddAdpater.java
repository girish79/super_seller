package com.Goingbo.Superseller.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.Goingbo.Superseller.Model.productdetails.Product;
import com.Goingbo.Superseller.R;
import com.Goingbo.Superseller.fragment.Select_Product;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ProductaddAdpater extends RecyclerView.Adapter<ProductaddAdpater.ProductView> {
    Context context;
    ArrayList<Product> arrayList;

    public ProductaddAdpater(Context context, ArrayList<Product> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public ProductView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.row_add_layout_product,parent,false);
        return new ProductView(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductView holder, int position) {
        if(arrayList.size()>0) {
            if(arrayList.get(position).getProImage()!=null){
                Log.e("imagepath",""+arrayList.get(position).getProImage());
                String imagelink = "https://www.goingbo.com/public/product_images/" + arrayList.get(position).getProImage();
                Picasso.get().load(imagelink).into(holder.select_product_image_list);}
            String dataname=arrayList.get(position).getProName().substring(0,1).toUpperCase()+arrayList.get(position).getProName().substring(1);

            holder.selected_product_name.setText(Html.fromHtml(dataname));
            holder.selected_product_category.setText(Html.fromHtml(arrayList.get(position).getCategroyName()));
            holder.selected_product_description.setText(Html.fromHtml(arrayList.get(position).getProDes()));

//        holder.tv_count_qty.setText(arrayList.get(position).getProQty());
//        holder.tv_count_price.setText(arrayList.get(position).getProPrice());

            holder.btn_rcv_add_product.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Fragment fragment = new Select_Product();
                        Bundle bundle = new Bundle();
                        bundle.putString("category_name", arrayList.get(position).getCategroyName());
                        bundle.putString("productid", arrayList.get(position).getId());
                        bundle.putString("productname", arrayList.get(position).getProName());
                        bundle.putString("productcategories", arrayList.get(position).getProCategory());
                        bundle.putString("productdescription", arrayList.get(position).getProDes());
                        bundle.putString("productimage", arrayList.get(position).getProImage());
//                    bundle.putString("productprice", arrayList.get(position).getProPrice());
//                    bundle.putString("productquantity", arrayList.get(position).getProQty());
                        bundle.putString("producttype", arrayList.get(position).getProType());

                        Log.d("sendlist", "" + arrayList.get(position).toString());
                        bundle.putString("senddata", "search");
                        fragment.setArguments(bundle);
                        ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction().replace(R.id.seller_fragment_dashboard, fragment, "search_list").addToBackStack("search_list").commit();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ProductView extends RecyclerView.ViewHolder{
        public ImageView select_product_image_list;
        public TextView selected_product_name;
        public TextView selected_product_category;
        public TextView selected_product_description,tv_count_qty,tv_count_price;
        public Button btn_rcv_add_product;
        public ProductView(@NonNull View itemView) {
            super(itemView);
            select_product_image_list=itemView.findViewById(R.id.select_product_image_list);
            selected_product_name=itemView.findViewById(R.id.selected_product_name);
            selected_product_category=itemView.findViewById(R.id.selected_product_category);
            selected_product_description=itemView.findViewById(R.id.selected_product_description);
            tv_count_qty=itemView.findViewById(R.id.tv_count_qty);
            tv_count_price=itemView.findViewById(R.id.tv_count_price_list);
            btn_rcv_add_product=itemView.findViewById(R.id.btn_rcv_add_product);

        }


    }
}
