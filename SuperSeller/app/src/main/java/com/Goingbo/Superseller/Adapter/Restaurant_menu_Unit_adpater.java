package com.Goingbo.Superseller.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.Goingbo.Superseller.Model.UntItem_Restuarant.MenuDetail;
import com.Goingbo.Superseller.R;
import java.util.List;
public class Restaurant_menu_Unit_adpater extends RecyclerView.Adapter<Restaurant_menu_Unit_adpater.unitView> {
    Context context;
    List<MenuDetail> parraylist;

    public Restaurant_menu_Unit_adpater(Context context, List<MenuDetail> parraylist) {
        this.context = context;
        this.parraylist = parraylist;
    }

    @NonNull
    @Override
    public unitView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.row_unit_recyclerview,parent,false);
        return new unitView(view);
    }

    @Override
    public void onBindViewHolder(@NonNull unitView holder, int position) {
        holder.inner_textView_unit.setText(parraylist.get(position).getUnitWeight()+parraylist.get(position).getUnitType());
        holder.inner_textView_price.setText(parraylist.get(position).getUnitPrice());
        holder.inner_textView_offer_price.setText(parraylist.get(position).getUnitOffer());
        double discount=00.00;
        double price=Double.parseDouble(parraylist.get(position).getUnitPrice());
        double offer_price=Double.parseDouble(parraylist.get(position).getUnitOffer());
        discount=price - offer_price;
        holder.inner_textView_discount.setText(parraylist.get(position).getMenuDescription());

    }

    @Override
    public int getItemCount() {
        return parraylist.size();
    }

    public class unitView extends RecyclerView.ViewHolder {
        TextView inner_textView_unit;
        TextView inner_textView_price;
        TextView inner_textView_offer_price;
        TextView inner_textView_discount;
        public unitView(@NonNull View itemView) {
            super(itemView);
            inner_textView_discount=itemView.findViewById(R.id.inner_textView_discount);
            inner_textView_offer_price=itemView.findViewById(R.id.inner_textView_offer_price);
            inner_textView_price=itemView.findViewById(R.id.inner_textView_price);
            inner_textView_unit=itemView.findViewById(R.id.inner_textView_unit);
        }
    }
}
