package com.Goingbo.Superseller.Interface;

import com.Goingbo.Superseller.Model.Bookingpre;
import com.Goingbo.Superseller.Model.CommissionSeller.CommissionCost;
import com.Goingbo.Superseller.Model.CreateUser;
import com.Goingbo.Superseller.Model.Deliver_seller_cost.DeliveryCharger;
import com.Goingbo.Superseller.Model.DeliveryAdrress;
import com.Goingbo.Superseller.Model.Login;
import com.Goingbo.Superseller.Model.NewOrderLISTSeller.NewSellerOrderList;
import com.Goingbo.Superseller.Model.PackLoginAdmin.LoginsellerADmin;
import com.Goingbo.Superseller.Model.PackLoginSeller.LoginsellerProduct;
import com.Goingbo.Superseller.Model.ProductSeller_Type.ProductSellerTypeData;
import com.Goingbo.Superseller.Model.Product_type.ProductTypeData;
import com.Goingbo.Superseller.Model.Productadmin.ProductAdmin;
import com.Goingbo.Superseller.Model.Restaurant_Item_Menu.Restuarantmenus;
import com.Goingbo.Superseller.Model.RestuarantInfo;
import com.Goingbo.Superseller.Model.Restuarant_Menu.MenuItem;
import com.Goingbo.Superseller.Model.SellerUorder.SellerOrder;
import com.Goingbo.Superseller.Model.productdetails.ProductData;
import com.Goingbo.Superseller.Model.seller_categories.Productcategories;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;


public interface ApiService {


    @FormUrlEncoded
    @POST("goingboapi/grocery.php?apicall=createseller")
    Call<CreateUser> getcreateseller(@Field("C_name") String name, @Field("C_email") String email, @Field("C_phone") String C_phone, @Field("C_password") String password, @Field("lng") double lan, @Field("lat") double lat);


    @FormUrlEncoded
    @POST("goingboapi/grocery.php?apicall=phonelogin")
    Call<Login> phonesellerdata(@Field("C_phone") String email, @Field("C_password") String password);

    @FormUrlEncoded
    @POST("goingboapi/grocery.php?apicall=searchlist")
    Call<ProductData> getProduct(@Field("pro_category") String text);

    @FormUrlEncoded
    @POST("goingboapi/grocery.php?apicall=category")
    Call<ProductTypeData> getcategory(@Field("userId") String text);

    @FormUrlEncoded
    @POST("goingboapi/grocery.php?apicall=category_selected")
    Call<Productcategories> getcategoryselected(@Field("catid") int text);

    @FormUrlEncoded
    @POST("goingboapi/grocery.php?apicall=addproduct")
    Call<Bookingpre> addproductdetails(@Field("Pro_type") String Pro_type,
                                       @Field("Pro_category") String Pro_category,
                                       @Field("Pro_name") String Pro_name,
                                       @Field("Pro_image") String Pro_image,
                                       @Field("Pro_des") String Pro_des,
                                       @Field("Pro_price_details") String offer_price
            , @Field("seller_id") String seller_id);

    @POST("goingboapi/grocery.php?apicall=updateproduct")
    Call<Bookingpre> sendjsonobject(@Body RequestBody jsonObject);

    @POST("goingboapi/grocery.php?apicall=seller_product_insert")
    Call<Bookingpre> seller_product_insert(@Body RequestBody jsonObject);

    @Multipart
    @POST("goingboapi/grocery.php?apicall=imageaddproduct")
    Call<Bookingpre> imageaddproductdetails(@Part("Pro_type") RequestBody Pro_type, @Part("Pro_category") RequestBody Pro_category,
                                            @Part("Pro_name") RequestBody Pro_name, @Part("product_qty") RequestBody Pro_qty,
                                            @Part("price") RequestBody Pro_price, @Part MultipartBody.Part file,
                                            @Part("Pro_des") RequestBody Pro_des, @Part("offer_price") RequestBody offer_price, @Part("seller_id") RequestBody seller_id);

    @FormUrlEncoded
    @POST("goingboapi/grocery.php?apicall=select_product_type")
    Call<ProductSellerTypeData> getproducttype(@Field("Pro_category") String Pro_category);
//

    @GET("api/admin/reset")
    Call<Bookingpre> getchangeemail(@Query("phone") String C_phone, @Query("password") String C_password);

    @FormUrlEncoded
    @POST("goingboapi/grocery.php?apicall=phonepasswordset")
    Call<Bookingpre> phonechangeemail(@Field("C_phone") String C_email, @Field("C_password") String C_password, @Field("C_newpassord") String C_newpassord);

    //
    @FormUrlEncoded
    @POST("goingboapi/grocery.php?apicall=seller_product")
    Call<ProductAdmin> getList(@Field("seller_id") int seller_id);

    //
    @FormUrlEncoded
    @POST("goingboapi/grocery.php?apicall=sellerdelete")
    Call<Bookingpre> deleteproduct(@Field("seller_id") int sid, @Field("product_id") String pid);

    @FormUrlEncoded
    @POST("goingboapi/grocery.php?apicall=deliver_charge")
    Call<DeliveryCharger> checkdelivery(@Field("sellerid") int sid);

    @FormUrlEncoded
    @POST("goingboapi/grocery.php?apicall=wallet_amount")
    Call<CommissionCost> commissioncheck(@Field("sellerid") int sid);

    //
    @FormUrlEncoded
    @POST("goingboapi/grocery.php?apicall=address_update")
    Call<DeliveryAdrress> setaddress(@Field("city") String city, @Field("state") String state, @Field("zip") int Zip,
                                     @Field("street") String street, @Field("block") String block, @Field("landmark") String landmark,
                                     @Field("plot_number") String plot_number, @Field("user_id") int user_id);

    //
    @FormUrlEncoded
    @POST("goingboapi/grocery.php?apicall=seller_orderlist")
    Call<SellerOrder> getorderlistseller(@Field("seller_id") String sellerid);

    @GET("api/seller-order?")
    Call<NewSellerOrderList> getneworderlistseller(@Query("id") String sellerid);

    @FormUrlEncoded
    @POST("goingboapi/grocery.php?apicall=sellerupdate")
    Call<Bookingpre> updateseller_order(@Field("orderid") String ordersid, @Field("sellerid") String sellerid, @Field("status") String status);

    @FormUrlEncoded
    @POST("goingboapi/grocery.php?apicall=delivery_status")
    Call<Bookingpre> update_delivery_status(@Field("seller_id") String sellerid, @Field("order_id") String ordersid, @Field("status") String status, @Field("otp_number") int otp);

    @FormUrlEncoded
    @POST("goingboapi/grocery.php?apicall=amount_add_wallet")
    Call<Bookingpre> update_commission(@Field("sellerid") int sellerid);

    @FormUrlEncoded
    @POST("goingboapi/grocery.php?apicall=updatedcharge")
    Call<Bookingpre> updatedelivery(@Field("sellerid") int sellerid, @Field("distance") double distance, @Field("min_order_amount") double min_order_amount, @Field(("delivery_charge")) double delivery_charge);


    @FormUrlEncoded
    @POST("goingboapi/grocery.php?apicall=checkdetails")
    Call<Bookingpre> getcheckdata(@Field("phone") String phone, @Field("email") String email);

    @FormUrlEncoded
    @POST("goingboapi/grocery.php?apicall=profileupdate")
    Call<Bookingpre> checksellerprofile(@Field("C_name") String name, @Field("C_email") String email, @Field("C_phone") String phone,
                                        @Field("C_type") String type);

    @FormUrlEncoded
    @POST("goingboapi/grocery.php?apicall=checkphone")
    Call<Bookingpre> getsellerverfyphone(@Field("C_phone") String phone);

    @FormUrlEncoded
    @POST("goingboapi/grocery.php?apicall=otpphonecheck")
    Call<Bookingpre> otpsellerverfyphone(@Field("C_phone") String phone);

    @FormUrlEncoded
    @POST("goingboapi/grocery.php?apicall=phoneotplogin")
    Call<CreateUser> otpsellerloginapi(@Field("C_phone") String phone, @Field("C_password") String C_password);

    @GET("api/admin/register")
    Call<LoginsellerProduct> getcreateseller(@Query("C_name") String name, @Query("C_email") String email, @Query("C_phone") String C_phone, @Query("password") String ppassword, @Query("C_password") String password, @Query("lng") double lan, @Query("lat") double lat);

    @GET("api/admin/alt-register")
    Call<LoginsellerProduct> getcreatenewseller(@Query("C_name") String name, @Query("C_email") String email, @Query("C_phone") String C_phone, @Query("C_aphone") String C_aphone, @Query("password") String ppassword, @Query("C_password") String password, @Query("lng") double lan, @Query("lat") double lat,
                                                @Query("address") String address, @Query("city") String city, @Query("state") String state, @Query("zip") String zip);

    @GET("api/admin/login")
    Call<LoginsellerADmin> getsellerdata(@Query("email") String email, @Query("password") String password);

    @FormUrlEncoded
    @POST("goingboapi/Restuarant_menu.php?apicall=Restuarant_menu_items_search")
    Call<MenuItem> getMenuITem(@Field("query") String query);

    @POST("goingboapi/Restuarant_menu.php?apicall=menu_item_selected")
    Call<Bookingpre> setmenuItems(@Body RequestBody query);

    @POST("goingboapi/Restuarant_menu.php?apicall=restaurant_menu_item_update")
    Call<Bookingpre> updatemenuItems(@Body RequestBody jsonObject);

    @Multipart
    @POST("goingboapi/Restuarant_menu.php?apicall=imageaddrestuarant")
    Call<RestuarantInfo> updateRestaurant(@Part("r_name") RequestBody r_name,
                                          @Part("r_contact") RequestBody r_contact,
                                          @Part("r_address") RequestBody r_address,
                                          @Part("seller_id") RequestBody seller_id,
                                          @Part MultipartBody.Part uploadfile);

    @FormUrlEncoded
    @POST("goingboapi/Restuarant_menu.php?apicall=restaurant_menu_item_seller")
    Call<Restuarantmenus> updatemenu(@Field("seller_id") String sellerid);

    @FormUrlEncoded
    @POST("goingboapi/Restuarant_menu.php?apicall=restaurant_menu_item_delete")
    Call<Bookingpre> deletemenu(@Field("id") String id, @Field("seller_id") String seller_id, @Field("menu_item_id") String menu_item_id);


    @POST("goingboapi/grocery.php?apicall=updatecharge_orderdistances")
    Call<Bookingpre> updateseller_delivery_charger(@Body RequestBody jsonObject);


    @GET("api/seller-order")
    Call<NewSellerOrderList> getsellerorder(@Query("id") String id);






}
