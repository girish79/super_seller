package com.Goingbo.Superseller.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.Goingbo.Superseller.Activity.Menu_Item_Edit;
import com.Goingbo.Superseller.Model.Restuarant_Menu.MENUITEMRESTURANT;
import com.Goingbo.Superseller.Model.productdetails.Product;
import com.Goingbo.Superseller.R;
import com.Goingbo.Superseller.fragment.Select_Product;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class Adpater_Restuarant_item extends RecyclerView.Adapter<Adpater_Restuarant_item.ProductView> {
    Context context;
    ArrayList<MENUITEMRESTURANT> arrayList;

    public Adpater_Restuarant_item(Context context, ArrayList<MENUITEMRESTURANT> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public ProductView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.row_add_layout_product,parent,false);
        return new ProductView(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductView holder, int position) {
        if(arrayList.size()>0) {
            if(arrayList.get(position).getMenuimage()!=null){
                Log.e("imagepath",""+arrayList.get(position).getMenuimage());
                String imagelink = "https://www.goingbo.com/public/restorentmenuimage/" + arrayList.get(position).getMenuimage();
                Picasso.get().load(imagelink).into(holder.select_product_image_list);}
            String dataname=arrayList.get(position).getMenuitem().substring(0,1).toUpperCase()+arrayList.get(position).getMenuitem().substring(1);

            holder.selected_product_name.setText(Html.fromHtml(dataname));
            holder.selected_product_category.setText(Html.fromHtml(arrayList.get(position).getMenucategory()));
            holder.selected_product_description.setText(Html.fromHtml(arrayList.get(position).getDetail()));
            holder.btn_rcv_add_product.setText("ADD MENU ITEM ");

            holder.btn_rcv_add_product.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                       //Fragment fragment = new Select_Product();
                        Intent intent=new Intent(context, Menu_Item_Edit.class);
                        intent.putExtra("menu_id", arrayList.get(position).getId());
                        intent.putExtra("menu_tittle", arrayList.get(position).getMenuitem());
                        intent.putExtra("menu_category", arrayList.get(position).getMenucategory());
                        intent.putExtra("menu_detail", arrayList.get(position).getDetail());
                        intent.putExtra("menu_image", arrayList.get(position).getMenuimage());
                        intent.putExtra("senddata", "search");
                       ((AppCompatActivity) context).startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ProductView extends RecyclerView.ViewHolder{
        public ImageView select_product_image_list;
        public TextView selected_product_name;
        public TextView selected_product_category;
        public TextView selected_product_description,tv_count_qty,tv_count_price;
        public Button btn_rcv_add_product;
        public ProductView(@NonNull View itemView) {
            super(itemView);
            select_product_image_list=itemView.findViewById(R.id.select_product_image_list);
            selected_product_name=itemView.findViewById(R.id.selected_product_name);
            selected_product_category=itemView.findViewById(R.id.selected_product_category);
            selected_product_description=itemView.findViewById(R.id.selected_product_description);
            tv_count_qty=itemView.findViewById(R.id.tv_count_qty);
            tv_count_price=itemView.findViewById(R.id.tv_count_price_list);
            btn_rcv_add_product=itemView.findViewById(R.id.btn_rcv_add_product);

        }


    }
}

