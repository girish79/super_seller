package com.Goingbo.Superseller.Model.UNITRestaurant_menu;

import com.Goingbo.Superseller.Model.UntItem_Restuarant.RestuarantUnitems;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Restuarantmenusunit implements Serializable
{

    @SerializedName("menu_item_id")
    @Expose
    private String menuItemId;
    @SerializedName("menu_detail")
    @Expose
    private List<RestuarantUnitems> menuDetail = new ArrayList<RestuarantUnitems>();
    private final static long serialVersionUID = 587923000746633138L;

    /**
     * No args constructor for use in serialization
     *
     */
    public Restuarantmenusunit() {
    }

    /**
     *
     * @param menuItemId
     * @param menuDetail
     */
    public Restuarantmenusunit(String menuItemId, List<RestuarantUnitems> menuDetail) {
        super();
        this.menuItemId = menuItemId;
        this.menuDetail = menuDetail;
    }

    public String getMenuItemId() {
        return menuItemId;
    }

    public void setMenuItemId(String menuItemId) {
        this.menuItemId = menuItemId;
    }

    public Restuarantmenusunit withMenuItemId(String menuItemId) {
        this.menuItemId = menuItemId;
        return this;
    }

    public List<RestuarantUnitems> getMenuDetail() {
        return menuDetail;
    }

    public void setMenuDetail(List<RestuarantUnitems> menuDetail) {
        this.menuDetail = menuDetail;
    }

    public Restuarantmenusunit withMenuDetail(List<RestuarantUnitems> menuDetail) {
        this.menuDetail = menuDetail;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("menuItemId", menuItemId).append("menuDetail", menuDetail).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(menuItemId).append(menuDetail).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Restuarantmenusunit) == false) {
            return false;
        }
        Restuarantmenusunit rhs = ((Restuarantmenusunit) other);
        return new EqualsBuilder().append(menuItemId, rhs.menuItemId).append(menuDetail, rhs.menuDetail).isEquals();
    }

}