package com.Goingbo.Superseller.Model.Restuarant_Menu;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class MenuItem implements Serializable
{

    @SerializedName("error")
    @Expose
    private boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("MENUITEM_RESTURANT")
    @Expose
    private List<MENUITEMRESTURANT> mENUITEMRESTURANT = new ArrayList<MENUITEMRESTURANT>();
    private final static long serialVersionUID = 8733632537922559069L;

    /**
     * No args constructor for use in serialization
     *
     */
    public MenuItem() {
    }

    /**
     *
     * @param mENUITEMRESTURANT
     * @param error
     * @param message
     */
    public MenuItem(boolean error, String message, List<MENUITEMRESTURANT> mENUITEMRESTURANT) {
        super();
        this.error = error;
        this.message = message;
        this.mENUITEMRESTURANT = mENUITEMRESTURANT;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public MenuItem withError(boolean error) {
        this.error = error;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public MenuItem withMessage(String message) {
        this.message = message;
        return this;
    }

    public List<MENUITEMRESTURANT> getMENUITEMRESTURANT() {
        return mENUITEMRESTURANT;
    }

    public void setMENUITEMRESTURANT(List<MENUITEMRESTURANT> mENUITEMRESTURANT) {
        this.mENUITEMRESTURANT = mENUITEMRESTURANT;
    }

    public MenuItem withMENUITEMRESTURANT(List<MENUITEMRESTURANT> mENUITEMRESTURANT) {
        this.mENUITEMRESTURANT = mENUITEMRESTURANT;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("error", error).append("message", message).append("mENUITEMRESTURANT", mENUITEMRESTURANT).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(error).append(message).append(mENUITEMRESTURANT).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof MenuItem) == false) {
            return false;
        }
        MenuItem rhs = ((MenuItem) other);
        return new EqualsBuilder().append(error, rhs.error).append(message, rhs.message).append(mENUITEMRESTURANT, rhs.mENUITEMRESTURANT).isEquals();
    }

}