package com.Goingbo.Superseller.Model.productdetails;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class Product implements Serializable, Parcelable
{

    @SerializedName("categroy_name")
    @Expose
    private String categroyName;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("Pro_type")
    @Expose
    private String proType;
    @SerializedName("Pro_category")
    @Expose
    private String proCategory;
    @SerializedName("Pro_name")
    @Expose
    private String proName;
    @SerializedName("Pro_image")
    @Expose
    private String proImage;
    @SerializedName("Pro_des")
    @Expose
    private String proDes;
    public final static Creator<Product> CREATOR = new Creator<Product>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        public Product[] newArray(int size) {
            return (new Product[size]);
        }

    }
            ;
    private final static long serialVersionUID = 1126073020139555285L;

    protected Product(Parcel in) {
        this.categroyName = ((String) in.readValue((String.class.getClassLoader())));
        this.id = ((String) in.readValue((String.class.getClassLoader())));
        this.proType = ((String) in.readValue((String.class.getClassLoader())));
        this.proCategory = ((String) in.readValue((String.class.getClassLoader())));
        this.proName = ((String) in.readValue((String.class.getClassLoader())));
        this.proImage = ((String) in.readValue((String.class.getClassLoader())));
        this.proDes = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public Product() {
    }

    /**
     *
     * @param proCategory
     * @param proType
     * @param proImage
     * @param categroyName
     * @param id
     * @param proName
     * @param proDes
     */
    public Product(String categroyName, String id, String proType, String proCategory, String proName, String proImage, String proDes) {
        super();
        this.categroyName = categroyName;
        this.id = id;
        this.proType = proType;
        this.proCategory = proCategory;
        this.proName = proName;
        this.proImage = proImage;
        this.proDes = proDes;
    }

    public String getCategroyName() {
        return categroyName;
    }

    public void setCategroyName(String categroyName) {
        this.categroyName = categroyName;
    }

    public Product withCategroyName(String categroyName) {
        this.categroyName = categroyName;
        return this;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Product withId(String id) {
        this.id = id;
        return this;
    }

    public String getProType() {
        return proType;
    }

    public void setProType(String proType) {
        this.proType = proType;
    }

    public Product withProType(String proType) {
        this.proType = proType;
        return this;
    }

    public String getProCategory() {
        return proCategory;
    }

    public void setProCategory(String proCategory) {
        this.proCategory = proCategory;
    }

    public Product withProCategory(String proCategory) {
        this.proCategory = proCategory;
        return this;
    }

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }

    public Product withProName(String proName) {
        this.proName = proName;
        return this;
    }

    public String getProImage() {
        return proImage;
    }

    public void setProImage(String proImage) {
        this.proImage = proImage;
    }

    public Product withProImage(String proImage) {
        this.proImage = proImage;
        return this;
    }

    public String getProDes() {
        return proDes;
    }

    public void setProDes(String proDes) {
        this.proDes = proDes;
    }

    public Product withProDes(String proDes) {
        this.proDes = proDes;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("categroyName", categroyName).append("id", id).append("proType", proType).append("proCategory", proCategory).append("proName", proName).append("proImage", proImage).append("proDes", proDes).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(proCategory).append(proType).append(proImage).append(categroyName).append(id).append(proName).append(proDes).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Product) == false) {
            return false;
        }
        Product rhs = ((Product) other);
        return new EqualsBuilder().append(proCategory, rhs.proCategory).append(proType, rhs.proType).append(proImage, rhs.proImage).append(categroyName, rhs.categroyName).append(id, rhs.id).append(proName, rhs.proName).append(proDes, rhs.proDes).isEquals();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(categroyName);
        dest.writeValue(id);
        dest.writeValue(proType);
        dest.writeValue(proCategory);
        dest.writeValue(proName);
        dest.writeValue(proImage);
        dest.writeValue(proDes);
    }

    public int describeContents() {
        return 0;
    }

}