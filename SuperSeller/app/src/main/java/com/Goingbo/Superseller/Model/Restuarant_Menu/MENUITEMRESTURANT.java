package com.Goingbo.Superseller.Model.Restuarant_Menu;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class MENUITEMRESTURANT implements Serializable
{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("menucategory")
    @Expose
    private String menucategory;
    @SerializedName("menuitem")
    @Expose
    private String menuitem;
    @SerializedName("menuimage")
    @Expose
    private String menuimage;
    @SerializedName("detail")
    @Expose
    private String detail;
    private final static long serialVersionUID = -5695652399198431155L;

    /**
     * No args constructor for use in serialization
     *
     */
    public MENUITEMRESTURANT() {
    }

    /**
     *
     * @param menuimage
     * @param id
     * @param menucategory
     * @param detail
     * @param menuitem
     */
    public MENUITEMRESTURANT(String id, String menucategory, String menuitem, String menuimage, String detail) {
        super();
        this.id = id;
        this.menucategory = menucategory;
        this.menuitem = menuitem;
        this.menuimage = menuimage;
        this.detail = detail;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public MENUITEMRESTURANT withId(String id) {
        this.id = id;
        return this;
    }

    public String getMenucategory() {
        return menucategory;
    }

    public void setMenucategory(String menucategory) {
        this.menucategory = menucategory;
    }

    public MENUITEMRESTURANT withMenucategory(String menucategory) {
        this.menucategory = menucategory;
        return this;
    }

    public String getMenuitem() {
        return menuitem;
    }

    public void setMenuitem(String menuitem) {
        this.menuitem = menuitem;
    }

    public MENUITEMRESTURANT withMenuitem(String menuitem) {
        this.menuitem = menuitem;
        return this;
    }

    public String getMenuimage() {
        return menuimage;
    }

    public void setMenuimage(String menuimage) {
        this.menuimage = menuimage;
    }

    public MENUITEMRESTURANT withMenuimage(String menuimage) {
        this.menuimage = menuimage;
        return this;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public MENUITEMRESTURANT withDetail(String detail) {
        this.detail = detail;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("menucategory", menucategory).append("menuitem", menuitem).append("menuimage", menuimage).append("detail", detail).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(id).append(menucategory).append(detail).append(menuimage).append(menuitem).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof MENUITEMRESTURANT) == false) {
            return false;
        }
        MENUITEMRESTURANT rhs = ((MENUITEMRESTURANT) other);
        return new EqualsBuilder().append(id, rhs.id).append(menucategory, rhs.menucategory).append(detail, rhs.detail).append(menuimage, rhs.menuimage).append(menuitem, rhs.menuitem).isEquals();
    }

}