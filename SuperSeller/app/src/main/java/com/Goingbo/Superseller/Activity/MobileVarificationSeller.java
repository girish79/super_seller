
package com.Goingbo.Superseller.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.Goingbo.Superseller.Interface.ApiService;
import com.Goingbo.Superseller.Model.Bookingpre;
import com.Goingbo.Superseller.Model.CreateUser;
import com.Goingbo.Superseller.R;
import com.Goingbo.Superseller.Util.CheckConnectivity;
import com.Goingbo.Superseller.Util.RetrofitClient;
import com.Goingbo.Superseller.Util.SessionManager;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MobileVarificationSeller extends AppCompatActivity {
    private static final String TAG = "VerificationActivity";
    TextView resend_timer;
    TextInputEditText OtpEditText;
    private Button smsVerificationButton;
    Handler handler;
    Runnable r;
    SessionManager sessionManager;
    RequestQueue mRequestQueue;
    TextView phoneText;
    ImageView image_backpress;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    String otp="";
    int count=1;
    String phone;
    boolean isshowing=false;
    String code;
    IntentFilter intentFilter;
    String intentdata="";
    ProgressDialog progressDialog;
    private static final int PERMISSION_REQUEST_ID = 100;
    CheckConnectivity checkConnectivity;
    Dialog fragmentdialog;
    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_varification_seller);
        checkConnectivity=new CheckConnectivity(MobileVarificationSeller.this);
        init();
         fragmentdialog = new Dialog(MobileVarificationSeller.this);
        intentdata=getIntent().getStringExtra("checktype");
//        intentFilter = new IntentFilter();
//        intentFilter.addAction(BROADCAST_ACTION);
//        mSmsBroadcastReceiver=new SMSBroadcastloader();
        //    requestRuntimePermissions(Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_SMS, Manifest.permission.SEND_SMS);
        image_backpress.setOnClickListener(v->{onBackPressed();});

        mRequestQueue = Volley.newRequestQueue(MobileVarificationSeller.this);/*String Request initialized*/

        // checkAndRequestPermissions();
        hitotprate();
        hithandler();
        smsVerificationButton.setOnClickListener(v -> {
            smsVerificationButton.setClickable(false);
            hitserverapi();
        });



    }

    private void hitotprate() {

        if(count<6) {
            otp= getRandomNumberString();
            if(otp.length()>=6){
                JSONObject params = new JSONObject();
                try {
                    params.put("authkey", "290188AwxNn68y5d5a9b35");
                    params.put("template_id", "5e426d42d6fc0507882c425a");
                    params.put("mobile", phone);
                    params.put("invisible", 1);
                    params.put("otp", otp);
                    params.put("message","Your Goingbo Seller Login OTP is "+otp);
                    params.put("userip", "43.230.197.198");
                    params.put("email", "ganeshku73@gmail.com");
                    params.put("sender", "GoingB");
                    params.put("otp_length", 6);
                    params.put("otp_expiry", 10);
                    String url = "https://api.msg91.com/api/v5/otp";
                    JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                            url,params,
                            response -> {

                                Log.v("responsesmsotp", response.toString());
                      Log.v("responsesmsotp", "" + otp);
                                String typesucess= null;
                                try {
                                    typesucess = response.getString("type");
                                    if(typesucess.equalsIgnoreCase("success")) {
                                        new AlertDialog.Builder(Objects.requireNonNull(MobileVarificationSeller.this))
                                                .setTitle("Alert")
                                                .setMessage("Requested OTP has been successfully send to your device !")
                                                .setCancelable(false)
                                                .setPositiveButton("OK", (dialog, which) -> {
                                                    dialog.dismiss();

                                                }).show();

                                        resend_timer.setClickable(false);
                                        count++;
                                        new CountDownTimer(60000, 1000) {
                                            public void onTick(long millisUntilFinished) {
                                                resend_timer.setTextColor(getResources().getColor(R.color.light_sliver));
                                                resend_timer.setText("Resend  ( " + millisUntilFinished / 1000 + " sec )");
                                            }

                                            public void onFinish() {
                                                resend_timer.setText("Resend ");
                                                resend_timer.setTextColor(getResources().getColor(R.color.black));
                                                resend_timer.setClickable(true);
                                            }
                                        }.start();
                                    }
                                    else{
                                        new AlertDialog.Builder(Objects.requireNonNull(MobileVarificationSeller.this))
                                                .setTitle("Alert")
                                                .setMessage("Requested OTP has not been send to your device !")
                                                .setCancelable(false)
                                                .setPositiveButton("OK", (dialog, which) -> {
                                                    dialog.dismiss();

                                                }).show();
                                        resend_timer.setText("send ");
                                        resend_timer.setTextColor(getResources().getColor(R.color.black));
                                        resend_timer.setClickable(true);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    new AlertDialog.Builder(Objects.requireNonNull(MobileVarificationSeller.this))
                                            .setTitle("Alert")
                                            .setMessage("Requested OTP has not been send to your device , due to technical Problem please try agin  !")
                                            .setCancelable(false)
                                            .setPositiveButton("OK", (dialog, which) -> {
                                                dialog.dismiss();

                                            }).show();
                                    resend_timer.setText("send ");
                                    resend_timer.setTextColor(getResources().getColor(R.color.black));
                                    resend_timer.setClickable(true);
                                }


                            }, error -> {

                    }) ;

// Adding request to request queue
                    mRequestQueue.add(jsonObjReq);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }else{
                hitotprate();
            }


        }else{
            new AlertDialog.Builder(Objects.requireNonNull(MobileVarificationSeller.this))
                    .setTitle("Alert")
                    .setMessage("you have succeeded maximum limit of OTP requesting !")
                    .setCancelable(false)
                    .setPositiveButton("OK", (dialog, which) -> {
                        dialog.dismiss();

                    }).show();
        }
    }

    //    private void requestRuntimePermissions(String... permissions) {
//        for (String perm : permissions) {
//
//            if (ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED) {
//
//                ActivityCompat.requestPermissions(this, new String[]{perm}, PERMISSION_REQUEST_ID);
//
//            }
//        }
//    }
    @SuppressLint("DefaultLocale")
    public static String getRandomNumberString() {
        int number1 = Integer.parseInt(new DecimalFormat("0").format(new Random().nextInt(9)));
        int number2 = Integer.parseInt(new DecimalFormat("0").format(new Random().nextInt(9)));
        int number3 = Integer.parseInt(new DecimalFormat("0").format(new Random().nextInt(9)));
        int number4 = Integer.parseInt(new DecimalFormat("0").format(new Random().nextInt(9)));
        int number5 = Integer.parseInt(new DecimalFormat("0").format(new Random().nextInt(9)));
        int number6 = Integer.parseInt(new DecimalFormat("0").format(new Random().nextInt(9)));
        String otpnumber=""+number1;
        otpnumber+=""+number2;
        otpnumber+=""+number3;
        otpnumber+=""+number4;
        otpnumber+=""+number5;
        otpnumber+=""+number6;
        Log.e("number", "" + number1);
        Log.e("number", "" + number2);
        Log.e("number", "" + number3);
        Log.e("number", "" + number4);
        Log.e("number", "" + number5);
        Log.e("number", "" + number6);
        Log.e("number", "" + otpnumber);
        return otpnumber ;
    }
    private void init() {
        image_backpress=findViewById(R.id.image_backpress);
        resend_timer =  findViewById(R.id.seller_resend_timer);
        OtpEditText = findViewById(R.id.seller_dataiputcode);
        phoneText =  findViewById(R.id.seller_numberText);
        resend_timer.setOnClickListener(v -> {
            hitotprate();
        });
        sessionManager=new SessionManager(getApplicationContext());
        Bundle intentdata=getIntent().getExtras();
        code=intentdata.getString("contrycode");
        phone=intentdata.getString("phone");
        phoneText.setText("+"+code+phone);

        smsVerificationButton = findViewById(R.id.smsVerificationButton);
//        mSmsBroadcastReceiver.bindListener( messageText -> {
//            OtpEditText.setText( messageText );
//            hitserverapi();
//        } );
        progressBar = findViewById(R.id.seller_progressIndicator);

    }

    private void hitserverapi() {
        if (Objects.requireNonNull(OtpEditText.getText()).toString().length() == 0) {
            OtpEditText.setError("Please Enter OTP !");
        } else if (OtpEditText.getText().toString().length() < 6) {
            OtpEditText.setError("Please Enter correct OTP !");
        } else {
            if (otp.length() >= 6) {
                progressBar.setVisibility(View.VISIBLE);
                String inputotp = OtpEditText.getText().toString().trim();
                String sotp = String.valueOf(otp);
                if (sotp.equalsIgnoreCase(inputotp)) {
                    if (!process()) {

                        if (intentdata.trim().equals("otpcheck")) {
                            Call<CreateUser> callloginotp = RetrofitClient.getClient("https://www.goingbo.com").create(ApiService.class)
                                    .otpsellerloginapi(phone, OtpEditText.getText().toString());

                            callloginotp.enqueue(new Callback<CreateUser>() {
                                @Override
                                public void onResponse(@NonNull Call<CreateUser> callotp, @NonNull Response<CreateUser> responseotp) {
                                    CreateUser createUser = responseotp.body();
                                    progressBar.setVisibility(View.GONE);
                                    smsVerificationButton.setClickable(true);
                                    assert createUser != null;
                                    if (!createUser.isError()) {
                                        String id = createUser.getUser().get(0).getClid();
                                        String name = createUser.getUser().get(0).getCName();
                                        String emails = "";
                                        if (createUser.getUser().get(0).getCEmailid() != null
                                                && createUser.getUser().get(0).getCEmailid().length() == 0) {
                                            emails = "";
                                        } else {
                                            emails = createUser.getUser().get(0).getCEmailid();
                                        }
                                        phone = createUser.getUser().get(0).getCMobileNo();
                                        String usertype = createUser.getUser().get(0).getCType();

                                        sessionManager.createLoginSession(id, name, emails, phone, usertype);
                                        Intent intent = new Intent(MobileVarificationSeller.this, Seller_dashboard.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        finish();


                                    }
                                }


                                @Override
                                public void onFailure(Call<CreateUser> call, Throwable t) {
                                    progressBar.setVisibility(View.GONE);
                                    smsVerificationButton.setClickable(true);
                                    new AlertDialog.Builder(Objects.requireNonNull(MobileVarificationSeller.this))
                                            .setTitle("Alert")
                                            .setMessage("Your OTP has been verification failed Due technical problem Please try again !")
                                            .setCancelable(false)
                                            .setPositiveButton("OK", (dialog, which) -> {
                                                dialog.dismiss();
                                            }).show();
                                }
                            });

                        }
                        if (intentdata.trim().equals("forgot")) {
                            Log.e("fetchingdata", intentdata);
                            progressBar.setVisibility(View.GONE);
                            smsVerificationButton.setClickable(true);
                            ViewGroup viewGroup = findViewById(android.R.id.content);

                            //then we will inflate the custom alert dialog xml that we created
                            View dialogView = LayoutInflater.from(this).inflate(R.layout.forgot_pass, viewGroup, false);


                            //Now we need an AlertDialog.Builder object
                            AlertDialog.Builder builder = new AlertDialog.Builder(MobileVarificationSeller.this,R.style.FullScreenDialog);

                            //setting the view of the builder to our custom view that we already inflated
                            builder.setView(dialogView);

                            //finally creating the alert dialog and displaying it
                            AlertDialog alertDialog = builder.create();

                            alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            alertDialog.setCancelable(false);
                            Window window = alertDialog.getWindow();
                            assert window != null;
                            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                            window.setGravity(Gravity.END);
                            Objects.requireNonNull(alertDialog.getWindow()).setBackgroundDrawableResource(android.R.color.background_dark);
                            EditText newtext = dialogView.findViewById(R.id.new_password);
                            EditText confirmtext = dialogView.findViewById(R.id.confirm_password);
                            ImageView close_forgetk = dialogView.findViewById(R.id.dialog_close_forget);
                            Button dialogButton = dialogView.findViewById(R.id.submit_button);
                            alertDialog.show();
                            if(close_forgetk!=null) {
                                close_forgetk.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        alertDialog.dismiss();
                                    }
                                });
                            }
                            if(dialogButton!=null) {
                                dialogButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if (!process()) {
                                            if (newtext.getText().toString().length() == 0) {
                                                newtext.setError("Please Enter your password");
                                            } else if (newtext.getText().toString().length() < 6) {
                                                newtext.setError(" Enter password should me atleast 6 digit");
                                            } else if (confirmtext.getText().toString().length() == 0) {
                                                confirmtext.setError("Please Enter your confirm password");
                                            } else if (!newtext.getText().toString().equals(confirmtext.getText().toString())) {
                                                confirmtext.setError(" Enter confirm password not match ");
                                            } else {
                                                confirmtext.setError(null);
                                                progressDialog = new ProgressDialog(MobileVarificationSeller.this);

                                                progressDialog.setMessage("Loading......");
                                                progressDialog.show();
//                                    if(finalCalllogin ==2) {
                                                Call<Bookingpre> call = RetrofitClient.getClient("https://www.goingbo.com/").create(ApiService.class).getchangeemail(
                                                        phone, confirmtext.getText().toString());
                                                call.enqueue(new Callback<Bookingpre>() {
                                                    @Override
                                                    public void onResponse(@NonNull Call<Bookingpre> call, @NonNull Response<Bookingpre> response) {
                                                        progressDialog.dismiss();
                                                        assert response.body() != null;
                                                        if (!response.body().getError()) {
                                                            new AlertDialog.Builder(Objects.requireNonNull(MobileVarificationSeller.this))
                                                                    .setTitle(" Alert")
                                                                    .setMessage("Password update successfully !")
                                                                    .setCancelable(false)
                                                                    .setPositiveButton("OK", (dialog, which) -> {
                                                                        dialog.dismiss();
                                                                        alertDialog.dismiss();
                                                                        Intent intent = new Intent(MobileVarificationSeller.this, MainActivity.class);
                                                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                                        startActivity(intent);
                                                                    }).show();

                                                        }
                                                    }

                                                    @Override
                                                    public void onFailure(@NonNull Call<Bookingpre> call, @NonNull Throwable t) {
                                                        // Toast.makeText(Seller_Login_Activity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                                                        progressDialog.dismiss();
                                                    }
                                                });
                                            }
//
                                        }
                                    }
                                });
                            }

                        }
                    }
                }
            }
        }
    }

    private void hithandler() {
        handler = new Handler();
        r = () -> {
            otp="";
        };
        startHandler();
    }

    @Override
    public void onUserInteraction() {

        super.onUserInteraction();
        stopHandler();//stop first and then start
        startHandler();
    }
    public void stopHandler() {
        handler.removeCallbacks(r);
    }
    public void startHandler() {
        handler.postDelayed(r, 10*60*1000); //for 5 minutes
    }
    //    private  boolean checkAndRequestPermissions() {
//        int permissionSendMessage = ContextCompat.checkSelfPermission(MobileVarificationFragment.this,
//                Manifest.permission.SEND_SMS);
//
//        int receiveSMS = ContextCompat.checkSelfPermission(MobileVarificationFragment.this,
//                Manifest.permission.RECEIVE_SMS);
//
//        int readSMS = ContextCompat.checkSelfPermission(MobileVarificationFragment.this,
//                Manifest.permission.READ_SMS);
//        List<String> listPermissionsNeeded = new ArrayList<>();
//
//        if (receiveSMS != PackageManager.PERMISSION_GRANTED) {
//            listPermissionsNeeded.add(Manifest.permission.RECEIVE_MMS);
//        }
//        if (readSMS != PackageManager.PERMISSION_GRANTED) {
//            listPermissionsNeeded.add(Manifest.permission.READ_SMS);
//        }
//        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
//            listPermissionsNeeded.add(Manifest.permission.SEND_SMS);
//        }
//        if (!listPermissionsNeeded.isEmpty()) {
//            ActivityCompat.requestPermissions(MobileVarificationFragment.this,
//                    listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
//                    REQUEST_ID_MULTIPLE_PERMISSIONS);
//            return false;
//        }
//        return true;
//    }
//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//    }
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        if(requestCode==PERMISSION_REQUEST_ID){
//
//            if (grantResults.length > 0
//                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//
//                // permission was granted
//
//                mSmsBroadcastReceiver.bindListener( messageText -> {
//                    OtpEditText.setText( messageText );
//                    hitserverapi();
//                } );
//            } else {
//                Log.e(TAG, "Permission not granted");
//
//
//
//            }
//        }
//    }
    @Override
    protected void onResume() {
        super.onResume();
        //  registerReceiver(mSmsBroadcastReceiver, intentFilter);
        Log.e("MainActivity","Registered receiver");

    }

    @Override
    protected void onPause() {
        super.onPause();
        // unregisterReceiver(mSmsBroadcastReceiver);
        Log.e("MainActivity","Unregistered receiver");

    }
 private boolean process() {
                        boolean show=true;
                        if (checkConnectivity.isConnected()) {
                            show=false;

                        } else {
                            showDialog();
                            show=true;
                        }
                        return show;
                    }
 private void showDialog() {

                        Dialog dialog = new Dialog(MobileVarificationSeller.this, R.style.FullScreenDialogWithStatusBarColorAccent);
                        if (!dialog.isShowing()) {
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            dialog.setContentView(R.layout.no_internet);
                            Window window = dialog.getWindow();
                            assert window != null;
                            WindowManager.LayoutParams wlp = Objects.requireNonNull(window).getAttributes();
                            wlp.gravity = Gravity.CENTER;
                            wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
                            window.setAttributes(wlp);
                            Objects.requireNonNull(dialog.getWindow()).setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
                            Button wifi_btn = dialog.findViewById(R.id.wifi_btn);
                            Button mobile_data_btn = dialog.findViewById(R.id.mobile_data_btn);
                            dialog.show();
                            dialog.setCancelable(false);
                            wifi_btn.setOnClickListener(v -> {
                                dialog.dismiss();
                                isshowing = false;
                                WifiManager wifiMgr = (WifiManager) this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);

                                assert wifiMgr != null;
                                if (wifiMgr.isWifiEnabled()) { // Wi-Fi adapter is ON

                                    WifiInfo wifiInfo = wifiMgr.getConnectionInfo();

                                    if (wifiInfo.getNetworkId() == -1) {

                                        showDialog();
                                        // Not connected to an access point
                                    } else {
                                        dialog.dismiss();
                                        process();
                                    }
                                    // Connected to an access point
                                } else {
                                    startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS)); // Wi-Fi adapter is OFF
                                }
                            });


                            mobile_data_btn.setOnClickListener(v -> {
                                dialog.dismiss();
                                isshowing = false;
                                startActivityForResult(new Intent(android.provider.Settings.ACTION_DATA_ROAMING_SETTINGS), 0);
                                if (checkConnectivity.isConnected()) {
                                    process();

                                } else {
                                    showDialog();
                                }
                            });
                        }

                    }

}
