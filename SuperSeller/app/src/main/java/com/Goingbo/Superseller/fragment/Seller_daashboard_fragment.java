package com.Goingbo.Superseller.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.Goingbo.Superseller.Adapter.adminselectedAdapted;
import com.Goingbo.Superseller.Interface.ApiService;
import com.Goingbo.Superseller.Interface.SelectedAdapterInterface;
import com.Goingbo.Superseller.Model.Bookingpre;
import com.Goingbo.Superseller.Model.CommissionSeller.CommissionCost;
import com.Goingbo.Superseller.Model.Deliver_seller_cost.DeliveryCharger;
import com.Goingbo.Superseller.Model.Productadmin.ProductAdmin;
import com.Goingbo.Superseller.Model.Productadmin.Productseller;
import com.Goingbo.Superseller.Model.SellerDeliveryUnit;
import com.Goingbo.Superseller.Model.seller_unit.Productdetail;
import com.Goingbo.Superseller.R;
import com.Goingbo.Superseller.Util.CheckConnectivity;
import com.Goingbo.Superseller.Util.LibConstant;
import com.Goingbo.Superseller.Util.RetrofitClient;
import com.Goingbo.Superseller.Util.SessionManager;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Seller_daashboard_fragment extends Fragment {
    private LinearLayout layout_current_order,linear_accept_order,linear_reject_order;
    private TextView tv_current_order,tv_accept_order,tv_reject_order;
    private TextView tv_count_current_order,tv_count_accept_order,tv_count_reject_order;
    private Button add_product_seller;
    private CardView card_list_product;
    private RecyclerView rcv_product_List;
    private View view;
    private LinearLayout dummy_layout;
    private SessionManager sessionManager;
    private ArrayList<Productseller> productarray=new ArrayList<Productseller>();
    private ProgressDialog progressDialog;
    private AlertDialog alertdialog;
    Toolbar toolbar;
    TextView delivery_amount,tv_ammount_delivery,commission_amount;
    LinearLayout layout_current_Delivery,layout_seller_wallet;
    private CheckConnectivity checkConnectivity;
    boolean isshowing=true;
    List<SellerDeliveryUnit> seller_unitlist;

    public Seller_daashboard_fragment() {
        // Required empty public constructor
    }


    public static Seller_daashboard_fragment newInstance(String param1, String param2) {
        Seller_daashboard_fragment fragment = new Seller_daashboard_fragment();
        Bundle args = new Bundle();


        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_seller_daashboard_fragment, container, false);
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        checkConnectivity = new CheckConnectivity(getContext());
        sessionManager=new SessionManager(getContext().getApplicationContext());
        init();

        return view;
    }
    private void init(){
        layout_current_order=view.findViewById(R.id.layout_current_order);
        linear_accept_order=view.findViewById(R.id.linear_accept_order);
        linear_reject_order=view.findViewById(R.id.linear_reject_order);
        tv_current_order=view.findViewById(R.id.tv_current_order);
        tv_accept_order=view.findViewById(R.id.tv_accept_order);
        tv_reject_order=view.findViewById(R.id.tv_reject_order);
        tv_count_current_order=view.findViewById(R.id.tv_count_current_order);
        tv_count_accept_order=view.findViewById(R.id.tv_count_accept_order);
        tv_count_reject_order=view.findViewById(R.id.tv_count_reject_order);
        add_product_seller=view.findViewById(R.id.add_product_seller);
        card_list_product=view.findViewById(R.id.card_list_product);
        rcv_product_List=view.findViewById(R.id.rcv_product_List);
        commission_amount=view.findViewById(R.id.commission_amount);
        tv_ammount_delivery=view.findViewById(R.id.tv_ammount_delivery);
        dummy_layout=view.findViewById(R.id.dummy_layout);
        delivery_amount=view.findViewById(R.id.delivery_amount);
        layout_current_Delivery=view.findViewById(R.id.deliver_charge_layout);
        layout_seller_wallet=view.findViewById(R.id.layout_seller_wallet);


        toolbar=getActivity().findViewById(R.id.title_seller_dashboard);
        toolbar.setTitle("Dashboard");
        add_product_seller.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.seller_fragment_dashboard,new Search_productFragment(),"search_product").addToBackStack("Search_product").commit();
            }
        });
        layout_current_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.seller_fragment_dashboard,new Seller_order_list(),"order_list").addToBackStack("order_list").commit();

            }
        });
        linear_accept_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.seller_fragment_dashboard,new Seller_delivery_order_fragment(),"delivery_list").addToBackStack("delivery_List").commit();

            }
        });
        linear_reject_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.seller_fragment_dashboard,new Seller_cancel_order(),"delivery_list").addToBackStack("delivery_List").commit();

            }
        });
        sellerproductdata();
        layout_current_Delivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Fragment fragmentdata = new Deliver_charge_update();
                Bundle bundlef = new Bundle();
                bundlef.putString("amount", delivery_amount.getText().toString());
                bundlef.putString("cost", tv_ammount_delivery.getText().toString());
                bundlef.putSerializable("delivery", (Serializable) seller_unitlist);
                fragmentdata.setArguments(bundlef);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.seller_fragment_dashboard, fragmentdata, "Delivery").addToBackStack("Delivert").commit();

            }
        });
        layout_seller_wallet.setVisibility(View.GONE);
        layout_seller_wallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragmentdata = new Commission_status_pay();
                Bundle bundlef = new Bundle();
                bundlef.putString("pending", commission_amount.getText().toString());

                fragmentdata.setArguments(bundlef);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.seller_fragment_dashboard, fragmentdata, "Delivery").addToBackStack("Delivert").commit();

            }
        });

    }

    private void delivercharge(int seller_id) {

        if (!process()) {
            Call<DeliveryCharger> calldelivery = RetrofitClient.getClient("https://www.goingbo.com/").
                    create(ApiService.class).checkdelivery(seller_id);
            calldelivery.enqueue(new Callback<DeliveryCharger>() {
                @Override
                public void onResponse(Call<DeliveryCharger> call, Response<DeliveryCharger> response) {
                    //  Log.e("deliverychrage",""+response.body().toString());
                    if (!response.body().isError()) {
                        DeliveryCharger deliveryCharger = response.body();
                        if(deliveryCharger.getDeliverycost()!=null){
                        if (deliveryCharger.getDeliverycost().size() > 0) {

                            for (int k = 0; k < deliveryCharger.getDeliverycost().size(); k++) {
                                // Toast.makeText(getContext(), "" + deliveryCharger.getDeliverycost().get(k).getDeliveryCharge(), Toast.LENGTH_SHORT).show();
                                //Toast.makeText(getContext(), "" + deliveryCharger.getDeliverycost().get(k).getMinOrder(), Toast.LENGTH_SHORT).show();
                                Gson gson = new Gson();
                                TypeToken<ArrayList<Productdetail>> token = new TypeToken<ArrayList<Productdetail>>() {
                                };
                                String reader =   deliveryCharger.getDeliverycost().get(0).getDeliveryCharge() ;
                                 seller_unitlist = Arrays.asList(gson.fromJson(reader,
                                        SellerDeliveryUnit[].class));
                                delivery_amount.setText("" + seller_unitlist.get(0).getOrder());
                                tv_ammount_delivery.setText("" + seller_unitlist.get(0).getOrder());

                            }
                            calldelivery.isCanceled();

                        }}
                        //commissioncheck(seller_id);
                    }
                }

                @Override
                public void onFailure(Call<DeliveryCharger> call, Throwable t) {

                }
            });
        }

    }
//    private void commissioncheck(int finalSeller_id){
//        if (!process()) {
//            Call<CommissionCost> callcommission = RetrofitClient.getClient("https://www.gingbo.com/").
//                    create(ApiService.class).commissioncheck(finalSeller_id);
//
//            callcommission.enqueue(new Callback<CommissionCost>() {
//                @Override
//                public void onResponse(Call<CommissionCost> call, Response<CommissionCost> response) {
//                    CommissionCost commissionCost = response.body();
//                    if (!commissionCost.isError()) {
//                        if(commissionCost.getWalletCost()!=null){
//                        if (commissionCost.getWalletCost().size() > 0) {
//                            for (int q = 0; q < commissionCost.getWalletCost().size(); q++) {
//                                commission_amount.setText(commissionCost.getWalletCost().get(q).getPendingAmount());
//                            }
//                        }
//                            //  Toast.makeText(getContext(), "commission", Toast.LENGTH_SHORT).show();
//
//                        }
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<CommissionCost> call, Throwable t) {
//
//                }
//            });
//            callcommission.isCanceled();
//           // updatecommission(finalSeller_id);
//        }
//    }

//    private void updatecommission(int seller_id) {
//
//        Call<Bookingpre> callupdate=RetrofitClient.getClient("https:/www.goingbo.com/").create(ApiService.class)
//                .update_commission(seller_id);
//        callupdate.enqueue(new Callback<Bookingpre>() {
//            @Override
//            public void onResponse(Call<Bookingpre> call, Response<Bookingpre> response) {
//                if(!response.body().getError()){
//                    callupdate.isCanceled();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<Bookingpre> call, Throwable t) {
//
//            }
//        });
//    }

    public void sellerproductdata(){
        if (!process()) {
            progressDialog = new ProgressDialog(getContext());

            progressDialog.setMessage("Loading......");
            progressDialog.show();
            dummy_layout.setVisibility(View.GONE);
            HashMap<String, String> hashMap = sessionManager.getsoicalDetails();
            int seller_id = Integer.parseInt(hashMap.get(LibConstant.socialid));
            productarray.clear();
            // proDialog.show();
            // proDialog = ProgressDialog.show(getContext(), "", "Loading......");
            dummy_layout.setVisibility(View.VISIBLE);
            Call<ProductAdmin> calldata = RetrofitClient.getClient("https://www.goingbo.com/").create(ApiService.class)
                    .getList(seller_id);
            int finalSeller_id = seller_id;
            calldata.enqueue(new Callback<ProductAdmin>() {
                @Override
                public void onResponse(Call<ProductAdmin> call, Response<ProductAdmin> response) {
                    progressDialog.dismiss();
                    productarray.clear();
                    assert response.body() != null;
                    //   Log.d("sizearraylist",""+response.body().getProductseller().size());
                    if (!response.body().isError()) {
                        // proDialog.dismiss();
                        ProductAdmin productAdmin = response.body();
                        if (productAdmin.getProductseller().size() > 0) {

                            dummy_layout.setVisibility(View.GONE);
                            card_list_product.setVisibility(View.VISIBLE);
                            rcv_product_List.setHasFixedSize(false);
                            LinearLayoutManager layoutManager=new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
                            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rcv_product_List.getContext(),
                                    layoutManager.getOrientation());
                            rcv_product_List.addItemDecoration(dividerItemDecoration);
                            rcv_product_List.setLayoutManager(layoutManager);
                            // proDialog.hide();
                            for (int v = 0; v < productAdmin.getProductseller().size(); v++) {
                                Log.e("printarray",""+ productAdmin.getProductseller().toString());
                                Productseller products = new Productseller();
                                products.setProductId(productAdmin.getProductseller().get(v).getProductId());
                                products.setProName(productAdmin.getProductseller().get(v).getProName());
                                products.setProCategory(productAdmin.getProductseller().get(v).getProCategory());
                                String delStr = "&lt;p&gt;";
                                String description = productAdmin.getProductseller().get(v).getProDes();
                                description.replace(delStr, "");
                                description.replace(";br&gt", "");
                                description.replace(";&lt", "");
                                description.replace("\\/p&gt;", "");
                                description.replace("p&gt;", "");
                                products.setProDes(description);
                                products.setCategoryName(productAdmin.getProductseller().get(v).getCategoryName());
                                products.setProImage(productAdmin.getProductseller().get(v).getProImage());
                                products.setProType(productAdmin.getProductseller().get(v).getProType());
                                products.setProductOfferPrice(productAdmin.getProductseller().get(v).getProductOfferPrice());
                                productarray.add(products);
                                // Log.d("arraylistsize",""+productarray.size());
                            }

                            adminselectedAdapted ad = new adminselectedAdapted(getContext(), productarray, new SelectedAdapterInterface() {
                                @Override
                                public void getAdapter(Integer position, String resultindex) {
                                    //    proDialog.show();
                                    if (resultindex.equals("edit")) {

                                        Fragment fragment = new Select_Product();
                                        Bundle bundle = new Bundle();
                                        bundle.putString("category_name",productarray.get(position).getCategoryName());
                                        bundle.putString("productid", productarray.get(position).getProductId());
                                        bundle.putString("productname", productarray.get(position).getProName());
                                        bundle.putString("productcategories", productarray.get(position).getProCategory());
                                        Log.e("cateid",""+productarray.get(position).getProCategory());
                                        bundle.putString("productdescription", productarray.get(position).getProDes());
                                        bundle.putString("productimage", productarray.get(position).getProImage());
                                        bundle.putString("producttype", productarray.get(position).getProType());
                                        bundle.putString("productOffer_price",productarray.get(position).getProductOfferPrice());
                                        bundle.putString("senddata", "edit");
                                        fragment.setArguments(bundle);
                                        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.seller_fragment_dashboard, fragment, "search_list").addToBackStack("search_list").commit();
                                        //   proDialog.dismiss();
                                    } else if (resultindex.equals("remove")) {
                                        alertdialog = new AlertDialog.Builder(getContext())
                                                .setTitle("Your Alert")
                                                .setMessage("Due you want to remove product for list")
                                                .setCancelable(true)
                                                .setNegativeButton("Cancel",(dialog, which) -> {
                                                    dialog.dismiss();
                                                } )
                                                .setPositiveButton("OK", (dialog, which) -> {
                                                    dialog.dismiss();
                                                    deteled(finalSeller_id, productarray.get(position).getProductId());

                                                }).create();
                                        if (!alertdialog.isShowing()) {
                                            alertdialog.show();
                                        }

                                    }
                                }
                            });
                            rcv_product_List.setAdapter(ad);
                            rcv_product_List.addItemDecoration(new DividerItemDecoration(getContext(),
                                    DividerItemDecoration.VERTICAL));
                            calldata.isCanceled();


                        } else {
                            card_list_product.setVisibility(View.GONE);
                            dummy_layout.setVisibility(View.VISIBLE);
                        }
                        //proDialog.dismiss();

                    }
                    delivercharge(finalSeller_id);

                }

                @Override
                public void onFailure(Call<ProductAdmin> call, Throwable t) {
                    progressDialog.dismiss();

                }
            });
        }

    }

    private void deteled(int seller_id, String id) {
        if (!process()) {
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Loading......");
            progressDialog.show();
            Call<Bookingpre> calldata = RetrofitClient.getClient("https://www.goingbo.com/").create(ApiService.class).deleteproduct(seller_id, id);
            calldata.enqueue(new Callback<Bookingpre>() {
                @Override
                public void onResponse(Call<Bookingpre> call, Response<Bookingpre> response) {
                    progressDialog.dismiss();
                    if (!response.body().getError()) {

                        alertdialog = new AlertDialog.Builder(getContext())
                                .setTitle("Your Alert")
                                .setMessage("remove product for list")
                                .setCancelable(false)
                                .setPositiveButton("OK", (dialog, which) -> {
                                    dialog.dismiss();
                                   init();
                                }).create();
                        if (!alertdialog.isShowing()) {
                            alertdialog.show();
                        }
                    } else {
                        alertdialog = new AlertDialog.Builder(getContext())
                                .setTitle("Your Alert")
                                .setMessage("remove product for list failed")
                                .setCancelable(false)
                                .setPositiveButton("OK", (dialog, which) -> {
                                    dialog.dismiss();
                                    //proDialog.dismiss();
                                }).create();
                        if (!alertdialog.isShowing()) {
                            alertdialog.show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<Bookingpre> call, Throwable t) {
                    progressDialog.dismiss();
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }
    private boolean process() {
        boolean show = true;
        if (checkConnectivity.isConnected()) {
            show = false;

        } else {

            showDialog();
            show = true;
        }
        return show;
    }

    private void showDialog() {

        Dialog dialog = new Dialog(getContext(), R.style.FullScreenDialogWithStatusBarColorAccent);
        if (!dialog.isShowing()){
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.no_internet);
            Window window = dialog.getWindow();
            assert window != null;
            WindowManager.LayoutParams wlp = Objects.requireNonNull(window).getAttributes();
            wlp.gravity = Gravity.CENTER;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
            window.setAttributes(wlp);
            Objects.requireNonNull(dialog.getWindow()).setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            Button wifi_btn = dialog.findViewById(R.id.wifi_btn);
            Button mobile_data_btn = dialog.findViewById(R.id.mobile_data_btn);
            dialog.show();
            dialog.setCancelable(false);
            wifi_btn.setOnClickListener(v -> {
                dialog.dismiss();
                isshowing = false;
                WifiManager wifiMgr = (WifiManager)getActivity(). getApplicationContext().getSystemService(Context.WIFI_SERVICE);

                assert wifiMgr != null;
                if (wifiMgr.isWifiEnabled()) { // Wi-Fi adapter is ON

                    WifiInfo wifiInfo = wifiMgr.getConnectionInfo();

                    if (wifiInfo.getNetworkId() == -1) {

                        showDialog();
                        // Not connected to an access point
                    } else {
                        dialog.dismiss();
                        process();
                    }
                    // Connected to an access point
                } else {
                    startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS)); // Wi-Fi adapter is OFF
                }
            });
            mobile_data_btn.setOnClickListener(v -> {
                dialog.dismiss();
                isshowing = false;
                startActivityForResult(new Intent(android.provider.Settings.ACTION_DATA_ROAMING_SETTINGS), 0);
                if (checkConnectivity.isConnected()) {
                    process();

                } else {
                    showDialog();
                }
            });
        }

    }
}
