package com.Goingbo.Superseller.Model.PackLoginAdmin;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class LoginsellerADmin implements Serializable, Parcelable
{

    @SerializedName("success")
    @Expose
    private Success success;
    @SerializedName("error")
    @Expose
    private boolean error;
    public final static Creator<LoginsellerADmin> CREATOR = new Creator<LoginsellerADmin>() {


        @SuppressWarnings({
                "unchecked"
        })
        public LoginsellerADmin createFromParcel(Parcel in) {
            return new LoginsellerADmin(in);
        }

        public LoginsellerADmin[] newArray(int size) {
            return (new LoginsellerADmin[size]);
        }

    }
            ;
    private final static long serialVersionUID = 5971837217424490481L;

    protected LoginsellerADmin(Parcel in) {
        this.success = ((Success) in.readValue((Success.class.getClassLoader())));
        this.error = ((boolean) in.readValue((boolean.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public LoginsellerADmin() {
    }

    /**
     *
     * @param success
     * @param error
     */
    public LoginsellerADmin(Success success, boolean error) {
        super();
        this.success = success;
        this.error = error;
    }

    public Success getSuccess() {
        return success;
    }

    public void setSuccess(Success success) {
        this.success = success;
    }

    public LoginsellerADmin withSuccess(Success success) {
        this.success = success;
        return this;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public LoginsellerADmin withError(boolean error) {
        this.error = error;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("success", success).append("error", error).toString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(success);
        dest.writeValue(error);
    }

    public int describeContents() {
        return 0;
    }

}