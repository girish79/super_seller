package com.Goingbo.Superseller.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import android.provider.Settings;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;

import com.Goingbo.Superseller.R;
import com.Goingbo.Superseller.Util.CheckConnectivity;
import com.Goingbo.Superseller.Util.LibConstant;
import com.Goingbo.Superseller.Util.SessionManager;
import com.google.android.material.textfield.TextInputEditText;

import java.util.HashMap;
import java.util.Objects;

public class Commission_status_pay extends Fragment {
    View view;
    SessionManager sessionManager;
    Toolbar toolbar;
    TextInputEditText ed_pending_amount,ed_paid;
    AlertDialog dialog;
    Button commission_btn;
    private CheckConnectivity checkConnectivity;
    boolean isshowing=true;


    public Commission_status_pay() {
        // Required empty public constructor
    }


    public static Commission_status_pay newInstance(String param1, String param2) {
        Commission_status_pay fragment = new Commission_status_pay();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_commission_status_pay, container, false);
        sessionManager=new SessionManager(getContext().getApplicationContext());
        checkConnectivity = new CheckConnectivity(getContext());
        HashMap<String,String> hashMap=sessionManager.getsoicalDetails();
        int seller_id= Integer.parseInt(hashMap.get(LibConstant.socialid));
        init();
        toolbar=getActivity().findViewById(R.id.title_seller_dashboard);
        toolbar.setTitle("Commission To Pay ");
        Bundle bundle = this.getArguments();
        String cost=bundle.getString("pending");


        ed_pending_amount.setText(cost);
        commission_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!process()){
                    if(ed_paid.getText().toString().length()==0){
                        ed_paid.setError("Please enter pending amount  !");
                    }else {
                        double amount = Double.parseDouble(ed_paid.getText().toString());
                        if (amount == 0) {
                            ed_paid.setError("Enter pending amount !");
                        } else {
                            dialog = new AlertDialog.Builder(getContext())
                                    .setTitle(" Alert")
                                    .setMessage("Payment option not available")
                                    .setCancelable(false)
                                    .setPositiveButton("OK", (dialog, which) -> {
                                        dialog.dismiss();
                                        getActivity().onBackPressed();
                                        // proDialog.dismiss();
                                    }).create();
                            if (!dialog.isShowing()) {
                                dialog.show();
                            }
                        }
                    }
                }
            }
        });
        return  view;
    }
    public void init(){
        ed_pending_amount=view.findViewById(R.id.ed_pending_amount);
        ed_paid=view.findViewById(R.id.ed_paid);
        commission_btn=view.findViewById(R.id.commission_btn);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
    private boolean process() {
        boolean show = true;
        if (checkConnectivity.isConnected()) {
            show = false;

        } else {

            showDialog();
            show = true;
        }
        return show;
    }

    private void showDialog() {

        Dialog dialog = new Dialog(getContext(), R.style.FullScreenDialogWithStatusBarColorAccent);
        if (!dialog.isShowing()){
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.no_internet);
            Window window = dialog.getWindow();
            assert window != null;
            WindowManager.LayoutParams wlp = Objects.requireNonNull(window).getAttributes();
            wlp.gravity = Gravity.CENTER;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
            window.setAttributes(wlp);
            Objects.requireNonNull(dialog.getWindow()).setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            Button wifi_btn = dialog.findViewById(R.id.wifi_btn);
            Button mobile_data_btn = dialog.findViewById(R.id.mobile_data_btn);
            dialog.show();
            dialog.setCancelable(false);
            wifi_btn.setOnClickListener(v -> {
                dialog.dismiss();
                isshowing = false;
                WifiManager wifiMgr = (WifiManager)getActivity(). getApplicationContext().getSystemService(Context.WIFI_SERVICE);

                assert wifiMgr != null;
                if (wifiMgr.isWifiEnabled()) { // Wi-Fi adapter is ON

                    WifiInfo wifiInfo = wifiMgr.getConnectionInfo();

                    if (wifiInfo.getNetworkId() == -1) {

                        showDialog();
                        // Not connected to an access point
                    } else {
                        dialog.dismiss();
                        process();
                    }
                    // Connected to an access point
                } else {
                    startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS)); // Wi-Fi adapter is OFF
                }
            });
            mobile_data_btn.setOnClickListener(v -> {
                dialog.dismiss();
                isshowing = false;
                startActivityForResult(new Intent(android.provider.Settings.ACTION_DATA_ROAMING_SETTINGS), 0);
                if (checkConnectivity.isConnected()) {
                    process();

                } else {
                    showDialog();
                }
            });
        }

    }
}