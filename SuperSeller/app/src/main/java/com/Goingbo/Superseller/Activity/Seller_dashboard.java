package com.Goingbo.Superseller.Activity;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.Goingbo.Superseller.R;
import com.Goingbo.Superseller.Util.AlarmReceiver;
import com.Goingbo.Superseller.Util.CheckConnectivity;
import com.Goingbo.Superseller.Util.LibConstant;
import com.Goingbo.Superseller.Util.SessionManager;
import com.Goingbo.Superseller.fragment.Select_Product;
import com.Goingbo.Superseller.fragment.Seller_cancel_order;
import com.Goingbo.Superseller.fragment.Seller_daashboard_fragment;
import com.Goingbo.Superseller.fragment.Seller_delivery_order_fragment;
import com.Goingbo.Superseller.fragment.Seller_order_list;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.HashMap;
import java.util.Objects;

public class Seller_dashboard extends AppCompatActivity {
    private NavigationView navigationView;
    private DrawerLayout drawer;
    private CheckConnectivity checkConnectivity;
    private GoogleApiClient mGoogleApiClient;
    private SessionManager session;
    private InputMethodManager imm;
    private View navHeader;
    private TextView txtName;
    private Toolbar toolbar;
    public static int navItemIndex = 0;
    FragmentManager fragmentManager;
    boolean isshowing = true;
    private String token;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller_dashboard);
        session = new SessionManager(getApplicationContext());
        fragmentManager=getSupportFragmentManager();
           drawer=findViewById(R.id.seller_drawer_layout);
        imm =(InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
        checkConnectivity = new CheckConnectivity(Seller_dashboard.this);
        navigationView = findViewById(R.id.nav_seller_dashboard);
        toolbar = findViewById(R.id.title_seller_dashboard);
        toolbar.setTitle("Dashboard");
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        mGoogleApiClient.connect();
        fragmentManager.beginTransaction().replace(R.id.seller_fragment_dashboard,new Seller_daashboard_fragment()
                ,"Dashboard").commit();
        navigationView.setNavigationItemSelectedListener(menuItem -> {
            switch (menuItem.getItemId()) {
                case R.id.nav_seller_home:
                    navItemIndex = 0;
                    toolbar.setTitle("Dashboard");
                    fragmentManager.beginTransaction().replace(R.id.seller_fragment_dashboard,new Seller_daashboard_fragment(),"Dashboard").commit();
                    break;
                case R.id.nav_seller_product:
                    navItemIndex = 1;
                    toolbar.setTitle("Dashboard");
                    fragmentManager.beginTransaction().replace(R.id.seller_fragment_dashboard,new Seller_daashboard_fragment(),"Dashboard").commit();
                    break;
                case R.id.nav_seller_restaurant:
                    navItemIndex = 2;
                    Intent restaurant = new Intent(Seller_dashboard.this,Restaurant_DashBoard.class);
                    startActivity(restaurant);
                    break;
                case R.id.nav_seller_add_producty:
                    navItemIndex = 3;
                    toolbar.setTitle("Add Product");
                    Fragment fragment=new Select_Product();
                    Bundle bundle=new Bundle();
                    bundle.putString("senddata","add");
                    fragment.setArguments(bundle);
                    fragmentManager.beginTransaction().replace(R.id.seller_fragment_dashboard, fragment,"Add Product").addToBackStack("Add_Product").commit();
                    break;
                case R.id.nav_Seller_order:
                    navItemIndex = 4;
                    toolbar.setTitle("New Orders");
                    fragmentManager.beginTransaction().replace(R.id.seller_fragment_dashboard,new Seller_order_list(),"Order List").addToBackStack("Order_List").commit();
                    break;
                case R.id.nav_seller_accept:
                    navItemIndex = 5;
                    toolbar.setTitle("Delivery Orders");
                    fragmentManager.beginTransaction().replace(R.id.seller_fragment_dashboard,new Seller_delivery_order_fragment(),"Accept List").addToBackStack("Accept_list").commit();
                    break;
                case R.id.nav_seller_reject:
                    navItemIndex = 6;
                    toolbar.setTitle("Cancel Orders");
                    fragmentManager.beginTransaction().replace(R.id.seller_fragment_dashboard,new Seller_cancel_order(),"Reject List").addToBackStack("Reject_list").commit();
                    break;
                case R.id.nav_seller_terms:
                    navItemIndex = 7;
                    toolbar.setTitle("Terms & Condition");
                    Intent terms = new Intent(Seller_dashboard.this,terms_Activity.class);
                    startActivity(terms);
                    break;

                case R.id.nav_seller_rate_us:
                    navItemIndex = 8;
                    if (!process()) {
                        Uri uri = Uri.parse("market://details?id=" + getPackageName());
                        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                        try {
                            startActivity(goToMarket);
                        } catch (ActivityNotFoundException e) {
                            startActivity(new Intent(Intent.ACTION_VIEW,
                                    Uri.parse("http://play.google.com/store/apps/details?id=" + getPackageName())));
                        }
                    }
                    break;
                case R.id.seller_logout:
                    navItemIndex = 9;
                    if (!process()) {
                        opendialogbox();
                    }
                    break;
                default:
                    drawer.closeDrawers();
                    break;
            }

            if (menuItem.isChecked()) {
                menuItem.setChecked(false);
            } else {
                menuItem.setChecked(true);
            }
            menuItem.setChecked(true);
            drawer.closeDrawers();
            return true;
        });
        navHeader = navigationView.getHeaderView(0);
        txtName = navHeader.findViewById(R.id.clientusername);
        HashMap<String, String> hashmap = session.getsoicalDetails();
        if (hashmap.get(LibConstant.Name) != null) {
            txtName.setText("" + hashmap.get(LibConstant.Name));
        } else {
            txtName.setText("");
        }
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(Seller_dashboard.this, drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawer.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.white));
        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
        ImageView edit_profile = navHeader.findViewById(R.id.edit_profile);
        edit_profile.setOnClickListener(v -> {
            drawer.closeDrawers();
            startActivity(new Intent(Seller_dashboard.this, UserProfile.class));
        });
        navigationView.getMenu().getItem(0).setChecked(true);
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                if (!task.isSuccessful()) {
                    token = task.getException().getMessage();
                    Log.w("FCM TOKEN Failed", task.getException());
                } else {
                    token = task.getResult().getToken();
                    Log.i("FCM TOKEN", token);
                }
            }
        });
    }
    private boolean process() {
        boolean show = true;
        if (checkConnectivity.isConnected()) {
            show = false;

        } else {

            showDialog();
            show = true;
        }
        return show;
    }

    private void showDialog() {

        Dialog dialog = new Dialog(Seller_dashboard.this, R.style.FullScreenDialogWithStatusBarColorAccent);
        if (!dialog.isShowing()){
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.no_internet);
            Window window = dialog.getWindow();
            assert window != null;
            WindowManager.LayoutParams wlp = Objects.requireNonNull(window).getAttributes();
            wlp.gravity = Gravity.CENTER;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
            window.setAttributes(wlp);
            Objects.requireNonNull(dialog.getWindow()).setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            Button wifi_btn = dialog.findViewById(R.id.wifi_btn);
            Button mobile_data_btn = dialog.findViewById(R.id.mobile_data_btn);
            dialog.show();
            dialog.setCancelable(false);
            wifi_btn.setOnClickListener(v -> {
                dialog.dismiss();
                isshowing = false;
                WifiManager wifiMgr = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);

                assert wifiMgr != null;
                if (wifiMgr.isWifiEnabled()) { // Wi-Fi adapter is ON

                    WifiInfo wifiInfo = wifiMgr.getConnectionInfo();

                    if (wifiInfo.getNetworkId() == -1) {

                        showDialog();
                        // Not connected to an access point
                    } else {
                        dialog.dismiss();
                        process();
                    }
                    // Connected to an access point
                } else {
                    startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS)); // Wi-Fi adapter is OFF
                }
            });
            mobile_data_btn.setOnClickListener(v -> {
                dialog.dismiss();
                isshowing = false;
                startActivityForResult(new Intent(android.provider.Settings.ACTION_DATA_ROAMING_SETTINGS), 0);
                if (checkConnectivity.isConnected()) {
                    process();

                } else {
                    showDialog();
                }
            });
        }

    }
    @Override
    protected void onPostResume() {
        super.onPostResume();
        navigationView.getMenu().getItem(navItemIndex).setChecked(true);
    }
    private void opendialogbox() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Seller_dashboard.this);
        alertDialogBuilder.setMessage("Did you want to logout !!");
        alertDialogBuilder.setPositiveButton("YES",
                (arg0, arg1) -> {
                    session.logoutUser();


                    arg0.dismiss();
                    Intent intent = new Intent(Seller_dashboard.this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK );
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                });
        alertDialogBuilder.setNegativeButton("Cancel",
                (arg0, arg1) -> {
                    arg0.dismiss();

                });
        alertDialogBuilder.show();

    }


}
