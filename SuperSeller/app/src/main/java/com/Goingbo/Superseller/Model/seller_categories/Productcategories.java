package com.Goingbo.Superseller.Model.seller_categories;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.List;

public class Productcategories implements Serializable, Parcelable
{

    @SerializedName("error")
    @Expose
    private boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("cat")
    @Expose
    private List<Cat> cat = null;
    public final static Creator<Productcategories> CREATOR = new Creator<Productcategories>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Productcategories createFromParcel(Parcel in) {
            return new Productcategories(in);
        }

        public Productcategories[] newArray(int size) {
            return (new Productcategories[size]);
        }

    }
            ;
    private final static long serialVersionUID = -2371507076616355786L;

    protected Productcategories(Parcel in) {
        this.error = ((boolean) in.readValue((boolean.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        in.readArrayList((Cat.class.getClassLoader()));
    }

    public Productcategories() {
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public Productcategories withError(boolean error) {
        this.error = error;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Productcategories withMessage(String message) {
        this.message = message;
        return this;
    }

    public List<Cat> getCat() {
        return cat;
    }

    public void setCat(List<Cat> cat) {
        this.cat = cat;
    }

    public Productcategories withCat(List<Cat> cat) {
        this.cat = cat;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("error", error).append("message", message).append("cat", cat).toString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(error);
        dest.writeValue(message);
        dest.writeList(cat);
    }

    public int describeContents() {
        return 0;
    }

}