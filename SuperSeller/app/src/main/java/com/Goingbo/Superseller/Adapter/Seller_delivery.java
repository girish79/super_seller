package com.Goingbo.Superseller.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.Goingbo.Superseller.Interface.SelectedAdapterInterface;
import com.Goingbo.Superseller.Model.SellerUorder.Uorderlist;
import com.Goingbo.Superseller.Model.seller_unit.Productdetail;
import com.Goingbo.Superseller.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Seller_delivery extends RecyclerView.Adapter<Seller_delivery.Seller_Order_View > {
        Context context;
        ArrayList<Uorderlist> arrayList;
        SelectedAdapterInterface selectedAdapterInterface;

public Seller_delivery(Context context, ArrayList<Uorderlist> arrayList, SelectedAdapterInterface selectedAdapterInterface) {
        this.context = context;
        this.arrayList = arrayList;
        this.selectedAdapterInterface = selectedAdapterInterface;
        }

@NonNull
@Override
public Seller_Order_View onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.row_seller_order_list,parent,false);
        return new Seller_Order_View(view);
        }

@Override
public void onBindViewHolder(@NonNull Seller_Order_View holder, int position) {
    String imagelink = "https://www.goingbo.com/public/product_images/" + arrayList.get(position).getProductImage();
    Picasso.get().load(imagelink).into(holder.seller_order_image);
    String dataname=arrayList.get(position).getProductName().substring(0,1).toUpperCase()+arrayList.get(position).getProductName().substring(1);

    holder.seller_order_item.setText(Html.fromHtml(dataname));
    holder.seller_order_user_phone.setText(arrayList.get(position).getUsersPhone());
    String datanames=arrayList.get(position).getUsersName().substring(0,1).toUpperCase()+arrayList.get(position).getUsersName().substring(1);

    holder.seller_order_user_name.setText(datanames);
    String address = "" + arrayList.get(position).getAddressesPlotNumber() + "," + arrayList.get(position).getAddressesBlock() + "," +
            arrayList.get(position).getAddressesStreet() + "," + arrayList.get(position).getAddressesLandmark() + "," + arrayList.get(position).getAddressesCity() + "," + arrayList.get(position).getAddressesState() + "," +
            arrayList.get(position).getAddressesZip();
    holder.seller_order_address.setText(address);
    Gson gson = new Gson();
    TypeToken<ArrayList<Productdetail>> token = new TypeToken<ArrayList<Productdetail>>() {
    };
    String reader=arrayList.get(position).getOrderdetails();
    Log.e("printdata",reader);
    try {
        List<Productdetail> unitlist = Arrays.asList(gson.fromJson(reader,
                Productdetail[].class));
        holder.seller_order_quantity.setText(unitlist.get(0).getUnit()+unitlist.get(0).getUnittype());
        holder.seller_order_price.setText(unitlist.get(0).getOfferPrice());
    } catch (Exception e){e.printStackTrace();}

    String status=arrayList.get(position).getOrderStatus();
    if(status.equals("Delivered")) {
        holder.seller_order_accept.setText("Delivered");
    }else if(status.equals("Order Cancel")){
        holder.seller_order_accept.setText("Rejected");
        holder.seller_order_accept.setBackgroundColor(Color.parseColor("#dc3545"));
    }

}

@Override
public int getItemCount() {
        return arrayList.size();
        }

public class Seller_Order_View extends RecyclerView.ViewHolder {
    ImageView seller_order_image;
    TextView seller_order_item,seller_order_user_name,seller_order_address,seller_order_quantity,seller_order_price,seller_order_user_phone;
    Button seller_order_accept;
    public Seller_Order_View(@NonNull View itemView) {
        super(itemView);
        seller_order_image=itemView.findViewById(R.id.seller_order_image);
        seller_order_item=itemView.findViewById(R.id.seller_order_item);
        seller_order_user_name=itemView.findViewById(R.id.seller_order_user_name);
        seller_order_address=itemView.findViewById(R.id.seller_order_address);
        seller_order_quantity=itemView.findViewById(R.id.seller_order_quantity);
        seller_order_price=itemView.findViewById(R.id.seller_order_price);
        seller_order_accept=itemView.findViewById(R.id.seller_order_accept);
        seller_order_user_phone=itemView.findViewById(R.id.seller_order_user_phone);

    }
}
}
