package com.Goingbo.Superseller.Model.CommissionSeller;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CommissionCost implements Serializable, Parcelable
{

    @SerializedName("error")
    @Expose
    private boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("wallet_cost")
    @Expose
    private List<WalletCost> walletCost = new ArrayList<WalletCost>();
    public final static Creator<CommissionCost> CREATOR = new Creator<CommissionCost>() {


        @SuppressWarnings({
                "unchecked"
        })
        public CommissionCost createFromParcel(Parcel in) {
            return new CommissionCost(in);
        }

        public CommissionCost[] newArray(int size) {
            return (new CommissionCost[size]);
        }

    }
            ;
    private final static long serialVersionUID = 3654596954688645932L;

    protected CommissionCost(Parcel in) {
        this.error = ((boolean) in.readValue((boolean.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.walletCost, (WalletCost.class.getClassLoader()));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public CommissionCost() {
    }

    /**
     *
     * @param walletCost
     * @param error
     * @param message
     */
    public CommissionCost(boolean error, String message, List<WalletCost> walletCost) {
        super();
        this.error = error;
        this.message = message;
        this.walletCost = walletCost;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public CommissionCost withError(boolean error) {
        this.error = error;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CommissionCost withMessage(String message) {
        this.message = message;
        return this;
    }

    public List<WalletCost> getWalletCost() {
        return walletCost;
    }

    public void setWalletCost(List<WalletCost> walletCost) {
        this.walletCost = walletCost;
    }

    public CommissionCost withWalletCost(List<WalletCost> walletCost) {
        this.walletCost = walletCost;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("error", error).append("message", message).append("walletCost", walletCost).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(walletCost).append(error).append(message).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof CommissionCost) == false) {
            return false;
        }
        CommissionCost rhs = ((CommissionCost) other);
        return new EqualsBuilder().append(walletCost, rhs.walletCost).append(error, rhs.error).append(message, rhs.message).isEquals();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(error);
        dest.writeValue(message);
        dest.writeList(walletCost);
    }

    public int describeContents() {
        return 0;
    }

}