package com.Goingbo.Superseller.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.Goingbo.Superseller.Interface.SelectedAdapterInterface;
import com.Goingbo.Superseller.Model.SellerDeliveryUnit;
import com.Goingbo.Superseller.R;

import java.util.ArrayList;

public class Seller_delivery_cost_adpater extends RecyclerView.Adapter<Seller_delivery_cost_adpater.Seller_delivery_cost_View> {
    ArrayList<SellerDeliveryUnit> arrayList;
    Context context;
    SelectedAdapterInterface selectedAdapterInterface;

    public Seller_delivery_cost_adpater(ArrayList<SellerDeliveryUnit> arrayList, Context context, SelectedAdapterInterface selectedAdapterInterface) {
        this.arrayList = arrayList;
        this.context = context;
        this.selectedAdapterInterface = selectedAdapterInterface;
    }

    @NonNull
    @Override
    public Seller_delivery_cost_View onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.row_delivery_rcv,parent,false);
        return new Seller_delivery_cost_View(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Seller_delivery_cost_View holder, int position) {
        if(arrayList.size()>0){
            holder.row_unit_order_distance.setText(arrayList.get(position).getDistance());
            holder.row_unit_order_delivery_charger.setText(arrayList.get(position).getDelcharge());
            holder.btn_edit_unit_delivery.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedAdapterInterface.getAdapter(position,"edit");
                }
            });
            holder.btn_delete_unit_delivery.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedAdapterInterface.getAdapter(position,"delete");
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class Seller_delivery_cost_View extends RecyclerView.ViewHolder {
        TextView row_unit_order_distance;
        TextView row_unit_order_delivery_charger;
        ImageView btn_edit_unit_delivery;
        ImageView btn_delete_unit_delivery;

        public Seller_delivery_cost_View(@NonNull View itemView) {
            super(itemView);
            row_unit_order_distance=itemView.findViewById(R.id.row_unit_order_distance);
            row_unit_order_delivery_charger=itemView.findViewById(R.id.row_unit_order_delivery_charger);
            btn_edit_unit_delivery=itemView.findViewById(R.id.btn_edit_unit_delivery);
            btn_delete_unit_delivery=itemView.findViewById(R.id.btn_delete_unit_delivery);
        }
    }
}
