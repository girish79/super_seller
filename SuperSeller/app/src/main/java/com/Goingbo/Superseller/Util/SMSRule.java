package com.Goingbo.Superseller.Util;

import java.util.regex.Pattern;

public class SMSRule {

    private String mSender;
    private String mGroupID;
    private Pattern mOTPPattern;


    private SMS sms;

    public String getSender() {

        return mSender;
    }

    public SMSRule setSender(final String sender) {

        mSender = sender;
        return this;
    }

    public String getGroupID() {

        return mGroupID;
    }

    public SMSRule setGroupID(final String groupID) {

        mGroupID = groupID;
        return this;

    }


    public Pattern getOTPPattern() {

        return mOTPPattern;
    }

    public SMSRule setOTPPattern(final Pattern otpPattern) {

        mOTPPattern = otpPattern;
        return this;
    }

}