package com.Goingbo.Superseller.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.Goingbo.Superseller.Interface.SelectedAdapterInterface;
import com.Goingbo.Superseller.Model.NewOrderLISTSeller.Sellerorderlist;
import com.Goingbo.Superseller.Model.NewOrder_List.Userorderlist;
import com.Goingbo.Superseller.Model.seller_unit.Productdetail;
import com.Goingbo.Superseller.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Seller_Order_New_Adpater extends RecyclerView.Adapter<Seller_Order_New_Adpater.SellerOrderViews> {
    ArrayList<ArrayList<Sellerorderlist>> secoundArray;
    Context context;
    double total=0;
    int count=0;
    SelectedAdapterInterface selectedAdapterInterface;

    public Seller_Order_New_Adpater(ArrayList<ArrayList<Sellerorderlist>> secoundArray, Context context, SelectedAdapterInterface selectedAdapterInterface) {
        this.secoundArray = secoundArray;
        this.context = context;
        this.selectedAdapterInterface = selectedAdapterInterface;
    }

    @NonNull
    @Override
    public SellerOrderViews onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.row_seller_super_orderview,parent,false);
        return new SellerOrderViews(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SellerOrderViews holder, int position) {
        holder.new_seller_user_name.setText(secoundArray.get(position).get(0).getName());
        holder.new_seller_user_phone.setText(secoundArray.get(position).get(0).getPhone());
        holder.new_seller_user_phone.setText(secoundArray.get(position).get(0).getPhone());
        holder.new_seller_inner_order.setHasFixedSize(true);
        holder.new_seller_inner_order.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));
        holder.new_seller_inner_order.setAdapter(new Seller_order_List_Adpater(context,secoundArray.get(position)));
        String address=""+secoundArray.get(position).get(0).getPlotNumber()+","+secoundArray.get(position).get(0).getBlock()+","+
                secoundArray.get(position).get(0).getStreet()+","+secoundArray.get(position).get(0).getLandmark()+","+secoundArray.get(position).get(0).getCity()+","
                +secoundArray.get(position).get(0).getState()+","+
                secoundArray.get(position).get(0).getZip();
        holder.new_seller_order_address.setText(address);
        Gson gson = new Gson();
        TypeToken<ArrayList<Productdetail>> token = new TypeToken<ArrayList<Productdetail>>() {
        };
        double total=0;
        for(int k=0;k<secoundArray.get(position).size();k++) {
            String reader = "[" + secoundArray.get(position).get(k).getOrderdetails() + "]";
            Log.e("printdata", reader);
            try {
                List<Productdetail> unitlist = Arrays.asList(gson.fromJson(reader,
                        Productdetail[].class));
                long quantity = secoundArray.get(position).get(k).getProQuentity();
                double price = Double.parseDouble(unitlist.get(0).getOfferPrice());
                total += price * quantity;
                if (k == secoundArray.size() - 1) {
                    String coupon = secoundArray.get(position).get(k).getCouponCode();
                    if (coupon.equals("Not Available") || coupon.equals("Not Applicable")) {
                        double delcharge = Double.parseDouble(secoundArray.get(position).get(k).getCuserDelivery());
                        if(delcharge==0){
                            holder.new_seller_user_delivery_cost.setText("FREE");
                            holder.tv_rs_seller_delivery.setVisibility(View.GONE);
                        }else {
                            holder.new_seller_user_delivery_cost.setText("" + Math.round(delcharge));
                            holder.tv_rs_seller_delivery.setVisibility(View.VISIBLE);
                        }
                        total += delcharge;
                        holder.new_seller_user_total_amount.setText("" + Math.round(total));

                    } else {
                        holder.new_seller_user_delivery_cost.setText("FREE");
                        holder.tv_rs_seller_delivery.setVisibility(View.GONE);
                    }
                }
                holder.new_seller_user_total_amount.setText("" + Math.round(total));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if(secoundArray.get(position).get(0).getOrderStatus().trim().equalsIgnoreCase("Order Has Been Accepted")){
            holder.new_seller_order_accept.setText("Update Status");
        }else if(secoundArray.get(position).get(0).getOrderStatus().trim().equalsIgnoreCase("Order Shipped")){
            holder.new_seller_order_accept.setText("Update Status");
        }else if (secoundArray.get(position).get(0).getOrderStatus().trim().equalsIgnoreCase("Order In Route")){
            holder.new_seller_order_accept.setText("Update Status");
        } else {
            holder.new_seller_order_accept.setText("Accept Order");
        }
        holder.new_seller_order_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedAdapterInterface.getAdapter(position,"Accept");
            }
        });


    }

    @Override
    public int getItemCount() {
        return secoundArray.size();
    }

    public class SellerOrderViews extends RecyclerView.ViewHolder {
        TextView new_seller_user_name;
        TextView new_seller_user_phone;
        TextView new_user_seller_otp;
        TextView tv_rs_seller_delivery;
        TextView new_seller_user_delivery_cost;
        TextView new_seller_user_total_amount;
        TextView new_seller_order_accept;
        TextView new_seller_order_address;
        RecyclerView new_seller_inner_order;
        public SellerOrderViews(@NonNull View itemView) {
            super(itemView);
            new_seller_inner_order=itemView.findViewById(R.id.new_seller_inner_order);
            new_seller_order_accept=itemView.findViewById(R.id.new_seller_order_accept);
            new_seller_user_total_amount=itemView.findViewById(R.id.new_seller_user_total_amount);
            new_seller_user_delivery_cost=itemView.findViewById(R.id.new_seller_user_delivery_cost);
            tv_rs_seller_delivery=itemView.findViewById(R.id.tv_rs_seller_delivery);
            new_user_seller_otp=itemView.findViewById(R.id.new_user_seller_otp);
            new_seller_user_phone=itemView.findViewById(R.id.new_seller_user_phone);
            new_seller_user_name=itemView.findViewById(R.id.new_seller_user_name);
            new_seller_order_address=itemView.findViewById(R.id.new_seller_order_address);
        }
    }
}
