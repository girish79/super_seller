package com.Goingbo.Superseller.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Bookingpre implements Serializable
{

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    private final static long serialVersionUID = -1319636061759648288L;

    /**
     * No args constructor for use in serialization
     *
     */
    public Bookingpre() {
    }

    /**
     *
     * @param error
     * @param message
     */
    public Bookingpre(Boolean error, String message) {
        super();
        this.error = error;
        this.message = message;
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}