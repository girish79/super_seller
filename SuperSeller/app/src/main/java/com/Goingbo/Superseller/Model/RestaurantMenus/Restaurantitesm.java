package com.Goingbo.Superseller.Model.RestaurantMenus;
import java.io.Serializable;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Restaurantitesm implements Serializable, Parcelable
{

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("aphone")
    @Expose
    private Object aphone;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("lat")
    @Expose
    private double lat;
    @SerializedName("lon")
    @Expose
    private double lon;
    @SerializedName("remember_token")
    @Expose
    private Object rememberToken;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("seller_id")
    @Expose
    private int sellerId;
    @SerializedName("menu_id")
    @Expose
    private int menuId;
    @SerializedName("menu_items")
    @Expose
    private String menuItems;
    @SerializedName("items_keywords")
    @Expose
    private Object itemsKeywords;
    @SerializedName("menucategory")
    @Expose
    private String menucategory;
    @SerializedName("menuitem")
    @Expose
    private String menuitem;
    @SerializedName("menuimage")
    @Expose
    private String menuimage;
    @SerializedName("detail")
    @Expose
    private String detail;
    @SerializedName("distance")
    @Expose
    private double distance;
    public final static Parcelable.Creator<Restaurantitesm> CREATOR = new Creator<Restaurantitesm>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Restaurantitesm createFromParcel(Parcel in) {
            return new Restaurantitesm(in);
        }

        public Restaurantitesm[] newArray(int size) {
            return (new Restaurantitesm[size]);
        }

    }
            ;
    private final static long serialVersionUID = 5235741293208976961L;

    protected Restaurantitesm(Parcel in) {
        this.id = ((int) in.readValue((int.class.getClassLoader())));
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.email = ((String) in.readValue((String.class.getClassLoader())));
        this.phone = ((String) in.readValue((String.class.getClassLoader())));
        this.aphone = ((Object) in.readValue((Object.class.getClassLoader())));
        this.password = ((String) in.readValue((String.class.getClassLoader())));
        this.lat = ((double) in.readValue((double.class.getClassLoader())));
        this.lon = ((double) in.readValue((double.class.getClassLoader())));
        this.rememberToken = ((Object) in.readValue((Object.class.getClassLoader())));
        this.createdAt = ((String) in.readValue((String.class.getClassLoader())));
        this.updatedAt = ((String) in.readValue((String.class.getClassLoader())));
        this.sellerId = ((int) in.readValue((int.class.getClassLoader())));
        this.menuId = ((int) in.readValue((int.class.getClassLoader())));
        this.menuItems = ((String) in.readValue((String.class.getClassLoader())));
        this.itemsKeywords = ((Object) in.readValue((Object.class.getClassLoader())));
        this.menucategory = ((String) in.readValue((String.class.getClassLoader())));
        this.menuitem = ((String) in.readValue((String.class.getClassLoader())));
        this.menuimage = ((String) in.readValue((String.class.getClassLoader())));
        this.detail = ((String) in.readValue((String.class.getClassLoader())));
        this.distance = ((double) in.readValue((double.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public Restaurantitesm() {
    }

    /**
     *
     * @param distance
     * @param menuItems
     * @param itemsKeywords
     * @param lon
     * @param menucategory
     * @param aphone
     * @param createdAt
     * @param password
     * @param sellerId
     * @param phone
     * @param menuimage
     * @param name
     * @param menuId
     * @param id
     * @param detail
     * @param rememberToken
     * @param email
     * @param lat
     * @param menuitem
     * @param updatedAt
     */
    public Restaurantitesm(int id, String name, String email, String phone, Object aphone, String password, double lat, double lon, Object rememberToken, String createdAt, String updatedAt, int sellerId, int menuId, String menuItems, Object itemsKeywords, String menucategory, String menuitem, String menuimage, String detail, double distance) {
        super();
        this.id = id;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.aphone = aphone;
        this.password = password;
        this.lat = lat;
        this.lon = lon;
        this.rememberToken = rememberToken;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.sellerId = sellerId;
        this.menuId = menuId;
        this.menuItems = menuItems;
        this.itemsKeywords = itemsKeywords;
        this.menucategory = menucategory;
        this.menuitem = menuitem;
        this.menuimage = menuimage;
        this.detail = detail;
        this.distance = distance;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Restaurantitesm withId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Restaurantitesm withName(String name) {
        this.name = name;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Restaurantitesm withEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Restaurantitesm withPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public Object getAphone() {
        return aphone;
    }

    public void setAphone(Object aphone) {
        this.aphone = aphone;
    }

    public Restaurantitesm withAphone(Object aphone) {
        this.aphone = aphone;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Restaurantitesm withPassword(String password) {
        this.password = password;
        return this;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public Restaurantitesm withLat(double lat) {
        this.lat = lat;
        return this;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public Restaurantitesm withLon(double lon) {
        this.lon = lon;
        return this;
    }

    public Object getRememberToken() {
        return rememberToken;
    }

    public void setRememberToken(Object rememberToken) {
        this.rememberToken = rememberToken;
    }

    public Restaurantitesm withRememberToken(Object rememberToken) {
        this.rememberToken = rememberToken;
        return this;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Restaurantitesm withCreatedAt(String createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Restaurantitesm withUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public int getSellerId() {
        return sellerId;
    }

    public void setSellerId(int sellerId) {
        this.sellerId = sellerId;
    }

    public Restaurantitesm withSellerId(int sellerId) {
        this.sellerId = sellerId;
        return this;
    }

    public int getMenuId() {
        return menuId;
    }

    public void setMenuId(int menuId) {
        this.menuId = menuId;
    }

    public Restaurantitesm withMenuId(int menuId) {
        this.menuId = menuId;
        return this;
    }

    public String getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(String menuItems) {
        this.menuItems = menuItems;
    }

    public Restaurantitesm withMenuItems(String menuItems) {
        this.menuItems = menuItems;
        return this;
    }

    public Object getItemsKeywords() {
        return itemsKeywords;
    }

    public void setItemsKeywords(Object itemsKeywords) {
        this.itemsKeywords = itemsKeywords;
    }

    public Restaurantitesm withItemsKeywords(Object itemsKeywords) {
        this.itemsKeywords = itemsKeywords;
        return this;
    }

    public String getMenucategory() {
        return menucategory;
    }

    public void setMenucategory(String menucategory) {
        this.menucategory = menucategory;
    }

    public Restaurantitesm withMenucategory(String menucategory) {
        this.menucategory = menucategory;
        return this;
    }

    public String getMenuitem() {
        return menuitem;
    }

    public void setMenuitem(String menuitem) {
        this.menuitem = menuitem;
    }

    public Restaurantitesm withMenuitem(String menuitem) {
        this.menuitem = menuitem;
        return this;
    }

    public String getMenuimage() {
        return menuimage;
    }

    public void setMenuimage(String menuimage) {
        this.menuimage = menuimage;
    }

    public Restaurantitesm withMenuimage(String menuimage) {
        this.menuimage = menuimage;
        return this;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Restaurantitesm withDetail(String detail) {
        this.detail = detail;
        return this;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public Restaurantitesm withDistance(double distance) {
        this.distance = distance;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("name", name).append("email", email).append("phone", phone).append("aphone", aphone).append("password", password).append("lat", lat).append("lon", lon).append("rememberToken", rememberToken).append("createdAt", createdAt).append("updatedAt", updatedAt).append("sellerId", sellerId).append("menuId", menuId).append("menuItems", menuItems).append("itemsKeywords", itemsKeywords).append("menucategory", menucategory).append("menuitem", menuitem).append("menuimage", menuimage).append("detail", detail).append("distance", distance).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(distance).append(menuItems).append(itemsKeywords).append(lon).append(menucategory).append(aphone).append(createdAt).append(password).append(sellerId).append(phone).append(menuimage).append(name).append(menuId).append(id).append(detail).append(rememberToken).append(email).append(lat).append(menuitem).append(updatedAt).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Restaurantitesm) == false) {
            return false;
        }
        Restaurantitesm rhs = ((Restaurantitesm) other);
        return new EqualsBuilder().append(distance, rhs.distance).append(menuItems, rhs.menuItems).append(itemsKeywords, rhs.itemsKeywords).append(lon, rhs.lon).append(menucategory, rhs.menucategory).append(aphone, rhs.aphone).append(createdAt, rhs.createdAt).append(password, rhs.password).append(sellerId, rhs.sellerId).append(phone, rhs.phone).append(menuimage, rhs.menuimage).append(name, rhs.name).append(menuId, rhs.menuId).append(id, rhs.id).append(detail, rhs.detail).append(rememberToken, rhs.rememberToken).append(email, rhs.email).append(lat, rhs.lat).append(menuitem, rhs.menuitem).append(updatedAt, rhs.updatedAt).isEquals();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(name);
        dest.writeValue(email);
        dest.writeValue(phone);
        dest.writeValue(aphone);
        dest.writeValue(password);
        dest.writeValue(lat);
        dest.writeValue(lon);
        dest.writeValue(rememberToken);
        dest.writeValue(createdAt);
        dest.writeValue(updatedAt);
        dest.writeValue(sellerId);
        dest.writeValue(menuId);
        dest.writeValue(menuItems);
        dest.writeValue(itemsKeywords);
        dest.writeValue(menucategory);
        dest.writeValue(menuitem);
        dest.writeValue(menuimage);
        dest.writeValue(detail);
        dest.writeValue(distance);
    }

    public int describeContents() {
        return 0;
    }

}