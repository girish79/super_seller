package com.Goingbo.Superseller.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.Goingbo.Superseller.Adapter.Seller_delivery;
import com.Goingbo.Superseller.Interface.ApiService;
import com.Goingbo.Superseller.Interface.SelectedAdapterInterface;
import com.Goingbo.Superseller.Model.SellerUorder.SellerOrder;
import com.Goingbo.Superseller.Model.SellerUorder.Uorderlist;
import com.Goingbo.Superseller.R;
import com.Goingbo.Superseller.Util.CheckConnectivity;
import com.Goingbo.Superseller.Util.EqualSpacingItemDecoration;
import com.Goingbo.Superseller.Util.LibConstant;
import com.Goingbo.Superseller.Util.RetrofitClient;
import com.Goingbo.Superseller.Util.SessionManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Seller_cancel_order extends Fragment {
    RecyclerView seller_rcv_delivery;
    View view;
    TextView tv_order_list_delivery;
    LinearLayout orderlistdummy_delivery;
    ArrayList<Uorderlist> uarrayList;
    SessionManager sessionManager;
    HashMap<String,String> hashMap;
    String seller_id;
    Toolbar toolbar;
    TextView text_data_no_order;
    private CheckConnectivity checkConnectivity;
    boolean isshowing=true;

    public Seller_cancel_order() {
        // Required empty public constructor
    }

    public static Seller_cancel_order newInstance(String param1, String param2) {
        Seller_cancel_order fragment = new Seller_cancel_order();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view= inflater.inflate(R.layout.fragment_seller_delivery_order_fragment, container, false);
        checkConnectivity = new CheckConnectivity(getContext());
        init();
        return view;
    }
    public void init(){
        seller_rcv_delivery=view.findViewById(R.id.seller_rcv_delivery);
        tv_order_list_delivery=view.findViewById(R.id.tv_order_list_delivery);
        orderlistdummy_delivery=view.findViewById(R.id.orderlistdummy_delivery);
        text_data_no_order=view.findViewById(R.id.text_data_no_order);
        text_data_no_order.setText("No cancel order found!");
        uarrayList=new ArrayList<>();
        sessionManager=new SessionManager(getActivity().getApplicationContext());
        hashMap=sessionManager.getsoicalDetails();
        seller_id=hashMap.get(LibConstant.socialid);
        toolbar=getActivity().findViewById(R.id.title_seller_dashboard);
        toolbar.setTitle("Cancel Orders");
        orderlist();
    }
    private void orderlist() {
        if(!process()) {
            ProgressDialog progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Loading......");
            progressDialog.show();
            Call<SellerOrder> call = RetrofitClient.getClient("https://www.goingbo.com/").create(ApiService.class).getorderlistseller(seller_id);
            call.enqueue(new Callback<SellerOrder>() {
                @Override
                public void onResponse(Call<SellerOrder> call, Response<SellerOrder> response) {
                    progressDialog.dismiss();
                    if (!response.body().isError()) {
                        uarrayList.clear();
                        SellerOrder sellerOrder = response.body();
                        if (sellerOrder.getUorderlist().size() > 0) {

                            for (int k = 0; k < sellerOrder.getUorderlist().size(); k++) {
                                Uorderlist uorderlist = sellerOrder.getUorderlist().get(k);
                                String status = uorderlist.getOrderStatus();

                                if (status.equals("Order Cancel")) {
                                    Log.e("OrderCancel", status);
                                    uarrayList.add(uorderlist);
                                }
                            }
                            if (uarrayList.size() > 0) {
                                orderlistdummy_delivery.setVisibility(View.GONE);

                                seller_rcv_delivery.setVisibility(View.VISIBLE);
                                tv_order_list_delivery.setVisibility(View.VISIBLE);
                                seller_rcv_delivery.setHasFixedSize(false);
                                seller_rcv_delivery.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
                                Seller_delivery seller_order_list_adpater = new Seller_delivery(getContext(), uarrayList, new SelectedAdapterInterface() {
                                    @Override
                                    public void getAdapter(Integer position, String resultindex) {
                                    }
                                });
                                seller_rcv_delivery.setAdapter(seller_order_list_adpater);
                                seller_rcv_delivery.addItemDecoration(new DividerItemDecoration(getContext(),
                                        DividerItemDecoration.VERTICAL));
                            } else {
                                orderlistdummy_delivery.setVisibility(View.VISIBLE);
                                tv_order_list_delivery.setVisibility(View.GONE);
                                tv_order_list_delivery.setVisibility(View.GONE);
                            }
                        } else {
                            orderlistdummy_delivery.setVisibility(View.VISIBLE);
                            tv_order_list_delivery.setVisibility(View.GONE);
                            tv_order_list_delivery.setVisibility(View.GONE);
                        }
                    }

                }

                @Override
                public void onFailure(Call<SellerOrder> call, Throwable t) {
                    progressDialog.dismiss();
                }
            });
        }
    }
    private boolean process() {
        boolean show = true;
        if (checkConnectivity.isConnected()) {
            show = false;

        } else {

            showDialog();
            show = true;
        }
        return show;
    }

    private void showDialog() {

        Dialog dialog = new Dialog(getContext(), R.style.FullScreenDialogWithStatusBarColorAccent);
        if (!dialog.isShowing()){
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.no_internet);
            Window window = dialog.getWindow();
            assert window != null;
            WindowManager.LayoutParams wlp = Objects.requireNonNull(window).getAttributes();
            wlp.gravity = Gravity.CENTER;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
            window.setAttributes(wlp);
            Objects.requireNonNull(dialog.getWindow()).setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            Button wifi_btn = dialog.findViewById(R.id.wifi_btn);
            Button mobile_data_btn = dialog.findViewById(R.id.mobile_data_btn);
            dialog.show();
            dialog.setCancelable(false);
            wifi_btn.setOnClickListener(v -> {
                dialog.dismiss();
                isshowing = false;
                WifiManager wifiMgr = (WifiManager)getActivity(). getApplicationContext().getSystemService(Context.WIFI_SERVICE);

                assert wifiMgr != null;
                if (wifiMgr.isWifiEnabled()) { // Wi-Fi adapter is ON

                    WifiInfo wifiInfo = wifiMgr.getConnectionInfo();

                    if (wifiInfo.getNetworkId() == -1) {

                        showDialog();
                        // Not connected to an access point
                    } else {
                        dialog.dismiss();
                        process();
                    }
                    // Connected to an access point
                } else {
                    startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS)); // Wi-Fi adapter is OFF
                }
            });
            mobile_data_btn.setOnClickListener(v -> {
                dialog.dismiss();
                isshowing = false;
                startActivityForResult(new Intent(android.provider.Settings.ACTION_DATA_ROAMING_SETTINGS), 0);
                if (checkConnectivity.isConnected()) {
                    process();

                } else {
                    showDialog();
                }
            });
        }

    }

}