package com.Goingbo.Superseller.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.Goingbo.Superseller.Adapter.Adpater_Restuarant_item;
import com.Goingbo.Superseller.Adapter.ProductaddAdpater;
import com.Goingbo.Superseller.Interface.ApiService;
import com.Goingbo.Superseller.Model.Restuarant_Menu.MENUITEMRESTURANT;
import com.Goingbo.Superseller.Model.Restuarant_Menu.MenuItem;
import com.Goingbo.Superseller.Model.productdetails.Product;
import com.Goingbo.Superseller.Model.productdetails.ProductData;
import com.Goingbo.Superseller.R;
import com.Goingbo.Superseller.Util.CheckConnectivity;
import com.Goingbo.Superseller.Util.RetrofitClient;

import java.util.ArrayList;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Restuarant_Detail_forum extends AppCompatActivity {
ArrayList<MENUITEMRESTURANT> menuitemarray;
EditText edit_search_menu;
RecyclerView search_menu_list;
LinearLayoutManager linearLayout;
LinearLayout search_dummy_menu;
CheckConnectivity checkConnectivity ;
boolean isshowing=true;
ImageView image_backpress;
TextView title_text;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restuarant__detail_forum);
        init();
        edit_search_menu.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//                if(s.length()>0){
//                    if(!process()) {
//                        try {
//                            Call<MenuItem> calldata = RetrofitClient.getClient("https://www.goingbo.com/").create(ApiService.class).getMenuITem(String.valueOf(s));
//                            calldata.enqueue(new Callback<MenuItem>() {
//                                @Override
//                                public void onResponse(Call<MenuItem> call, Response<MenuItem> response) {
//                                    assert response.body() != null;
//                                    if (!response.body().isError()) {
//
//                                        menuitemarray.clear();
//                                        MenuItem productData = response.body();
//                                        if (productData.getMENUITEMRESTURANT().size() > 0) {
//                                            // arrayList.addAll(productData.getProduct());
//                                            search_dummy_menu.setVisibility(View.GONE);
//                                            search_menu_list.setVisibility(View.VISIBLE);
//                                            for (int v = 0; v < productData.getMENUITEMRESTURANT().size(); v++) {
//                                                MENUITEMRESTURANT products = new MENUITEMRESTURANT();
//                                                String id = productData.getMENUITEMRESTURANT().get(v).getId();
//                                                String name = productData.getMENUITEMRESTURANT().get(v).getMenuitem();
//                                                String catid = productData.getMENUITEMRESTURANT().get(v).getMenucategory();
//                                                String description = productData.getMENUITEMRESTURANT().get(v).getDetail();
//                                                String image = productData.getMENUITEMRESTURANT().get(v).getMenuimage();
//
//                                                products.setId(id);
//                                                products.setMenuitem(name);
//                                                products.setMenucategory(catid);
//                                                products.setDetail(description);
//
//                                                // products.setProPrice(productData.getProduct().get(v).getProPrice());
//                                                products.setMenuimage(image);
//
////                                            products.setProQty(productData.getProduct().get(v).getProQty());
//                                                menuitemarray.add(products);
//
//                                            }
//                                            linearLayout = new LinearLayoutManager(Restuarant_Detail_forum.this, LinearLayoutManager.VERTICAL, false);
//                                            search_menu_list.setHasFixedSize(true);
//                                            search_menu_list.setLayoutManager(linearLayout);
//                                            Adpater_Restuarant_item productaddAdpater = new Adpater_Restuarant_item(Restuarant_Detail_forum.this, menuitemarray);
//                                            search_menu_list.setAdapter(productaddAdpater);
//
//                                            // search_product_list.addItemDecoration(new EqualSpacingItemDecoration(16, EqualSpacingItemDecoration.HORIZONTAL));
//
//
//                                            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(search_menu_list.getContext(),
//                                                    LinearLayout.VERTICAL);
//                                            search_menu_list.addItemDecoration(dividerItemDecoration);
//
//                                        } else {
//                                            search_dummy_menu.setVisibility(View.VISIBLE);
//                                            search_menu_list.setVisibility(View.GONE);
//                                        }
//                                    }
//
//                                }
//
//                                @Override
//                                public void onFailure(Call<MenuItem> call, Throwable t) {
//
//                                }
//                            });
//                        }catch (Exception ex){
//                            ex.printStackTrace();
//                        }
//                    }
//                }
            }

            @Override
            public void onTextChanged(CharSequence newText, int start, int before, int count) {
                if(newText.length()>0){
                    if(!process()) {
                        try {
                            Call<MenuItem> calldata = RetrofitClient.getClient("https://www.goingbo.com/").create(ApiService.class).getMenuITem(String.valueOf(newText));
                            calldata.enqueue(new Callback<MenuItem>() {
                                @Override
                                public void onResponse(Call<MenuItem> call, Response<MenuItem> response) {
                                    assert response.body() != null;
                                    if (!response.body().isError()) {
                                        menuitemarray.clear();
                                        MenuItem productData = response.body();
                                        if (productData.getMENUITEMRESTURANT().size() > 0) {
                                            // arrayList.addAll(productData.getProduct());
                                            search_dummy_menu.setVisibility(View.GONE);
                                            search_menu_list.setVisibility(View.VISIBLE);
                                            for (int v = 0; v < productData.getMENUITEMRESTURANT().size(); v++) {
                                                MENUITEMRESTURANT products = new MENUITEMRESTURANT();
                                                String id = productData.getMENUITEMRESTURANT().get(v).getId();
                                                String name = productData.getMENUITEMRESTURANT().get(v).getMenuitem();
                                                String catid = productData.getMENUITEMRESTURANT().get(v).getMenucategory();
                                                String description = productData.getMENUITEMRESTURANT().get(v).getDetail();
                                                String image = productData.getMENUITEMRESTURANT().get(v).getMenuimage();

                                                products.setId(id);
                                                products.setMenuitem(name);
                                                products.setMenucategory(catid);
                                                products.setDetail(description);

                                                // products.setProPrice(productData.getProduct().get(v).getProPrice());
                                                products.setMenuimage(image);

//                                            products.setProQty(productData.getProduct().get(v).getProQty());
                                                menuitemarray.add(products);

                                            }
                                            linearLayout = new LinearLayoutManager(Restuarant_Detail_forum.this, LinearLayoutManager.VERTICAL, false);
                                            search_menu_list.setHasFixedSize(true);
                                            search_menu_list.setLayoutManager(linearLayout);
                                            Adpater_Restuarant_item productaddAdpater = new Adpater_Restuarant_item(Restuarant_Detail_forum.this, menuitemarray);
                                            search_menu_list.setAdapter(productaddAdpater);

                                            // search_product_list.addItemDecoration(new EqualSpacingItemDecoration(16, EqualSpacingItemDecoration.HORIZONTAL));


                                            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(search_menu_list.getContext(),
                                                    LinearLayout.VERTICAL);
                                            search_menu_list.addItemDecoration(dividerItemDecoration);

                                        } else {
                                            search_dummy_menu.setVisibility(View.VISIBLE);
                                            search_menu_list.setVisibility(View.GONE);
                                        }
                                    }

                                }

                                @Override
                                public void onFailure(Call<MenuItem> call, Throwable t) {
                                    search_dummy_menu.setVisibility(View.VISIBLE);
                                    search_menu_list.setVisibility(View.GONE);
                                }
                            });
                        }catch (Exception ex){
                            ex.printStackTrace();
                        }
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length()>0){
                    if(!process()) {
                        try {
                            Call<MenuItem> calldata = RetrofitClient.getClient("https://www.goingbo.com/").create(ApiService.class).getMenuITem(String.valueOf(s));
                            calldata.enqueue(new Callback<MenuItem>() {
                                @Override
                                public void onResponse(Call<MenuItem> call, Response<MenuItem> response) {
                                    assert response.body() != null;
                                    if (!response.body().isError()) {

                                        menuitemarray.clear();
                                        MenuItem productData = response.body();
                                        if (productData.getMENUITEMRESTURANT().size() > 0) {
                                            // arrayList.addAll(productData.getProduct());
                                            search_dummy_menu.setVisibility(View.GONE);
                                            search_menu_list.setVisibility(View.VISIBLE);
                                            for (int v = 0; v < productData.getMENUITEMRESTURANT().size(); v++) {
                                                MENUITEMRESTURANT products = new MENUITEMRESTURANT();
                                                String id = productData.getMENUITEMRESTURANT().get(v).getId();
                                                String name = productData.getMENUITEMRESTURANT().get(v).getMenuitem();
                                                String catid = productData.getMENUITEMRESTURANT().get(v).getMenucategory();
                                                String description = productData.getMENUITEMRESTURANT().get(v).getDetail();
                                                String image = productData.getMENUITEMRESTURANT().get(v).getMenuimage();

                                                products.setId(id);
                                                products.setMenuitem(name);
                                                products.setMenucategory(catid);
                                                products.setDetail(description);

                                                // products.setProPrice(productData.getProduct().get(v).getProPrice());
                                                products.setMenuimage(image);

//                                            products.setProQty(productData.getProduct().get(v).getProQty());
                                                menuitemarray.add(products);

                                            }
                                            linearLayout = new LinearLayoutManager(Restuarant_Detail_forum.this, LinearLayoutManager.VERTICAL, false);
                                            search_menu_list.setHasFixedSize(true);
                                            search_menu_list.setLayoutManager(linearLayout);
                                            Adpater_Restuarant_item productaddAdpater = new Adpater_Restuarant_item(Restuarant_Detail_forum.this, menuitemarray);
                                            search_menu_list.setAdapter(productaddAdpater);

                                            // search_product_list.addItemDecoration(new EqualSpacingItemDecoration(16, EqualSpacingItemDecoration.HORIZONTAL));


                                            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(search_menu_list.getContext(),
                                                    LinearLayout.VERTICAL);
                                            search_menu_list.addItemDecoration(dividerItemDecoration);

                                        } else {
                                            search_dummy_menu.setVisibility(View.VISIBLE);
                                            search_menu_list.setVisibility(View.GONE);
                                        }
                                    }

                                }

                                @Override
                                public void onFailure(Call<MenuItem> call, Throwable t) {

                                }
                            });
                        }catch (Exception Exp){
                            Exp.printStackTrace();
                        }
                    }
                }
            }
        });

        edit_search_menu.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    private void init() {
        edit_search_menu=findViewById(R.id.edit_search_menu);
        search_menu_list=findViewById(R.id.search_menu_list);
        search_dummy_menu=findViewById(R.id.search_dummy_menu);
        image_backpress=findViewById(R.id.image_backpress);
        title_text=findViewById(R.id.title_text);
        title_text.setText("Search Menu Item");
        image_backpress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        checkConnectivity=new CheckConnectivity(Restuarant_Detail_forum.this);
        menuitemarray=new ArrayList<>();
    }
    private boolean process() {
        boolean show=true;
        if (checkConnectivity.isConnected()) {
            show=false;

        } else {
            showDialog();
            show=true;
        }
        return show;
    }

    private void showDialog() {

        Dialog dialog = new Dialog(Restuarant_Detail_forum.this, android.R.style.Theme_Translucent_NoTitleBar);
        if (!dialog.isShowing()) {
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            dialog.setContentView(R.layout.no_internet);
            Window window = dialog.getWindow();
            assert window != null;
            WindowManager.LayoutParams wlp = Objects.requireNonNull(window).getAttributes();
            wlp.gravity = Gravity.CENTER;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
            window.setAttributes(wlp);
            Objects.requireNonNull(dialog.getWindow()).setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            Button wifi_btn = dialog.findViewById(R.id.wifi_btn);
            Button mobile_data_btn = dialog.findViewById(R.id.mobile_data_btn);
            dialog.show();
            dialog.setCancelable(false);
            wifi_btn.setOnClickListener(v -> {
                dialog.dismiss();
                isshowing = false;
                WifiManager wifiMgr = (WifiManager) this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                assert wifiMgr != null;
                if (wifiMgr.isWifiEnabled()) { // Wi-Fi adapter is ON
                    WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
                    if (wifiInfo.getNetworkId() == -1) {
                        showDialog();
                        // Not connected to an access point
                    } else {
                        dialog.dismiss();
                        process();
                    }
                    // Connected to an access point
                } else {
                    startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS)); // Wi-Fi adapter is OFF
                }
            });
            mobile_data_btn.setOnClickListener(v -> {
                dialog.dismiss();
                isshowing = false;
                startActivityForResult(new Intent(android.provider.Settings.ACTION_DATA_ROAMING_SETTINGS), 0);
                if (checkConnectivity.isConnected()) {
                    process();
                } else {
                    showDialog();
                }
            });
        }

    }
}