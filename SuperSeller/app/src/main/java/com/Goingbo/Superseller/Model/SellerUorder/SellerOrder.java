package com.Goingbo.Superseller.Model.SellerUorder;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.List;


public class SellerOrder implements Serializable, Parcelable
{

    @SerializedName("error")
    @Expose
    private boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("Uorderlist")
    @Expose
    private List<Uorderlist> uorderlist = null;
    public final static Creator<SellerOrder> CREATOR = new Creator<SellerOrder>() {


        @SuppressWarnings({
                "unchecked"
        })
        public SellerOrder createFromParcel(Parcel in) {
            return new SellerOrder(in);
        }

        public SellerOrder[] newArray(int size) {
            return (new SellerOrder[size]);
        }

    }
            ;
    private final static long serialVersionUID = -5738471010467104335L;

    protected SellerOrder(Parcel in) {
        this.error = ((boolean) in.readValue((boolean.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.uorderlist, (Uorderlist.class.getClassLoader()));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public SellerOrder() {
    }

    /**
     *
     * @param uorderlist
     * @param error
     * @param message
     */
    public SellerOrder(boolean error, String message, List<Uorderlist> uorderlist) {
        super();
        this.error = error;
        this.message = message;
        this.uorderlist = uorderlist;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public SellerOrder withError(boolean error) {
        this.error = error;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public SellerOrder withMessage(String message) {
        this.message = message;
        return this;
    }

    public List<Uorderlist> getUorderlist() {
        return uorderlist;
    }

    public void setUorderlist(List<Uorderlist> uorderlist) {
        this.uorderlist = uorderlist;
    }

    public SellerOrder withUorderlist(List<Uorderlist> uorderlist) {
        this.uorderlist = uorderlist;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("error", error).append("message", message).append("uorderlist", uorderlist).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(uorderlist).append(error).append(message).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof SellerOrder) == false) {
            return false;
        }
        SellerOrder rhs = ((SellerOrder) other);
        return new EqualsBuilder().append(uorderlist, rhs.uorderlist).append(error, rhs.error).append(message, rhs.message).isEquals();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(error);
        dest.writeValue(message);
        dest.writeList(uorderlist);
    }

    public int describeContents() {
        return 0;
    }

}