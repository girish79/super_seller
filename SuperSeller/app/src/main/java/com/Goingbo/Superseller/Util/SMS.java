package com.Goingbo.Superseller.Util;

import androidx.annotation.NonNull;

public class SMS {

    private String mID;
    private String mAddress;
    private String mMessage;
    private boolean isRead = false;
    private String mTime;

    public String getID() {

        return mID;
    }

    public SMS setID(@NonNull final String ID) {

        mID = ID;
        return this;
    }

    public String getAddress() {

        return mAddress;
    }

    public SMS setAddress(@NonNull final String Address) {

        mAddress = Address;
        return this;
    }

    public String getMessage() {
        return mMessage;
    }

    public SMS setMessage(@NonNull final String Message) {

        mMessage = Message;
        return this;
    }

    public boolean isRead() {

        return isRead;
    }

    public SMS setReadState(boolean ReadState) {

        isRead = ReadState;
        return this;
    }

    public String getTime() {

        return mTime;
    }

    public SMS setTime(@NonNull String Time) {

        mTime = Time;
        return this;
    }
}